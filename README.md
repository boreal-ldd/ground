# Ground

Ground (Generic and Reusable Organized Useful Natural Definitions)
A usefull extension to Coq's standard library

# Install Ground

```shell
opam repo add coq-released https://coq.inria.fr/opam/released
opam install coq-ground
```

To instead build and install manually, do:

``` shell
git clone https://gitlab.com/boreal-ldd/ground.git
cd ground
make   # or make -j <number-of-cores-on-your-machine> 
make install
```
