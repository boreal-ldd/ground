Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_Logic.

Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.
Require Import SimplEnum.

Require Import FiniteCurryfy.

(* Section. Basic Type Definition *)

Definition ba n := array bool n.
Definition bf n := fnary bool bool n.
Definition bt n := nary bool bool n.

Fixpoint beq_bt {n} (t1 t2:bt n) : bool.
Proof.
dependent destruction t1.
- dependent destruction t2.
  apply (eqb b0 b1).
- dependent destruction t2.
  apply ((beq_bt _ (f0 false) (f1 false)) && (beq_bt _ (f0 true) (f1 true))).
Defined.

Lemma beq_bt_iff_true n : beq_iff_true (@beq_bt n).
Proof with curry1.
unfold beq_iff_true.
intro t1.
dependent induction t1; intro t2...
- dependent destruction t2...
  setoid_rewrite Value_eq_Value...
- dependent destruction t2...
  setoid_rewrite Param_eq_Param...
  rewrite andb_true_iff...
  repeat rewrite H.
  split...
  apply functional_extensionality...
  destruct x...
Qed.

Fixpoint beq_bf {n} : bf n -> bf n -> bool.
Proof.
destruct n; intros f1 f2.
- apply (eqb (f1 sempty) (f2 sempty)).
- apply
    ((beq_bf _ (fnary_evalo1 f1 false) (fnary_evalo1 f2 false)) &&
     (beq_bf _ (fnary_evalo1 f1 true ) (fnary_evalo1 f2 true ))).
Defined.

Lemma beq_bf_iff_true n : beq_iff_true (@beq_bf n).
Proof with curry1.
unfold beq_iff_true.
induction n; intros f1 f2...
split...
- rewrite IHn in H, H0...
  apply functional_extensionality...
  apply_both H (stail x).
  apply_both H0 (stail x).
  unfold fnary_evalo1 in *...
  rewrite <- (scons_shead_stail x).
  destruct(shead x)eqn:E...
- rewrite andb_true_iff...
  repeat rewrite IHn...
Qed.

Definition uop_bf (m:bool -> bool) {n} (f:bf n) : bf n := comp f m.
Definition bop_bf (m:bool -> bool -> bool) {n} (f1 f2:bf n) : bf n :=
  (fun a => m (f1 a) (f2 a)).

Definition neg_bf {n} f := @uop_bf negb n f.
Definition xorb_bf b {n} f := @uop_bf (xorb b) n f.
Definition and_bf {n} f1 f2 := @bop_bf andb n f1 f2.
Definition xor_bf {n} f1 f2 := @bop_bf xorb n f1 f2.

Definition cst_bf n b : bf n := (fun _ => b).
Definition is_cst_bf (b:bool) {n} (f:bf n) := beq_bf f (cst_bf n b).


Lemma bf0 (f:bf 0) : f = cst_bf _ (f sempty).
Proof with curry1.
apply functional_extensionality...
rewrite (sempty_unique x)...
Qed.

(* Section. Shannon operator *)

Definition sha_bf {n} (f0 f1:bf n) : bf(S n) :=
  (fun x => if shead x then f1(stail x) else f0(stail x)).

Lemma sha_bf_fnary_evalo1_lr n (f:bf(S n)) :
  sha_bf (fnary_evalo1 f false) (fnary_evalo1 f true) = f.
Proof with curry1.
unfold sha_bf, fnary_evalo1...
apply functional_extensionality...
dest_match_step; rewrite <- D; rewrite scons_shead_stail...
Qed.
#[export] Hint Rewrite sha_bf_fnary_evalo1_lr : curry1_rewrite.

Lemma fnary_evalo1_sha_bf n (f0 f1:bf n) b :
  fnary_evalo1 (sha_bf f0 f1) b = if b then f1 else f0.
Proof with curry1.
unfold fnary_evalo1, sha_bf...
apply functional_extensionality...
dest_match_step...
Qed.
#[export] Hint Rewrite fnary_evalo1_sha_bf : curry1_rewrite.

Lemma shannon_expansion {n} (f1 f2:bf(S n)) :
  f1 = f2 <->
    fnary_evalo1 f1 false = fnary_evalo1 f2 false /\
    fnary_evalo1 f1 true = fnary_evalo1 f2 true.
Proof with curry1.
split...
apply functional_extensionality...
apply_both H (stail x)...
apply_both H0 (stail x)...
rewrite <- (scons_shead_stail x).
destruct(shead x)eqn:E...
Qed.

Lemma shannon_expansion_gen b {n} (f1 f2:bf(S n)) :
  f1 = f2 <->
    fnary_evalo1 f1 b = fnary_evalo1 f2 b /\
    fnary_evalo1 f1 (negb b) = fnary_evalo1 f2 (negb b).
Proof with curry1.
split...
rewrite shannon_expansion...
destruct b...
Qed.

Lemma sha_bf_injective n (f0 f1 g0 g1:bf n) :
  sha_bf f0 f1 = sha_bf g0 g1 <-> f0 = g0 /\ f1 = g1.
Proof with curry1.
rewrite shannon_expansion...
Qed.
#[export] Hint Rewrite sha_bf_injective : curry1_rewrite.

Lemma xorb_bf_false n (f:bf n) : xorb_bf false f = f.
Proof. compute; curry1. Qed.
Lemma xorb_bf_true n (f:bf n) : xorb_bf true f = neg_bf f.
Proof. compute; curry1. Qed.
#[export] Hint Rewrite xorb_bf_false : curry1_rewrite.
#[export] Hint Rewrite xorb_bf_true : curry1_rewrite.

Lemma neg_bf_neg_bf n (f:bf n) : neg_bf(neg_bf f) = f.
Proof. compute; curry1. Qed.
#[export] Hint Rewrite neg_bf_neg_bf : curry1_rewrite.

Lemma neg_bf_shift n (f1 f2:bf n) : neg_bf f1 = f2 <-> f1 = neg_bf f2.
Proof. split; curry1. Qed.
#[export] Hint Rewrite neg_bf_shift : curry1_rewrite.

Lemma neg_bf_nofixpoint n (f:bf n) : f = neg_bf f <-> False.
Proof with curry1.
simpl...
apply_both H ((fun _ => true):ba n)...
compute in H0...
Qed.
#[export] Hint Rewrite neg_bf_nofixpoint : curry1_rewrite.

Lemma beq_bf_neg_bf_r n (f:bf n) : beq_bf f (neg_bf f) = false.
Proof with curry1.
rewrite (beq_iff_false_of_iff_true (beq_bf_iff_true n) _)...
Qed.
#[export] Hint Rewrite beq_bf_neg_bf_r : curry1_rewrite.

Ltac beq_bf := simpleq_main_step beq_bf beq_bf_iff_true.

Lemma fold_cst_bf n  b : (fun _ : array bool n => b) = cst_bf n b.
Proof. reflexivity. Qed.
#[export] Hint Rewrite fold_cst_bf : curry1_rewrite.

Lemma cst_bf_eq_cst_bf n b1 b2 : cst_bf n b1 = cst_bf n b2 <-> b1 = b2.
Proof with curry1.
split...
apply_both H ((fun _ => false):ba n)...
Qed.
#[export] Hint Rewrite cst_bf_eq_cst_bf : curry1_rewrite.

Lemma beq_bf_cst_bf_both n b1 b2 : beq_bf (cst_bf n b1) (cst_bf n b2) = eqb b1 b2.
Proof with curry1.
rewrite bool_eq_iff_iff...
rewrite (beq_bf_iff_true _ _)...
Qed.
#[export] Hint Rewrite beq_bf_cst_bf_both : curry1_rewrite.

Lemma beq_bf_cst_bf_shift n b f : beq_bf (cst_bf n b) f = beq_bf f (cst_bf n b).
Proof. rewrite (beq_sym_of_iff_true (beq_bf_iff_true n)); curry1. Qed.
#[export] Hint Rewrite beq_bf_cst_bf_shift : curry1_rewrite.

Lemma fold_is_cst_bf n b f : beq_bf f (cst_bf n b) = is_cst_bf b f.
Proof. reflexivity. Qed.
#[export] Hint Rewrite fold_is_cst_bf : curry1_rewrite.

Example rewrite_beq_bf_r th0 n (f:bf n) :
  beq_bf f (cst_bf n th0) = is_cst_bf th0 f.
Proof. curry1. Qed.

Example rewrite_beq_bf_l th0 n (f:bf n) :
  beq_bf (fun _ => th0) f = is_cst_bf th0 f.
Proof. curry1. Qed.

Lemma is_cst_bf_eq_true b n (f:bf n) : is_cst_bf b f = true <-> f = cst_bf n b.
Proof with curry1.
unfold is_cst_bf.
rewrite (beq_bf_iff_true _ _)...
Qed.
#[export] Hint Rewrite is_cst_bf_eq_true : curry1_rewrite.

Lemma is_cst_bf_negb_bf b n (f:bf n) :
  is_cst_bf b (neg_bf f) = is_cst_bf (negb b) f.
Proof. rewrite bool_eq_iff_iff; curry1. Qed.
#[export] Hint Rewrite is_cst_bf_negb_bf : curry1_rewrite.

Lemma beg_bf_cst_bf n th0 :
  neg_bf (cst_bf n th0) = cst_bf n (negb th0).
Proof. reflexivity. Qed.
#[export] Hint Rewrite beg_bf_cst_bf : curry1_rewrite.

Lemma xorb_bf_xorb_bf b1 b2 n (f:bf n) :
  xorb_bf b1 (xorb_bf b2 f) = xorb_bf (xorb b1 b2) f.
Proof with curry1.
unfold xorb_bf, uop_bf, comp...
apply functional_extensionality...
Qed.
#[export] Hint Rewrite xorb_bf_xorb_bf : curry1_rewrite.

Lemma xorb_bf_eq_shift b n (f1 f2:bf n) :
  xorb_bf b f1 = f2 <-> f1 = xorb_bf b f2.
Proof. split; curry1. Qed.
#[export] Hint Rewrite xorb_bf_eq_shift : curry1_rewrite.

Lemma xorb_bf_cst_bf b1 b2 n :
  xorb_bf b1 (cst_bf n b2) = cst_bf n (xorb b1 b2).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite xorb_bf_cst_bf : curry1_rewrite.

Example cst_bf_injective n (b1 b2:bool) :
  (fun _ : ba n => b1) = (fun _ : ba n => b2) <-> b1 = b2.
Proof with curry1.
setoid_rewrite cst_bf_eq_cst_bf...
Qed.

Lemma is_cst_bf_cst_bf b1 n b2 : is_cst_bf b1 (cst_bf n b2) = eqb b1 b2.
Proof with curry1.
rewrite bool_eq_iff_iff...
Qed.
#[export] Hint Rewrite is_cst_bf_cst_bf : curry1_rewrite.

Lemma is_cst_bf_xorb_bf b1 b2 n (f: bf n) :
  is_cst_bf b1 (xorb_bf b2 f) = is_cst_bf (xorb b1 b2) f.
Proof with curry1.
rewrite bool_eq_iff_iff...
generalize dependent f.
rewrite xorb_comm...
Qed.
#[export] Hint Rewrite is_cst_bf_xorb_bf : curry1_rewrite.

Example is_cst_bf_cst_bf_2 n b1 b2 : is_cst_bf b1 (fun _ : ba n => b2) = eqb b1 b2.
Proof. curry1. Qed.

Lemma forall_is_cst_bf_same_f n b1 b2 :
  (forall f : bf n, is_cst_bf b1 f = is_cst_bf b2 f) <-> b1 = b2.
Proof with curry1.
split...
specialize(H (cst_bf _ b1))...
Qed.

Lemma negb_bf_comp_negb n (f:bf n) : neg_bf (comp f negb) = f.
Proof. curry1. Qed.
#[export] Hint Rewrite negb_bf_comp_negb : curry1_rewrite.

(* minsat *)

Fixpoint minsat_bf {n} (f:bf n) : option(array bool n) :=
match n as n0 return bf n0 -> option(array bool n0) with
| 0 => fun f => if f sempty then Some sempty else None
| S n => fun f =>
  match minsat_bf (fnary_evalo1 f false) with
  | Some a => Some(scons false a)
  | None =>
  match minsat_bf (fnary_evalo1 f true ) with
  | Some a => Some(scons true  a)
  | None => None
  end
  end
end f.

Lemma minsat_weak_minsat_bf {n} : minsat_weak cmp_array_bool (@minsat_bf n).
Proof with curry1.
unfold minsat_weak...
induction n...
- dest_match_step...
  + unfold is_min_positive...
    rewrite (sempty_unique a1) in H0...
  + rewrite (bf0 F)...
- specialize(IHn(fnary_evalo1 F false)) as H0.
  specialize(IHn(fnary_evalo1 F true )) as H1.
  clear IHn.
  dest_match_step...
  + clear H1.
    unfold is_min_positive in *...
    rewrite rewrite_cmp_array_bool_S...
    dest_match_step...
    rewrite array_head_expansion in H2...
    specialize(H0(stail a1))...
    rewrite <- D0 in H0; rewrite fnary_evalo1_shead_stail in H0...
  + unfold opmap; dest_match_step...
    * unfold is_min_positive in *...
      rewrite rewrite_cmp_array_bool_S...
      rewrite array_head_expansion in H3...
      specialize(H1(stail a1)).
      dest_match_step...
      -- rewrite <- D1 in H1; rewrite fnary_evalo1_shead_stail in H1...
      -- specialize(f_equal(fun f => f (stail a1)) H0)...
         rewrite <- D1 in H3;  rewrite fnary_evalo1_shead_stail in H3...
         unfold cst_bf in H3...
    * rewrite (shannon_expansion F)...
Qed.

Lemma minsat_positive_minsat_bf {n} : minsat_positive cmp_array_bool (@minsat_bf n).
Proof with curry1.
rewrite <- minsat_weak_iff_minsat_positive.
- apply minsat_weak_minsat_bf.
- apply cmp_P_cmp_array_bool.
Qed.


Lemma minsat_alternative_minsat_bf n : minsat_alternative (@cmp_array_bool n) minsat_bf.
Proof with curry1.
rewrite <- minsat_positive_iff_minsat_alternative.
apply minsat_positive_minsat_bf.
Qed.


(* SubSection. Dealing with LDD-Ordered Corner Case *)

Definition pi0 (n:nat) (b:bool) : bf(S n) := (fun x => xorb b (shead x)).

Lemma fnary_evalo1_pi0 n b1 b2 : fnary_evalo1 (pi0 n b1) b2 = (fun _ => xorb b1 b2).
Proof. reflexivity. Qed.
#[export] Hint Rewrite fnary_evalo1_pi0 : curry1_rewrite.

Lemma cst_bf_eval n b (a:ba n) : cst_bf n b a = b.
Proof. reflexivity. Qed.
#[export] Hint Rewrite cst_bf_eval : curry1_rewrite.

Lemma shead_cst A n (b:A) : shead (fun _ : ltN (S n) => b) = b.
Proof. reflexivity. Qed.
#[export] Hint Rewrite shead_cst : curry1_rewrite.

Lemma pi0_eq_cst_bf n b1 b2 : pi0 n b1 = cst_bf (S n) b2 <-> False.
Proof with curry1.
simpl...
unfold pi0, cst_bf in *.
apply_both_as H ((fun _ => false):ba (S n)) HH...
apply_both_as H ((fun _ => true ):ba (S n)) HH...
Qed.
#[export] Hint Rewrite pi0_eq_cst_bf : curry1_rewrite.

Lemma fnary_evalo1_cst_bf n b1 b2 : fnary_evalo1 (cst_bf (S n) b1) b2 = cst_bf n b1.
Proof. reflexivity. Qed.
#[export] Hint Rewrite fnary_evalo1_cst_bf : curry1_rewrite.

(* SubSection. Extra From LddO_Primitives *)

Lemma fnary_evalo1_bop_bf p n f1 f2 x :
  fnary_evalo1 (@bop_bf p (S n) f1 f2) x = bop_bf p (fnary_evalo1 f1 x) (fnary_evalo1 f2 x).
Proof. unfold bop_bf; curry1. Qed.
#[export] Hint Rewrite fnary_evalo1_bop_bf : curry1_rewrite.

Lemma shannon_expansion_bop_bf n (f1 f2:bf(S n)) p :
  bop_bf p f1 f2 = sha_bf (bop_bf p (fnary_evalo1 f1 false) (fnary_evalo1 f2 false))
                          (bop_bf p (fnary_evalo1 f1 true ) (fnary_evalo1 f2 true )).
Proof. rewrite shannon_expansion; curry1. Qed.

Lemma is_cst_bf_sha_bf b n f1 f2 : is_cst_bf b (@sha_bf n f1 f2) = is_cst_bf b f1 && is_cst_bf b f2.
Proof. unfold is_cst_bf; curry1. Qed.
#[export] Hint Rewrite is_cst_bf_sha_bf : curry1_rewrite.

Lemma is_cst_bf_bop_bf_eqb b n f1 f2 : is_cst_bf b (@bop_bf eqb n f1 f2) = beq_bf f1 (xorb_bf (negb b) f2).
Proof with curry1.
generalize dependent f2.
generalize dependent f1.
induction n...
- unfold is_cst_bf, bop_bf, xorb_bf, uop_bf...
- rewrite shannon_expansion_bop_bf...
  repeat rewrite IHn...
Qed.
#[export] Hint Rewrite is_cst_bf_bop_bf_eqb : curry1_rewrite.

Lemma is_cst_bf_bop_bf_xorb b n f1 f2 : is_cst_bf b (@bop_bf xorb n f1 f2) = beq_bf f1 (xorb_bf b f2).
Proof with curry1.
generalize dependent f2.
generalize dependent f1.
induction n...
- unfold is_cst_bf, bop_bf, xorb_bf, uop_bf...
- rewrite shannon_expansion_bop_bf...
  repeat rewrite IHn...
Qed.
#[export] Hint Rewrite is_cst_bf_bop_bf_xorb : curry1_rewrite.

Lemma bop_bf_x1_only u n f0 f1 : @bop_bf (fun _ x1 => u x1) n f0 f1 = uop_bf u f1.
Proof with curry1.
generalize dependent f0.
generalize dependent f1.
induction n...
Qed.

Lemma bop_bf_x0_only u n f0 f1 : @bop_bf (fun x0 _ => u x0) n f0 f1 = uop_bf u f0.
Proof with curry1.
generalize dependent f0.
generalize dependent f1.
induction n...
Qed.

Lemma fact_xorb_bf b n f : (fun x : array bool n => xorb b (f x)) = xorb_bf b f.
Proof. reflexivity. Qed.
#[export] Hint Rewrite fact_xorb_bf : curry1_rewrite.

Lemma eq_xorb_bf n b f : f = @xorb_bf b n f <-> b = false.
Proof. destruct b; curry1. Qed.
#[export] Hint Rewrite eq_xorb_bf : curry1_rewrite.

Lemma sha_bf_scons n (f0 f1:bf n) x0 x : (sha_bf f0 f1) (scons x0 x) = if x0 then f1 x else f0 x.
Proof. unfold sha_bf; curry1. Qed.

Lemma eval_bf_S_n n (f:bf(S n)) x :
  f x = if shead x then fnary_evalo1 f true (stail x) else fnary_evalo1 f false (stail x).
Proof. rewrite <- (scons_shead_stail x); destruct(shead x) eqn:E; rewrite <- E; curry1. Qed.

(* SubSection. Dealing with LDD-Uniform Corner Case *)

Lemma shannon_expansion_u {n} k (f1 f2:bf(S n)) :
       f1 = f2 <->
        fnary_evalu1 k f1 false = fnary_evalu1 k f2 false /\ fnary_evalu1 k f1 true = fnary_evalu1 k f2 true.
Proof with curry1.
split...
apply functional_extensionality...
specialize(f_equal (fun f => f (spop k x)) H)...
specialize(f_equal (fun f => f (spop k x)) H0)...
clear H. clear H0.
unfold fnary_evalu1 in *...
rewrite <- (spush_spop_same k x).
destruct (x k)eqn:E...
Qed.

Lemma shannon_expansion_u_gen {n} k b (f1 f2:bf(S n)) :
       f1 = f2 <->
        fnary_evalu1 k f1 b = fnary_evalu1 k f2 b /\ fnary_evalu1 k f1 (negb b) = fnary_evalu1 k f2 (negb b).
Proof with curry1.
destruct b; curry1; rewrite shannon_expansion_u...
Qed.

Definition piN (n:nat) (k:ltN n) (b:bool) : bf n :=
  (fun x => xorb b (x k)).

Lemma fnary_evalu1_spop_same {n} k (f:bf(S n)) x : fnary_evalu1 k f (x k) (spop k x) = f x.
Proof. unfold fnary_evalu1; curry1. Qed.
#[export] Hint Rewrite @fnary_evalu1_spop_same : curry1_rewrite.

Lemma eval_bf_S_n_u {n} (k:ltN(S n)) (f : bf (S n)) (x : array bool (S n)) :
       f x = (if x k then fnary_evalu1 k f true (spop k x) else fnary_evalu1 k f false (spop k x)).
Proof with curry1.
rewrite <- (spush_spop_same k x).
dest_match_step; curry1; rewrite <- D; rewrite fnary_evalu1_spop_same; reflexivity.
Qed.

(* Section. Dealing with LDD-Linear models *)

Definition axor_input {n} (v:array bool n) (f:bf n) := fun x => f(axor v x).

Definition dot_bf {n} (b:bool) (a0:array bool n) : bf n := (fun a1 => xorb b (adot a0 a1)).