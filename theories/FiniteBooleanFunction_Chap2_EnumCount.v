Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Nat.
Require Import Bool Ring.
Require Import FunInd.
Require Import Recdef.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_VAX.
Require Import MiniBool_Logic.

Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import SimplEnum.
Require Import SimplEnumCount.
Require Import SimplEnumArray.

Fixpoint count_array_bool {n} (p:array bool n -> bool) {struct n} : nat :=
match n as n0 return (array bool n0 -> bool) -> nat with
| 0 => fun p => if p(fun _ => false) then 1 else 0
| S n => fun p => (count_array_bool (fnary_evalo1 p false) + count_array_bool (fnary_evalo1 p true))
end p.

Definition nat_of_bool (b:bool) : nat := if b then 1 else 0.

Lemma ltN_of_bool_lemma0 (b:bool) : nat_of_bool b <? 2 = true.
Proof. destruct b; reflexivity. Qed.

Definition ltN_of_bool (b:bool) : ltN 2 :=
  ltN_of_nat (nat_of_bool b) 2 (ltN_of_bool_lemma0 b).

Definition bool_of_nat (n:nat) : bool := match n with 0 => false | _ => true end.

Definition bool_of_ltN (k:ltN 2) : bool := bool_of_nat(proj1_sig k).

Lemma explicit_finite_constr_bool : explicit_finite_constr bool 2 ltN_of_bool bool_of_ltN.
Proof with curry1.
constructor; apply functional_extensionality...
- destruct x...
- compute...
  destruct x...
  destruct x...
Qed.

Definition idx_array {A nA} (idx_A:A -> ltN nA) {n} (a:array A n) : ltN(nA ^ n) :=
  ltN_of_array_ltN (comp a idx_A).

Definition pi_array  {A nA} (pi_A:ltN nA -> A) {n} (k:ltN(nA ^ n)) : array A n :=
  comp (array_ltN_of_ltN k) pi_A.

Lemma explicit_finite_constr_array
  {A nA iA pA} (FA:explicit_finite_constr A nA iA pA) (n:nat) :
  explicit_finite_constr (array A n) (nA ^ n) (idx_array iA) (pi_array pA).
Proof with curry1.
constructor...
- apply functional_extensionality...
  unfold pi_array, idx_array...
  rewrite <- comp_assoc.
  rewrite (proj1 FA)...
- apply functional_extensionality...
  unfold pi_array, idx_array...
  rewrite <- comp_assoc.
  rewrite (proj2 FA)...
  rewrite comp_id_r...
Qed.



Definition pi_array_bool {n} (k:ltN(2 ^ n)) : array bool n := pi_array bool_of_ltN k.
Definition idx_array_bool {n} (a:array bool n) : ltN(2 ^ n) := idx_array ltN_of_bool a.

Lemma explicit_finite_constr_array_bool n :
  explicit_finite_constr (array bool n) (2 ^ n) idx_array_bool pi_array_bool.
Proof with curry1.
apply explicit_finite_constr_array.
apply explicit_finite_constr_bool.
Qed.

Lemma pi_array_bool_idx_array_bool {n} (a:array bool n) : pi_array_bool (idx_array_bool a) = a.
Proof.
fold (comp idx_array_bool pi_array_bool a).
rewrite(proj1(explicit_finite_constr_array_bool n)); reflexivity.
Qed.
#[export] Hint Rewrite @pi_array_bool_idx_array_bool : curry1_rewrite.

Lemma idx_array_bool_pi_array_bool {n} (k:ltN(2 ^ n)): idx_array_bool (pi_array_bool k) = k.
Proof.
fold (comp pi_array_bool idx_array_bool k).
rewrite(proj2(explicit_finite_constr_array_bool n)); reflexivity.
Qed.
#[export] Hint Rewrite @idx_array_bool_pi_array_bool : curry1_rewrite.


Lemma count_array_bool_vs_count_ltN_lemma0 {n} (p:array bool(S n) -> bool) (b:bool) :
  ltN_count (comp pi_array_bool (fnary_evalo1 p b)) =
    ltN_count (fun k : ltN (2 ^ S n) => comp pi_array_bool p k && (eqb b(shead (pi_array_bool k)))).
Proof with curry1.
apply ltN_count_unique_exists.
pose (fun (k1:{k : ltN (2 ^ n) | comp pi_array_bool (fnary_evalo1 p b) k = true}) =>
  idx_array_bool (scons b (pi_array_bool (proj1_sig k1)))) as proto_i12.
pose (fun (k1:{k : ltN (2 ^ S n) | comp pi_array_bool p k && (eqb b(shead (pi_array_bool k))) = true}) =>
  idx_array_bool (stail (pi_array_bool (proj1_sig k1)))) as proto_p12.
assert(lemma_i12:forall k1,
  (fun k => comp pi_array_bool p k && (eqb b (shead (pi_array_bool k))) = true) (proto_i12 k1)).
{
  intros [k1 H1]...
  unfold proto_i12...
}
assert(lemma_p12:forall k1,
  (fun k => comp pi_array_bool (fnary_evalo1 p b) k = true) (proto_p12 k1)).
{
  intros [k1 H1]...
  unfold proto_p12...
  unfold fnary_evalo1...
}
exists (fun k => (exist _ (proto_i12 k) (lemma_i12 k))).
exists (fun k => (exist _ (proto_p12 k) (lemma_p12 k))).
constructor...
- apply functional_extensionality...
  unfold proto_p12, proto_i12...
- apply functional_extensionality...
  unfold proto_p12, proto_i12...
  destruct x as [x Hx]...
Qed.

Lemma count_array_bool_vs_count_ltN {n} (p:array bool n -> bool) :
  count_array_bool p = ltN_count(comp pi_array_bool p).
Proof with curry1.
induction n...
- unfold shead...
  rewrite (bf0 p)...
- repeat rewrite IHn.
  setoid_rewrite (ltN_count_split (comp pi_array_bool p) (fun k => shead(pi_array_bool k))).
  rewrite count_array_bool_vs_count_ltN_lemma0, eqb_false.
  rewrite count_array_bool_vs_count_ltN_lemma0, eqb_true.
  unfold id.
  lia.
Qed.

Definition explicit_endomorphism_constr {A:Type} (f:A -> A) (g:A -> A) : Prop :=
  @explicit_bijection_constr A A f g.

Lemma explicit_bijection_sig_comp_sig_using_explicit_bijection_constr
  {A B f g} (HE:@explicit_bijection_constr A B f g) (p:B -> bool) :
    exists f' g', @explicit_bijection_constr {a : A | comp f p a = true} {b : B | p b = true} f' g'.
Proof with curry1.
pose (fun (k:{a : A | comp f p a = true}) => f(proj1_sig k)) as proto_f'.
pose (fun (k:{b : B | p b = true}) => g(proj1_sig k)) as proto_g'.
assert(lemma_f':forall (k:{a : A | comp f p a = true}), (fun a => p a = true) (proto_f' k)).
{
  unfold proto_f'...
  destruct k...
}
assert(lemma_g':forall (k:{b : B | p b = true}), (fun a => comp f p a = true) (proto_g' k)).
{
  unfold proto_g'...
  destruct k...
  fold(comp g f x).
  rewrite(proj2 HE)...
}
exists (fun k => exist _ (proto_f' k) (lemma_f' k)).
exists (fun k => exist _ (proto_g' k) (lemma_g' k)).
constructor; apply functional_extensionality; unfold proto_f', proto_g'; intros [x Hx]...
- fold(comp f g x).
  rewrite(proj1 HE)...
- fold(comp g f x).
  rewrite(proj2 HE)...
Qed.

Lemma explicit_bijection_sig_comp_sig_using_explicit_endormorphism_constr
  {A f g} (HE:@explicit_endomorphism_constr A f g) (p:A -> bool) :
    exists f' g', @explicit_bijection_constr {a : A | comp f p a = true} {a : A | p a = true} f' g'.
Proof with curry1.
unfold explicit_endomorphism_constr in HE.
apply (explicit_bijection_sig_comp_sig_using_explicit_bijection_constr HE).
Qed.

Lemma counting_endomorphism {A f g} (HE:explicit_endomorphism_constr f g) (p:A -> bool)
  (count:count_t A) (HC:is_counting A count) : count(comp f p) = count p.
Proof with curry1.
unfold is_counting in HC.
specialize(HC(comp f p)) as Hfp.
destruct Hfp as [ifp [pfp Hfp]]...
specialize(HC p) as [ip [pp Hp]]...
specialize(explicit_bijection_sig_comp_sig_using_explicit_endormorphism_constr HE p)...
specialize(compose_explicit_finite_constr_and_explicit_bijection_constr H Hp)...
specialize(explicit_finite_constr_unique _ _ _ _ _ _ _ Hfp H0)...
Qed.

Lemma rewrite_count_array_bool_as_count_ltN (n:nat) : @count_array_bool n = comp (comp pi_array_bool) ltN_count.
Proof.
apply functional_extensionality; intros.
rewrite count_array_bool_vs_count_ltN; reflexivity.
Qed.

Lemma is_counting_comp {A B:Type} (count_B:count_t B) (HB:is_counting B count_B) :
  forall f g, @explicit_bijection_constr A B f g -> is_counting A (comp (comp g) count_B).
Proof with curry1.
intros.
unfold is_counting in *...
specialize(HB (comp g p)).
destruct HB as [iB [pB HB]]...
rewrite comm_explicit_bijection_constr in H.
specialize(explicit_bijection_sig_comp_sig_using_explicit_bijection_constr H p) as Hp...
rewrite comm_explicit_bijection_constr in Hp.
specialize(compose_explicit_finite_constr_and_explicit_bijection_constr Hp HB) as HpB...
eauto.
Qed.

Lemma is_counting_count_ltN n : is_counting (ltN n) ltN_count.
Proof with curry1.
unfold is_counting...
specialize(explicit_finite_constr_ltN_sig_bool p)...
eauto.
Qed.

Lemma is_counting_count_array_bool (n:nat) : is_counting (array bool n) (@count_array_bool n).
Proof with curry1.
rewrite rewrite_count_array_bool_as_count_ltN.
apply (is_counting_comp _ (is_counting_count_ltN _) _ _ (explicit_finite_constr_array_bool n)).
Qed.

Definition count_is_grounded (A:Type) (count:count_t A) : Prop := count (fun _ => false) = 0.

Definition same_but_one {A B} (p1 p2:A -> B) a b1 b2 : Prop :=
  (forall (a':A), a' <> a -> p1 a' = p2 a') /\ p1 a = b1 /\ p2 a = b2.

Definition count_is_incremental (A:Type) (count:count_t A) : Prop :=
  forall (a:A) (p0 p1:A -> bool), same_but_one p0 p1 a false true -> count p1 = S(count p0).

Lemma count_array_bool_cst_bf_false n : count_array_bool (cst_bf n false) = 0.
Proof. induction n; curry1. Qed.
#[export] Hint Rewrite @count_array_bool_cst_bf_false : curry1_rewrite.

Lemma count_is_grounded_count_array_bool (n:nat) : count_is_grounded _ (@count_array_bool n).
Proof. unfold count_is_grounded; curry1. Qed.

Lemma same_but_one_bf_bool_S {n} (p0 p1:bf (S n)) a b0 b1 :
  same_but_one p0 p1 a b0 b1 <->
    fnary_evalo1 p0 (negb(shead a)) = fnary_evalo1 p1 (negb(shead a)) /\
      same_but_one (fnary_evalo1 p0 (shead a)) (fnary_evalo1 p1 (shead a)) (stail a) b0 b1.
Proof with curry1.
unfold same_but_one...
split...
- repeat rewrite fnary_evalo1_shead_stail.
  split...
  + apply functional_extensionality...
    specialize(H(scons(negb(shead a)) x))...
    unfold fnary_evalo1...
    rewrite H...
    rewrite array_head_expansion in H0...
  + specialize(H(scons(shead a)a'))...
    unfold fnary_evalo1...
    rewrite H...
    rewrite array_head_expansion in H1...
- repeat rewrite fnary_evalo1_shead_stail.
  curry1.
  destruct(eqb(shead a')(shead a))eqn:E0...
  + specialize(H0(stail a'))...
    rewrite array_head_expansion in H1...
    rewrite <- E0 in H0.
    repeat rewrite fnary_evalo1_shead_stail in H0.
    curry1.
  + specialize(f_equal(fun f => f(stail a'))H) as Ha'...
    rewrite <- E0 in Ha'.
    repeat rewrite fnary_evalo1_shead_stail in Ha'.
    curry1.
Qed.

Lemma count_is_incremental_count_array_bool (n:nat) : count_is_incremental _ (@count_array_bool n).
Proof with curry1.
unfold count_is_incremental...
induction n...
- rewrite (bf0 p0) in *...
  rewrite (bf0 p1) in *...
  unfold same_but_one in H...
- rewrite same_but_one_bf_bool_S in H...
  destruct(shead a)eqn:E0; curry1; rewrite H, (IHn _ _ _ H0); lia.
Qed.

Definition cntsat_bf {n} (f:bf n) : nat := count_array_bool f.

Lemma fnary_evalo1_fnary_evalu1_ltN_S {A B n} k x y (f:fnary A B(S(S n))) :
  fnary_evalo1 (fnary_evalu1 (ltN_S k) f x) y = fnary_evalu1 k (fnary_evalo1 f y) x.
Proof with curry1.
unfold fnary_evalo1, fnary_evalu1...
apply functional_extensionality...
apply f_equal...
rewrite spush_ltN_S_scons...
Qed.

Lemma spush_ltN_of_nat_0 {A n} p (a:array A n) b : spush (ltN_of_nat 0 (S n) p) b a = scons b a.
Proof with curry1.
apply functional_extensionality...
unfold spush, scons...
destruct(ltN_pred x) as [x'|]eqn:E0...
destruct x...
Qed.
#[export] Hint Rewrite @spush_ltN_of_nat_0 : curry1_rewrite.

Lemma fnary_evalu1_ltN_of_nat_0 {A B n} p (f:fnary A B(S n)) b :
  fnary_evalu1 (ltN_of_nat 0 (S n) p) f b = fnary_evalo1 f b.
Proof with curry1.
unfold fnary_evalo1, fnary_evalu1...
apply functional_extensionality...
Qed.
#[export] Hint Rewrite @fnary_evalu1_ltN_of_nat_0 : curry1_rewrite.

Lemma rewrite_cnsat_bf_using_fnary_evalu1 {n} k (f:bf(S n)) :
  cntsat_bf f = cntsat_bf(fnary_evalu1 k f false) + cntsat_bf(fnary_evalu1 k f true).
Proof with curry1.
induction n...
destruct(ltN_pred k) as [k'|] eqn:E0...
* specialize(IHn k' (fnary_evalo1 f false)) as H0...
  specialize(IHn k' (fnary_evalo1 f true)) as H1...
  repeat rewrite fnary_evalo1_fnary_evalu1_ltN_S in *...
  lia.
* destruct k...
Qed.

Lemma rewrite_cnsat_bf_using_fnary_evalu1_gen {n} k (b:bool) (f:bf(S n)) :
  cntsat_bf f = cntsat_bf(fnary_evalu1 k f b) + cntsat_bf(fnary_evalu1 k f (negb b)).
Proof. destruct b; rewrite (rewrite_cnsat_bf_using_fnary_evalu1 k); simpl; lia. Qed.


Lemma cntsat_bf_le_pow_2_n {n} (f:bf n) : cntsat_bf f <= 2 ^ n.
Proof with curry1.
induction n...
- dest_match_step...
- specialize(IHn(fnary_evalo1 f false)) as H0.
  specialize(IHn(fnary_evalo1 f true )) as H1.
  lia.
Qed.

Lemma fnary_evalo1_neg_bf {n} (f:bf(S n)) b : fnary_evalo1 (neg_bf f) b = neg_bf(fnary_evalo1 f b).
Proof. reflexivity. Qed.

Lemma cntsat_bf_neg_bf {n} (f:bf n) : cntsat_bf (neg_bf f) = (2 ^ n) - cntsat_bf f.
Proof with curry1.
induction n...
- unfold neg_bf, uop_bf...
  dest_match_step...
- repeat rewrite fnary_evalo1_neg_bf.
  repeat rewrite IHn.
  specialize(cntsat_bf_le_pow_2_n(fnary_evalo1 f false)) as L0.
  specialize(cntsat_bf_le_pow_2_n(fnary_evalo1 f true )) as L1. 
  lia.
Qed.
#[export] Hint Rewrite @cntsat_bf_neg_bf : curry1_rewrite.

Lemma cntsat_bf_cst_bf n b : cntsat_bf (cst_bf n b) = (if b then 2 ^ n else 0).
Proof with curry1.
destruct b...
induction n...
Qed.
#[export] Hint Rewrite @cntsat_bf_cst_bf : curry1_rewrite.

Lemma cntsat_bf_eq_0 {n} (f:bf n) : cntsat_bf f = 0 <-> f = cst_bf _ false.
Proof with curry1.
split...
induction n...
- rewrite(bf0 f) in *...
  dest_match_step...
- specialize(IHn _ H) as F0.
  specialize(IHn _ H0) as F1.
  rewrite shannon_expansion...
Qed.
#[export] Hint Rewrite @cntsat_bf_eq_0 : curry1_rewrite.

Lemma cntsat_bf_eq_pow_2_n {n} (f:bf n) : cntsat_bf f = 2 ^ n <-> f = cst_bf _ true.
Proof with curry1.
split...
induction n...
- rewrite(bf0 f) in *...
  dest_match_step...
- specialize(cntsat_bf_le_pow_2_n (fnary_evalo1 f false))...
  specialize(cntsat_bf_le_pow_2_n (fnary_evalo1 f true ))...
  assert(cntsat_bf (fnary_evalo1 f false) = 2 ^ n /\ cntsat_bf (fnary_evalo1 f true) = 2 ^ n).
  { lia. }
  destruct H2 as [F0 F1]...
  rewrite_subst.
  specialize(IHn _ F0) as E0.
  specialize(IHn _ F1) as E1.
  rewrite shannon_expansion...
Qed.
#[export] Hint Rewrite @cntsat_bf_eq_pow_2_n : curry1_rewrite.

Lemma pow_2_n_plus_cntsat_bf_eq_cntsat_bf {n} (f1 f2:bf n) :
  2 ^ n + cntsat_bf f1 = cntsat_bf f2 <-> f1 = cst_bf _ false /\ f2 = cst_bf _ true.
Proof with curry1.
specialize(cntsat_bf_le_pow_2_n f2)...
split...
assert(cntsat_bf f1 = 0); [ lia | curry1 ].
symmetry in H0...
Qed.
#[export] Hint Rewrite @pow_2_n_plus_cntsat_bf_eq_cntsat_bf : curry1_rewrite.

Lemma cntsat_bf_eq_pow_2_n_plus_cntsat_bf {n} (f1 f2:bf n) :
  cntsat_bf f2  = 2 ^ n + cntsat_bf f1 <-> f1 = cst_bf _ false /\ f2 = cst_bf _ true.
Proof. rewrite (eq_sym_iff (cntsat_bf f2) _ ); curry1. Qed.
#[export] Hint Rewrite @cntsat_bf_eq_pow_2_n_plus_cntsat_bf : curry1_rewrite.

Lemma cntsat_bf_absorb_involutive {n} F (f:bf n) : comp F F = id -> cntsat_bf(comp F f) = cntsat_bf f.
Proof with curry1.
intros.
apply (@counting_endomorphism _ F F)...
- constructor; assumption.
- apply is_counting_count_array_bool.
Qed.