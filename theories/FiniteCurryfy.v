Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.
Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

(* Section 3. Finite Functions : $`A^n \rightarrow B`$ *)

Definition fnary (A B:Type) (n:nat) := array A n -> B.

(* A -> A -> .... -> A -> B *)
Inductive nary (A B:Type) : nat -> Type :=
| Value (b0:B) : nary A B 0
| Param (n:nat) (f0:(A -> nary A B n)) : nary A B (S n).

Lemma Param_eq_Param A B n f1 f2 : Param A B n f1 = Param A B n f2 <-> f1 = f2.
Proof with curry1.
split...
inversion H.
dependent destruction H1...
Qed.
#[export] Hint Rewrite Param_eq_Param : curry1_rewrite.

Lemma Value_eq_Value A B b1 b2 : Value A B b1 = Value A B b2 <-> b1 = b2.
Proof with curry1.
split...
inversion H...
Qed.
#[export] Hint Rewrite Value_eq_Value : curry1_rewrite.

Definition fnary_evalo1 {A B n} (f:fnary A B (S n)) (a:A) : fnary A B n :=
  (fun s => f(scons a s)).

(* [Extraction fnary_evalo1.]
(** val fnary_evalo1 : nat -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary **)

let fnary_evalo1 n f a s =
  f (scons n a s)
 *)


Definition nary_evalo1 {A B n} (fn:nary A B (S n)) (a:A) : nary A B n.
Proof.
remember (S n) as Sn.
destruct fn as [b|n' f].
- exfalso. inversion HeqSn.
- specialize(eq_add_S _ _ HeqSn) as H. subst.
  apply (f a).
Defined.

(* [Extraction nary_evalo1.]
(** val nary_evalo1 : nat -> ('a1, 'a2) nary -> 'a1 -> ('a1, 'a2) nary **)

let nary_evalo1 _ fn a =
  match fn with
  | Value _ -> assert false (* absurd case *)
  | Param (_, f) -> f a
 *)

(* [INITIAL]
Fixpoint eval {A B:Type} {n:nat} (fn:nary A B n) : array A n -> B.
Proof.
destruct fn as [b|n' f].
- apply (fun _ => b).
- apply (fun a => eval _ _ _ (f(shead a)) (stail a)).
Defined.
*)

Fixpoint eval {A B:Type} {n:nat} (fn:nary A B n) (a:array A n) : B :=
  match fn in (nary _ _ n0) return (@fnary A B n0) with
  | Value _ _ b => (fun _ => b)
  | Param _ _ _ f => (fun a => eval (f(shead a)) (stail a))
  end a .

(* [Extraction eval.]
(** val eval : nat -> 'a1 array -> ('a1, 'a2) nary -> 'a2 **)

let rec eval _ a = function
| Value b -> b
| Param (n0, f) -> eval n0 (stail n0 a) (f (shead n0 a))
 *)

(* [INITIAL]
Fixpoint uncurryfy {A B:Type} {n:nat} (fn:nary A B n) : fnary A B n.
Proof.
destruct fn as [b|n f].
- apply ((fun _ => b) : fnary A B 0).
- apply ((fun a => (uncurryfy A B n (f(shead a))(stail a))):fnary A B (S n)).
Defined.
 *)


Fixpoint uncurryfy {A B:Type} {n:nat} (fn:nary A B n) : fnary A B n :=
match fn in (nary _ _ n0) return (@fnary A B n0) with
| Value _ _ b => (fun _  => b)
| Param _ _ n0 f => (fun a => @uncurryfy A B n0 (f (shead a)) (stail a))
end.

(* [Extraction uncurryfy.]
(** val uncurryfy : nat -> ('a1, 'a2) nary -> ('a1, 'a2) fnary **)

let rec uncurryfy _ fn x =
match fn with
| Value b -> b
| Param (n0, f) -> uncurryfy n0 (f (shead n0 x)) (stail n0 x)
 *)

Lemma fun_eval_eq_uncurryfy A B n fn :
  (fun s => eval fn s) = @uncurryfy A B n fn.
Proof. induction fn; reflexivity. Qed.

Fixpoint curryfy {A B:Type} {n:nat} : fnary A B n -> nary A B n.
Proof.
destruct n; intro fn.
- apply (Value _ _ (fn sempty)).
- apply (Param _ _ n (fun a0 => curryfy A B n (fnary_evalo1 fn a0))).
Defined.

(* [Extraction curryfy.]
(** val curryfy : nat -> ('a1, 'a2) fnary -> ('a1, 'a2) nary **)

let rec curryfy n fn =
  match n with
  | O -> Value (fn sempty)
  | S n0 -> Param (n0, (fun a0 -> curryfy n0 (fnary_evalo1 n0 fn a0)))
 *)

Lemma eval_empty_injective {A B} (f1 f2:fnary A B 0) :
  f1 sempty = f2 sempty <-> f1 = f2.
Proof with curry1.
split...
apply functional_extensionality...
rewrite (sempty_unique x) in *...
Qed.
#[export] Hint Rewrite @eval_empty_injective : curry1_rewrite.

Lemma curryfy_injective {A B n} (f1 f2:fnary A B n) :
  curryfy f1 = curryfy f2 <-> f1 = f2.
Proof with curry1.
generalize dependent f2.
generalize dependent f1.
induction n...
split...
apply functional_extensionality...
apply_both_as H (shead x) H0.
rewrite IHn in H0.
apply_both_as H0 (stail x) H1.
unfold fnary_evalo1 in H1...
Qed.
#[export] Hint Rewrite @curryfy_injective : curry1_rewrite.

Lemma uncurryfy_injective {A B n} (f1 f2:nary A B n) :
  uncurryfy f1 = uncurryfy f2 <-> f1 = f2.
Proof with curry1.
generalize dependent f2.
generalize dependent f1.
induction n; intros f1 f2; dependent destruction f1; dependent destruction f2...
- split...
  apply_both H (@sempty A)...
- split...
  apply functional_extensionality...
  rewrite <- IHn.
  apply functional_extensionality...
  specialize(apply_f (fun f => f(scons x x0)) H) as H0...
Qed.
#[export] Hint Rewrite @uncurryfy_injective : curry1_rewrite.

Lemma curryfy_uncurryfy {A B n} (f:nary A B n) : curryfy(uncurryfy f) = f.
Proof with curry1.
generalize dependent f.
induction n; intro f; dependent destruction f...
apply functional_extensionality...
unfold fnary_evalo1.
assert(
  (fun s : array A n => uncurryfy (f1 (shead (scons x s))) (stail (scons x s))) =
   uncurryfy (f1 x)); rewrite_subst.
apply functional_extensionality...
Qed.
#[export] Hint Rewrite @curryfy_uncurryfy : curry1_rewrite.

Lemma uncurryfy_curryfy {A B n} (f:fnary A B n) : uncurryfy(curryfy f) = f.
(* Proof. rewrite <- curryfy_injective; curry1. Qed. *)
Proof.
rewrite <- curryfy_injective.
rewrite curryfy_uncurryfy.
rewrite curryfy_injective.
reflexivity.
Qed.
#[export] Hint Rewrite @uncurryfy_curryfy : curry1_rewrite.

Lemma eval_currify {A B n} (f:fnary A B n) (s:array A n) :
  eval (curryfy f) s = f s.
Proof with curry1.
generalize dependent s.
rewrite fun_eval_eq_uncurryfy...
Qed.

Lemma forall_subst_fnary_by_nary {A B n} (P:fnary A B n -> Prop) :
  (forall f, P f) <-> (forall f, P(uncurryfy f)).
Proof with curry1.
split...
rewrite <- uncurryfy_curryfy.
apply H.
Qed.

Lemma forall_subst_nary_by_fnary {A B n} (P:nary A B n -> Prop) :
  (forall f, P f) <-> (forall f, P(curryfy f)).
Proof with curry1.
split...
rewrite <- curryfy_uncurryfy.
apply H.
Qed.

Lemma nary_extensionality {A B n} (f1 f2:nary A B n) :
  f1 = f2 <-> (forall s, eval f1 s = eval f2 s).
Proof with curry1.
split...
generalize dependent f1.
rewrite forall_subst_nary_by_fnary...
generalize dependent f2.
rewrite forall_subst_nary_by_fnary...
apply functional_extensionality...
Qed.

(* Section. Defining Functions of $`A^n -> B^m`$ *)

Definition fnmary A B n m : Type := fnary A (array B m) n.

Definition fmnary A B n m : Type := array (fnary A B n) m.

Definition fnmary_of_fmnary {A B n m} (f:fmnary A B n m) : fnmary A B n m :=
  (fun v => (fun k => f k v)).

Definition fmnary_of_fnmary {A B n m} (f:fnmary A B n m) : fmnary A B n m :=
  (fun k => (fun v => f v k)).

Lemma fnmary_of_fmnary_injective A B n m (f1 f2:fmnary A B n m) :
  fnmary_of_fmnary f1 = fnmary_of_fmnary f2 <-> f1 = f2.
Proof with curry1.
split...
apply functional_extensionality; intro k.
apply functional_extensionality; intro v.
apply_both_as H v H0.
apply_both_as H0 k H1...
Qed.
#[export] Hint Rewrite fnmary_of_fmnary_injective : curry1_rewrite.

Lemma fmnary_of_fnmary_injective A B n m (f1 f2:fnmary A B n m) :
  fmnary_of_fnmary f1 = fmnary_of_fnmary f2 <-> f1 = f2.
Proof with curry1.
split...
apply functional_extensionality; intro v.
apply functional_extensionality; intro k.
apply_both_as H k H0.
apply_both_as H0 v H1...
Qed.
#[export] Hint Rewrite fmnary_of_fnmary_injective : curry1_rewrite.

Lemma fnmary_of_fmnary_of_fnmary A B n m (f:fnmary A B n m) :
  fnmary_of_fmnary(fmnary_of_fnmary f) = f.
Proof. compute; curry1. Qed.
#[export] Hint Rewrite fnmary_of_fmnary_of_fnmary : curry1_rewrite.

Lemma fmnary_of_fnmary_of_fmnary A B n m (f:fmnary A B n m) :
  fmnary_of_fnmary(fnmary_of_fmnary f) = f.
Proof. compute; curry1. Qed.
#[export] Hint Rewrite fmnary_of_fnmary_of_fmnary : curry1_rewrite.

(* Section. Map *)

(* [MOVEME] *)
Definition fmap {A B1 B2} (map:B1 -> B2) (f:A -> B1) : A -> B2 :=
  comp f map.

Definition fnary_map {A B1 B2} (map:B1 -> B2) {n} (f:fnary A B1 n) : fnary A B2 n :=
  comp f map.

Fixpoint nary_map {A B1 B2} (map:B1 -> B2) {n} (f:nary A B1 n) : nary A B2 n :=
match f with
| Value _ _ b => Value A B2 (map b)
| Param _ _ n0 f0 => Param A B2 n0 (fun a0 => nary_map map (f0 a0))
end.

Lemma fnary_map_fnary_of_nary A B1 B2 map n f :
  uncurryfy (@nary_map A B1 B2 map n f) = fnary_map map (uncurryfy f).
Proof with curry1.
induction f...
apply functional_extensionality...
rewrite H...
Qed.
#[export] Hint Rewrite fnary_map_fnary_of_nary : curry1_rewrite.

Lemma nary_map_nary_of_fnary A B1 B2 map n f :
  nary_map map (curryfy f) = curryfy (@fnary_map A B1 B2 map n f).
Proof. rewrite <- uncurryfy_injective; curry1. Qed.
#[export] Hint Rewrite nary_map_nary_of_fnary : curry1_rewrite.

(* Section. Input Concatenation : $`A^n -> A^m -> B = A^{n+m} -> B`$ *)

(* In this section we prove that :
     $`A^n \rightarrow A^m \rightarrow B \equiv A^{n+m} \rightarrow B`$.
   And we do it for both representation of n-ary function, starting by the
   inductive one. *)

Fixpoint nary_app {A B n m} (f:nary A (nary A B m) n) : nary A B (n+m) :=
  match f with
  | Value _ _ t => t
  | Param _ _ n0 f0 => Param A B (n0 + m) (fun a0 => nary_app (f0 a0))
  end.


Definition fnary_app {A B n1 n2} (f:fnary A (fnary A B n2) n1) : fnary A B (n1+n2) :=
  (fun (a12 : array A (n1+n2)) =>
    (f (array_fst_app n1 n2 a12) (array_snd_app n1 n2 a12))).

Definition fnary_split {A B} n1 n2 (f:fnary A B (n1+n2)) : (fnary A (fnary A B n2) n1) :=
  (fun a1 => (fun a2 => f (array_app a1 a2))).

Lemma fnary_app_fnary_split A B n1 n2 f : fnary_app (@fnary_split A B n1 n2 f) = f.
Proof with curry1.
apply functional_extensionality...
unfold fnary_app, fnary_split...
Qed.
#[export] Hint Rewrite fnary_app_fnary_split : curry1_rewrite.

Lemma fnary_split_fnary_app A B n1 n2 f : @fnary_split A B n1 n2 (fnary_app f) = f.
Proof with curry1.
apply functional_extensionality...
apply functional_extensionality...
unfold fnary_app, fnary_split...
Qed.
#[export] Hint Rewrite fnary_split_fnary_app : curry1_rewrite.

Lemma fnary_split_eq_as_fnary_app_eq A B n1 n2 f g :
  @fnary_split A B n1 n2 f = g <-> f = fnary_app g.
Proof. split; curry1. Qed.

Lemma fnary_split_injective A B n1 n2 f1 f2 :
  @fnary_split A B n1 n2 f1 = fnary_split n1 n2 f2 <-> f1 = f2.
Proof. rewrite fnary_split_eq_as_fnary_app_eq; curry1. Qed.
#[export] Hint Rewrite fnary_split_injective : curry1_rewrite.

Lemma fnary_app_injective A B n1 n2 f1 f2 :
  @fnary_app A B n1 n2 f1 = fnary_app f2 <-> f1 = f2.
Proof. rewrite <- fnary_split_eq_as_fnary_app_eq; curry1. Qed.
#[export] Hint Rewrite fnary_app_injective : curry1_rewrite.

Lemma eval_nary_app_array_app A B n1 n2 f s1 s2 :
  eval (@nary_app A B n1 n2 f) (array_app s1 s2) = eval (eval f s1) s2.
Proof with curry1.
induction f...
specialize(sempty_unique s1)...
Qed.
#[export] Hint Rewrite eval_nary_app_array_app : curry1_rewrite.

Lemma eval_nary_app A B n1 n2 f s :
  eval (@nary_app A B n1 n2 f) s =
    eval (eval f (array_fst_app n1 n2 s)) (array_snd_app n1 n2 s).
Proof. rewrite <- (array_app_array_fst_app_array_snd_app _ n1 n2 s); curry1. Qed.
#[export] Hint Rewrite eval_nary_app : curry1_rewrite.

Lemma nary_app_curryfy_both A B n m f :
  @fnary_app A B n m (uncurryfy (nary_map uncurryfy f)) = uncurryfy (nary_app f).
Proof with curry1.
rewrite <- curryfy_injective...
rewrite nary_extensionality...
Qed.
#[export] Hint Rewrite nary_app_curryfy_both : curry1_rewrite.

Lemma fnary_app_uncurryfy_both A B n m f :
  @nary_app A B n m (curryfy (fnary_map curryfy f)) = curryfy (fnary_app f).
Proof with curry1.
rewrite <- uncurryfy_injective...
apply functional_extensionality...
unfold fnary_map, comp...
Qed.

(* Section. [ABORTED] Ordered Universal Primitives *)

Definition nary_is_oU {A B n} (f:nary A B (S n)) :=
  forall a0 a1, nary_evalo1 f a0 = nary_evalo1 f a1.

Definition fnary_is_oU {A B n} (f:fnary A B (S n)) :=
  forall a0 a1, fnary_evalo1 f a0 = fnary_evalo1 f a1.

Lemma nary_is_oU_curryfy {A B n} f :
  @nary_is_oU A B n (curryfy f) <-> fnary_is_oU f.
Proof with curry1.
unfold nary_is_oU, fnary_is_oU.
split...
rewrite <- curryfy_injective...
Qed.
#[export] Hint Rewrite @nary_is_oU_curryfy : curry1_rewrite.

Lemma uncurryfy_nary_evalo1 {A B n} f a :
  uncurryfy (nary_evalo1 f a) = @fnary_evalo1 A B n (uncurryfy f) a.
Proof with curry1.
dependent destruction f...
unfold fnary_evalo1...
apply functional_extensionality...
Qed.
#[export] Hint Rewrite @uncurryfy_nary_evalo1 : curry1_rewrite.

Lemma curryfy_fnary_evalo1 {A B n} f a :
  @curryfy A B n (fnary_evalo1 f a) = nary_evalo1 (curryfy f) a.
Proof. rewrite <- uncurryfy_injective; curry1. Qed.
#[export] Hint Rewrite @curryfy_fnary_evalo1 : curry1_rewrite.

Lemma fnary_is_oU_uncurryfy {A B n} f :
  @fnary_is_oU A B n (uncurryfy f) <-> nary_is_oU f.
Proof with curry1.
unfold fnary_is_oU, nary_is_oU.
split...
- rewrite <- uncurryfy_injective...
- rewrite <- curryfy_injective...
Qed.
#[export] Hint Rewrite @nary_is_oU_curryfy : curry1_rewrite.

Definition fnary_intro_oU {A B n} (f:fnary A B n) : fnary A B (S n) :=
  (fun a => f(stail a)).

Definition nary_intro_oU {A B n} (f:nary A B n) : nary A B (S n) :=
  Param _ _ _ (fun _ => f).

Definition fnary_is_cst {A B n} b (f:fnary A B n) := f = (fun _ => b).

Definition nary_is_cst {A B n} b (f:nary A B n) := f = curryfy (fun _ => b).

Lemma nary_is_cst_curryfy {A B n} b f :
  @nary_is_cst A B n b (curryfy f) <-> fnary_is_cst b f.
Proof with curry1.
unfold nary_is_cst, fnary_is_cst...
Qed.
#[export] Hint Rewrite @nary_is_cst_curryfy : curry1_rewrite.

Lemma fnary_is_cst_uncurryfy {A B n} b f :
  @fnary_is_cst A B n b (uncurryfy f) <-> nary_is_cst b f.
Proof with curry1.
generalize f.
rewrite forall_subst_nary_by_fnary...
Qed.
#[export] Hint Rewrite @fnary_is_cst_uncurryfy : curry1_rewrite.

Definition fnary_is_oC {A B n} if0 th0 (f:fnary A B (S n)) :=
  forall a0, a0 <> if0 -> fnary_is_cst th0 (fnary_evalo1 f a0).

Definition nary_is_oC {A B n} if0 th0 (f:nary A B (S n)) :=
  forall a0, a0 <> if0 -> nary_is_cst th0 (nary_evalo1 f a0).

Lemma fnary_is_oC_and_is_oU {A B:Set} {n} if0 th0 (f:fnary A B (S n))
(* extra *) beqA some (N:some <> if0) :
  @beq_iff_true A beqA ->
    fnary_is_oC if0 th0 f -> fnary_is_oU f -> fnary_is_cst th0 f.
Proof with curry1.
unfold fnary_is_oC, fnary_is_oU, fnary_is_cst...
apply functional_extensionality...
specialize(H1 (shead x) some)...
specialize(H0 some)...
rewrite H0 in H1...
unfold fnary_evalo1 in *...
apply_both H1 (stail x); simpl in *...
Qed.

(* Section. Alternative Definition of Curryfied Functions *)

Fixpoint cnary (A B:Type) (n:nat) : Type :=
  match n with
  | 0 => B
  | S n' => A -> cnary A B n'
  end.

Fixpoint cnary_of_nary {A B:Type} {n:nat} (f:nary A B n) : cnary A B n :=
match f in (nary _ _ n0) return cnary A B n0 with
| Value _ _ b => b
| Param _ _ _ f0 => (fun (a:A) => (cnary_of_nary (f0 a)))
end.

Fixpoint nary_of_cnary {A B:Type} {n:nat} (f:cnary A B n) : nary A B n :=
match n as n0 return cnary A B n0 -> nary A B n0 with
| 0 => (fun b => Value _ _ b)
| S n' => (fun f => Param _ _ _ (fun a => nary_of_cnary (f a)))
end f.

Lemma cnary_of_nary_of_cnary A B n f :
  cnary_of_nary(@nary_of_cnary A B n f) = f.
Proof with curry1.
induction n...
apply functional_extensionality...
Qed.

Lemma nary_of_cnary_of_nary A B n f :
  nary_of_cnary(@cnary_of_nary A B n f) = f.
Proof with curry1.
induction f...
apply functional_extensionality...
Qed.

(* Uniform Model Operators *)

Definition fnary_evalu1 {A B n} (k:ltN(S n)) (f:fnary A B (S n)) (v:A) : fnary A B n :=
  (fun a => f(spush k v a)).

(* Stuff *)

Lemma fnary_evalo1_shead_stail {A B n} (f:fnary A B (S n)) (a:array A(S n)) :
  fnary_evalo1 f (shead a) (stail a) = f a.
Proof. unfold fnary_evalo1; curry1. Qed.
