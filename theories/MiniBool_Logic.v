Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_VAX.
Import MiniBool_VAX.VAX.

Module BPS. (* Boolean Proposition Solver *)

  (* Basic Type Definition *)

  Ltac simplvax1 := VAX.simplvax1.

  (*  Ltac simplvax1 := simplmore; repeat (progress (autorewrite with simplvax1_rewrite in * ); simplmore). *)

  Inductive BOP2 := AND | OR | XOR | IMP | IFF.

  Lemma AND_eq_AND : AND = AND <-> True.  Proof. simplprop; solve_by_invert. Qed.
  Lemma AND_eq_OR  : AND = OR <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma AND_eq_XOR : AND = XOR <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma AND_eq_IMP : AND = IMP <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma AND_eq_IFF : AND = IFF <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma OR_eq_AND : OR = AND <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma OR_eq_OR  : OR = OR  <-> True .  Proof. simplprop; solve_by_invert. Qed.
  Lemma OR_eq_XOR : OR = XOR <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma OR_eq_IMP : OR = IMP <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma OR_eq_IFF : OR = IFF <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma XOR_eq_AND : XOR = AND <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma XOR_eq_OR  : XOR = OR <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma XOR_eq_XOR : XOR = XOR <-> True. Proof. simplprop; solve_by_invert. Qed.
  Lemma XOR_eq_IMP : XOR = IMP <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma XOR_eq_IFF : XOR = IFF <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma IMP_eq_AND : IMP = AND <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma IMP_eq_OR  : IMP = OR <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma IMP_eq_XOR : IMP = XOR <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma IMP_eq_IMP : IMP = IMP <-> True. Proof. simplprop; solve_by_invert. Qed.
  Lemma IMP_eq_IFF : IMP = IFF <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma IFF_eq_AND : IFF = AND <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma IFF_eq_OR  : IFF = OR <-> False.  Proof. simplprop; solve_by_invert. Qed.
  Lemma IFF_eq_XOR : IFF = XOR <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma IFF_eq_IMP : IFF = IMP <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma IFF_eq_IFF : IFF = IFF <-> True. Proof. simplprop; solve_by_invert. Qed.

  #[export] Hint Rewrite  AND_eq_AND : simplvax1_rewrite.
  #[export] Hint Rewrite  AND_eq_OR  : simplvax1_rewrite.
  #[export] Hint Rewrite  AND_eq_XOR : simplvax1_rewrite.
  #[export] Hint Rewrite  AND_eq_IMP : simplvax1_rewrite.
  #[export] Hint Rewrite  AND_eq_IFF : simplvax1_rewrite.
  #[export] Hint Rewrite  OR_eq_AND : simplvax1_rewrite.
  #[export] Hint Rewrite  OR_eq_OR  : simplvax1_rewrite.
  #[export] Hint Rewrite  OR_eq_XOR : simplvax1_rewrite.
  #[export] Hint Rewrite  OR_eq_IMP : simplvax1_rewrite.
  #[export] Hint Rewrite  OR_eq_IFF : simplvax1_rewrite.
  #[export] Hint Rewrite  XOR_eq_AND : simplvax1_rewrite.
  #[export] Hint Rewrite  XOR_eq_OR  : simplvax1_rewrite.
  #[export] Hint Rewrite  XOR_eq_XOR : simplvax1_rewrite.
  #[export] Hint Rewrite  XOR_eq_IMP : simplvax1_rewrite.
  #[export] Hint Rewrite  XOR_eq_IFF : simplvax1_rewrite.
  #[export] Hint Rewrite  IMP_eq_AND : simplvax1_rewrite.
  #[export] Hint Rewrite  IMP_eq_OR  : simplvax1_rewrite.
  #[export] Hint Rewrite  IMP_eq_XOR : simplvax1_rewrite.
  #[export] Hint Rewrite  IMP_eq_IMP : simplvax1_rewrite.
  #[export] Hint Rewrite  IMP_eq_IFF : simplvax1_rewrite.
  #[export] Hint Rewrite  IFF_eq_AND : simplvax1_rewrite.
  #[export] Hint Rewrite  IFF_eq_OR  : simplvax1_rewrite.
  #[export] Hint Rewrite  IFF_eq_XOR : simplvax1_rewrite.
  #[export] Hint Rewrite  IFF_eq_IMP : simplvax1_rewrite.
  #[export] Hint Rewrite  IFF_eq_IFF : simplvax1_rewrite.

  Inductive expr :=
  | BVar : nat -> expr
  | BCst : bool -> expr
  | BNot : expr -> expr
  | BOp2 : BOP2 -> expr -> expr -> expr
  | BITE : expr -> expr -> expr -> expr.

  Lemma BVar_eq_BVar n1 n2 : BVar n1 = BVar n2 <-> n1 = n2. Proof. split; solve_by_invert. Qed.
  Lemma BCst_eq_BCst c1 c2 : BCst c1 = BCst c2 <-> c1 = c2. Proof. split; solve_by_invert. Qed.
  Lemma BNot_eq_BNot e1 e2 : BNot e1 = BNot e2 <-> e1 = e2. Proof. split; solve_by_invert. Qed.
  Lemma BOp2_eq_BOp2 o1 f1 g1 o2 f2 g2 : BOp2 o1 f1 g1 = BOp2 o2 f2 g2 <-> o1 = o2 /\ f1 = f2 /\ g1 = g2.
  Proof. split; solve_by_invert. Qed.
  Lemma BITE_eq_BITE o1 f1 g1 o2 f2 g2 : BITE o1 f1 g1 = BITE o2 f2 g2 <-> o1 = o2 /\ f1 = f2 /\ g1 = g2.
  Proof. split; solve_by_invert. Qed.

  #[export] Hint Rewrite  BVar_eq_BVar : simplvax1_rewrite.
  #[export] Hint Rewrite  BCst_eq_BCst : simplvax1_rewrite.
  #[export] Hint Rewrite  BNot_eq_BNot : simplvax1_rewrite.
  #[export] Hint Rewrite  BOp2_eq_BOp2 : simplvax1_rewrite.
  #[export] Hint Rewrite  BITE_eq_BITE : simplvax1_rewrite.

  Lemma BVar_eq_BCst n1 c2 : BVar n1 = BCst c2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BVar_eq_BNot n1 e2 : BVar n1 = BNot e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BVar_eq_BOp2 n1 o2 f2 g2 : BVar n1 = BOp2 o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BVar_eq_BITE n1 o2 f2 g2 : BVar n1 = BITE o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BCst_eq_BVar n1 c2 : BCst n1 = BVar c2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BCst_eq_BNot n1 e2 : BCst n1 = BNot e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BCst_eq_BOp2 n1 o2 f2 g2 : BCst n1 = BOp2 o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BCst_eq_BITE n1 o2 f2 g2 : BCst n1 = BITE o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BNot_eq_BVar n1 c2 : BNot n1 = BVar c2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BNot_eq_BCst n1 e2 : BNot n1 = BCst e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BNot_eq_BOp2 n1 o2 f2 g2 : BNot n1 = BOp2 o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BNot_eq_BITE n1 o2 f2 g2 : BNot n1 = BITE o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BOp2_eq_BVar o1 f1 g1 c2 : BOp2 o1 f1 g1 = BVar c2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BOp2_eq_BCst o1 f1 g1 e2 : BOp2 o1 f1 g1 = BCst e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BOp2_eq_BNot o1 f1 g1 e2 : BOp2 o1 f1 g1 = BNot e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BOp2_eq_BITE o1 f1 g1 o2 f2 g2 : BOp2 o1 f1 g1 = BITE o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BITE_eq_BVar o1 f1 g1 c2 : BITE o1 f1 g1 = BVar c2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BITE_eq_BCst o1 f1 g1 e2 : BITE o1 f1 g1 = BCst e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BITE_eq_BNot o1 f1 g1 e2 : BITE o1 f1 g1 = BNot e2 <-> False. Proof. simplprop; solve_by_invert. Qed.
  Lemma BITE_eq_BOp2 o1 f1 g1 o2 f2 g2 : BITE o1 f1 g1 = BOp2 o2 f2 g2 <-> False. Proof. simplprop; solve_by_invert. Qed.

  #[export] Hint Rewrite  BVar_eq_BCst : simplvax1_rewrite.
  #[export] Hint Rewrite  BVar_eq_BNot : simplvax1_rewrite.
  #[export] Hint Rewrite  BVar_eq_BOp2 : simplvax1_rewrite.
  #[export] Hint Rewrite  BVar_eq_BITE : simplvax1_rewrite.
  #[export] Hint Rewrite  BCst_eq_BVar : simplvax1_rewrite.
  #[export] Hint Rewrite  BCst_eq_BNot : simplvax1_rewrite.
  #[export] Hint Rewrite  BCst_eq_BOp2 : simplvax1_rewrite.
  #[export] Hint Rewrite  BCst_eq_BITE : simplvax1_rewrite.
  #[export] Hint Rewrite  BNot_eq_BVar : simplvax1_rewrite.
  #[export] Hint Rewrite  BNot_eq_BCst : simplvax1_rewrite.
  #[export] Hint Rewrite  BNot_eq_BOp2 : simplvax1_rewrite.
  #[export] Hint Rewrite  BNot_eq_BITE : simplvax1_rewrite.
  #[export] Hint Rewrite  BOp2_eq_BVar : simplvax1_rewrite.
  #[export] Hint Rewrite  BOp2_eq_BCst : simplvax1_rewrite.
  #[export] Hint Rewrite  BOp2_eq_BNot : simplvax1_rewrite.
  #[export] Hint Rewrite  BOp2_eq_BITE : simplvax1_rewrite.
  #[export] Hint Rewrite  BITE_eq_BVar : simplvax1_rewrite.
  #[export] Hint Rewrite  BITE_eq_BCst : simplvax1_rewrite.
  #[export] Hint Rewrite  BITE_eq_BNot : simplvax1_rewrite.
  #[export] Hint Rewrite  BITE_eq_BOp2 : simplvax1_rewrite.

  Definition bool_of_BOP2 (op:BOP2) : (bool->bool->bool) :=
  match op with
  | AND => andb
  | OR  => orb
  | XOR => xorb
  | IMP => impb
  | IFF => eqb
  end.

  Fixpoint bool_of_expr (vals:list bool) (e:expr) : bool.
  Proof.
  destruct e as [n|c|e|op e1 e2|eC e0 e1].
  - apply (List.nth n vals false).
  - apply c.
  - apply (negb(bool_of_expr vals e)).
  - apply ((bool_of_BOP2 op) (bool_of_expr vals e1) (bool_of_expr vals e2)).
  - apply (if (bool_of_expr vals eC)
      then (bool_of_expr vals e1)
      else (bool_of_expr vals e0)).
  Defined.

  Definition bOp2 (op:BOP2) (e1 e2:VAX.bexpr) : VAX.bexpr :=
  match op with
  | AND => VAX.bAnd e1 e2
  | OR  => VAX.bOr  e1 e2
  | XOR => VAX.bXor e1 e2
  | IMP => VAX.bImp e1 e2
  | IFF => VAX.bIff e1 e2
  end.

  Fixpoint bexpr_of_expr (e:expr) : VAX.bexpr :=
  match e with
  | BVar n => VAX.bVar n
  | BCst b => VAX.bCst b
  | BNot e => VAX.bNot(bexpr_of_expr e)
  | BOp2 op e1 e2 => bOp2 op (bexpr_of_expr e1) (bexpr_of_expr e2)
  | BITE eC e0 e1 => VAX.bITE (bexpr_of_expr eC) (bexpr_of_expr e0) (bexpr_of_expr e1)
  end.

  Lemma bool_of_bexpr_of_expr (vals:list bool) (e:expr) :
    VAX.bool_of_bexpr vals (bexpr_of_expr e) = bool_of_expr vals e.
  Proof with simplvax1.
  symmetry.
  induction e; simplvax1; rewrite_subst... 
  unfold bOp2; dest_match_step; simplvax1; rewrite_subst.
  Qed.

  Inductive prop_expr :=
  | PVar : nat -> prop_expr
  | PBEq : expr -> expr -> prop_expr
  | PCst : bool -> prop_expr
  | PNot : prop_expr -> prop_expr
  | POp2 : BOP2 -> prop_expr -> prop_expr -> prop_expr.

  Definition prop_of_BOP2 (op:BOP2) : (Prop->Prop->Prop) :=
  match op with
  | AND => fun x y => x /\ y
  | OR  => fun x y => x \/ y
  | XOR => fun x y => x <-> ~y
  | IMP => fun x y => x -> y
  | IFF => fun x y => x <-> y
  end.

  Fixpoint prop_of_expr (valsP:list Prop) (valsB:list bool) (e:prop_expr) : Prop.
  Proof.
  destruct e as [n|be1 be2|c|e|op e1 e2].
  - apply (List.nth n valsP False).
  - apply ((bool_of_expr valsB be1) = (bool_of_expr valsB be2)).
  - apply (if c then True else False).
  - apply (~(prop_of_expr valsP valsB e)).
  - apply ((prop_of_BOP2 op) (prop_of_expr valsP valsB e1) (prop_of_expr valsP valsB e2)).
  Defined.

  Ltac inList x xs :=
    match xs with
      | tt => false
      | (x, _) => true
      | (_, ?xs') => inList x xs'
    end.

  Ltac addToList x xs :=
    let b := inList x xs in
      match b with
        | true => xs
        | false => constr:(pair x xs)
      end.

  Ltac allVars xs e :=
    match e with
(* Proposition *)
      | True => xs
      | False => xs
      | ?e1 /\ ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | ?e1 \/ ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | ?e1 -> ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | ?e1 <-> ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | ?e1 = ?e2 =>
        match type of e1 with
        | bool =>
          let xs := allVars xs e1 in
            allVars xs e2
        end
(* Boolean Logic *)
      | true => xs
      | false => xs
      | negb ?e => allVars xs e
      | andb ?e1 ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | orb ?e1 ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | impb ?e1 ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | eqb ?e1 ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | xorb ?e1 ?e2 =>
        let xs := allVars xs e1 in
          allVars xs e2
      | if ?e1 then ?e2 else ?e3 =>
        let xs := allVars xs e1 in
        let xs := allVars xs e2 in
          allVars xs e3
      | _ => addToList e xs
    end.

  Ltac lookup x xs :=
    match xs with
      | (x, _) => O
      | (_, ?xs') =>
        let n := lookup x xs' in
          constr:(S n)
    end.

  Ltac BoolReify xs e := match e with
    | true => constr:(BCst true)
    | false => constr:(BCst false)
    | negb ?e =>
      let p := BoolReify xs e in
      constr:(BNot p)
    | andb ?e1 ?e2 =>
      let p1 := BoolReify xs e1 in
      let p2 := BoolReify xs e2 in
        constr:(BOp2 AND p1 p2)
    | orb ?e1 ?e2 =>
      let p1 := BoolReify xs e1 in
      let p2 := BoolReify xs e2 in
        constr:(BOp2 OR p1 p2)
    | impb ?e1 ?e2 =>
      let p1 := BoolReify xs e1 in
      let p2 := BoolReify xs e2 in
        constr:(BOp2 IMP p1 p2)
    | eqb ?e1 ?e2 =>
      let p1 := BoolReify xs e1 in
      let p2 := BoolReify xs e2 in
        constr:(BOp2 IFF p1 p2)
    | xorb ?e1 ?e2 =>
      let p1 := BoolReify xs e1 in
      let p2 := BoolReify xs e2 in
        constr:(BOp2 XOR p1 p2)
    | if ?e1 then ?e2 else ?e3 =>
      let p1 := BoolReify xs e1 in
      let p2 := BoolReify xs e2 in
      let p3 := BoolReify xs e3 in
        constr:(BITE p1 p3 p2)
    | _ =>
      let n := lookup e xs in
        constr:(BVar n)
  end.

  Ltac PropReify xs e :=
    match e with
      | True => constr:(PCst true)
      | False => constr:(PCst false)
      | ?e1 /\ ?e2 =>
        let p1 := PropReify xs e1 in
        let p2 := PropReify xs e2 in
          constr:(POp2 AND p1 p2)
      | ?e1 \/ ?e2 =>
        let p1 := PropReify xs e1 in
        let p2 := PropReify xs e2 in
          constr:(POp2 OR p1 p2)
      | ?e1 -> ?e2 =>
        let p1 := PropReify xs e1 in
        let p2 := PropReify xs e2 in
          constr:(POp2 IMP p1 p2)
      | ?e1 <-> ?e2 =>
        let p1 := PropReify xs e1 in
        let p2 := PropReify xs e2 in
          constr:(POp2 IFF p1 p2)
      | ?e1 = ?e2 =>
        match type of e1 with
        | bool =>
          let e1 := BoolReify xs e1 in
          let e2 := BoolReify xs e2 in
            constr:(PBEq e1 e2)
        end
      | _ =>
        let n := lookup e xs in
          constr:(PVar n)
    end.

  Ltac TypeFilter t d xs :=
    match xs with
      | (?x, ?xs) =>
        let tail := TypeFilter t d xs in
        let tx := type of x in
        match type of x with
          | t => constr:(cons x tail)
          | _ => constr:(cons d tail)
        end
      | tt => constr:(@nil t)
    end.

  Ltac BoolFilter xs :=
    match xs with
      | (?x, ?xs) =>
        let tail := BoolFilter xs in
        let tx := type of x in
        match type of x with
          | bool => constr:(cons x tail)
          | _    => constr:(cons false tail)
        end
      | tt => constr:(@nil bool)
    end.

  Fixpoint bool_of_prop (e:prop_expr) : option expr :=
  match e with
  | PVar _ => None
  | PCst b => Some (BCst b)
  | PBEq e1 e2 => Some(BOp2 IFF e1 e2)
  | PNot e => match bool_of_prop e with
    | Some b => Some(BNot b)
    | None => None
    end
  | POp2 op e1 e2 => match bool_of_prop e1, bool_of_prop e2 with
    | Some b1, Some b2 => Some(BOp2 op b1 b2)
    | _, _ => None
    end
  end.

  Lemma correction_bool_of_prop valsP valsB eP eB
    (H:bool_of_prop eP = Some eB) :
    prop_of_expr valsP valsB eP <-> bool_of_expr valsB eB = true.
  Proof with VAX.simplvax2.
  generalize dependent eB.
  induction eP...
  - dest_if...
  - unfold opmap in *; dest_match_step...
    destruct(bool_of_expr valsB e) eqn:E...
    rewrite IHeP...
  - dest_match...
    destruct(bool_of_expr valsB e) eqn:Ee;
    destruct(bool_of_expr valsB e0) eqn:Ee0;
    destruct b; simpl; try rewrite IHeP1, IHeP2; split...
  Qed.

  Lemma main_MicroBPSolver valsP valsB eP :
    match bool_of_prop eP with
    | Some eB => prop_of_expr valsP valsB eP <-> VAX.bool_of_bexpr valsB (VAX.normalize (bexpr_of_expr eB)) = true
    | None => True
    end.
  Proof with VAX.simplvax2.
  dest_match.
  rewrite VAX.bool_of_bexpr_normalize, bool_of_bexpr_of_expr.
  apply correction_bool_of_prop...
  Qed.

  Ltac MicroBPSolver :=
  time match goal with
  | |- ?E =>
    let xs := allVars tt E in
    let pe := PropReify xs E in
    let lb := TypeFilter bool false xs in
    let lp := TypeFilter Prop False xs in
    let A := fresh "A" in
    specialize(main_MicroBPSolver lp lb pe) as A;
    match type of A with
    | match bool_of_prop ?eP with _ => _ end =>
      let X := fresh "X" in
      let HX := fresh "HX" in remember (bool_of_prop eP) as X eqn:HX;
      compute in HX;
      rewrite HX in A; clear HX; clear X
    end;
    match type of A with
    | True => idtac "out of reach"; fail 1
    | prop_of_expr _ _ _ <-> VAX.bool_of_bexpr _ ?EX = true =>
      let X := fresh "X" in
      let HX := fresh "HX" in remember EX as X eqn:HX;
      compute in HX;
      rewrite HX in A; clear HX; clear X
    end;
    rewrite A; clear A; simpl
  end.

End BPS.

Ltac intro_beq := repeat
  match goal with
  | H : ?E = _ |- _ => match type of E with
    | bool => generalize dependent H
    end
  end.

(* Examples

Definition formula1 := forall (b9 b10 b7 b8 b6 b5 b3 b4 b1 b2 b0 b : bool),
  (negb (b9 && b3)
   || negb (b10 && b4) && negb (b7 && b1) && negb (b8 && b2) && negb (b6 && b0) &&
      negb (b5 && b)) &&
  (negb (b10 && b4)
   || negb (b9 && b3) && negb (b7 && b1) && negb (b8 && b2) && negb (b6 && b0) &&
      negb (b5 && b)) &&
  (negb (b7 && b1)
   || negb (b9 && b3) && negb (b10 && b4) && negb (b8 && b2) && negb (b6 && b0) &&
      negb (b5 && b)) &&
  (negb (b8 && b2)
   || negb (b9 && b3) && negb (b10 && b4) && negb (b7 && b1) && negb (b6 && b0) &&
      negb (b5 && b)) &&
  (negb (b6 && b0)
   || negb (b9 && b3) && negb (b10 && b4) && negb (b7 && b1) && negb (b8 && b2) &&
      negb (b5 && b)) &&
  (negb (b5 && b)
   || negb (b9 && b3) && negb (b10 && b4) && negb (b7 && b1) && negb (b8 && b2) &&
      negb (b6 && b0)) = false ->
    negb (b9 && negb (negb b10 && negb b7 && negb b8 && negb b6 && negb b5)) &&
    negb (b10 && negb (negb b9 && negb b7 && negb b8 && negb b6 && negb b5)) &&
    negb (b7 && negb (negb b9 && negb b10 && negb b8 && negb b6 && negb b5)) &&
    negb (b8 && negb (negb b9 && negb b10 && negb b7 && negb b6 && negb b5)) &&
    negb (b6 && negb (negb b9 && negb b10 && negb b7 && negb b8 && negb b5)) &&
    negb (b5 && negb (negb b9 && negb b10 && negb b7 && negb b8 && negb b6)) = false /\
    negb (b3 && negb (negb b4 && negb b1 && negb b2 && negb b0 && negb b)) &&
    negb (b4 && negb (negb b3 && negb b1 && negb b2 && negb b0 && negb b)) &&
    negb (b1 && negb (negb b3 && negb b4 && negb b2 && negb b0 && negb b)) &&
    negb (b2 && negb (negb b3 && negb b4 && negb b1 && negb b0 && negb b)) &&
    negb (b0 && negb (negb b3 && negb b4 && negb b1 && negb b2 && negb b)) &&
    negb (b && negb (negb b3 && negb b4 && negb b1 && negb b2 && negb b0)) = false.

Example formula1_test1 : formula1.
Proof.
unfold formula1. lazy.
intros.
time dest_if.
(* time : 7.345 secs *)
Qed.

Example formula1_test2 : formula1.
Proof with simplbool.
unfold formula1.
intros.
generalize dependent H.
MiniBool.MicroBPSolver...
(* time : 0.354 secs *)
Qed.

Definition formula1bis := forall (b9 b10 b7 b8 b6 b5 b3 b4 b1 b2 b0 b : bool),
    (if
      if
       if
        if
         if
          if if if b9 then b3 else false then false else true
          then true
          else
           if
            if
             if
              if if if b10 then b4 else false then false else true
              then if if b7 then b1 else false then false else true
              else false
             then if if b8 then b2 else false then false else true
             else false
            then if if b6 then b0 else false then false else true
            else false
           then if if b5 then b else false then false else true
           else false
         then
          if if if b10 then b4 else false then false else true
          then true
          else
           if
            if
             if
              if if if b9 then b3 else false then false else true
              then if if b7 then b1 else false then false else true
              else false
             then if if b8 then b2 else false then false else true
             else false
            then if if b6 then b0 else false then false else true
            else false
           then if if b5 then b else false then false else true
           else false
         else false
        then
         if if if b7 then b1 else false then false else true
         then true
         else
          if
           if
            if
             if if if b9 then b3 else false then false else true
             then if if b10 then b4 else false then false else true
             else false
            then if if b8 then b2 else false then false else true
            else false
           then if if b6 then b0 else false then false else true
           else false
          then if if b5 then b else false then false else true
          else false
        else false
       then
        if if if b8 then b2 else false then false else true
        then true
        else
         if
          if
           if
            if if if b9 then b3 else false then false else true
            then if if b10 then b4 else false then false else true
            else false
           then if if b7 then b1 else false then false else true
           else false
          then if if b6 then b0 else false then false else true
          else false
         then if if b5 then b else false then false else true
         else false
       else false
      then
       if if if b6 then b0 else false then false else true
       then true
       else
        if
         if
          if
           if if if b9 then b3 else false then false else true
           then if if b10 then b4 else false then false else true
           else false
          then if if b7 then b1 else false then false else true
          else false
         then if if b8 then b2 else false then false else true
         else false
        then if if b5 then b else false then false else true
        else false
      else false
     then
      if if if b5 then b else false then false else true
      then true
      else
       if
        if
         if
          if if if b9 then b3 else false then false else true
          then if if b10 then b4 else false then false else true
          else false
         then if if b7 then b1 else false then false else true
         else false
        then if if b8 then b2 else false then false else true
        else false
       then if if b6 then b0 else false then false else true
       else false
     else false) = false ->
(if
  if
   if
    if
     if
      if
       if b9
       then
        if
         if
          if
           if if if b10 then false else true then if b7 then false else true else false
           then if b8 then false else true
           else false
          then if b6 then false else true
          else false
         then if b5 then false else true
         else false
        then false
        else true
       else false
      then false
      else true
     then
      if
       if b10
       then
        if
         if
          if
           if if if b9 then false else true then if b7 then false else true else false
           then if b8 then false else true
           else false
          then if b6 then false else true
          else false
         then if b5 then false else true
         else false
        then false
        else true
       else false
      then false
      else true
     else false
    then
     if
      if b7
      then
       if
        if
         if
          if if if b9 then false else true then if b10 then false else true else false
          then if b8 then false else true
          else false
         then if b6 then false else true
         else false
        then if b5 then false else true
        else false
       then false
       else true
      else false
     then false
     else true
    else false
   then
    if
     if b8
     then
      if
       if
        if
         if if if b9 then false else true then if b10 then false else true else false
         then if b7 then false else true
         else false
        then if b6 then false else true
        else false
       then if b5 then false else true
       else false
      then false
      else true
     else false
    then false
    else true
   else false
  then
   if
    if b6
    then
     if
      if
       if
        if if if b9 then false else true then if b10 then false else true else false
        then if b7 then false else true
        else false
       then if b8 then false else true
       else false
      then if b5 then false else true
      else false
     then false
     else true
    else false
   then false
   else true
  else false
 then
  if
   if b5
   then
    if
     if
      if
       if if if b9 then false else true then if b10 then false else true else false
       then if b7 then false else true
       else false
      then if b8 then false else true
      else false
     then if b6 then false else true
     else false
    then false
    else true
   else false
  then false
  else true
 else false) = false /\
(if
  if
   if
    if
     if
      if
       if b3
       then
        if
         if
          if
           if if if b4 then false else true then if b1 then false else true else false
           then if b2 then false else true
           else false
          then if b0 then false else true
          else false
         then if b then false else true
         else false
        then false
        else true
       else false
      then false
      else true
     then
      if
       if b4
       then
        if
         if
          if
           if if if b3 then false else true then if b1 then false else true else false
           then if b2 then false else true
           else false
          then if b0 then false else true
          else false
         then if b then false else true
         else false
        then false
        else true
       else false
      then false
      else true
     else false
    then
     if
      if b1
      then
       if
        if
         if
          if if if b3 then false else true then if b4 then false else true else false
          then if b2 then false else true
          else false
         then if b0 then false else true
         else false
        then if b then false else true
        else false
       then false
       else true
      else false
     then false
     else true
    else false
   then
    if
     if b2
     then
      if
       if
        if
         if if if b3 then false else true then if b4 then false else true else false
         then if b1 then false else true
         else false
        then if b0 then false else true
        else false
       then if b then false else true
       else false
      then false
      else true
     else false
    then false
    else true
   else false
  then
   if
    if b0
    then
     if
      if
       if
        if if if b3 then false else true then if b4 then false else true else false
        then if b1 then false else true
        else false
       then if b2 then false else true
       else false
      then if b then false else true
      else false
     then false
     else true
    else false
   then false
   else true
  else false
 then
  if
   if b
   then
    if
     if
      if
       if if if b3 then false else true then if b4 then false else true else false
       then if b1 then false else true
       else false
      then if b2 then false else true
      else false
     then if b0 then false else true
     else false
    then false
    else true
   else false
  then false
  else true
 else false) = false.

Example formula1bis_test1 : formula1bis.
Proof.
unfold formula1bis. lazy.
intros.
time dest_if.
(* time : 7.345 secs *)
Qed.

Example formula1bis_test2 : formula1bis.
Proof with simplbool.
unfold formula1bis.
intros.
generalize dependent H.
MiniBool.MicroBPSolver.
(* time : 0.354 secs *)
Qed.

*)

(* First Attempt at extending MiniBool to work under binders *)
(*
Inductive type : Type :=
| TUnit : type
| TBool : type
| TType : nat -> type
| TProd : type -> type -> type
| TFunc : type -> type -> type.

Fixpoint sem_type (assoc : nat -> Type) (t : type) : Type :=
  match t with
  | TUnit => unit
  | TBool => bool
  | TType n => (assoc n)
  | TProd t1 t2 => (sem_type assoc t1) *  (sem_type assoc t2)
  | TFunc t1 t2 => (sem_type assoc t1) -> (sem_type assoc t2)
  end.

Inductive term_leaf : type -> Type :=
| LUnit : term_leaf TUnit
| LBool (b:bool) :  term_leaf TBool
| LAndB : term_leaf (TFunc TBool (TFunc TBool TBool))
| LXorB : term_leaf (TFunc TBool (TFunc TBool TBool))
| LNegB : term_leaf (TFunc TBool TBool)
| LFst t1 t2 : term_leaf (TFunc (TProd t1 t2) t1)
| LSnd t1 t2 : term_leaf (TFunc (TProd t1 t2) t2)
| LPair t1 t2 : term_leaf (TFunc t1 (TFunc t2 (TProd t1 t2)))
.

Definition sem_term_leaf assoc {t} (tl:term_leaf t) : sem_type assoc t :=
match tl with
| LUnit => tt
| LBool b => b
| LAndB => andb
| LXorB => xorb
| LNegB => negb
| LFst t1 t2 => @fst (sem_type assoc t1) (sem_type assoc t2)
| LSnd t1 t2 => @snd (sem_type assoc t1) (sem_type assoc t2)
| LPair t1 t2 => @pair (sem_type assoc t1) (sem_type assoc t2)
end.

Inductive term {assoc : nat -> Type} : type -> type -> Type :=
| VId  t : term t t
| VVar t1 t2 (n:nat) : term t1 t2
| VCst t1 t2 (tl:term_leaf t2) : term t1 t2
| VAbs t1 t2 t3 (e : term (TProd t1 t2) t3) : term t1 (TFunc t2 t3)
| VApp t1 t2 t3 (ff : term t1 (TFunc t2 t3)) (xx : term t1 t2) : term t1 t3.

Definition vassoc_t (assoc : nat -> Type) :=
  forall (t:type), nat -> option(sem_type assoc t).

Fixpoint sem_term {assoc} (vassoc : vassoc_t assoc) {t1 t2} (e : @term assoc t1 t2) :
  option(sem_type assoc (TFunc t1 t2)) :=
  match e with
    | VId t => Some (fun x => x)
    | VVar t1 t2 n =>
      match vassoc t2 n with
      | Some v => Some (fun x => v)
      | None   => None
      end
    | VCst t1 t2 tl => Some (fun _ => sem_term_leaf assoc tl)
    | VAbs t1 t2 t3 e =>
      match sem_term vassoc e with
      | Some v1 => Some (fun x y => v1 (x, y))
      | None => None
      end
    | VApp t1 t2 t3 e1 e2 =>
      match sem_term vassoc e1 with
      | Some ff => match sem_term vassoc e2 with
        | Some xx => Some (fun x => ff x (xx x))
        | None => None
        end
      | None => None
      end
  end.

Ltac reify_type lT T :=
  match T with
  | bool => TBool
  | prod ?A ?B =>
    let tA := reify_type lT A in
    let tB := reify_type lT B in
    constr:(TProd tA tB)
  | ?A -> ?B =>
    let tA := reify_type lT A in
    let tB := reify_type lT B in
    constr:(TFunc tA tB)
  | _ => idtac "failure"
(*     let n := VAX.lookup T lT in constr:(TType n) *)
  end.

Goal False.
Proof.
let E := reify_type tt (prod bool bool) in
idtac E.
Abort.

Ltac reify_term_leaf lT e :=
  match e with
  | tt => LUnit
  | false => constr:(LBool false)
  | true  => constr:(LBool true )
  | andb => LAndB
  | xorb => LXorB
  | negb => LNegB
  | @fst ?T1 ?T2 =>
    let t1 := reify_type lT T1 in
    let t2 := reify_type lT T2 in
    constr:(LFst t1 t2)
  | @snd ?T1 ?T2 =>
    let t1 := reify_type lT T1 in
    let t2 := reify_type lT T2 in
    constr:(LSnd t1 t2)
  end.

Ltac reify_term assoc lT lV e :=
  match eval simpl in e with
  | (fun ( x : ?T ) => x) =>
    let t := reify_type lT T in
    constr:(@VId assoc t)
  | (fun (x : ?T1) (y : ?T2) => @?E1 x y) =>
    let ff := constr:(fun p : prod T1 T2 => E1 (fst p) (snd p)) in
    let ff' := eval simpl in ff in
    idtac "VAbs" ff';
    let e1 := reify_term assoc lT lV (fun p : prod T1 T2 => E1 (fst p) (snd p)) in
    constr:(@VAbs assoc _ _ _ e1)
  | (fun (x : ?T1) => ?F (@?E2 x)) =>
    let e1 := reify_term assoc lT lV (fun x : T1 => F) in
    let e2 := reify_term assoc lT lV E2 in
    constr:(@VApp assoc _ _ _ e1 e2)
  | (fun ( _ : ?T1) => ?E ) =>
    let t1 := reify_type lT T1 in
    let tl := reify_term_leaf lT E in
    constr:(@VCst assoc t1 _ tl)
  | (fun ( _ : ?T1) => ?V1 ) =>
    idtac "VVar(1)" T1 V1;
    let T2 := type of V1 in
    let t1 := reify_type lT T1 in
    let t2 := reify_type lT T2 in
    let n := VAX.lookup lV V1 in
    constr:(@VVar assoc t1 t2 n)
  end.

Definition empty_vassoc (assoc : nat -> Type) : vassoc_t assoc :=
  (fun t0 ( _ : nat) => @None (sem_type assoc t0)).

Goal False.
Proof.
let vv0 := constr:(fun x => x && negb x && false && true) in
let vv1 := constr:(fun x y => x && negb y && false && true) in
let vv2 := constr:(fun ( _ : unit ) x y => x && negb y && false && true) in
let vv3 := constr:(fun p : bool * bool => (fun x y : bool => x && negb y && false && true) (fst p) (snd p)) in
let vv4 := constr:(fun p : unit * bool * bool => snd (fst p) && negb (snd p) && false && true) in
let E1 := reify_term ((fun  _ => unit) : nat -> Type) tt tt vv4 in
pose E1 as v1.
pose (sem_term (empty_vassoc (fun _ => unit)) v1) as e1.
simpl in e1.
Abort.

Ltac refl' e :=
  match eval simpl in e with
    | fun x : ?T => andb ( @?E1 x ) ( @?E2 x ) =>
      let r1 := refl' E1 in
      let r2 := refl' E2 in
        constr:(fun x => AndB (r1 x) (r2 x))

    | _ => let vv := idtac "Const blabla" e in constr:(fun x => Const (e x))
  end.

Ltac refl :=
  match goal with
    | [ |- ?E1 = ?E2 ] =>
      let E1' := refl' (fun _ : unit => E1) in
      let E2' := refl' (fun _ : unit => E2) in
        idtac E1' E2';
        change (termDenote (E1' tt) = termDenote (E2' tt));
          cbv beta iota delta [fst snd]
  end.

Goal forall (x0:bool), (fun (x y : bool) => x && y && false) = (fun (_ z : bool) => z).
intro x0.
refl.
simpl.
Abort. *)