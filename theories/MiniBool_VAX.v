Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Module VAX. (* Variable And Xor *)

  (* Basic Type Definition *)

  Ltac simplvax1 := simplmore; repeat (progress (autorewrite with simplvax1_rewrite in *); simplmore).

  Inductive aexpr :=
  | AVar : nat -> aexpr
  | AAnd : bool -> aexpr -> bool -> aexpr -> aexpr
  | AXor : aexpr -> aexpr -> aexpr
  | AITE : aexpr -> aexpr -> bool -> aexpr -> aexpr.

  Lemma AVar_eq_AVar n1 n2 : AVar n1 = AVar n2 <-> n1 = n2. Proof. split; solve_by_invert. Qed.
  Lemma AAnd_eq_AAnd a1 b1 c1 d1 a2 b2 c2 d2 : AAnd a1 b1 c1 d1 = AAnd a2 b2 c2 d2 <-> a1 = a2 /\ b1 = b2 /\ c1 = c2 /\ d1 = d2. Proof. split; solve_by_invert. Qed.
  Lemma AXor_eq_AXor a1 b1 a2 b2 : AXor a1 b1 = AXor a2 b2 <-> a1 = a2 /\ b1 = b2. Proof. split; solve_by_invert. Qed.
  Lemma AITE_eq_AITE a1 b1 c1 d1 a2 b2 c2 d2 : AITE a1 b1 c1 d1 = AITE a2 b2 c2 d2 <-> a1 = a2 /\ b1 = b2 /\ c1 = c2 /\ d1 = d2. Proof. split; solve_by_invert. Qed.

  #[export] Hint Rewrite  AVar_eq_AVar : simplvax1_rewrite.
  #[export] Hint Rewrite  AAnd_eq_AAnd : simplvax1_rewrite.
  #[export] Hint Rewrite  AXor_eq_AXor : simplvax1_rewrite.
  #[export] Hint Rewrite  AITE_eq_AITE : simplvax1_rewrite.

  Lemma AVar_eq_AAnd n1 a2 b2 c2 d2 : AVar n1 = AAnd a2 b2 c2 d2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AVar_eq_AXor n1 a2 b2 : AVar n1 = AXor a2 b2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AVar_eq_AITE n1 a2 b2 c2 d2 : AVar n1 = AITE a2 b2 c2 d2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AAnd_eq_AVar a1 b1 c1 d1 n2 :  AAnd a1 b1 c1 d1 = AVar n2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AAnd_eq_AXor a1 b1 c1 d1 a2 b2 :  AAnd a1 b1 c1 d1 = AXor a2 b2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AAnd_eq_AITE a1 b1 c1 d1 a2 b2 c2 d2 :  AAnd a1 b1 c1 d1 = AITE a2 b2 c2 d2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AXor_eq_AVar a1 b1 n2 :  AXor a1 b1 = AVar n2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AXor_eq_AAnd a1 b1 a2 b2 c2 d2 : AXor a1 b1 = AAnd a2 b2 c2 d2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AXor_eq_AITE a1 b1 a2 b2 c2 d2 : AXor a1 b1 = AITE a2 b2 c2 d2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AITE_eq_AVar a1 b1 c1 d1 n2 :  AITE a1 b1 c1 d1 = AVar n2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AITE_eq_AAnd a1 b1 c1 d1 a2 b2 c2 d2 : AITE a1 b1 c1 d1 = AAnd a2 b2 c2 d2 <-> False. Proof. split; solve_by_invert. Qed.
  Lemma AITE_eq_AXor a1 b1 c1 d1 a2 b2 : AITE a1 b1 c1 d1 = AXor a2 b2 <-> False. Proof. split; solve_by_invert. Qed.

  #[export] Hint Rewrite  AVar_eq_AAnd : simplvax1_rewrite.
  #[export] Hint Rewrite  AVar_eq_AXor : simplvax1_rewrite.
  #[export] Hint Rewrite  AVar_eq_AITE : simplvax1_rewrite.
  #[export] Hint Rewrite  AAnd_eq_AVar : simplvax1_rewrite.
  #[export] Hint Rewrite  AAnd_eq_AXor : simplvax1_rewrite.
  #[export] Hint Rewrite  AAnd_eq_AITE : simplvax1_rewrite.
  #[export] Hint Rewrite  AXor_eq_AVar : simplvax1_rewrite.
  #[export] Hint Rewrite  AXor_eq_AAnd : simplvax1_rewrite.
  #[export] Hint Rewrite  AXor_eq_AITE : simplvax1_rewrite.
  #[export] Hint Rewrite  AITE_eq_AVar : simplvax1_rewrite.
  #[export] Hint Rewrite  AITE_eq_AAnd : simplvax1_rewrite.
  #[export] Hint Rewrite  AITE_eq_AXor : simplvax1_rewrite.

  Definition bexpr : Type := bool * (option aexpr).

  (* Decidable Equality *)

  Fixpoint aexpr_beq (a b:aexpr) : bool :=
  match a, b with
  | AVar na, AVar nb => Nat.eqb na nb
  | AAnd b1a e1a b2a e2a, AAnd b1b e1b b2b e2b =>
    if (eqb b1a b1b) && (eqb b2a b2b)
    then if aexpr_beq e1a e1b
         then aexpr_beq e2a e2b else false
    else false
  | AXor e1a e2a, AXor e1b e2b =>
    if aexpr_beq e1a e1b
    then aexpr_beq e2a e2b else false
  | AITE eCa e0a b1a e1a, AITE eCb e0b b1b e1b =>
    if (eqb b1a b1b)
    then if aexpr_beq eCa eCb
      then if aexpr_beq e0a e0b
        then aexpr_beq e1a e1b
        else false
      else false
    else false
  | _, _ => false
  end.

  Lemma aexpr_beq_reflexive : beq_reflexive aexpr_beq.
  Proof with simplmore.
  intro a.
  induction a...
  Qed.

  Lemma aexpr_beq_iff_true : beq_iff_true aexpr_beq.
  Proof with simplvax1.
  intros a.
  induction a; destruct b; simplvax1;
    (try split); try solve_by_invert;
    dest_match;
    try rewrite IHa1 in *; try rewrite IHa2 in *; try rewrite IHa3 in *; simplvax1;
    try rewrite aexpr_beq_reflexive in *; simplvax1;
    try rewrite andb_true_iff; firstorder.
  Qed.
  #[export] Hint Resolve aexpr_beq_iff_true : simpleq_iff_true.

  Definition bexpr_beq : @beqt bexpr := beq_pair eqb (beq_option aexpr_beq).
  Lemma bexpr_beq_iff_true : beq_iff_true bexpr_beq.
  Proof with auto with simpleq_iff_true; try autorewrite with simpleq_iff_true; simpleq_auto.
  unfold bexpr_beq...
  apply beq_pair_iff_true...
  Qed.

  Ltac simplvax2_step_aexpr := simpleq_main_step aexpr_beq aexpr_beq_iff_true.
  Ltac simplvax2_step_bexpr := simpleq_main_step bexpr_beq bexpr_beq_iff_true.

  Ltac simplvax2 := simplvax1; repeat((simplvax2_step_aexpr || simplvax2_step_bexpr); simplvax1).

  (* Semantic *)

  Fixpoint bool_of_aexpr (vals:list bool) (e:aexpr) : bool.
  Proof.
  destruct e as [n|b1 e1 b2 e2|e1 e2|eC e0 b1 e1].
  - apply (List.nth n vals false).
  - apply (andb(xorb b1 (bool_of_aexpr vals e1))(xorb b2(bool_of_aexpr vals e2))).
  - apply (xorb(bool_of_aexpr vals e1)(bool_of_aexpr vals e2)).
  - apply (if(bool_of_aexpr vals eC)
      then(xorb b1(bool_of_aexpr vals e1))
      else        (bool_of_aexpr vals e0)).
  Defined.

  Definition bool_of_bexpr (vals:list bool) (e:bexpr) : bool :=
    let (b, opa) := e in
    match opa with
    | Some e => xorb b (bool_of_aexpr vals e)
    | None => b end.

  Definition bVar (n:nat) : bexpr := (false, Some (AVar n)).
  Definition bCst (b:bool) : bexpr := (b, None).
  Definition bNot (e:bexpr) : bexpr := let (b, e) := e in (negb b, e).
  Definition bxor (b:bool) (e:bexpr) : bexpr := let (b1, e1) := e in (xorb b b1, e1).

  Definition bAnd (e1 e2:bexpr) : bexpr :=
  match e1, e2 with
  | e, (b, None)
  | (b, None), e => (if b then e else (false, None))
  | (b1, Some ae1), (b2, Some ae2) => if aexpr_beq ae1 ae2
    then if eqb b1 b2 then e1 else (bCst false)
    else (false, Some(AAnd b1 ae1 b2 ae2))
  end.

  Definition bOr (e1 e2:bexpr) : bexpr := bNot(bAnd(bNot e1)(bNot e2)).
  Definition bImp (e1 e2:bexpr) : bexpr := bNot(bAnd e1 (bNot e2)).

  Definition bXor (e1 e2:bexpr) : bexpr :=
  let (b1, ae1) := e1 in
  let (b2, ae2) := e2 in
  let b := xorb b1 b2 in
  match ae1, ae2 with
  | None, None => (b, None)
  | None, Some ae
  | Some ae, None => (b, Some ae)
  | Some ae1, Some ae2 => if aexpr_beq ae1 ae2
    then (bCst b)
    else (b, Some(AXor ae1 ae2))
  end.

  Definition bIff (e1 e2:bexpr) : bexpr := bNot(bXor e1 e2).

  Definition bITE (eC e0 e1:bexpr) : bexpr :=
  match eC with
  | (b, None) => if b then e1 else e0
  | (bC, Some aeC) => match e0, e1 with
    | (b0, None), (b1, None) => (if eqb b0 b1 then (b0, None) else (xorb b0 bC, Some aeC))
    | (b0, None), (b1, Some ae1) => if b0
      (* - EC \/ E1 *)
      then (true , Some(AAnd bC aeC (negb b1) ae1))
      (* EC /\ E1 *)
      else (false, Some(AAnd bC aeC       b1  ae1))
    | (b0, Some ae0), (b1, None) => if b1
      (* EC \/ E0 *)
      then (true , Some(AAnd (negb bC) aeC (negb b0) ae0))
      (* - EC /\ E0 *)
      else (false, Some(AAnd (negb bC) aeC       b0  ae0))
    | (b0, Some ae0), (b1, Some ae1) =>
      if aexpr_beq ae0 ae1
      then if eqb b0 b1
        then e0
        else bXor eC e0
      else if bC
        then (b1, Some(AITE aeC ae1 (xorb b0 b1) ae0))
        else (b0, Some(AITE aeC ae0 (xorb b0 b1) ae1))
    end
  end.

  Lemma bool_of_bexpr_bNot vals e : bool_of_bexpr vals (bNot e) = negb(bool_of_bexpr vals e).
  Proof. unfold bNot. dest_match; simplvax1. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bNot : simplvax1_rewrite.
  Lemma bool_of_bexpr_bxor vals b e : bool_of_bexpr vals (bxor b e) = xorb b (bool_of_bexpr vals e).
  Proof. unfold bxor. dest_match; simplvax1. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bxor : simplvax1_rewrite.
  Lemma bool_of_bexpr_bAnd vals e1 e2 : bool_of_bexpr vals (bAnd e1 e2) = (bool_of_bexpr vals e1) && (bool_of_bexpr vals e2).
  Proof. unfold bAnd. dest_match; simplvax2. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bAnd : simplvax1_rewrite.
  Lemma bool_of_bexpr_bOr vals e1 e2 : bool_of_bexpr vals (bOr e1 e2) = (bool_of_bexpr vals e1) || (bool_of_bexpr vals e2).
  Proof. unfold bOr; simplvax1. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bOr : simplvax1_rewrite.
  Lemma bool_of_bexpr_bImp vals e1 e2 : bool_of_bexpr vals (bImp e1 e2) = impb (bool_of_bexpr vals e1) (bool_of_bexpr vals e2).
  Proof. unfold bImp; simplvax1. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bImp : simplvax1_rewrite.
  Lemma bool_of_bexpr_bXor vals e1 e2 : bool_of_bexpr vals (bXor e1 e2) = xorb (bool_of_bexpr vals e1) (bool_of_bexpr vals e2).
  Proof. unfold bXor; simplvax2. dest_match; try ring; simplvax2. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bXor : simplvax1_rewrite.
  Lemma bool_of_bexpr_bIff vals e1 e2 : bool_of_bexpr vals (bIff e1 e2) = eqb (bool_of_bexpr vals e1) (bool_of_bexpr vals e2).
  Proof. unfold bIff; simplvax2. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bIff : simplvax1_rewrite.
  Lemma bool_of_bexpr_bITE vals eC e0 e1 : bool_of_bexpr vals (bITE eC e0 e1) = if (bool_of_bexpr vals eC) then (bool_of_bexpr vals e1) else (bool_of_bexpr vals e0).
  Proof. unfold bITE; simplvax2. dest_match; try ring; simplvax2; try ring. Qed.
  #[export] Hint Rewrite bool_of_bexpr_bITE : simplvax1_rewrite.

  Definition aXor b e1 e2 : bexpr := bxor b (bXor e1 e2).

  Lemma bool_of_bexpr_aXor vals b e0 e1 :
  bool_of_bexpr vals (aXor b e0 e1) =
    xorb b (xorb (bool_of_bexpr vals e0) (bool_of_bexpr vals e1)).
  Proof. unfold aXor; simplvax2. Qed.
  #[export] Hint Rewrite bool_of_bexpr_aXor : simplvax1_rewrite.

  Lemma bexpr_beq_bxor_l b e0 e1 : bexpr_beq (bxor b e0) e1 = bexpr_beq e0 (bxor b e1).
  Proof. destruct e0; simplvax2. dest_match... dest_bool. Qed.
  #[export] Hint Rewrite bexpr_beq_bxor_l : simplvax1_rewrite.

  Lemma bxor_bxor b1 b2 e : bxor b1 (bxor b2 e) = bxor (xorb b1 b2) e.
  Proof. destruct e; simplvax2. Qed.
  #[export] Hint Rewrite bxor_bxor : simplvax1_rewrite.

  Lemma bxor_false e : bxor false e = e.
  Proof. destruct e; simplvax2. Qed.
  #[export] Hint Rewrite bxor_false : simplvax1_rewrite.

  Definition aAnd b b1 e1 b2 e2 : bexpr := bxor b (bAnd(bxor b1 e1)(bxor b2 e2)).

  Lemma bool_of_bexpr_aAnd vals b b0 e0 b1 e1 :
  bool_of_bexpr vals (aAnd b b0 e0 b1 e1) =
    xorb b (andb (bool_of_bexpr vals (bxor b0 e0))
                 (bool_of_bexpr vals (bxor b1 e1))).
  Proof. unfold aAnd; simplvax2. Qed.
  #[export] Hint Rewrite bool_of_bexpr_aAnd : simplvax1_rewrite.

  Definition aITE b bC eC b0 e0 b1 e1 : bexpr := bxor b (bITE(bxor bC eC)(bxor b0 e0)(bxor b1 e1)).

  Lemma bool_of_bexpr_aITE vals b bC eC b0 e0 b1 e1 :
  bool_of_bexpr vals (aITE b bC eC b0 e0 b1 e1) =
    if bool_of_bexpr vals (bxor bC eC)
      then bool_of_bexpr vals (bxor (xorb b b1) e1)
      else bool_of_bexpr vals (bxor (xorb b b0) e0).
  Proof with simplvax2.
  unfold aITE...
  dest_match_step...
  Qed.
  #[export] Hint Rewrite bool_of_bexpr_aITE : simplvax1_rewrite.

  Fixpoint subst_aexpr (vals:list(nat*bexpr)) (e:aexpr) : bexpr :=
  match e with
  | AVar n => match assoc_find n vals with
    | Some e => e
    | None  => bVar n
    end
  | AAnd b1 e1 b2 e2 => aAnd false b1 (subst_aexpr vals e1) b2 (subst_aexpr vals e2)
  | AXor e1 e2 => aXor false (subst_aexpr vals e1) (subst_aexpr vals e2)
  | AITE eC e0 b1 e1 => aITE false false (subst_aexpr vals eC) false (subst_aexpr vals e0) b1 (subst_aexpr vals e1)
  end.

  Definition subst_bexpr (vals:list(nat*bexpr)) (e:bexpr) : bexpr :=
  match e with
  | (b, None) => (b, None)
  | (b, Some ae) => bxor b (subst_aexpr vals ae)
  end.

  Lemma subst_bexpr_bCst (vals:list bool) n b e :
    bool_of_bexpr vals (subst_bexpr (cons (n, bCst b) nil) e) =
      bool_of_bexpr (subst_list b n vals false) e.
  Proof with simplvax1.
  destruct e...
  symmetry...
  destruct o...
  induction a; simplvax1; rewrite_subst...
  rewrite nth_subst_list...
  dest_match_step...
  Qed.
  #[export] Hint Rewrite subst_bexpr_bCst : simplvax1_rewrite.

  Lemma bool_of_aexpr_app_make_false vals n a :
    bool_of_aexpr (vals ++ make n false) a = bool_of_aexpr vals a.
  Proof with simplmore.
  induction a; simplmore; rewrite_subst.
  rewrite nth_app_make_d...
  Qed.
  #[export] Hint Rewrite bool_of_aexpr_app_make_false : simplvax1_rewrite.

  Lemma bool_of_bexpr_app_make_false vals n a :
    bool_of_bexpr (vals ++ make n false) a = bool_of_bexpr vals a.
  Proof with simplmore.
  destruct a...
  dest_match_step...
  apply bool_of_aexpr_app_make_false.
  Qed.
  #[export] Hint Rewrite bool_of_bexpr_app_make_false : simplvax1_rewrite.

  Lemma bool_of_bexpr_subst_aexpr_bCst (vals : list bool) (n : nat) (b : bool) (e : aexpr) :
       bool_of_bexpr vals (subst_aexpr ((n, bCst b) :: nil) e) =
       bool_of_aexpr (subst_list b n vals false) e.
  Proof. specialize(subst_bexpr_bCst vals n b (false, Some e)); simplvax1. Qed.
  #[export] Hint Rewrite bool_of_bexpr_subst_aexpr_bCst : simplvax1_rewrite.

  Lemma bool_of_aexpr_make_false n a :
    bool_of_aexpr (make n false) a = bool_of_aexpr nil a.
  Proof. specialize(bool_of_aexpr_app_make_false nil n a); simplmore. Qed.
  #[export] Hint Rewrite bool_of_aexpr_make_false : simplvax1_rewrite.

  Lemma bool_of_bexpr_make_false n a :
    bool_of_bexpr (make n false) a = bool_of_bexpr nil a.
  Proof. specialize(bool_of_bexpr_app_make_false nil n a); simplmore. Qed.
  #[export] Hint Rewrite bool_of_bexpr_make_false : simplvax1_rewrite.

  Lemma aexpr_shannon_expension n vals a :
    bool_of_aexpr vals a =
    (if List.nth n vals false
     then bool_of_aexpr (subst_list true n vals false) a
     else bool_of_aexpr (subst_list false n vals false) a).
  Proof with simplvax1.
  specialize(subst_list_nth n vals false).
  rewrite S_eq_SS...
  dest_match_step; simplvax1; rewrite_subst...
  Qed.

  Lemma bexpr_shannon_expension n vals a :
    bool_of_bexpr vals a =
    (if List.nth n vals false
     then bool_of_bexpr (subst_list true n vals false) a
     else bool_of_bexpr (subst_list false n vals false) a).
  Proof with simplvax1.
  specialize(subst_list_nth n vals false).
  rewrite S_eq_SS...
  dest_match_step; simplvax1; rewrite_subst...
  Qed.

  Lemma subst_aexpr_aITE_expend n vals a :
    bool_of_aexpr vals a = bool_of_bexpr vals
      (aITE false false (false, Some (AVar n))
                  false (subst_aexpr ((n, bCst false) :: nil) a)
                  false (subst_aexpr ((n, bCst true ) :: nil) a)).
  Proof. simplvax1. apply aexpr_shannon_expension. Qed.

  Lemma bool_of_bexpr_subst_bexpr_nth vals n e :
    bool_of_bexpr vals (subst_bexpr ((n, bCst (List.nth n vals false)) :: nil) e) =
      bool_of_bexpr vals e.
  Proof with simplvax1.
  simplvax1.
  rewrite subst_list_nth...
  Qed.

  Lemma subst_bexpr_aITE_expend n vals a :
    bool_of_bexpr vals a = bool_of_bexpr vals
      (aITE false false (false, Some(AVar n))
                  false (subst_bexpr ((n, bCst false) :: nil) a)
                  false (subst_bexpr ((n, bCst true ) :: nil) a)).
  Proof. simplvax1. apply bexpr_shannon_expension. Qed.

  Lemma bool_of_bexpr_aITE_factor vals b bC eC b1 e1 b2 e2 :
    bool_of_bexpr vals (aITE b bC eC b1 e1 b2 e2) =
      bool_of_bexpr vals (aITE false false (bxor bC eC)
                                           false (bxor (xorb b b1) e1)
                                           false (bxor (xorb b b2) e2)).
  Proof. simplvax1. Qed.

  Example subst_aexpr_aITE_propa_single (vals:list bool) b bC bC' n b1 e1 b2 e2 :
    bool_of_bexpr vals (aITE b bC (bC', Some (AVar n)) b1 e1 b2 e2) =
      bool_of_bexpr vals (aITE (xorb b b1) false (xorb bC bC', Some(AVar n)) false
        (subst_bexpr (cons (n, bCst (     (xorb bC bC'))) nil) e1) false
        (subst_bexpr (cons (n, bCst (negb (xorb bC bC'))) nil)
          (aXor false (bCst (xorb b1 b2)) e2))).
  Proof with simplvax1.
  simplvax1.
  dest_if; simplvax1; rewrite subst_list_nth...
  Qed.

  Fixpoint fold_var_aexpr {A} (f:nat->A) (g:A->A->A) (a:aexpr) : A :=
  match a with
  | AVar n => f n
  | AAnd b1 e1 b2 e2 => g (fold_var_aexpr f g e1) (fold_var_aexpr f g e2)
  | AXor e1 e2 => g (fold_var_aexpr f g e1) (fold_var_aexpr f g e2)
  | AITE eC e0 b1 e1 => g (fold_var_aexpr f g eC)
                          (g (fold_var_aexpr f g e0) (fold_var_aexpr f g e1))
  end.

  Definition fold_var_bexpr {A} (f:nat->A) (g:A->A->A) (e:bexpr) : option A :=
  match e with
  | (_, Some e) => Some(fold_var_aexpr f g e)
  | _ => None
  end.

  Definition nITE n e0 e1 := bITE (bVar n) e0 e1.

  Fixpoint normalize_rec (fuel:nat) (e:bexpr) : bexpr :=
  match fold_var_bexpr id max e with
  | None => e
  | Some m => match fuel with
    | O => e
    | S fuel => (
      nITE m (normalize_rec fuel (subst_bexpr (cons (m, bCst false) nil) e))
             (normalize_rec fuel (subst_bexpr (cons (m, bCst true ) nil) e))
    )
    end
  end.

  Definition normalize (e:bexpr) :=
  match fold_var_bexpr id max e with
  | None => e
  | Some m => normalize_rec (S m) e
  end.

  Lemma bool_of_bexpr_subst_list_U n vals e :
    bool_of_bexpr (subst_list false n vals false) e =
    bool_of_bexpr (subst_list true  n vals false) e ->
      bool_of_bexpr (subst_list true n vals false) e = bool_of_bexpr vals e.
  Proof with simplvax1.
  intros H1...
  specialize(subst_bexpr_aITE_expend n vals e) as H2...
  rewrite_subst...
  Qed.

  Lemma bool_of_aexpr_subst_list_U n vals e :
    bool_of_aexpr (subst_list false n vals false) e =
    bool_of_aexpr (subst_list true  n vals false) e ->
      bool_of_aexpr (subst_list true n vals false) e = bool_of_aexpr vals e.
  Proof with simplmore.
  specialize (bool_of_bexpr_subst_list_U n vals (false, Some e))...
  Qed.

  Lemma bool_of_bexpr_subst_list_A n vals e :
    bool_of_bexpr (subst_list false n vals false) e =
    negb(bool_of_bexpr (subst_list true  n vals false) e) ->
      bool_of_bexpr (subst_list false n vals false) e =
        xorb (List.nth n vals false) (bool_of_bexpr vals e).
  Proof with simplvax1.
  intros H1...
  specialize(subst_bexpr_aITE_expend n vals e) as H2...
  rewrite_subst...
  dest_if...
  Qed.

  Lemma bool_of_bexpr_nITE vals n e0 e1 :
  bool_of_bexpr vals (nITE n e0 e1) =
    if List.nth n vals false then bool_of_bexpr vals e1 else bool_of_bexpr vals e0.
  Proof with simplvax2.
  unfold nITE. rewrite bool_of_bexpr_bITE...
  Qed.
  #[export] Hint Rewrite bool_of_bexpr_nITE : simplvax1_rewrite.

  Lemma bool_of_bexpr_normalize_rec vals fuel e :
    bool_of_bexpr vals (normalize_rec fuel e) = bool_of_bexpr vals e.
  Proof with simplvax1.
  generalize dependent e.
  induction fuel; simplvax1...
  dest_match_step...
  specialize(IHfuel (subst_bexpr ((n, bCst false) :: nil) e)) as H0...
  specialize(IHfuel (subst_bexpr ((n, bCst true ) :: nil) e)) as H1... clear IHfuel.
  rewrite H0, H1...
  rewrite <- (bexpr_shannon_expension n)...
  Qed.
  #[export] Hint Rewrite bool_of_bexpr_normalize_rec : simplvax1_rewrite.

  Lemma bool_of_bexpr_normalize vals e :
    bool_of_bexpr vals (normalize e) = bool_of_bexpr vals e.
  Proof with simplvax1.
  unfold normalize.
  dest_match_step.
  - apply bool_of_bexpr_normalize_rec.
  - destruct e...
  Qed.
  #[export] Hint Rewrite bool_of_bexpr_normalize : simplvax1_rewrite.

  Fixpoint lower_aexpr (m:nat) (a:aexpr) : bool :=
  match a with
  | AVar n => n <? m
  | AAnd _ e0 _ e1 => lower_aexpr m e0 && lower_aexpr m e1
  | AXor e0 e1 => lower_aexpr m e0 && lower_aexpr m e1
  | AITE eC e0 _ e1 => lower_aexpr m eC && lower_aexpr m e0 && lower_aexpr m e1
  end.

  Definition lower_bexpr (m:nat) (e:bexpr) : bool :=
  match snd e with
  | Some a => lower_aexpr m a
  | None => true
  end.

  Fixpoint normalized_aexpr (a:aexpr) : bool :=
  match a with
  | AVar n => true
  | AAnd bn (AVar n) _ e => lower_aexpr n e && normalized_aexpr e
  | AXor (AVar n) e => lower_aexpr n e && normalized_aexpr e
  | AITE (AVar n) e0 _ e1 =>
    lower_aexpr n e0 && normalized_aexpr e0 &&
    lower_aexpr n e1 && normalized_aexpr e1 &&
    (negb(aexpr_beq e0 e1))
  | _ => false
  end.

  Definition normalized_bexpr (e:bexpr) : bool :=
  match snd e with
  | Some a => normalized_aexpr a
  | None => true
  end.

  Lemma normalize_rec_bCst f b : normalize_rec f (bCst b) = bCst b.
  Proof with simplmore.
  destruct f...
  Qed.
  #[export] Hint Rewrite normalize_rec_bCst : simplvax1_rewrite.

  Lemma normalize_rec_bCst' f b : normalize_rec f (b, None) = bCst b.
  Proof with simplmore.
  destruct f...
  Qed.
  #[export] Hint Rewrite normalize_rec_bCst' : simplvax1_rewrite.

  Lemma bxor_eq_shift_l b e1 e2 : bxor b e1 = e2 <-> e1 = bxor b e2.
  Proof. split; simplvax2. Qed.

  Lemma subst_aexpr_with_lower_aexpr n ne e :
    normalized_aexpr e = true ->
    lower_aexpr n e = true -> subst_aexpr ((n, ne)::nil) e = (false, Some e).
  Proof with simplvax2.
  induction e; simplvax2; rewrite_subst...
  - dest_match_step...
  - dest_match_step...
    dest_match_step...
    rewrite_subst...
    unfold aAnd...
    dest_match_step...
    dest_match_step...
  - dest_match_step...
    dest_match_step...
    rewrite_subst...
    unfold aXor...
    dest_match_step...
    dest_match_step...
  - dest_match_step...
    dest_match_step...
    rewrite_subst...
    unfold aITE...
  Qed.

  Lemma fold_var_aexpr_vs_lower_aexpr n e :
    lower_aexpr n e = Nat.ltb (fold_var_aexpr id max e) n.
  Proof with simplvax2.
  induction e; simplvax2; rewrite_subst; repeat rewrite rewrite_max_ltb; ring.
  Qed.

  Lemma normalized_aexpr_le_trans n m (LE:n<=m) e:
    lower_aexpr n e = true -> lower_aexpr m e = true.
  Proof with simplvax2.
  repeat rewrite fold_var_aexpr_vs_lower_aexpr...
  lia.
  Qed.

  Lemma if_AExpr (b:bool) (b0 b1:bool) (a0 a1:aexpr) :
    (if b then (b0, Some a0) else (b1, Some a1)) =
      (if b then b0 else b1, Some(if b then a0 else a1)).
  Proof. dest_if. Qed.

  Lemma xorb_lemma1 b b0 b0' b1 b1' :
    xorb b0 (xorb b b1) = xorb b (xorb b0' b1') <-> xorb b0 b1 = xorb b0' b1'.
  Proof. dest_bool. Qed.
  #[export] Hint Rewrite xorb_lemma1 : simplprop1_rewrite.

  (* (* [MOVEME] *)
  Lemma pair_inj {A B} (a a':A) (b b':B) : pair a b = pair a' b' <-> a' = a /\ b' = b.
  Proof. split; try solve_by_invert. Qed.
  #[export] Hint Rewrite @pair_inj : simplvax1_rewrite. *)

  Lemma bXor_bxor_r e1 e2 b : bXor e1 (bxor b e2) = bxor b (bXor e1 e2).
  Proof. unfold bXor, bxor; dest_match; unfold bCst in *; simplvax2. Qed.
  #[export] Hint Rewrite bXor_bxor_r : simplvax1_rewrite.

  Lemma bXor_bxor_l e1 e2 b : bXor (bxor b e1) e2 = bxor b (bXor e1 e2).
  Proof. unfold bXor, bxor; dest_match; unfold bCst in *; simplvax2. Qed.
  #[export] Hint Rewrite bXor_bxor_l : simplvax1_rewrite.

  Lemma andb_eq_l b b' : b && b' = b <-> (b = true -> b' = true).
  Proof. dest_bool. Qed.
  #[export] Hint Rewrite andb_eq_l : simplprop1_rewrite.

  Lemma bexpr_beq_bxor_same b e : bexpr_beq e (bxor b e) = negb b.
  Proof with simplvax2.
  destruct e...
  destruct o...
  Qed.
  #[export] Hint Rewrite bexpr_beq_bxor_same : simplvax1_rewrite.

  Lemma bITE_bxor_0 eC b0 e0 e1 :
    bITE eC (bxor b0 e0) e1 = bxor b0 (bITE eC e0 (bxor b0 e1)).
  Proof with simplvax2.
  unfold bITE, bxor.
  dest_match...
  unfold bCst in *... ring.
  Qed.
  #[export] Hint Rewrite bITE_bxor_0 : simplvax1_rewrite.

  Lemma bxor_bITE b eC e0 e1 : bxor b (bITE eC e0 e1) = bITE eC (bxor b e0) (bxor b e1).
  Proof. rewrite bITE_bxor_0; simplvax2. Qed.

  Lemma nITE_bxor_0 eC b0 e0 e1 :
    nITE eC (bxor b0 e0) e1 = bxor b0 (nITE eC e0 (bxor b0 e1)).
  Proof with simplvax2.
  unfold nITE. rewrite bITE_bxor_0...
  Qed.
  #[export] Hint Rewrite nITE_bxor_0 : simplvax1_rewrite.

  Lemma normalize_rec_bxor fuel b e :
    normalize_rec fuel (bxor b e) = bxor b (normalize_rec fuel e).
  Proof with simplvax2.
  generalize dependent b.
  generalize dependent e.
  induction fuel...
  destruct e...
  destruct o...
  repeat rewrite IHfuel in *...
  Qed.
  #[export] Hint Rewrite normalize_rec_bxor : simplvax1_rewrite.

  Lemma bxor_bCst a b : bxor a (bCst b) = bCst(xorb a b).
  Proof. reflexivity. Qed.
  #[export] Hint Rewrite bxor_bCst : simplvax1_rewrite.

  Lemma bAnd_bCst_l b0 e1 : bAnd (bCst b0) e1 = if b0 then e1 else (bCst false).
  Proof. dest_match; simplvax1. Qed.

  Lemma bITE_bCst b e0 e1 : bITE (bCst b) e0 e1 = if b then e1 else e0.
  Proof. unfold bITE; reflexivity. Qed.
  #[export] Hint Rewrite bITE_bCst : simplvax1_rewrite.

  Lemma normalize_rec_bAnd fuel e b :
    normalize_rec fuel e = e ->
    normalize_rec fuel (bAnd (bCst b) e) = bAnd (bCst b) (normalize_rec fuel e).
  Proof with simplvax2.
  unfold bAnd...
  destruct o...
  + destruct b...
  + unfold bCst in *...
    destruct b0...
  Qed.

  (* [TODO] a < c -> (min a b < c <-> True) *)
  (* [TODO] b < c -> (min a b < c <-> True) *)
  (* [TODO] idem for [max] and [c < min a b] *)

  Lemma max_eq_same n m : n = max n m <-> m <= n.
  Proof with simplnat. for_lia1. Qed.
  #[export] Hint Rewrite max_eq_same : simplprop1_rewrite.

  Lemma aXor_bCst b1 b2 e : aXor b1 (bCst b2) e = bxor(xorb b1 b2)e.
  Proof with simplvax2.
  unfold aXor, bCst, bxor...
  dest_match_step...
  Qed.
  #[export] Hint Rewrite aXor_bCst : simplvax1_rewrite.

  Lemma nITE_X n opa :
    nITE n (false, opa) (true, opa) = bXor(bVar n)(false, opa).
  Proof with simplvax2.
  simpl...
  unfold nITE...
  dest_match_step...
  Qed.
  #[export] Hint Rewrite nITE_X : simplvax1_rewrite.

  Lemma fold_var_aexpr_with_lower_aexpr n e :
    lower_aexpr n e = true -> fold_var_aexpr id Init.Nat.max e < n.
  Proof with simplvax2.
  rewrite fold_var_aexpr_vs_lower_aexpr...
  Qed.

  Lemma max_max_lt n m k : n < k -> m < k -> max k (max n m) = k.
  Proof. for_lia1. Qed.

  Lemma nITE_contraction_with_normalized n e :
    normalized_bexpr e = true ->
    fold_var_bexpr id Init.Nat.max e = Some n ->
      nITE n (subst_bexpr ((n, bCst false) :: nil) e) (subst_bexpr ((n, bCst true) :: nil) e) = e.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  unfold normalized_bexpr in H...
  destruct a...
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    repeat rewrite subst_aexpr_with_lower_aexpr...
    destruct b0...
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    repeat rewrite subst_aexpr_with_lower_aexpr...
    dest_match_step...
    dest_match_step...
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H2)...
    rewrite (max_max_lt _ _ _ H4 H5)...
    repeat rewrite subst_aexpr_with_lower_aexpr...
    unfold aITE...
    unfold nITE...
  Qed.

  Lemma subst_bexpr_with_lower_bexpr n ne e :
    normalized_bexpr e = true ->
    lower_bexpr n e = true -> subst_bexpr ((n, ne)::nil) e = e.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  rewrite subst_aexpr_with_lower_aexpr...
  Qed.

  Lemma normalized_bexpr_bxor b e : normalized_bexpr (bxor b e) = normalized_bexpr e.
  Proof. unfold bxor; simplvax2. Qed.
  #[export] Hint Rewrite normalized_bexpr_bxor : simplvax1_rewrite.

  Lemma lower_bexpr_bVar n m : lower_bexpr n (bVar m) = (m <? n).
  Proof. unfold bVar; simplvax2. Qed.
  #[export] Hint Rewrite lower_bexpr_bVar : simplvax1_rewrite.

  Lemma lower_bexpr_bAnd n e0 e1 :
    lower_bexpr n e0 = true -> lower_bexpr n e1 = true -> lower_bexpr n (bAnd e0 e1) = true.
  Proof with simplvax2.
  unfold bAnd...
  unfold lower_bexpr in *...
  dest_match_step...
  - dest_match_step...
    + dest_match_step...
      dest_match_step...
    + dest_match_step...
  - dest_match_step...
    + dest_match_step...
    + dest_match_step...
  Qed.

  Lemma lower_bexpr_bXor n e0 e1 :
    lower_bexpr n e0 = true -> lower_bexpr n e1 = true -> lower_bexpr n (bXor e0 e1) = true.
  Proof with simplvax2.
  unfold bXor...
  unfold lower_bexpr in *...
  dest_match_step...
  - dest_match_step...
    dest_match_step...
  - dest_match_step...
  Qed.

  Lemma lower_bexpr_bxor n b e : lower_bexpr n (bxor b e) = lower_bexpr n e.
  Proof. unfold bxor; simplvax2. Qed.
  #[export] Hint Rewrite lower_bexpr_bxor : simplvax1_rewrite.

  Lemma lower_bexpr_bITE n eC e0 e1 :
    lower_bexpr n eC = true -> lower_bexpr n e0 = true -> lower_bexpr n e1 = true -> lower_bexpr n (bITE eC e0 e1) = true.
  Proof with simplvax2.
  unfold bITE...
  unfold lower_bexpr in *...
  gen_dest_match simplprop...
  Qed.

  Lemma lower_bexpr_subst_aexpr n0 e (H:lower_aexpr n0 e = true) n b : lower_bexpr n0 (subst_aexpr ((n, bCst b) :: nil) e) = true.
  Proof with simplvax2.
  induction e...
  - dest_match_step...
  - unfold aAnd...
    apply lower_bexpr_bAnd...
  - unfold aXor...
    apply lower_bexpr_bXor...
  - unfold aITE. repeat rewrite bxor_false.
    apply lower_bexpr_bITE...
  Qed.

  Lemma normalized_bexpr_bAnd_bVar e (H0:normalized_bexpr e = true) n (H1:lower_bexpr n e = true) b :
    normalized_bexpr (bAnd (bxor b (bVar n)) e) = true.
  Proof with simplvax2.
  unfold bAnd...
  unfold lower_bexpr in *...
  unfold normalized_bexpr in *...
  dest_match_step...
  - dest_match_step...
    dest_match_step...
  - dest_match_step...
  Qed.

  Lemma normalized_bexpr_bXor_bVar e (H0:normalized_bexpr e = true) n (H1:lower_bexpr n e = true) :
    normalized_bexpr (bXor (bVar n) e) = true.
  Proof with simplvax2.
  unfold bXor...
  unfold lower_bexpr in *...
  unfold normalized_bexpr in *...
  dest_match_step...
  dest_match_step...
  dest_match_step...
  Qed.

  Lemma normalized_bexpr_bITE_bVar
    e0 (A0:normalized_bexpr e0 = true) e1 (A1:normalized_bexpr e1 = true)
    n (B0:lower_bexpr n e0 = true) (B1:lower_bexpr n e1 = true) :
      normalized_bexpr (bITE (bVar n) e0 e1) = true.
  Proof with simplvax2.
  unfold bITE...
  unfold lower_bexpr in *...
  unfold normalized_bexpr in *...
  dest_match_step...
  - dest_match_step...
    + dest_match_step...
      dest_match_step...
      dest_match_step...
      dest_match_step...
    + dest_match_step...
  - dest_match_step...
    + dest_match_step...
    + dest_match_step...
  Qed.

  Lemma normalized_bexpr_nITE
    e0 (A0:normalized_bexpr e0 = true) e1 (A1:normalized_bexpr e1 = true)
    n (B0:lower_bexpr n e0 = true) (B1:lower_bexpr n e1 = true) :
      normalized_bexpr (nITE n e0 e1) = true.
  Proof with simplvax2.
  unfold nITE. apply normalized_bexpr_bITE_bVar; simplprop.
  Qed.

  Lemma normalized_bexpr_subst_aexpr_with_normalized_aexpr e (H:normalized_aexpr e = true) n b :
    normalized_bexpr (subst_aexpr ((n, bCst b) :: nil) e) = true.
  Proof with simplvax2.
  induction e...
  - dest_match_step...
  - dest_match_step...
    dest_match_step...
    + rewrite subst_aexpr_with_lower_aexpr...
      unfold aAnd...
      dest_match_step...
    + unfold aAnd. rewrite bxor_false.
      apply normalized_bexpr_bAnd_bVar...
      apply lower_bexpr_subst_aexpr...
  - dest_match_step...
    dest_match_step...
    unfold aXor. rewrite bxor_false.
    apply normalized_bexpr_bXor_bVar...
    apply lower_bexpr_subst_aexpr...
  - dest_match_step...
    dest_match_step...
    + unfold aITE...
      destruct b...
    + unfold aITE. repeat rewrite bxor_false.
      apply normalized_bexpr_bITE_bVar...
      * apply lower_bexpr_subst_aexpr...
      * apply lower_bexpr_subst_aexpr...
  Qed.

  Lemma normalized_bexpr_subst_bexpr_with_normalized_bexpr e (H:normalized_bexpr e = true) n b :
    normalized_bexpr (subst_bexpr ((n, bCst b) :: nil) e) = true.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  unfold normalized_bexpr in H...
  apply normalized_bexpr_subst_aexpr_with_normalized_aexpr...
  Qed.

  Lemma normalize_rec_normalized fuel e :
    normalized_bexpr e = true ->
      normalize_rec fuel e = e.
  Proof with simplvax2.
  intro H.
  generalize dependent e.
  induction fuel...
  dest_match_step...
  repeat rewrite IHfuel...
  - rewrite nITE_contraction_with_normalized...
  - apply normalized_bexpr_subst_bexpr_with_normalized_bexpr...
  - apply normalized_bexpr_subst_bexpr_with_normalized_bexpr...
  Qed.

  Lemma fold_var_aexpr_min_le_max a :
    fold_var_aexpr id min a <= fold_var_aexpr id max a.
  Proof with simplvax2.
  induction a; simplvax2; for_lia1.
  Qed.

  Lemma fold_var_aexpr_max_0 a (H:fold_var_aexpr id max a = 0) :
    fold_var_aexpr id min a = 0.
  Proof with simplvax2.
  specialize(fold_var_aexpr_min_le_max a)...
  Qed.

  Lemma normalize_rec_0 e : normalize_rec 0 e = e.
  Proof. destruct e, o; simpl; reflexivity. Qed.

  Fixpoint asub_aexpr (sub main:aexpr) : bool :=
  if aexpr_beq sub main
  then true
  else match main with
  | AVar _ => false
  | AXor e1 e2
  | AAnd _ e1 _ e2 =>
    if      (asub_aexpr sub e1)
      then true
    else     asub_aexpr sub e2
  | AITE eC e0 _ e1 =>
    if      (asub_aexpr sub eC)
      then true
    else if (asub_aexpr sub e0)
      then true
    else     asub_aexpr sub e1
  end.

  Definition asub_bexpr (sub:aexpr) (main:bexpr) : bool :=
  let (_, opa) := main in
  match opa with
  | Some e => asub_aexpr sub e
  | None => false
  end.

  Lemma asub_aepx_leq_fold_min_is_false a k
    (LE : k < fold_var_aexpr id min a) :
      asub_aexpr (AVar k) a = false.
  Proof with simplvax2.
  generalize dependent k.
  induction a; simplvax2; specialize(IHa1 _ H)...
  specialize(IHa2 _ H0)...
  Qed.

  Lemma asub_aepx_leq_fold_max_is_false a k
    (LE : fold_var_aexpr id max a < k ) :
      asub_aexpr (AVar k) a = false.
  Proof with simplvax2.
  generalize dependent k.
  induction a; simplvax2; specialize(IHa1 _ H)...
  specialize(IHa2 _ H0)...
  Qed.

  Lemma not_asub_aexpr_AVar_k_fold_min a :
    asub_aexpr (AVar (fold_var_aexpr id min a)) a = true.
  Proof with simplvax2.
  induction a; simplvax2; repeat rewrite min_to_if_leb in *; dest_if...
  Qed.
  #[export] Hint Rewrite not_asub_aexpr_AVar_k_fold_min : simplvax1_rewrite.

  Lemma not_asub_aexpr_AVar_k_fold_max a :
    asub_aexpr (AVar (fold_var_aexpr id max a)) a = true.
  Proof with simplvax2.
  induction a; simplvax2; repeat rewrite max_to_if_leb in *; dest_if...
  Qed.
  #[export] Hint Rewrite not_asub_aexpr_AVar_k_fold_max : simplvax1_rewrite.

  Lemma fold_min_iff_asub_aexpr a n :
    n <= fold_var_aexpr id min a <-> forall k, k < n -> asub_aexpr (AVar k) a = false.
  Proof with simplvax2.
  split...
  - apply asub_aepx_leq_fold_min_is_false...
    lia.
  - rewrite le_of_forall_lt_neq...
    specialize(H _ H0).
    rewrite not_asub_aexpr_AVar_k_fold_min in H...
  Qed.

  Lemma fold_max_iff_asub_aexpr a n :
    fold_var_aexpr id max a <= n <-> forall k, n < k -> asub_aexpr (AVar k) a = false.
  Proof with simplvax2.
  split...
  - apply asub_aepx_leq_fold_max_is_false...
    lia.
  - rewrite ge_of_forall_lt_neq...
    specialize(H _ H0).
    rewrite not_asub_aexpr_AVar_k_fold_max in H...
  Qed.

  Lemma asub_bexpr_bAnd n e0 e1 : asub_bexpr (AVar n) (bAnd e0 e1) = true -> asub_bexpr (AVar n) e0 = true \/ asub_bexpr (AVar n) e1 = true.
  Proof with simplvax2.
  unfold bAnd...
  dest_match_step...
  - dest_match_step...
    + dest_match_step...
      * dest_match_step...
      * rewrite andb_false_iff in H...
    + dest_match_step...
  - dest_match_step...
    + dest_match_step...
    + dest_match_step...
  Qed.

  Lemma asub_bexpr_bXor n e0 e1 : asub_bexpr (AVar n) (bXor e0 e1) = true -> asub_bexpr (AVar n) e0 = true \/ asub_bexpr (AVar n) e1 = true.
  Proof with simplvax2.
  unfold bXor...
  dest_match_step...
  - dest_match_step...
    dest_match_step...
    rewrite andb_false_iff in H...
  - dest_match_step...
  Qed.

  Lemma asub_bexpr_bITE n eC e0 e1 :
    asub_bexpr (AVar n) (bITE eC e0 e1) = true -> asub_bexpr (AVar n) eC = true \/ asub_bexpr (AVar n) e0 = true \/ asub_bexpr (AVar n) e1 = true.
  Proof with simplvax2.
  unfold bITE...
  dest_match...
  Qed.

  Lemma asub_bexpr_bxor e0 b1 e1 : asub_bexpr e0 (bxor b1 e1) = asub_bexpr e0 e1.
  Proof. unfold bxor; simplvax2. Qed.
  #[export] Hint Rewrite asub_bexpr_bxor : simplvax1_rewrite.

  Lemma asub_aexpr_with_subst_aexpr_true n e a :
    asub_bexpr (AVar n) (subst_aexpr ((n, e) :: nil) a) = true -> asub_bexpr (AVar n) e = true.
  Proof with simplvax2.
  induction a...
  - dest_match_step...
  - unfold aAnd in *. rewrite bxor_false in *.
    apply asub_bexpr_bAnd in H...
    destruct H...
  - unfold aXor in *. rewrite bxor_false in *.
    apply asub_bexpr_bXor in H...
    destruct H...
  - unfold aITE in *. repeat rewrite bxor_false in *.
    apply asub_bexpr_bITE in H...
    destruct H...
    destruct H...
  Qed.

  Lemma asub_aexpr_with_subst_aexpr_false n e a :
    asub_bexpr (AVar n) e = false -> asub_bexpr (AVar n) (subst_aexpr ((n, e) :: nil) a) = false.
  Proof with simplvax2.
  destruct(asub_bexpr (AVar n) (subst_aexpr ((n, e) :: nil) a))eqn:E0...
  apply asub_aexpr_with_subst_aexpr_true in E0...
  Qed.

  Lemma asub_aexpr_with_subst_aexpr_conserve_true n k e a :
    asub_bexpr (AVar n) (subst_aexpr ((k, e) :: nil) a) = true ->
      asub_aexpr (AVar n) a = true \/ (asub_aexpr (AVar k) a = true /\ asub_bexpr (AVar n) e = true).
  Proof with simplvax2.
  induction a...
  - dest_match_step...
  - unfold aAnd in *. rewrite bxor_false in *.
    apply asub_bexpr_bAnd in H...
    dest_match...
  - unfold aXor in *. rewrite bxor_false in *.
    apply asub_bexpr_bXor in H...
    dest_match...
  - unfold aITE in *. repeat rewrite bxor_false in *.
    apply asub_bexpr_bITE in H...
    dest_match...
  Qed.

  Lemma asub_aexpr_with_subst_aexpr_conserve_false n k e a :
      asub_aexpr (AVar n) a = false /\ (asub_aexpr (AVar k) a = false \/ asub_bexpr (AVar n) e = false) ->
        asub_bexpr (AVar n) (subst_aexpr ((k, e) :: nil) a) = false.
  Proof with simplvax2.
  specialize(asub_aexpr_with_subst_aexpr_conserve_true n k e a)...
  dest_match...
  Qed.

  Lemma lower_aexpr_ind n a : lower_aexpr n a = negb (asub_aexpr (AVar n) a) && lower_aexpr (S n) a.
  Proof with simplvax2.
  induction a...
  - rewrite bool_eq_iff_iff... for_lia1.
  - rewrite_subst. ring.
  - rewrite_subst. ring.
  - rewrite_subst. ring.
  Qed.

  Lemma lower_bexpr_ind n e : lower_bexpr n e = (negb(asub_bexpr (AVar n) e)) && lower_bexpr (S n) e.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  unfold lower_bexpr in *...
  apply lower_aexpr_ind.
  Qed.

  Lemma lower_bexpr_subst_aexpr_fold_var_max a b :
    lower_bexpr (fold_var_aexpr id max a) (subst_aexpr ((fold_var_aexpr id max a, bCst b)::nil) a) = true.
  Proof with simplvax2.
  rewrite lower_bexpr_ind...
  rewrite asub_aexpr_with_subst_aexpr_false...
  apply lower_bexpr_subst_aexpr.
  rewrite fold_var_aexpr_vs_lower_aexpr...
  Qed.

  Lemma min_is_member e n : fold_var_bexpr id min e = Some n -> asub_bexpr (AVar n) e = true.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  Qed.

  Lemma max_is_member e n : fold_var_bexpr id max e = Some n -> asub_bexpr (AVar n) e = true.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  Qed.

  Lemma asub_bexpr_subst_bexpr n0 n1 b e (NE:n0 <> n1) :
    asub_bexpr (AVar n0) (subst_bexpr ((n1, bCst b) :: nil) e) = true ->
      asub_bexpr (AVar n0) e = true.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  apply asub_aexpr_with_subst_aexpr_conserve_true in H...
  Qed.

  Lemma asub_bexpr_nITE k n e0 e1 :
    asub_bexpr (AVar k) (nITE n e0 e1) = true -> k = n \/ asub_bexpr (AVar k) e0 = true \/ asub_bexpr (AVar k) e1 = true.
  Proof with simplvax2.
  unfold nITE; intros.
  apply asub_bexpr_bITE in H...
  Qed.

  Lemma normalize_rec_conserve fuel n e :
    asub_bexpr (AVar n) (normalize_rec fuel e) = true -> asub_bexpr (AVar n) e = true.
  Proof with simplvax2.
  generalize dependent e.
  induction fuel...
  dest_match_step...
  specialize(IHfuel (subst_bexpr ((n0, bCst false) :: nil) e)) as IH0...
  specialize(IHfuel (subst_bexpr ((n0, bCst true ) :: nil) e)) as IH1...
  clear IHfuel.
  rewrite_subst...
  destruct(n =? n0)eqn:E0...
  - apply max_is_member in D...
  - apply asub_bexpr_nITE in H...
    destruct H...
    + apply asub_bexpr_subst_bexpr in IH0...
    + apply asub_bexpr_subst_bexpr in IH1...
  Qed.

  Lemma above_min_aexpr e n' :
    asub_aexpr (AVar n') e = true -> fold_var_aexpr id Init.Nat.min e <= n'.
  Proof with simplvax2.
  specialize(asub_aepx_leq_fold_min_is_false e n')...
  lia.
  Qed.

  Lemma above_min_bexpr e n n' :
    fold_var_bexpr id Init.Nat.min e = Some n -> asub_bexpr (AVar n') e = true -> n' >= n.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  unfold ge.
  apply above_min_aexpr...
  Qed.

  Lemma normalize_rec_fold_min_le fuel e n n'
    (H0:fold_var_bexpr id min e = Some n)
    (H1:fold_var_bexpr id min (normalize_rec fuel e) = Some n') : n <= n'.
  Proof with simplvax2.
  destruct(n <=? n')eqn:E0...
  specialize(normalize_rec_conserve fuel n' e)...
  apply min_is_member in H1...
  specialize(above_min_bexpr _ _ _ H0 H)...
  lia.
  Qed.

  Definition unop {A} (d:A) (x:option A) : A := match x with Some a => a | None => d end.

  Lemma fold_var_bexpr_bxor {A} f g b e : @fold_var_bexpr A f g (bxor b e) = fold_var_bexpr f g e.
  Proof. unfold bxor; simplvax2. Qed.
  #[export] Hint Rewrite @fold_var_bexpr_bxor : simplvax1_rewrite.

  Lemma lower_aexpr_0 a : lower_aexpr 0 a = false.
  Proof. rewrite fold_var_aexpr_vs_lower_aexpr... for_lia1. Qed.
  #[export] Hint Rewrite lower_aexpr_0 : simplvax1_rewrite.

  Lemma lower_bexpr_subst_bexpr n e k b :
    lower_bexpr n e = true -> lower_bexpr n (subst_bexpr ((k, bCst b) :: nil) e) = true.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  rewrite lower_bexpr_subst_aexpr...
  Qed.

  Lemma lower_bexpr_with_fold_bar_bexpr n e k : lower_bexpr n e = true -> fold_var_bexpr id max e = Some k -> k < n.
  Proof with simplvax2.
  unfold lower_bexpr in *...
  unfold snd in *...
  dest_match...
  rewrite fold_var_aexpr_vs_lower_aexpr in H...
  Qed.

  Lemma under_max_aexpr e n' :
    asub_aexpr (AVar n') e = true -> n' <= fold_var_aexpr id max e.
  Proof with simplvax2.
  specialize(asub_aepx_leq_fold_max_is_false e n')...
  lia.
  Qed.

  Lemma under_max_bexpr e n n' :
    fold_var_bexpr id max e = Some n -> asub_bexpr (AVar n') e = true -> n >= n'.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  unfold ge.
  apply under_max_aexpr...
  Qed.

  Lemma asub_bexpr_subst_bexpr_same n b e :
    asub_bexpr (AVar n) (subst_bexpr ((n, bCst b) :: nil) e) = false.
  Proof with simplvax2.
  destruct e...
  dest_match_step...
  rewrite asub_aexpr_with_subst_aexpr_false...
  Qed.
  #[export] Hint Rewrite asub_bexpr_subst_bexpr_same : simplvax1_rewrite.

  Lemma lower_bexpr_fold_var_max fuel e n b :
    lower_bexpr (S fuel) e = true ->
    fold_var_bexpr id Init.Nat.max e = Some n ->
    lower_bexpr fuel (subst_bexpr ((n, bCst b) :: nil) e) = true.
  Proof with simplvax2.
  intros...
  rewrite lower_bexpr_ind...
  rewrite lower_bexpr_subst_bexpr...
  specialize(lower_bexpr_with_fold_bar_bexpr _ _ _ H H0)...
  destruct(n <? fuel)eqn:E0...
  destruct(asub_bexpr (AVar fuel) (subst_bexpr ((n, bCst b) :: nil) e))eqn:E1...
  apply asub_bexpr_subst_bexpr in E1...
  specialize(under_max_bexpr _ _ _ H0 E1)...
  lia.
  Qed.

  Lemma lower_aexpr_as_asub_aexpr n a : lower_aexpr n a = true <-> (forall k, n <= k -> asub_aexpr (AVar k) a = false).
  Proof with simplvax2.
  rewrite fold_var_aexpr_vs_lower_aexpr...
  destruct n...
  - specialize(H(fold_var_aexpr id max a))...
  - rewrite fold_max_iff_asub_aexpr...
  Qed.

  Lemma lower_bexpr_as_asub_bexpr n a : lower_bexpr n a = true <-> (forall k, n <= k -> asub_bexpr (AVar k) a = false).
  Proof with simplvax2.
  unfold lower_bexpr, asub_bexpr...
  dest_match_step...
  apply lower_aexpr_as_asub_aexpr.
  Qed.

  Lemma asub_bexpr_subst_bexpr_bCst k n b e :
    asub_bexpr (AVar k) (subst_bexpr ((n, bCst b) :: nil) e) = true -> k <> n /\ asub_bexpr (AVar k) e = true.
  Proof with simplvax2.
  destruct(k =? n)eqn:E0...
  specialize(asub_bexpr_subst_bexpr _ _ _ _ E0 H)...
  Qed.

  Lemma lower_bexpr_normalize_rec_subst_bexpr_same e n b fuel :
    fold_var_bexpr id Init.Nat.max e = Some n ->
      lower_bexpr n (normalize_rec fuel (subst_bexpr ((n, bCst b) :: nil) e)) = true.
  Proof with simplvax2.
  intros...
  rewrite lower_bexpr_as_asub_bexpr...
  destruct(asub_bexpr (AVar k) (normalize_rec fuel (subst_bexpr ((n, bCst b) :: nil) e)))eqn:E0...
  apply normalize_rec_conserve in E0...
  apply asub_bexpr_subst_bexpr_bCst in E0...
  specialize(under_max_bexpr _ _ _ H H2)...
  lia.
  Qed.

  Lemma normalized_normalize_rec fuel e (GE:lower_bexpr fuel e = true) :
    normalized_bexpr (normalize_rec fuel e) = true.
  Proof with simplvax2.
  generalize dependent e.
  induction fuel...
  - unfold lower_bexpr in *...
    destruct e...
    dest_match_step...
  - dest_match_step...
    + apply normalized_bexpr_nITE...
      * apply IHfuel...
        apply lower_bexpr_fold_var_max...
      * apply IHfuel...
        apply lower_bexpr_fold_var_max...
      * apply lower_bexpr_normalize_rec_subst_bexpr_same...
      * apply lower_bexpr_normalize_rec_subst_bexpr_same...
    + destruct e...
  Qed.

  Lemma normalized_normalize (e:bexpr) : normalized_bexpr(normalize e) = true.
  Proof with simplvax2.
  destruct e...
  unfold normalize, fold_var_bexpr.
  destruct o; try rewrite S_eq_SS...
  rewrite normalized_normalize_rec...
  unfold lower_bexpr...
  rewrite fold_var_aexpr_vs_lower_aexpr...
  unfold SS...
  Qed.

  Lemma length_subst_list {A} b n l d : @length A (subst_list b n l d) = max (length l) (S n).
  Proof with simpllist1.
  generalize dependent l.
  induction n; destruct l...
  Qed.
  #[export] Hint Rewrite @length_subst_list :simpllist1_rewrite.

  Lemma bool_of_aexpr_subst_list n a b l :
    lower_aexpr n a = true -> normalized_aexpr a = true -> bool_of_aexpr (subst_list b n l false) a = bool_of_aexpr l a.
  Proof with simplvax2.
  intros.
  rewrite <- bool_of_bexpr_subst_aexpr_bCst.
  rewrite subst_aexpr_with_lower_aexpr...
  Qed.

  (* [TODO] generalize either as a decision procedure or to locally disjoint formulas *)
  Lemma find_aexpr_lemma (a:aexpr) (H:normalized_aexpr a = true) (s:bool) :
    exists (vals:list bool), length vals <= S(fold_var_aexpr id max a) /\ bool_of_aexpr vals a = s.
  Proof with simplvax2.
  generalize dependent s.
  induction a...
  - exists (subst_list s n nil false)...
  - dest_match_step...
    destruct (IHa2 (negb b0)) as [v2 IH2].
    exists (subst_list (xorb b s) n v2 false)...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    split...
    + lia.
    + rewrite <- bool_of_bexpr_subst_aexpr_bCst.
      rewrite subst_aexpr_with_lower_aexpr...
      rewrite_subst...
  - dest_match_step...
    destruct (IHa2 s) as [v2 IH2].
    exists v2...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    rewrite List.nth_overflow...
    + lia.
    + lia.
  - dest_match_step...
    destruct(IHa2 s) as [v2 IH2].
    exists v2...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    rewrite List.nth_overflow...
    + for_lia1.
    + lia.
  Qed.

  (* assumes [normalized_aexpr a = true] *)
  Fixpoint find_aexpr (a:aexpr) (s:bool) : list bool :=
  match a with
  | AVar n => subst_list s n nil false
  | AAnd b0 a0 b1 a1 => subst_list (xorb b0 s) (fold_var_aexpr id max a0) (find_aexpr a1 (negb b1)) false
  | AXor a0 a1 => find_aexpr a1 s
  | AITE aC a0 b1 a1 => find_aexpr a0 s
  end.

  Lemma length_find_aexpr_spec (a:aexpr) (H:normalized_aexpr a = true) (s:bool) : length (find_aexpr a s) <= S(fold_var_aexpr id max a) .
  Proof with simplvax2.
  generalize dependent s.
  induction a...
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    specialize(IHa2 (negb b0))...
    lia.
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    specialize(IHa2 s)...
    lia.
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H)...
    specialize(IHa2 s)...
    lia.
  Qed.

  Lemma length_find_aexpr_with_lower e (H:normalized_aexpr e = true) n b : lower_aexpr n e = true -> length (find_aexpr e b) <= n.
  Proof with simplvax2.
  intros.
  specialize(length_find_aexpr_spec _ H b)...
  specialize(fold_var_aexpr_with_lower_aexpr _ _ H0)...
  lia.
  Qed.

  Lemma bool_of_aexpr_find_aexpr a (H:normalized_aexpr a = true) s : bool_of_aexpr (find_aexpr a s) a = s.
  Proof with simplvax2.
  generalize dependent s.
  induction a...
  - dest_match_step...
    rewrite bool_of_aexpr_subst_list...
    rewrite IHa2...
  - dest_match_step...
    rewrite IHa2...
    rewrite List.nth_overflow...
    apply length_find_aexpr_with_lower...
  - destruct a1...
    rewrite List.nth_overflow...
    apply length_find_aexpr_with_lower...
  Qed.

  Lemma normalized_aexpr_same_max_aux eA eB
    (NA: normalized_aexpr eA = true) (NB : normalized_aexpr eB = true)
    (EQ:forall vals, bool_of_aexpr vals eA = bool_of_aexpr vals eB) : fold_var_aexpr id max eA < fold_var_aexpr id max eB -> False.
  Proof with simplvax2.
  intros.
  destruct eB...
  - specialize(EQ (find_aexpr eA true)) as EQ1...
    rewrite bool_of_aexpr_find_aexpr in EQ1...
    rewrite List.nth_overflow in EQ1...
    specialize(length_find_aexpr_spec _ NA true)...
    lia.
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H0)...
    specialize(EQ (subst_list b n (find_aexpr eA true) false)) as EQ1...
    rewrite bool_of_aexpr_subst_list in EQ1...
    + rewrite bool_of_aexpr_find_aexpr in EQ1...
    + rewrite fold_var_aexpr_vs_lower_aexpr...
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H0)...
    specialize(EQ (subst_list (negb(xorb (bool_of_aexpr nil eB2) (bool_of_aexpr nil eA))) n nil false)) as EQ1...
    rewrite bool_of_aexpr_subst_list in EQ1...
    + rewrite bool_of_aexpr_subst_list in EQ1...
    + rewrite fold_var_aexpr_vs_lower_aexpr...
  - dest_match_step...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H0)...
    specialize(fold_var_aexpr_with_lower_aexpr _ _ H3)...
    rewrite Max.max_assoc in H...
    specialize(EQ nil) as EQ0...
  Abort.

  Lemma normalized_aexpr_canonical eA eB
    (NA: normalized_aexpr eA = true) (NB : normalized_aexpr eB = true)
    (EQ:forall vals, bool_of_aexpr vals eA = bool_of_aexpr vals eB) : eA = eB.
  Proof with simplvax2.
  generalize dependent eB.
  induction eA... destruct eB; simpl in *...
  - rewrite forall_list_bool in EQ...
  - dest_match_step...
    destruct(n0 =? n)eqn:E1...
    + specialize(EQ (subst_list b n (find_aexpr eB2 b0) false)) as EQ1...
      specialize(EQ (subst_list true n (find_aexpr eB2 b0) false)) as EQ1...
      rewrite bool_of_aexpr_subst_list in EQ1...
      rewrite bool_of_aexpr_find_aexpr in EQ1...
    + specialize(EQ (let vals := find_aexpr eB2 (negb b0) in subst_list (negb(xorb b (List.nth n vals false))) n0 vals false)) as EQ1...
      rewrite bool_of_aexpr_subst_list in EQ1...
      rewrite bool_of_aexpr_find_aexpr in EQ1...
      rewrite nth_subst_list in EQ1...
  - dest_match_step...
    destruct(n0 =? n)eqn:E1...
    + specialize(EQ (find_aexpr eB2 true)) as EQ1...
      rewrite bool_of_aexpr_find_aexpr in EQ1...
    + specialize(EQ (let vals := find_aexpr eB2 true in subst_list (List.nth n vals false) n0 vals false)) as EQ1...
      rewrite bool_of_aexpr_subst_list in EQ1...
      rewrite bool_of_aexpr_find_aexpr in EQ1...
      rewrite nth_subst_list in EQ1...
  - dest_match_step...
    destruct(n0 =? n)eqn:E1...
    + specialize(EQ (find_aexpr eB2 true)) as EQ1...
      rewrite bool_of_aexpr_find_aexpr in EQ1...
      rewrite List.nth_overflow in EQ1...
      apply length_find_aexpr_with_lower...
    +
  Abort.

  Fixpoint aexpr_simpl1 (e:aexpr) : bexpr.
  Proof.
  destruct e as [n|b1 e1 b2 e2|e1 e2|eC e0 b1 e1].
  - apply (false, Some (AVar n)).
  - apply (match b1, e1, b2, e2 with
      | b1, AVar n1, b2, AVar n2 => if Nat.eqb n1 n2
        then if eqb b1 b2 then (b1, Some (AVar n1)) else (bCst false)
        else if Nat.leb n1 n2
          then (false, Some (AAnd b1 (AVar n1) b2 (AVar n2)))
          else (false, Some (AAnd b2 (AVar n2) b1 (AVar n1)))
      | bn, AVar n, be, e
      | be, e, bn, AVar n =>
        (aAnd false false (bn, Some (AVar n))
                    false (subst_bexpr (cons (n, bCst (negb bn)) nil)
                            (bxor be (aexpr_simpl1 e))))
      | b1, e1, b2, e2 =>
        (aAnd false b1 (aexpr_simpl1 e1) b2 (aexpr_simpl1 e2))
      end).
  - apply (aXor false (aexpr_simpl1 e1) (aexpr_simpl1 e2)).
  - apply (match eC with
      | AVar nC => (aITE false false (false, Some (AVar nC))
        false (subst_bexpr (cons (nC, bCst false) nil) (aexpr_simpl1 e0))
        b1    (subst_bexpr (cons (nC, bCst true ) nil) (aexpr_simpl1 e1)))
      | eC => (aITE false false (aexpr_simpl1 eC)
                          false (aexpr_simpl1 e0)
                          b1    (aexpr_simpl1 e1))
      end).
  Defined.

  Definition bexpr_simpl1 (e:bexpr) : bexpr :=
    let (b, opa) := e in
    match opa with
    | Some a => bxor b (aexpr_simpl1 a)
    | None   => (b, None)
    end.

  Lemma subst_bexpr_axor s b e :
    subst_bexpr s (bxor b e) = bxor b (subst_bexpr s e).
  Proof with simplvax2.
  destruct e...
  dest_match...
  Qed.

End VAX.