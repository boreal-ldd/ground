Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplEq.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplSimpl.

Ltac simplab1 := simplsimpl; repeat (progress (autorewrite with simplab1_rewrite in *); simplsimpl).

Inductive AB (A B:Type) : Type :=
| AA : A -> AB A B
| BB : B -> AB A B.

Arguments AA {A B}%type_scope _ .
Arguments BB {A B}%type_scope _ .


Ltac hidden_AB_types :=
match goal with
| X : AB ?A ?B |- _ => idtac X": AB "A B
| H: context[@AA ?A ?B] |- _ => idtac "|- AA" A B
| H: context[@BB ?A ?B] |- _ => idtac "|- BB" A B
| |- context[@AA ?A ?B] => idtac "|- AA" A B
| |- context[@BB ?A ?B] => idtac "|- BB" A B
end.

Definition BA {A B} (x:AB A B) : AB B A :=
  match x with
  | AA y => BB y
  | BB y => AA y
  end.

Definition mapAB {A1 A2 B1 B2} (fA:A1->A2) (fB:B1->B2) :=
  (fun x => match x with AA a => AA(fA a) | BB b => BB(fB b) end).

Definition isAB {A B} (s:bool) (ab:AB A B) : bool :=
  eqb s (match ab with AA _ => true | BB _ => false end).
#[export] Hint Unfold isAB : core.


Lemma mapAB_id_id {A B} : @mapAB A _ B _ id id = id.
Proof with simplprop.
apply functional_extensionality.
lazy...
dest_match.
Qed.

#[export] Hint Rewrite @mapAB_id_id : simplab1_rewrite.

Lemma AA_inj {A B} x y : @AA A B x = AA y <-> x = y.
Proof with simplprop.
split...
solve_by_invert.
Qed.

Lemma BB_inj {A B} x y : @BB A B x = BB y <-> x = y.
Proof with simplprop.
split...
solve_by_invert.
Qed.

Lemma AA_BB {A B} x y : @AA A B x = BB y <-> False.
Proof with simplprop.
split...
solve_by_invert.
Qed.

Lemma BB_AA {A B} x y : @BB A B x = AA y <-> False.
Proof with simplprop.
split...
solve_by_invert.
Qed.

#[export] Hint Rewrite @AA_inj : simplab1_rewrite.
#[export] Hint Rewrite @BB_inj : simplab1_rewrite.
#[export] Hint Rewrite @AA_BB : simplab1_rewrite.
#[export] Hint Rewrite @BB_AA : simplab1_rewrite.

Lemma AB_converge {A B} (a:AB A B) C (b:C) : match a with AA _ => b | BB _ => b end = b.
Proof. dest_match. Qed.

#[export] Hint Rewrite @AB_converge : simplab1_rewrite.

Definition beq_AB {A B} (eqa:@beqt A) (eqb:@beqt B) (x1 x2:AB A B) :=
match x1, x2 with
| AA a1, AA a2 => eqa a1 a2
| BB b1, BB b2 => eqb b1 b2
| _    , _     => false
end.

Lemma beq_AB_iff_true {A B}
  {eqa:@beqt A} (CA:beq_iff_true eqa)
  {eqb:@beqt B} (CB:beq_iff_true eqb) : beq_iff_true (beq_AB eqa eqb).
Proof with simplab1.
intros x y.
destruct x, y; simplab1; try solve_by_invert.
Qed.

Lemma fun_AB_converge A B {C} (c:C):
  (fun x : AB A B =>
          match x with
          | AA _ => c
          | BB _ => c
          end) = (fun _ => c).
Proof with simplab1.
apply functional_extensionality...
Qed.

#[export] Hint Rewrite @fun_AB_converge : simplab1_rewrite.

Lemma fun_fold_isAB_true A B :
  (fun x : AB A B => match x with AA _ => true | BB _ => false end) = isAB true.
Proof. lazy. rewrite fun_fold_ITE_id. reflexivity. Qed.

#[export] Hint Rewrite @fun_fold_isAB_true : simplab1_rewrite.

Lemma fun_fold_isAB_false A B :
  (fun x : AB A B => match x with AA _ => false | BB _ => true end) = isAB false.
Proof. apply functional_extensionality; dest_match. Qed.

#[export] Hint Rewrite @fun_fold_isAB_false : simplab1_rewrite.

Lemma comp_isAB_negb A B b : comp (@isAB A B b) negb = isAB (negb b).
Proof. apply functional_extensionality; compute; dest_match. Qed.

#[export] Hint Rewrite @comp_isAB_negb : simplab1_rewrite.

Lemma comp_isAB_xorb A B b1 b2 : comp (@isAB A B b1) (xorb b2) = isAB (xorb b1 b2).
Proof. apply functional_extensionality; compute; dest_match. Qed.

#[export] Hint Rewrite @comp_isAB_xorb : simplab1_rewrite.

Lemma comp_mapAB_isAB {A A' B B'} f g b :
  comp (@mapAB A A' B B' f g) (isAB b) = isAB b.
Proof with simplab1.
apply functional_extensionality...
lazy...
dest_match...
Qed.

#[export] Hint Rewrite @comp_mapAB_isAB : simplab1_rewrite.

Lemma fun_negb_isAB A B b : (fun x => negb (@isAB A B b x)) = isAB (negb b).
Proof.
apply functional_extensionality.
destruct b, x; reflexivity.
Qed.

#[export] Hint Rewrite @fun_negb_isAB : simplab1_rewrite.

(* $2019-04-18 *)
Lemma rewrite_AB_unit_unit {A} (f:AB unit unit -> A) :
  f = (fun x => match x with AA _ => f(AA tt) | BB _ => f(BB tt)end).
Proof with simplab1.
apply functional_extensionality...
dest_match...
Qed.

(* $2019-04-18 *)
Ltac rewrite_AB_unit_unit_step := match goal with
  | H : context[fun (x:AB unit unit) => @?f x] |- _ =>
    match type of f with
    | _ -> ?A => match A with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of A with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_AB_unit_unit f) in H; simpl in H);
    idtac "normalized :" f
  | |- context[fun (x:AB unit unit) => @?f x] =>
    match type of f with
    | _ -> ?A => match A with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of A with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_AB_unit_unit f); simpl);
    idtac "normalized :" f
  end.


(* $2019-04-18 *)
Lemma rewrite_AB_unitA {B C} (f:AB unit B -> C) :
  f = (fun x => match x with AA _ => f(AA tt) | BB b => f(BB b)end).
Proof with simplab1.
apply functional_extensionality...
dest_match...
Qed.

(* $2019-04-18 *)
Ltac rewrite_AB_unitA_step := match goal with
  | H : context[fun (x:AB unit ?B) => @?f x] |- _ =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_AB_unitA f) in H; simpl in H);
    idtac "normalized :" f
  | |- context[fun (x:AB unit ?B) => @?f x] =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_AB_unitA f); simpl);
    idtac "normalized :" f
  end.

(* $2019-04-18 *)
Ltac rewrite_AB_unit_unit := repeat rewrite_AB_unit_unit_step.
Ltac rewrite_AB_unitA     := repeat rewrite_AB_unitA_step.

Lemma fun_unfold_match_AB {A B C} (f:AB A B -> C) :
  f = (fun x => match x with AA a => f(AA a) | BB b => f(BB b) end).
Proof with simplab1.
apply functional_extensionality...
dest_match...
Qed.

(* $2019-04-18 *)
Lemma rewrite_AB_unit_bool {A} (f:AB unit bool -> A) :
  f = (fun x => match x with
    | AA _ => f(AA tt)
    | BB b => if b then f(BB true) else f(BB false) end).
Proof with simplab1.
apply functional_extensionality...
dest_match...
Qed.

(* $2019-04-23 *)
Ltac rewrite_unfold_mapAB_step := match goal with
  | H : context[fun (x:AB ?A ?B) => @?f x] |- _ =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (fun_unfold_match_AB f) in H; simpl in H);
    idtac "normalized :" f
  | |- context[fun (x:AB ?A ?B) => @?f x] =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (fun_unfold_match_AB f); simpl);
    idtac "normalized :" f
  end.

(* $2019-04-23 *)
Ltac rewrite_unfold_mapAB := repeat rewrite_unfold_mapAB_step.

Definition match_AB {A B C} (fA:A->C) fB (x:AB A B) :=
 match x with
 | AA a => fA a
 | BB b => fB b
 end.

Lemma fold_match_AB_when_mapAB {A A' B B'} fA fB :
  match_AB (fun x => AA (fA x)) (fun x => BB (fB x)) = @mapAB A A' B B' fA fB.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @fold_match_AB_when_mapAB : simplablist1_rewrite.

Lemma fun_fold_match_AB A B C (f:AB A B -> C) :
  f = match_AB (fun x => f(AA x)) (fun x => f(BB x)).
Proof with simplsimpl.
apply functional_extensionality...
compute...
dest_match...
Qed.

Lemma match_AB_true_false A B :
   @match_AB A B _ (fun _ => true) (fun _ => false) = isAB true.
Proof. compute; rewrite_unfold_mapAB; reflexivity. Qed.

#[export] Hint Rewrite match_AB_true_false : simplab1_rewrite.

Lemma match_AB_false_true A B :
   @match_AB A B _ (fun _ => false) (fun _ => true) = isAB false.
Proof. compute; rewrite_unfold_mapAB; reflexivity. Qed.
#[export] Hint Rewrite match_AB_false_true : simplab1_rewrite.

Lemma match_AB_bcst_bcst A B bA bB :
  @match_AB A B _ (fun _ => bA) (fun _ => bB) =
    if eqb bA bB
    then (fun _ => bA)
    else isAB bA.
Proof. compute; apply functional_extensionality; dest_match; auto. Qed.
#[export] Hint Rewrite match_AB_bcst_bcst : simplab1_rewrite.

Lemma match_AB_unit_unit (x:AB unit unit) : match x with AA _ => AA tt | BB _ => BB tt end = x.
Proof. dest_match. Qed.
#[export] Hint Rewrite match_AB_unit_unit : simplab1_rewrite.

Ltac fun_fold_match_AB_step := match goal with
  | H : context[fun (x:AB ?A ?B) => @?f x] |- _ =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (fun_fold_match_AB _ _ _ f) in H; simpl in H);
    idtac "normalized :" f
  | |- context[fun (x:AB ?A ?B) => @?f x] =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (fun_fold_match_AB _ _ _ f); simpl);
    idtac "normalized :" f
  end.

(* $2019-04-23 *)
Ltac fun_fold_match_AB := repeat fun_fold_match_AB_step.

Lemma match_AB_id {A B} (x:AB A B) :
  match x with AA x => AA x | BB x => BB x end = x.
Proof. destruct x; simplsimpl. Qed.
#[export] Hint Rewrite @match_AB_id : simplab1_rewrite.

Lemma fun_match_AB_id {A B} :
  (fun (x:AB A B) => match x with AA x => AA x | BB x => BB x end) = id.
Proof with simplsimpl.
apply functional_extensionality...
apply match_AB_id...
Qed.
#[export] Hint Rewrite @fun_match_AB_id : simplab1_rewrite.

Lemma fun_fold_mapAB {A A' B B'} (f:A->A') (g:B -> B') :
  (fun x => match x with AA x => AA(f x) | BB x => BB(g x) end) = mapAB f g.
Proof. reflexivity. Qed.

#[export] Hint Rewrite @fun_fold_mapAB : simplab1_rewrite.

Lemma fun_facto_match_AB {A B C} (f:AB A B -> C) (x:AB A B) :
  match x with AA a => f(AA a) | BB b => f(BB b) end = f x.
Proof. dest_match. Qed.

#[export] Hint Rewrite @fun_facto_match_AB : simplab1_rewrite.

Ltac simplab := simplab1.

(* Section. To Be Sorted *)

Lemma mapAB_mapAB {A0 A1 A2} {B0 B1 B2} (a01:A0 -> A1) (a12:A1 -> A2) (b01:B0 -> B1) (b12:B1 -> B2) x :
  mapAB a12 b12 (mapAB a01 b01 x) = mapAB (comp a01 a12) (comp b01 b12) x.
Proof. destruct x; simplab. Qed.

Lemma isAB_mapAB {A A' B B'} fA fB b x : isAB b (@mapAB A A' B B' fA fB x) = isAB b x.
Proof. destruct x; reflexivity. Qed.
#[export] Hint Rewrite @isAB_mapAB : simplab1_rewrite.

Lemma opmap_eq_None {A B} f x : @opmap A B f x = None <-> x = None.
Proof. destruct x; simplab. Qed.
#[export] Hint Rewrite @opmap_eq_None : simplab1_rewrite.

Lemma rewrite_match_AB_true_false_eq_true {B} (x:AB unit B) :
  match x with  AA _ => true | BB _ => false end = true <-> x = AA tt.
Proof. dest_match_step; simplab. Qed.
#[export] Hint Rewrite @rewrite_match_AB_true_false_eq_true : simplab1_rewrite.