Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplSimpl.
Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.

Ltac simplablist0 := simpllist1; repeat (progress (autorewrite with simplab1_rewrite in *); simpllist1).

Ltac simplablist1 := simplablist0; repeat (progress (autorewrite with simplablist1_rewrite in *); simplablist0).

Lemma list_nth_error_cons_B_eq_A {A B} a l n b :
  List.nth_error (cons(BB b) l) n = Some(@AA A B a) <->
    1 <= n /\ List.nth_error l (pred n) = Some(AA a).
Proof with simplablist1.
destruct n...
Qed.
#[export] Hint Rewrite @list_nth_error_cons_B_eq_A : simplablist1_rewrite.

Lemma list_nth_error_cons_A_eq_B {A B} a l n b :
  List.nth_error (cons(AA a) l) n = Some(@BB A B b) <->
    1 <= n /\ List.nth_error l (pred n) = Some(BB b).
Proof with simplablist1.
destruct n...
Qed.
#[export] Hint Rewrite @list_nth_error_cons_A_eq_B : simplablist1_rewrite.

Lemma nth_error_cons_B_nil {B} (x:B) n :
  List.nth_error (BB x :: nil) n = Some (AA tt) <-> False.
Proof with simplablist1.
destruct n...
Qed.
#[export] Hint Rewrite @nth_error_cons_B_nil : simplablist1_rewrite.

Fixpoint filter_AA {A B C} (f:A->AB B C) (l:list A) : list B :=
  match l with
  | nil => nil
  | cons l0 l => match f l0 with
    | AA a0 => cons a0(filter_AA f l)
    | BB b0 => filter_AA f l
    end
  end.

Definition filter_BB {A B C} (f:A->AB B C) (l:list A) : list C
  := filter_AA (comp f BA) l.

Lemma list_count_B_0_vs_make_A {B} (l:list(AB unit B)) :
  list_count (isAB false) l = 0 <-> l = make(length l) (AA tt).
Proof with simplablist1.
unfold make.
induction l...
dest_match...
Qed.

Lemma list_decount_B_make_A {A B} (a:A) n i :
  list_decount (@isAB A B false) (make n (AA a)) i = i.
Proof with simplablist1.
unfold make.
generalize dependent n.
induction i; destruct n...
Qed.

Lemma list_count_isAB_plus {A B} l :
  list_count (isAB false) l + list_count(@isAB A B true) l = length l.
Proof with simplsimpl1.
induction l...
dest_match...
Qed.

Lemma nth_error_minus_list_count_D_with_le {A B lu} b :
  list_count (@isAB A B b) lu = length lu - list_count (isAB (negb b)) lu.
Proof with simplsimpl2.
specialize(list_count_isAB_plus lu)...
destruct b; simpl; lia.
Qed.

Lemma list_forallb_A_vs_make_A {B} (l:list(AB unit B)) :
  List.forallb (isAB true) l = true <-> l = make(length l) (AA tt).
Proof with simplablist1.
unfold make.
induction l...
dest_match...
Qed.

Lemma list_forallb_B_vs_make_B {A} (l:list(AB A unit)) :
  List.forallb (isAB false) l = true <-> l = make (length l) (BB tt).
Proof with simplablist1.
unfold make.
induction l...
dest_match...
Qed.

Lemma list_count_A_until_rm_trail_A {B} l i :
  list_count_until (fun _ => true) (isAB true) (rm_trail (isAB true) l) i =
    if S i <=? length (rm_trail (isAB true) l)
      then list_decount (@isAB unit B false) l i
      else list_count(isAB true)(rm_trail (isAB true) l).
Proof with simplablist1.
generalize dependent l.
induction i; destruct l...
- dest_match.
- specialize(IHi l).
  destruct(List.forallb (isAB true) l) eqn:E0...
  + dest_match...
  + gen_dest_match simplablist1; simpl in *...
Qed.

Lemma rewrite_using_rm_trail {T} (l:list T) p p0 (Hp: forall x, p x = true <-> x = p0) :
  l = List.app (rm_trail p l) (make (length l - length (rm_trail p l)) p0).
Proof with simpllist1.
rewrite list_eq_rewrite...
rewrite list_nth_error_rm_trail...
symmetry...
dest_match_step...
dest_match_step...
specialize(nth_error_vs_rm_trail _ _ _ D p)...
apply list_nth_error_eq_Some_implies_lt_length in D...
destruct(p t)eqn:E...
rewrite Hp in E... 
lia.
Qed.

Lemma rm_trail_A_eq_rewrite B l1 l2 :
  rm_trail(@isAB unit B true)l1 = rm_trail(isAB true)l2 <->
    l1 = List.app (rm_trail(isAB true)l1) (make(length l1 - length(rm_trail(isAB true)l1))(AA tt)) /\
    l2 = List.app (rm_trail(isAB true)l1) (make(length l2 - length(rm_trail(isAB true)l1))(AA tt)).
Proof with simplablist1.
rewrite <- rewrite_using_rm_trail...
split; intro H...
- rewrite H.
  rewrite <- rewrite_using_rm_trail...
- specialize (f_equal (fun l => rm_trail (isAB true) l) H)...
  rewrite rm_trail_app in H0...
  rewrite rm_trail_rm_trail_same in H0...
Qed.

Lemma rm_trail_elim_with_eq_length {B} l1 l2 (H:length l1 = length l2) :
  rm_trail (@isAB unit B true) l1 = rm_trail (isAB true) l2 <-> l1 = l2.
Proof with simplablist1.
split...
rewrite rm_trail_A_eq_rewrite in H0...
assert(HH:=H).
rewrite H0, H1 in HH...
repeat rewrite List.app_length in HH...
rewrite list_eq_rewrite...
rewrite H0, H1.
repeat rewrite list_nth_error_app.
repeat rewrite nth_error_make.
dest_match...
Qed.

Lemma list_count_until_isAB_case0 {A B} (lAB:list(AB A B)) i :
  list_count_until (fun _ => true) (isAB false) lAB i +
  list_count_until (fun _ => true) (isAB true) lAB i = min i (length lAB).
Proof with simplablist1.
generalize dependent i.
induction lAB...
dest_match...
Qed.

Lemma list_count_until_isAB_case0_rfalse {A B} lAB i :
  list_count_until (fun _ => true) (@isAB A B false) lAB i =
    (min i (length lAB)) - (list_count_until (fun _ => true) (isAB true) lAB i).
Proof with simplablist1.
specialize(list_count_until_isAB_case0 lAB i).
lia.
Qed.

Lemma list_count_until_isAB_case0_rtrue {A B} lAB i :
  list_count_until (fun _ => true) (@isAB A B true) lAB i =
    (min i (length lAB)) - (list_count_until (fun _ => true) (isAB false) lAB i).
Proof with simplablist1.
specialize(list_count_until_isAB_case0 lAB i).
lia.
Qed.

Lemma list_count_until_isAB_plus_l {A B} pU l n:
list_count_until pU (@isAB A B false) l n + list_count_until pU (isAB true) l n =
  list_count_until pU (fun _ => true) l n.
Proof with simplablist1.
generalize dependent n.
induction l...
dest_match...
Qed.

Lemma list_find_last_index_true {A} l :
  @list_find_last_index A (fun _ => true) l = if 0 <? length l then Some(pred(length l)) else None.
Proof with simplablist1.
induction l...
dest_match...
Qed.
#[export] Hint Rewrite @list_find_last_index_true : simpllist1_rewrite.

Lemma list_count_lemma0B {A B} (l:list(AB A B)) n :
  list_count (isAB false)
    (List.firstn (n + list_count_until (isAB true) (isAB false) l n) l) =
      list_count_until (isAB true) (isAB false) l n.
Proof with simplablist1.
generalize dependent n.
induction l...
destruct a...
- destruct n...
- specialize(IHl n).
  destruct n...
Qed.

Lemma list_count_lemma0A {A B} (l:list(AB A B)) n :
  list_count (isAB true)
    (List.firstn (n + list_count_until (isAB true) (isAB false) l n) l) =
      min n (list_count (isAB true) l).
Proof with simplablist1.
generalize dependent n.
induction l...
destruct a...
- destruct n...
- specialize(IHl n).
  destruct n...
Qed.

Lemma list_count_until_A_B_small {A B} (l:list(AB A B)) n :
  n + list_count_until (isAB true) (isAB false) l n <= length l <->
  n + list_count_until (isAB true) (isAB false) l n =
    list_count_until (isAB true) (fun _ => true) l n /\
    n <= list_count(isAB true) l.
Proof with simplablist1.
generalize dependent n.
induction l...
destruct a...
+ destruct n...
+ specialize(IHl n)...
  destruct n...
Qed.

Lemma rewrite_list_count_until_A_B_with_le_list_count {A B}
  n (l:list(AB A B)) (H:n <= list_count (isAB true) l) :
    n + list_count_until (isAB true) (isAB false) l n =
      list_count_until (isAB true) (fun _ => true) l n.
Proof with simplablist1.
generalize dependent n.
induction l...
destruct a...
- destruct n...
- specialize(IHl n)...
  destruct n...
Qed.

Lemma list_decount_vs_minus_list_count {A B} (l:list(AB A B)) i (LE: length l <= i) :
  list_decount (isAB false) l i = i - list_count(isAB false) l.
Proof with simplablist1.
rewrite list_decount_as_list_count_until.
rewrite list_count_until_with_length_le...
Qed.

Lemma list_decount_vs_list_count_case1 {B} l i (H: length (rm_trail (isAB true) l) <= i) :
  list_decount (isAB false) l i = i - list_count (@isAB unit B false) l.
Proof with simplablist1.
specialize(list_decount_vs_minus_list_count _ _ H) as HH...
rewrite list_count_rm_trail_with_disjoint in HH...
rewrite list_decount_rm_trail_with_disjoint in HH...
Qed.

(* Section 2. uclist *)

(* Section 3. ablist [WIP] *)

Definition ablist {A B:Type} : nat -> nat -> nat -> Type :=
  (fun nA nB m => {l:list (AB A B) &
    (List.length l =? m) &&
    (list_count (isAB true) l =? nA) &&
    (list_count (isAB false) l =? nB) = true}).

Lemma PI_bool {b1 b2:bool} (p1 p2:b1 = b2) : p1 = p2.
Proof.
destruct b1, b2; simplbool;
  dependent destruction p1;
  dependent destruction p2; reflexivity.
Qed.

Lemma ablist_eq_inj {A B:Type} {nA nB m} (abl1 abl2:@ablist A B nA nB m) :
  abl1 = abl2 <-> projT1 abl1 = projT1 abl2.
Proof with simplsimpl1.
destruct abl1, abl2...
split...
inv H...
specialize(PI_bool e e0)...
Qed.