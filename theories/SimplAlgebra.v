Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.
Require Import FunInd.
Require Import Recdef.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_Logic.

Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

(* Section. [MOVEME] *)

(* [MOVEME] *)
Lemma eq_iff_eq_false : forall b1 b2 : bool, b1 = b2 <-> (b1 = false <-> b2 = false).
Proof. dest_bool. Qed.

(* [MOVEME] *)
Lemma break_sym_eq_bool {A B} (f:A -> A -> B) :
  (forall x y, f x y = f y x) <-> (forall x y z, f x y = z -> f y x = z).
Proof. firstorder; simplprop. Qed.

(* [MOVEME] *)
Lemma break_sym_iff {A} (R:A -> A -> Prop) :
  (forall x y, R x y <-> R y x) <-> (forall x y, R x y -> R y x).
Proof. firstorder. Qed.

Ltac rewrite_unop :=
match goal with
| |- context[unop ?x ?H] =>
  let e := fresh "e" in
  let E := fresh "E" in
  (remember (unop x H) as e eqn:E);
  symmetry in E; rewrite unop_rewrite in E
end.

(* Section 1. Monoid *)

Record monoid_T (A:Type) : Type := {
  monoid_id : A;
  monoid_comp : A -> A -> A;
}.

Record monoid_P {A:Type} (C:monoid_T A) := {
  monoid_id0 := monoid_id _ C;
  monoid_comp0 := monoid_comp _ C;
  monoid_assoc : forall (x y z:A), monoid_comp0 (monoid_comp0 x y) z = monoid_comp0 x (monoid_comp0 y z);
  monoid_id_comp : forall y, monoid_comp0 monoid_id0 y = y;
  monoid_comp_id : forall x, monoid_comp0 x monoid_id0 = x;
}.

Definition monoid (A:Type) : Type := { M : monoid_T A | monoid_P M }.

Definition idM {A} (M:monoid A) : A := monoid_id _ (proj1_sig M).
Definition compM {A} (M:monoid A) : A -> A -> A := monoid_comp _ (proj1_sig M).

Lemma idM_is_unique {A} (M:monoid A) (e:A) : (forall x, compM M x e = x) <-> e = idM M.
Proof with curry1.
destruct M...
destruct x, m...
unfold compM, idM...
split...
rewrite <- H, monoid_id_comp0...
Qed.

Lemma monoid_eq_weak {A} (M1 M2:monoid A) : M1 = M2 <-> idM M1 = idM M2 /\ compM M1 = compM M2.
Proof with curry1.
split...
destruct M1 as [T1 P1], M2 as [T2 P2]...
unfold idM, compM in *...
destruct T1, T2...
Qed.

Lemma monoid_eq {A} (M1 M2:monoid A) : M1 = M2 <-> compM M1 = compM M2.
Proof with curry1.
rewrite monoid_eq_weak...
rewrite <- (idM_is_unique M2)...
destruct M1 as [T1 P1], M2 as [T2 P2]...
unfold compM, idM in *...
destruct T1, T2, P1, P2...
Qed.


Record sig_monoid_P {A} (CA:monoid_T A) (P:A -> Prop)  : Prop := {
  sig_monoid_P_idP : P(monoid_id _ CA);
  sig_monoid_P_compP : forall x y, P x -> P y -> P(monoid_comp _ CA x y);
}.

Definition sig_monoid_P_id {A CA P} (SCP:@sig_monoid_P A CA P) : sig P :=
  (exist _ (monoid_id _ CA) (sig_monoid_P_idP _ _ SCP)).

Definition sig_monoid_P_comp_lemma0 {A CA P} (SCP:@sig_monoid_P A CA P) (x y:sig P) :
  P (monoid_comp A CA (proj1_sig x) (proj1_sig y)).
Proof with curry1.
destruct x, y...
apply (sig_monoid_P_compP _ _ SCP)...
Qed.

Definition sig_monoid_P_comp {A CA P} (SCP:@sig_monoid_P A CA P) : sig P -> sig P -> sig P :=
  (fun x y => exist _ (monoid_comp _ CA (proj1_sig x) (proj1_sig y)) (sig_monoid_P_comp_lemma0 SCP x y)).

Definition monoid_T_of_sig_monoid_P {A MT P} (SMP:@sig_monoid_P A MT P) : monoid_T (sig P) :=
  {| monoid_id := sig_monoid_P_id SMP; monoid_comp := sig_monoid_P_comp SMP |}.

Lemma monoid_P_of_sig_monoid_P {A} (M:monoid A) {P} (SMP:@sig_monoid_P A (proj1_sig M) P) :
  monoid_P (monoid_T_of_sig_monoid_P SMP).
Proof with curry1.
destruct SMP...
destruct M as [MT MP]...
destruct MT, MP...
unfold monoid_T_of_sig_monoid_P, sig_monoid_P_id, sig_monoid_P_comp...
constructor...
Qed.

Definition monoid_of_sig_monoid_P {A} (M:monoid A) {P} (SMP:@sig_monoid_P A (proj1_sig M) P) : monoid (sig P) :=
  exist _ (monoid_T_of_sig_monoid_P SMP) (monoid_P_of_sig_monoid_P M SMP).

Record monoid_morphism_P {A B:Type} (f:A -> B) (MA:monoid A) (MB:monoid B) : Type := {
  monoid_morph_id : f (idM MA) = idM MB;
  monoid_morph_comp : forall (x y:A), f (compM MA x y) = compM MB (f x) (f y);
}.

Definition monoid_T_endo (A:Type) : monoid_T (A -> A) := {| monoid_id := id; monoid_comp := comp |}.

Lemma monoid_P_endo (A:Type) : monoid_P (monoid_T_endo A).
Proof. constructor; curry1. Qed.

Definition monoid_endo (A:Type) : monoid (A -> A) := exist _ (monoid_T_endo A) (monoid_P_endo A).

(* Section 2. Group *)

Record group_T (A:Type) := {
  group_monoid : monoid_T A;
  group_zero := monoid_id _ group_monoid;
  group_plus := monoid_comp _ group_monoid;
  group_neg : A -> A;
}.

Record group_P {A:Type} (G:group_T A) := {
  group_monoid0 := group_monoid _ G;
  group_monoidP : monoid_P group_monoid0;
  group_zero0 := group_zero _ G;
  group_plus0 := group_plus _ G;
  group_neg0 := group_neg _ G;
  group_neg_comp : forall x, group_plus0 (group_neg0 x) x = group_zero0;
  group_comp_neg : forall x, group_plus0 x (group_neg0 x) = group_zero0;
}.

Definition group (A:Type) : Type := { G : group_T A | group_P G }.


Definition monoG {A} (G:group A) : monoid A :=
  exist _ (group_monoid _ (proj1_sig G)) (group_monoidP _ (proj2_sig G)).

Definition zeroG {A} (G:group A) : A := group_zero _ (proj1_sig G).
Definition plusG {A} (G:group A) : A -> A -> A := group_plus _ (proj1_sig G).
Definition negG {A} (G:group A) : A -> A := group_neg _ (proj1_sig G).
Definition minusG {A} (G:group A) : A -> A -> A := fun x y => plusG G x (negG G y).

Record group_morphism_P {A B:Type} (f:A -> B) (GA:group A) (GB:group B) := {
  grp_morph_monoid : @monoid_morphism_P A B f (monoG GA) (monoG GB);
  grp_morph_neg : forall x, f (negG GA x) = negG GB (f x);
}.

Lemma plusG_eq_shift {A} (G:group A) x y z : plusG G x y = z <-> y = plusG G (negG G x) z.
Proof with curry1.
destruct G as [GT GP]...
unfold plusG, negG...
destruct GT...
destruct group_monoid1...
destruct GP...
unfold group_zero, group_plus in *...
unfold group_monoid1 in *...
clear group_monoid1.
destruct group_monoidP0...
unfold monoid_id2, monoid_comp2 in *...
unfold group_zero1, group_plus1, group_neg2 in *...
split...
- rewrite <- monoid_assoc0.
  rewrite group_neg_comp0...
- rewrite <- monoid_assoc0.
  rewrite group_comp_neg0...
Qed.

Lemma plusG_zeroG_l {A} (G:group A) x : plusG G (zeroG G) x = x.
Proof with curry1.
destruct G...
destruct g...
destruct group_monoidP0...
Qed.
#[export] Hint Rewrite @plusG_zeroG_l : curry1_rewrite.

Lemma plusG_zeroG_r {A} (G:group A) x : plusG G x (zeroG G) = x.
Proof with curry1.
destruct G...
destruct g...
destruct group_monoidP0...
Qed.
#[export] Hint Rewrite @plusG_zeroG_r : curry1_rewrite.

Lemma plusG_assoc {A} (G:group A) x y z : plusG G (plusG G x y) z = plusG G x (plusG G y z).
Proof with curry1.
destruct G...
destruct g...
destruct group_monoidP0...
Qed.

Lemma plusG_negG_same_l {A} (G:group A) x : plusG G (negG G x) x = zeroG G.
Proof with curry1.
destruct G...
destruct g...
Qed.
#[export] Hint Rewrite @plusG_negG_same_l : curry1_rewrite.

Lemma plusG_negG_same_r {A} (G:group A) x : plusG G x (negG G x) = zeroG G.
Proof with curry1.
destruct G...
destruct g...
Qed.
#[export] Hint Rewrite @plusG_negG_same_r : curry1_rewrite.

Lemma group_P_group_plus_eq_both_same_l {A} (G:group A) x y z : plusG G x y = plusG G x z <-> y = z.
Proof with curry1.
rewrite plusG_eq_shift...
rewrite <- plusG_assoc...
Qed.
#[export] Hint Rewrite @group_P_group_plus_eq_both_same_l : curry1_rewrite.

Lemma plusG_r_eq_shift {A} (G:group A) x y z : plusG G x y = z <-> x = plusG G z (negG G y).
Proof with curry1.
destruct G as [GT GP]...
unfold plusG, negG...
destruct GT...
destruct group_monoid1...
destruct GP...
unfold group_zero, group_plus in *...
unfold group_monoid1 in *...
clear group_monoid1.
destruct group_monoidP0...
unfold monoid_id2, monoid_comp2 in *...
unfold group_zero1, group_plus1, group_neg2 in *...
split...
- rewrite monoid_assoc0.
  rewrite group_comp_neg0...
- rewrite monoid_assoc0.
  rewrite group_neg_comp0...
Qed.

Lemma plusG_eq_same_both_r {A} (G:group A) x y z : plusG G y x = plusG G z x <-> y = z.
Proof with curry1.
rewrite plusG_r_eq_shift...
rewrite plusG_assoc...
Qed.
#[export] Hint Rewrite @plusG_eq_same_both_r : curry1_rewrite.

Lemma group_eq_weak {A} (G1 G2:group A) : G1 = G2 <-> monoG G1 = monoG G2 /\ negG G1 = negG G2.
Proof with curry1.
split...
destruct G1 as [T1 P1], G2 as [T2 P2]...
unfold monoG, negG in *...
destruct T1, T2...
Qed.

Ltac unfold_all_let := repeat(match goal with x := _ |- _ => subst x end).

Lemma group_eq {A} (G1 G2: group A) : G1 = G2 <-> plusG G1 = plusG G2.
Proof with curry1.
split...
rewrite group_eq_weak...
assert(monoG G1 = monoG G2); [ rewrite monoid_eq | ]...
apply functional_extensionality; intro x.
apply(@eq_trans _ _ (plusG G1 (negG G1 x) (plusG G1 x (negG G2 x)))).
- rewrite H...
- rewrite <- plusG_assoc...
Qed.

Lemma negG_involutive {A} (G:group A) (x:A) : negG G (negG G x) = x.
Proof with curry1.
apply(@eq_trans _ _ (plusG G (negG G (negG G x)) (plusG G (negG G x) x))).
- curry1.
- rewrite <- plusG_assoc...
Qed.
#[export] Hint Rewrite @negG_involutive : curry1_rewrite.

Lemma negG_injective {A:Type} (G:group A) x y : negG G x = negG G y <-> x = y.
Proof with curry1.
split...
specialize(f_equal (negG G) H) as HH...
Qed.
#[export] Hint Rewrite @negG_injective : curry1_rewrite.

Lemma minusG_eq_0 {T:Type} (G:group T) x y : minusG G x y = zeroG G <-> x = y.
Proof. unfold minusG; rewrite plusG_eq_shift; curry1. Qed.
#[export] Hint Rewrite @minusG_eq_0 : curry1_rewrite.

Lemma minusG_diag {T:Type} (G:group T) x : minusG G x x = zeroG G.
Proof. curry1. Qed.
#[export] Hint Rewrite @minusG_diag : curry1_rewrite.

Lemma negG_eq_shift {A} (G:group A) x y : negG G x = y <-> plusG G x y = zeroG G.
Proof with curry1.
rewrite (eq_sym_iff _ (zeroG G)).
rewrite <- (negG_involutive G x).
rewrite <- plusG_eq_shift...
Qed.

Lemma plusG_negG_same_left_assoc {A} (G:group A) x y : plusG G x (plusG G (negG G x) y) = y.
Proof. rewrite plusG_eq_shift; curry1. Qed.
#[export] Hint Rewrite @plusG_negG_same_left_assoc : curry1_rewrite.

Lemma negG_plusG {A} (G:group A) x y : negG G (plusG G x y) = plusG G (negG G y) (negG G x).
Proof with curry1.
rewrite negG_eq_shift.
repeat rewrite plusG_assoc...
Qed.

Lemma fold_minusG {A} (G:group A) x y : plusG G x (negG G y) = minusG G x y.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @fold_minusG : curry1_rewrite.

Lemma fold_negG_minusG {A} (G:group A) x y : plusG G (negG G x) y = negG G (minusG G (negG G y) (negG G x)).
Proof. unfold minusG; rewrite negG_plusG; repeat rewrite negG_involutive. curry1.  Qed.
#[export] Hint Rewrite @fold_negG_minusG : curry1_rewrite.

(* Quotienting A Type with Equivalence Relation *)

Definition projection_P {A:Type} (p:A -> A) : Prop := forall (a:A), p(p a) = p a.
Definition projected_P {A:Type} (p:A -> A) (p':A -> bool) : Prop := (forall a, p a = a <-> p' a = true).

Definition projected_of_projection {A:Type} (beq:A -> A -> bool) (p:A -> A) : A -> bool := fun a => beq (p a) a.

Lemma projected_of_projection_spec0 {A:Type} beq (EE:@beq_iff_true A beq) p :
  projected_P p (projected_of_projection  beq p).
Proof with curry1.
unfold projected_P, projected_of_projection...
rewrite (EE _)...
Qed.

Lemma projection_id {A:Type} : projection_P (@id A).
Proof. unfold projection_P; simplprop. Qed.

Lemma projected_id_true {A:Type} : projected_P (@id A) (fun _ => true).
Proof. unfold projected_P; simplprop. Qed.

Record equivalent_P {A:Type} (r:A -> A -> bool) : Type := {
  equiv_refl  : forall a, r a a = true;
  equiv_sym   : forall a b, r a b = r b a;
  equiv_trans : forall a b c, r a b = true -> r b c = true -> r a c = true;
}.

Lemma equivalent_P_beq_iff_true {A} beq (EE:beq_iff_true beq) : @equivalent_P A beq.
Proof with curry1.
constructor...
- rewrite (EE _)...
- rewrite bool_eq_iff_iff.
  repeat rewrite (EE _).
  curry1.
- rewrite (EE _) in H...
Qed.

Definition map_both {A B} (re:B -> B -> bool) (p:A -> B) : A -> A -> bool := fun x y => re (p x) (p y).

Lemma equivalent_P_map_both {A B} re (RE:equivalent_P re) (f:A -> B) :
  equivalent_P (map_both re f).
Proof with curry1.
unfold map_both.
destruct RE as [RR RS RT].
constructor...
apply(RT _ _ _ H H0).
Qed.

Definition equivalent_iff_projection {A} (re:A -> A -> bool) (p:A -> A) : Prop :=
  forall x y, re x y = true <-> p x = p y.

Definition equivalent_iff_projection_weak {A} (re:A -> A -> bool) (p:A -> A) : Prop :=
  forall x y, re x y = re (p x) y.

Lemma equivalent_iff_projection_implies_equivalent_iff_projection_weak
  {A} (re:A -> A -> bool) (RE:equivalent_P re) (p:A -> A) (HP:projection_P p) :
  equivalent_iff_projection re p -> equivalent_iff_projection_weak re p.
Proof with curry1.
unfold equivalent_iff_projection, equivalent_iff_projection_weak...
rewrite bool_eq_iff_iff.
repeat rewrite H.
rewrite (HP _)...
Qed.

Lemma equivalent_iff_projection_beq_is_id
  {A} (re:A -> A -> bool) (RE:beq_iff_true re) (p:A -> A) (HP:projection_P p) :
    equivalent_iff_projection re p -> p = id.
Proof with curry1.
intros.
unfold equivalent_iff_projection in H...
apply functional_extensionality...
specialize(H (p x) x).
rewrite (RE _) in H...
rewrite (HP _) in H...
Qed.

Definition equivalent_iff_projection_weak_alternative {A} (re:A -> A -> bool) (p:A -> A) : Prop :=
  forall x, re x (p x) = true.

Lemma equivalent_iff_projection_weak_implies_equivalent_iff_projection_weak_alternative
  {A} (re:A -> A -> bool) (RE:equivalent_P re) (p:A -> A) (HP:projection_P p):
    equivalent_iff_projection_weak re p <-> equivalent_iff_projection_weak_alternative re p.
Proof with curry1.
unfold equivalent_iff_projection_weak, equivalent_iff_projection_weak_alternative.
destruct RE as [RR RS RT]...
split...
- specialize(H x (p x))...
  rewrite RR in H...
- rewrite bool_eq_iff_iff...
  specialize(H x) as Hx...
  split...
  + apply (RT _ x _)...
    rewrite RS...
  + apply (RT _ (p x) _)...
Qed.

Definition equivalent_iff_projection_alternative {A} (re:A -> A -> bool) (p:A -> A) : Prop :=
  (forall x, re x (p x) = true) /\ (forall x y, re (p x) (p y) = true <-> p x = p y).

Lemma strong_transitivity_equivalent_P {A} re (RE:@equivalent_P A re) :
  forall a b, re a b = true -> forall c, re a c = re b c.
Proof with curry1.
destruct RE as [RR RS RT]...
rewrite bool_eq_iff_iff.
split...
- apply (RT _ a _)...
  rewrite RS...
- apply (RT _ b _)...
Qed.

Lemma equivalent_iff_projection_iff_alternative
  {A} (re:A -> A -> bool) (RE:equivalent_P re) (p:A -> A) (HP:projection_P p):
    equivalent_iff_projection re p <-> equivalent_iff_projection_alternative re p.
Proof with curry1.
unfold equivalent_iff_projection, equivalent_iff_projection_alternative...
split...
- split...
  + rewrite H, HP...
  + rewrite H.
    repeat rewrite HP...
- rewrite <- H0.
  specialize(strong_transitivity_equivalent_P _ RE) as RT'.
  destruct RE as [RR RS RT].
  specialize(H x) as Hx.
  specialize(H y) as Hy.
  rewrite (RT' _ _ Hx).
  rewrite RS.
  rewrite (RT' _ _ Hy).
  rewrite RS...
Qed.

Definition equivalent_morphism {A} (rA:A -> A -> bool) {B} (rB:B -> B -> bool) (f:A -> B) : Prop :=
  forall x y, rA x y = true -> rB (f x) (f y) = true.

Definition projection_using_projected_P {A} (p:A -> A) (p':A -> bool) : Prop :=
  (forall x, p' x = true -> p x = x) /\ (forall x, p'(p x) = true).

Lemma projection_P_using_projection_using_projected_P {A} p p' :
  @projection_using_projected_P A p p' <-> projection_P p /\ projected_P p p'.
Proof with curry1.
unfold projection_using_projected_P, projection_P, projected_P...
split...
- split...
  split...
  rewrite <- H1...
- split...
  + rewrite H0...
  + rewrite <- H0, H...
Qed.

Definition projected_using_equivalent_P {A} (re:A -> A -> bool) (p':A -> bool) : Prop :=
  (equivalent_P re) /\
  (forall (x:A), exists (y:A), re x y = true /\ p' y = true) /\
  (forall (x y:A), re x y = true -> p' x = true -> p' y = true -> x = y).

Lemma unique_equivalent_iff_projection {A} p (HP:@projection_P A p)
  re1 (R1:equivalent_iff_projection re1 p) re2 (R2:equivalent_iff_projection re2 p) : re1 = re2.
Proof with curry1.
apply functional_extensionality; intro x.
apply functional_extensionality; intro y.
rewrite bool_eq_iff_iff.
rewrite (R1 _ _), (R2 _ _)...
Qed.

Lemma equivalent_iff_projection_map_both_beq_projection
  {A} p (HP:@projection_P A p) beq (HE:@beq_iff_true A beq) :
  equivalent_iff_projection (map_both beq p) p.
Proof. unfold equivalent_iff_projection, map_both; curry1. Qed.

(* Quotient Set A / Group B (QSG) *)

Record QSG_T (A B:Type) := { QSG_groupB : group_T B; QSG_semB : B -> A -> A }.

Record QSG_P {A B:Type} (Q:QSG_T A B) := {
  QSG_groupB0 := QSG_groupB _ _ Q;
  QSG_semB0 := QSG_semB _ _ Q;
  QSG_groupBP : group_P QSG_groupB0 ;
  QSG_groupB1 := ((exist _ QSG_groupB0 QSG_groupBP) : group B);
  QSG_morphBA : monoid_morphism_P QSG_semB0 (monoG QSG_groupB1) (monoid_endo A);
}.

Definition QSG (A B:Type) := { Q : QSG_T A B | QSG_P Q }.

Definition groupB {A B} (Q:QSG A B) : group B := QSG_groupB1 _ (proj2_sig Q).
Definition semB {A B} (Q:QSG A B) : B -> A -> A := QSG_semB _ _ (proj1_sig Q).

Lemma monoid_morphism_P_monoid_P_endo_comp {A B f} {MA:monoid A} (CM:monoid_morphism_P f MA (monoid_endo B)) :
  forall a0 a1 b, f a0 (f a1 b) = f (compM MA a1 a0) b.
Proof with curry1.
destruct CM...
rewrite monoid_morph_comp0...
Qed.

Lemma semB_semB {A B} (Q:QSG A B) b1 b2 a :
  semB Q b2 (semB Q b1 a) = semB Q (plusG (groupB Q) b1 b2) a.
Proof with curry1.
destruct Q...
unfold semB...
destruct q...
rewrite (monoid_morphism_P_monoid_P_endo_comp QSG_morphBA0)...
Qed.

Definition QSG_Similar_T (A B:Type) :Type := A -> A -> option B.

Definition QSG_Equiv {A B:Type} (similar:QSG_Similar_T A B) : A -> A -> bool :=
  (fun a1 a2 => isSome(similar a1 a2)).

Definition QSG_Similar_P {A B} (Q:QSG A B) (similar : QSG_Similar_T A B) : Prop :=
  forall a1 a2 r, similar a1 a2 = r <->
      match r with
      | Some b12 => semB Q b12 a1 = a2
      | None => forall b12, semB Q b12 a1 <> a2
      end.

Definition QSG_Similar {A B} (qsg:QSG A B) : Type := { S : QSG_Similar_T A B | QSG_Similar_P qsg S }.

Lemma QSG_Equiv_eq_false {A B} (Q:QSG A B) (S:QSG_Similar Q) a1 a2 :
  QSG_Equiv (proj1_sig S) a1 a2 = false <-> forall b12, semB Q b12 a1 <> a2.
Proof with curry1.
unfold QSG_Equiv.
destruct S as [similar SP]...
rewrite (SP _)...
Qed.

Lemma semB_zeroG {A B} (Q:QSG A B) a : semB Q (zeroG (groupB Q)) a = a.
Proof with curry1.
destruct Q...
unfold semB...
destruct q...
destruct QSG_morphBA0...
unfold groupB, zeroG...
unfold QSG_groupB0...
unfold QSG_semB1, monoG, QSG_groupB3 in monoid_morph_id0...
unfold idM in *...
unfold group_zero...
unfold QSG_groupB2 in *...
rewrite monoid_morph_id0...
Qed.
#[export] Hint Rewrite @semB_zeroG : curry1_rewrite.


Lemma QSG_Similar_eq_None_comm {A B} (Q:QSG A B) (S:QSG_Similar Q) x y :
  proj1_sig S x y = None <-> proj1_sig S y x = None.
Proof with curry1.
generalize dependent y.
generalize dependent x.
rewrite break_sym_iff...
destruct S as [similar SP]...
rewrite (SP _) in H.
destruct(similar y x)eqn:E0...
specialize(H(negG(groupB Q) b))...
rewrite (SP _) in E0...
rewrite semB_semB in H...
Qed.

Lemma equivalent_P_QSG_equiv {A B} (Q:QSG A B) (S:QSG_Similar Q) :
  equivalent_P (QSG_Equiv (proj1_sig S)).
Proof with curry1.
constructor...
- apply reverse_bool_eq...
  rewrite QSG_Equiv_eq_false in H.
  specialize(H(zeroG (groupB Q)))...
- rewrite eq_iff_eq_false...
  unfold QSG_Equiv...
  apply QSG_Similar_eq_None_comm.
- apply reverse_bool_eq...
  rewrite QSG_Equiv_eq_false in H1.
  destruct S as [similar SP]...
  unfold QSG_Equiv in *...
  destruct(similar a b)eqn:E0...
  destruct(similar b c)eqn:E1...
  rewrite (SP _) in E0.
  rewrite (SP _) in E1.
  specialize(H1(plusG (groupB Q) b0 b1)).
  rewrite <- semB_semB in H1...
Qed.

Definition QSG_Similar_vs_projection_P
  {A B} (Q:QSG A B) (S:QSG_Similar Q) p (HP:@projection_P A p) :=
    equivalent_iff_projection (QSG_Equiv (proj1_sig S)) p.

Record QSG_Selection_P {A B:Type} {Q:@QSG A B} (S:QSG_Similar Q) (p:A -> A) := {
  QSG_projP : projection_P p;
  QSG_projPE : equivalent_iff_projection (QSG_Equiv (proj1_sig S)) p;
}.

Definition QSG_Selection {A B Q} (S:@QSG_Similar A B Q) := { p : A -> A | QSG_Selection_P S p }.

Lemma coproj_of_QSG_Selection_lemma0 {A B Q S} (P:@QSG_Selection A B Q S) :
  let QSG_similar0 : A -> A -> option B := proj1_sig S in
  let QSG_proj0 : A -> A := proj1_sig P in
  forall a, QSG_similar0 (QSG_proj0 a) a <> None.
Proof with curry1.
simpl...
specialize(equivalent_P_QSG_equiv Q S).
destruct P as [proj PP]...
destruct PP...
rewrite equivalent_iff_projection_iff_alternative in QSG_projPE0...
unfold equivalent_iff_projection_alternative in *...
specialize(H1 a)...
unfold QSG_Equiv in H1...
rewrite QSG_Similar_eq_None_comm in H...
Qed.

Definition coproj_of_QSG_Selection_P {A B Q S} (P:@QSG_Selection A B Q S) (a:A) : B :=
  let QSG_similar0 := proj1_sig S in
  let QSG_proj0 := proj1_sig P in
    unop (QSG_similar0 (QSG_proj0 a) a) (coproj_of_QSG_Selection_lemma0 P a).

Lemma coproj_of_QSG_Selection_P_spec0  {A B Q S} (P:@QSG_Selection A B Q S) :
  let QSG_proj0 := proj1_sig P in
  let QSG_coproj0 : A -> B := @coproj_of_QSG_Selection_P A B Q S P in
  forall a, semB Q (QSG_coproj0 a) (QSG_proj0 a) = a.
Proof with curry1.
simpl...
unfold coproj_of_QSG_Selection_P.
rewrite_unop.
destruct S as [similar SP]...
rewrite (SP _) in E...
Qed.

Definition QSG_kernel {A B} (Q:QSG A B) (a:A) : Type := { b : B | semB Q b a = a }.

Record sig_group_P  {A} (G:group_T A) (P:A -> Prop) : Prop := {
  sig_group_P_monoidP : sig_monoid_P (group_monoid _ G) P;
  sig_group_P_invP : forall x, P x -> P(group_neg _ G x);
}.

Definition sig_group_P_monoid_T {A GA P} (SGP:@sig_group_P A GA P) : monoid_T (sig P) :=
  monoid_T_of_sig_monoid_P (sig_group_P_monoidP _ _ SGP).

(* [REMOVE] *)
Definition sig_group_P_zero {A GA P} (SGP:@sig_group_P A GA P) : sig P :=
  monoid_id _ (sig_group_P_monoid_T SGP).

(* [REMOVE] *)
Definition sig_group_P_plus {A GA P} (SGP:@sig_group_P A GA P) : sig P -> sig P -> sig P :=
  monoid_comp _ (sig_group_P_monoid_T SGP).

Lemma sig_group_P_neg_lemma0 {A GA P} (SGP:@sig_group_P A GA P) (x:sig P) :
  P (group_neg A GA (proj1_sig x)).
Proof with curry1.
destruct x...
destruct SGP...
Qed.

Definition sig_group_P_neg {A GA P} (SGP:@sig_group_P A GA P) : sig P -> sig P :=
  (fun x => exist _ (group_neg _ GA (proj1_sig x)) (sig_group_P_neg_lemma0 SGP x)).

Definition group_T_of_sig_group_P {A GA P} (SGP:@sig_group_P A GA P) : group_T (sig P) := {|
  group_monoid := sig_group_P_monoid_T SGP;
  group_neg := sig_group_P_neg SGP;
|}.

Lemma group_P_of_sig_group_P {A} (G:group A) {P} (SGP:@sig_group_P A (proj1_sig G) P) :
  group_P (group_T_of_sig_group_P SGP).
Proof with curry1.
unfold group_T_of_sig_group_P...
constructor...
- unfold sig_group_P_monoid_T...
  apply(monoid_P_of_sig_monoid_P (monoG G)).
- rewrite <- proj1_sig_injective...
  destruct G...
  destruct g...
- rewrite <- proj1_sig_injective...
  destruct G...
  destruct g...
Qed.

Definition group_of_sig_group_P {A} (G:group A) {P} (SGP:@sig_group_P A (proj1_sig G) P) : group (sig P) :=
  exist _ (group_T_of_sig_group_P SGP) (group_P_of_sig_group_P G SGP).

Lemma negG_zeroG {A} (G:group A) : negG G (zeroG G) = zeroG G.
Proof. rewrite negG_eq_shift; curry1. Qed.
#[export] Hint Rewrite @negG_zeroG : curry1_rewrite.

Lemma QSG_sem_group_neg {A B} (Q:QSG A B) x a0 a1 : semB Q (negG(groupB Q) x) a0 = a1 <-> a0 = semB Q x a1.
Proof with curry1.
split...
- rewrite semB_semB...
- rewrite semB_semB...
Qed.

Lemma sig_group_P_QSG_kernel {A B} (Q:QSG A B) (a:A) :
  sig_group_P (proj1_sig (groupB Q)) (fun b : B => semB Q b a = a).
Proof with curry1.
constructor...
- constructor...
  setoid_rewrite <- semB_semB...
  rewrite_subst.
- setoid_rewrite QSG_sem_group_neg...
Qed.

Definition group_QSG_kernel {A B} (Q:QSG A B) (a:A) : group (QSG_kernel Q a) :=
  group_of_sig_group_P (groupB Q) (sig_group_P_QSG_kernel Q a).

Require Import SimplEnum.

(* [MOVEME] *)
Definition implicit_finite_constr (T:Type) (n:nat) := exists i p, explicit_finite_constr T n i p.
Definition singleton_P (T:Type) := implicit_finite_constr T 1.

Definition QSG_kernel_singleton {A B} (qsg:QSG A B) (a:A) : Prop := singleton_P (QSG_kernel qsg a).

Lemma singleton_P_alternative (T:Type) (x:T) : singleton_P T <-> forall y, y = x.
Proof with curry1.
unfold singleton_P, implicit_finite_constr...
split...
- unfold explicit_finite_constr in H...
  unfold explicit_bijection_constr in H...
  assert(E0:i y = i x)...
  + rewrite ltN_PI...
    destruct(i x)...
    destruct(i y)...
  + specialize(f_equal p E0) as E1.
    fold (comp i p x) in E1.
    fold (comp i p y) in E1.
    repeat rewrite H in E1.
    curry1.
- exists (fun _ => ltN_of_nat 0 1 eq_refl).
  exists (fun _ => x)...
  unfold explicit_finite_constr...
  unfold explicit_bijection_constr...
  split; apply functional_extensionality...
Qed.

Lemma singleton_P_monoid_P_alternative (A:Type) (M:monoid A) : singleton_P A <-> forall y, y = idM M.
Proof. rewrite (singleton_P_alternative _ (idM M)); reflexivity. Qed.

Lemma singleton_P_group_P_alternative (A:Type) (G:group A) : singleton_P A <-> forall y, y = zeroG G.
Proof. rewrite (singleton_P_alternative _ (zeroG G)); reflexivity. Qed.


Lemma semB_injective_left_iff_QSG_kernel_singleton {A B} (Q:QSG A B) (a:A) :
  (forall b1 b2, semB Q b1 a = semB Q b2 a <-> b1 = b2) <-> QSG_kernel_singleton Q a.
Proof with curry1.
simpl...
unfold QSG_kernel_singleton.
rewrite (singleton_P_group_P_alternative _ (group_QSG_kernel Q a)).
unfold QSG_kernel, group_zero...
split...
- destruct y...
  specialize(H x (zeroG(groupB Q)))...
- rewrite <- (QSG_sem_group_neg Q), semB_semB...
  split...
  specialize(H(exist _ _ H0))...
Qed.