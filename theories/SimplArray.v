Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_Logic.

Require Import SimplDTT.
Require Import SimplLtN.

(* Section 2. Arrays *)

(* [array A n] : the of arrays of length [n] over type [A]
 * represented as the function which associate
 * a value to each valid index [k:ltN n]
 *)
Definition array (A:Type) (n:nat) : Type := ltN n -> A.

(* empty array of type [A] *)
Definition sempty {A} : array A 0 :=
  (fun k => match ltN_0_empty k with end).

(* (** val sempty : 'a1 array **)

   let sempty _ =
     assert false (* absurd case *)
 *)

(* Adding an element on top of a valuation *)
Definition scons {A n} (a:A) (s:array A n) : array A (S n) :=
  (fun k => match ltN_pred k with Some k' => s k' | None => a end).
(* [REMOVE]
Proof.
intro k.
destruct k as [k H].
destruct k.
- apply a.
- apply (s (ltN_of_nat k _ H)).
Defined.
 *)

(* [Extraction scons.]
(** val scons : nat -> 'a1 -> 'a1 array -> 'a1 array **)

let scons _ a s = function
| O -> a
| S k0 -> s k0
*)

Definition shead {A n} (s:array A (S n)) : A :=
  (s (ltN_of_nat 0 (S n) (eq_refl _))).

(* [Extraction shead.]
(** val shead : nat -> 'a1 array -> 'a1 **)

let shead _ s = s O
*)

Definition stail {A n} (s:array A (S n)) : array A n :=
  (fun k => s(ltN_S k)).

(* (** val stail : nat -> 'a1 array -> 'a1 array **)

   let stail n s k = s (ltN_S n k)
 *)

Lemma shead_scons {A n} a s : shead(@scons A n a s) = a.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @shead_scons : curry1_rewrite.

Lemma stail_scons {A n} a s : stail(@scons A n a s) = s.
Proof with curry1.
apply functional_extensionality; intros.
unfold stail, scons...
Qed.
#[export] Hint Rewrite @stail_scons : curry1_rewrite.

Lemma scons_ltN_S {A n} x0 x k : @scons A n x0 x (ltN_S k) = x k.
Proof. unfold scons; curry1. Qed.
#[export] Hint Rewrite @scons_ltN_S : curry1_rewrite.


Lemma array_head_expansion {A n} (a1 a2:array A (S n)) :
  a1 = a2 <-> shead a1 = shead a2 /\ stail a1 = stail a2.
Proof with curry1.
split...
apply functional_extensionality...
destruct x as [k H1]...
unfold shead, stail in *...
unfold ltN_of_nat in *...
destruct k...
apply_both H0 (ltN_of_nat k n H1)...
Qed.

Lemma scons_injective {A n} x0 x y0 y : @scons A n x0 x = scons y0 y <-> ( x0 = y0 /\ x = y ).
Proof. rewrite array_head_expansion; curry1. Qed.
#[export] Hint Rewrite @scons_injective : curry1_rewrite.


Lemma scons_shead_stail {A n} (x:array A (S n)) : scons (shead x) (stail x) = x.
Proof. rewrite array_head_expansion; curry1. Qed.
#[export] Hint Rewrite @scons_shead_stail : curry1_rewrite.

(* [TODO] make tactic *)
Lemma fun_shead_scons {X Y:Type} {A n} (outer: X -> A -> Y) iH iT :
  (fun (x:X) => outer x (shead(@scons A n (iH x) (iT x)))) =
    (fun (x:X) => outer x (iH x)).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @fun_shead_scons : curry1_rewrite.

(* [TODO] make tactic *)
Lemma fun_stail_scons {X Y:Type} {A n} (outer: X -> array A n -> Y) iH iT :
  (fun (x:X) => outer x (stail(@scons A n (iH x) (iT x)))) =
    (fun (x:X) => outer x (iT x)).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @fun_stail_scons : curry1_rewrite.

(* [TODO] make tactic *)
Lemma sempty_unique {A} (a : array A 0) : a = @sempty A.
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @sempty_unique : curry1_rewrite.

Fixpoint fold_left_ltN_comp {A:Type} {n} (a:array (A -> A) n) : A -> A :=
  match n as n0 return (array (A -> A) n0 -> A -> A) with
  | 0 => fun _ : array (A -> A) 0 => id
  | S n0 =>
      fun a0 : array (A -> A) (S n0) =>
      comp (fold_left_ltN_comp (stail a0)) (shead a0)
  end a.

Fixpoint fold_right_ltN_comp {A:Type} {n} (a:array (A -> A) n) : A -> A :=
  match n as n0 return (array (A -> A) n0 -> A -> A) with
  | 0 => fun _ : array (A -> A) 0 => id
  | S n0 =>
      fun a0 : array (A -> A) (S n0) =>
      comp (shead a0) (fold_right_ltN_comp (stail a0))
  end a.

Definition fold_left_ltN {A B:Type} {n} (f:A -> B -> B) (b0:B) (a:array A n) : B :=
  fold_left_ltN_comp (fun k => f (a k)) b0.

Definition foldi_left_ltN {A B:Type} {n}
  (f:ltN n -> A -> B -> B) (b0:B) (a:array A n) : B :=
    fold_left_ltN_comp (fun k => f k (a k)) b0.

Definition array_app {A n1 n2} (a1:array A n1) (a2:array A n2) : array A (n1+n2) :=
  (fun k =>
    match ltN_split n1 n2 k with
    | AA k1 => a1 k1
    | BB k2 => a2 k2
    end).

Definition array_fst_app {A} n1 n2 (a:array A (n1+n2)) : array A n1 :=
  (fun k => a (ltN_plus k n2)).

Definition array_snd_app {A} n1 n2 (a:array A (n1+n2)) : array A n2 :=
  (fun k => a (plus_ltN n1 k)).

Lemma array_fst_app_array_app A n1 n2 a1 a2 :
  @array_fst_app A n1 n2 (array_app a1 a2) = a1.
Proof with curry1.
unfold array_fst_app, array_app.
apply functional_extensionality...
Qed.
#[export] Hint Rewrite array_fst_app_array_app : curry1_rewrite.

Lemma array_snd_app_array_app A n1 n2 a1 a2 :
  @array_snd_app A n1 n2 (array_app a1 a2) = a2.
Proof with curry1.
unfold array_snd_app, array_app.
apply functional_extensionality...
Qed.
#[export] Hint Rewrite array_snd_app_array_app : curry1_rewrite.

Lemma array_app_array_fst_app_array_snd_app A n1 n2 a :
  array_app (@array_fst_app A n1 n2 a) (array_snd_app n1 n2 a) = a.
Proof with curry1.
apply functional_extensionality...
unfold array_app, array_fst_app, array_snd_app...
specialize(match_ltN_split_with_ltN_plus_plus_ltN n1 n2 x).
dest_match_step...
Qed.
#[export] Hint Rewrite array_app_array_fst_app_array_snd_app : curry1_rewrite.

Lemma array_app_decompose A n1 n2 a1 a2 a12 :
  array_app a1 a2 = a12 <->
    a1 = @array_fst_app A n1 n2 a12 /\
    a2 = array_snd_app n1 n2 a12.
Proof. split; curry1. Qed.

Lemma array_app_injective A n1 n2 a1 a2 b1 b2 :
  @array_app A n1 n2 a1 a2 = array_app b1 b2 <-> a1 = b1 /\ a2 = b2.
Proof. rewrite array_app_decompose; curry1. Qed.
#[export] Hint Rewrite array_app_injective : curry1_rewrite.

Lemma array_app_sempty_l A n s : @array_app A _ n sempty s = s.
Proof with curry1.
apply functional_extensionality...
unfold array_app...
apply apply_f...
Qed.
#[export] Hint Rewrite array_app_sempty_l : curry1_rewrite.

Definition cast_array_plus_0 {A n} (a:array A n) : array A (n+0) :=
  (eq_rect _ _ a _ (plus_n_O n)).

Lemma array_eq_rect A n0 n1 x p1 s :
  s (eq_rect n1 ltN x n0 p1) = eq_rect n0 (array A) s n1 (eq_sym p1) x.
Proof with curry1.
destruct x; simpl.
destruct p1...
Qed.
#[export] Hint Rewrite array_eq_rect : curry1_rewrite.

Lemma array_app_sempty_r A n s :
  @array_app A n _ s sempty = cast_array_plus_0 s.
Proof with curry1.
apply functional_extensionality...
unfold array_app, cast_array_plus_0.
rewrite ltN_split_0_r.
rewrite array_eq_rect.
eq_rect_unify...
Qed.
#[export] Hint Rewrite array_app_sempty_r : curry1_rewrite.

Lemma shead_array_app A n1 n2 (s1:array A (S n1)) (s2:array A n2) :
  shead (array_app s1 s2) = shead s1.
Proof. reflexivity. Qed.
#[export] Hint Rewrite shead_array_app : curry1_rewrite.

Lemma stail_array_app A n1 n2 (s1:array A (S n1)) (s2:array A n2) :
  stail (array_app s1 s2) = array_app (stail s1) s2.
Proof with curry1.
apply functional_extensionality...
fold (plus n1 n2) in x.
unfold stail, array_app.
rewrite ltN_split_S_ltN_S.
dest_match_step...
Qed.
#[export] Hint Rewrite stail_array_app : curry1_rewrite.

(* Uniform Operators *)

Definition spop {A n} (k:ltN (S n)) (s:array A (S n)) : array A n :=
  (fun x => s(ltN_intro k x)).

Definition spush {A n} (k:ltN (S n)) (v:A) (s:array A n) : array A (S n) :=
  (fun x => match ltN_pop k x with AA tt => v | BB x => s x end).

Lemma spop_spush_same {A n} (k:ltN(S n)) v (x:array A n) : spop k (spush k v x) = x.
Proof with curry1.
unfold spop, spush.
apply functional_extensionality...
Qed.
#[export] Hint Rewrite @spop_spush_same : curry1_rewrite.

Lemma spush_same {A n} k v (x:array A n) : spush k v x k = v.
Proof with curry1.
unfold spush...
Qed.
#[export] Hint Rewrite @spush_same : curry1_rewrite.

Lemma spop_spush {A n} (k1 k2:ltN(S(S n))) v (x:array A(S n)) :
  spop k2 (spush k1 v x) =
    match ltN_pop k2 k1 with
      | AA _ => x
      | BB k1' =>
        let k2' := ltN_comm_intro k1' k2 in
        spush k1' v (spop k2' x)
    end.
Proof with curry1.
unfold spop, spush.
apply functional_extensionality...
rewrite ltN_pop_ltN_intro...
dest_match.
Qed.

Lemma spush_ltN_rev_elim_ltN_rev_intro A r n (k1 k2:ltN(S n)) (x:array A _) :
  spush (ltN_rev_intro k2 k1) r x (ltN_rev_elim k1 k2) = x k2.
Proof. unfold spush; curry1. Qed.
#[export] Hint Rewrite spush_ltN_rev_elim_ltN_rev_intro : curry1_rewrite.

Lemma spop_ltN_rev_elim_spush_ltN_rev_intro A r n (k1 k2:ltN(S n)) (x:array A _) :
  spop (ltN_rev_elim k1 k2) (spush (ltN_rev_intro k2 k1) r x) = spush k1 r (spop k2 x).
Proof with curry1.
unfold spop, spush...
apply functional_extensionality...
rewrite ltN_pop_ltN_rev_intro_ltN_intro_ltN_rev_elim...
dest_match_step...
Qed.
#[export] Hint Rewrite spop_ltN_rev_elim_spush_ltN_rev_intro : curry1_rewrite.

Lemma spush_spop_same {A n} k (x:array A (S n)) : spush k (x k) (spop k x) = x.
Proof with curry1.
unfold spush, spop...
apply functional_extensionality...
dest_match_step...
Qed.
#[export] Hint Rewrite @spush_spop_same : curry1_rewrite.

Definition array_and {n} (a:array Prop n) : Prop :=
  fold_left_ltN_comp (fun (k:ltN n) (p:Prop) => p /\ a k) True.

Lemma array_and_S {n} (a:@array Prop (S n)) : array_and a <-> shead a /\ array_and (stail a).
Proof with curry1.
unfold array_and...
unfold shead, stail...
Qed.

Lemma array_and_0 (a:@array Prop 0) : array_and a <-> True.
Proof. curry1. Qed.

Lemma array_ind_correct_lemma1 {n} (a:@array Prop n) : (forall (k:ltN n), a k) -> array_and a.
Proof with curry1.
induction n...
rewrite array_and_S...
specialize(IHn (stail a))...
unfold shead, stail in *...
Qed.

Lemma forall_ltN_S {n} (P:ltN(S n) -> Prop) :
  (forall k : ltN (S n), P k) <-> (shead P /\ (forall (k:ltN n), stail P k)).
Proof with curry1.
unfold shead, stail.
split...
destruct k as [k kP]...
destruct k...
unfold ltN_S in H0.
assert(kP' : k <? n = true). { curry1. }
specialize(H0 (ltN_of_nat _ _ kP'))...
Qed.

Lemma array_ind_correct_lemma2 {n} a (H:array_and a) (k:ltN n) : a k.
Proof with curry1.
induction n...
generalize dependent k.
rewrite forall_ltN_S...
specialize(IHn (stail a)).
rewrite array_and_S in H...
Qed.

Lemma array_ind_correct {n} a : @array_and n a <-> forall (k:ltN n), a k.
Proof with curry1.
split...
- apply array_ind_correct_lemma2...
- apply array_ind_correct_lemma1...
Qed.

(* minsat_ltN *)

Fixpoint minsat_ltN {n} (a:array bool n) : option (ltN n) :=
match n as n0 return array bool n0 -> option (ltN n0) with
| 0 => fun _ => None
| S n => fun (a:array bool (S n)) =>
  let k0 := ltN_of_nat 0 (S n) eq_refl in
  if a k0 then Some k0 else (opmap ltN_S (minsat_ltN (stail a)))
end a.

Lemma minsat_ltN_rewrite {n} (a0:bool) a :
  minsat_ltN (@scons _ n a0 a) = if a0
    then Some(ltN_of_nat 0 (S n) eq_refl)
    else (opmap ltN_S (minsat_ltN a)).
Proof. curry1. Qed.

Lemma qinv_ltN n : qinv (ltN n).
Proof with curry1.
unfold qinv.
induction n...
specialize(IHn(stail p))...
destruct(shead p)eqn:E0...
+ destruct IHn...
  * rewrite forall_ltN_S...
  * right...
    unfold stail in H...
    exists(ltN_S a)...
+ right...
  exists(ltN_of_nat 0 (S n) eq_refl)...
Qed.

Lemma qinv_ltN_iff_true n : qinv (ltN n) <-> True.
Proof. curry1. apply qinv_ltN. Qed.
#[export] Hint Rewrite qinv_ltN_iff_true : curry1_rewrite.

Lemma neg_forall_ltN n a b : ~ (forall (k:ltN n), a k = b) <-> exists (k:ltN n), a k = negb b.
Proof. apply neg_forall_as_exists_with_qinv; curry1. Qed.

Lemma neg_exists_ltN n a b : ~ (exists (k:ltN n), a k = b) <-> forall (k:ltN n), a k = negb b.
Proof. apply neg_exists_as_forall. Qed.

Lemma is_min_positive_cmp_ltN_0 {n} f p :
  is_min_positive cmp_ltN f (ltN_of_nat 0 n p) <-> f(ltN_of_nat 0 n p) = true.
Proof. unfold is_min_positive; curry1. Qed.
#[export] Hint Rewrite @is_min_positive_cmp_ltN_0 : curry1_rewrite.

Lemma rewrite_assert_left (X Y:Prop) : X /\ Y <-> X /\ (X -> Y).
Proof. simplprop. Qed.

Lemma rewrite_assert_right (X Y:Prop) : X /\ Y <-> Y /\ (Y -> X).
Proof. simplprop. Qed.

Lemma is_min_positive_stail_iff_is_min_positive_ltN_S {n} (a:array bool (S n)) (k:ltN n) :
  is_min_positive cmp_ltN a (ltN_S k) <->
    (a (ltN_of_nat 0 (S n) eq_refl) = false /\ is_min_positive cmp_ltN (stail a) k).
Proof with curry1.
unfold stail, is_min_positive...
split...
- rewrite rewrite_assert_left; split...
  + unfold is_min_positive in H...
    specialize(H0(ltN_of_nat 0 (S n) eq_refl))...
  + specialize(H0(ltN_S a1))...
- generalize dependent a1.
  rewrite forall_ltN_S...
  unfold shead, stail...
Qed.

Lemma minsat_ltN_minsat_weak n : minsat_weak (@cmp_ltN n) minsat_ltN.
Proof with curry1.
unfold minsat_weak...
induction n...
- rewrite (sempty_unique F)...
- specialize(IHn(stail F)).
  dest_match_step...
  unfold opmap; dest_match_step...
  + rewrite is_min_positive_stail_iff_is_min_positive_ltN_S...
  + rewrite (array_head_expansion F (fun _ => false))...
Qed.

Lemma minsat_positive_minsat_ltN n : minsat_positive (@cmp_ltN n) minsat_ltN.
Proof with curry1.
rewrite <- (minsat_weak_iff_minsat_positive _ (cmp_P_cmp_ltN n))...
apply minsat_ltN_minsat_weak.
Qed.

Lemma isSome_minsat_ltN {n} a k : a k = true -> isSome(@minsat_ltN n a) = true.
Proof with curry1.
destruct(minsat_positive_minsat_ltN n)...
autospec.
Qed.

Lemma minsat_ltN_eq_Some_iff_is_min_positive {n} (a:array bool n) (k:ltN n) :
  minsat_ltN a = Some k <-> is_min_positive cmp_ltN a k.
Proof. destruct(minsat_positive_minsat_ltN n); curry1. Qed.

Lemma minsat_alternative_minsat_ltN n : minsat_alternative (@cmp_ltN n) minsat_ltN.
Proof.
rewrite <- minsat_positive_iff_minsat_alternative.
apply minsat_positive_minsat_ltN.
Qed.

Lemma minsat_ltN_eq_None n f : @minsat_ltN n f = None <-> f = fun _ => false.
Proof. rewrite (minsat_alternative_minsat_ltN n _ _); curry1. Qed.
#[export] Hint Rewrite minsat_ltN_eq_None : curry1_rewrite.

Lemma spush_fun_same {A n} k b : @spush A n k b (fun _ : ltN n => b) = (fun _ => b).
Proof with curry1.
unfold spush...
apply functional_extensionality...
dest_match_step...
Qed.
#[export] Hint Rewrite @spush_fun_same : curry1_rewrite.

Fixpoint maxsat_ltN {n} (a:array bool n) : option (ltN n) :=
match n as n0 return array bool n0 -> option (ltN n0) with
| 0 => fun _ => None
| S n => fun (a:array bool (S n)) =>
  match maxsat_ltN (stail a) with
  | Some k => Some(ltN_S k)
  | None => (
    let k0 := ltN_of_nat 0 (S n) eq_refl in
    if a k0 then Some k0 else None
  )
  end
end a.

Lemma maxsat_ltN_rewrite {n} (a0:bool) a :
  maxsat_ltN (@scons _ n a0 a) =
    match maxsat_ltN a with
    | Some k => Some(ltN_S k)
    | None => (
      let k0 := ltN_of_nat 0 (S n) eq_refl in
      if a0 then Some k0 else None
    )
    end.
Proof. curry1; dest_match_step; curry1. Qed.

Definition maxsat_of_minsat {A} (cmpA:cmp A) (minsat:minsat_t A) : minsat_t A :=
  (fun (p:A -> bool) => minsat (fun a0 => p a0 && isNone (minsat(fun a1 => (beq_ord (cmpA a0 a1) Lt) && p a1)))).

Lemma minsat_maxsat_pair_unique {A}
  (cmpA:cmp A) (cmpPA:cmp_P cmpA)
  (minsat:minsat_t A) (MinP:minsat_positive cmpA minsat)
  (maxsat:minsat_t A) (MaxP:maxsat_positive cmpA maxsat) :
    maxsat_of_minsat cmpA minsat = maxsat.
Proof with curry1.
unfold maxsat_of_minsat...
apply functional_extensionality; intro p.
rewrite minsat_positive_iff_minsat_alternative in MinP.
rewrite maxsat_positive_iff_maxsat_alternative in MaxP.
rewrite (MinP _)...
dest_match_step...
- rewrite (MaxP _) in D.
  unfold is_max_positive in D.
  unfold is_min_positive...
  rewrite (MinP _).
  assert(E0:(fun a1 : A => beq_ord (cmpA a a1) Lt && p a1) = (fun _ : A => false))...
  + apply functional_extensionality; intro a'...
    specialize(H0 a') as H0a'...
    destruct(p a')eqn:E0...
    apply reverse_bool_eq...
    rewrite (beq_iff_true_beq_ord _ _) in H1...
  + rewrite (MinP _) in H3...
    specialize(f_equal(fun f => f a)H3) as H3a...
    destruct cmpPA as [RA TA AA].
    rewrite <- (AA _)...
    destruct(cmpA a1 a)eqn:E1...
    rewrite (RA _ _) in E1...
- rewrite (MaxP _) in D...
Qed.

Lemma minsat_ltN_false {n} : @minsat_ltN n (fun _ => false) = None.
Proof. curry1. Qed.
#[export] Hint Rewrite @minsat_ltN_false : curry1_rewrite.


Lemma spop_0_as_stail {A n} (a:array A(S n)) p : spop (ltN_of_nat 0 (S n) p) a = stail a.
Proof with curry1.
apply functional_extensionality...
unfold spop...
Qed.
#[export] Hint Rewrite @spop_0_as_stail : curry1_rewrite.

Lemma fold_left_ltN_S {A B n} (f:A -> B -> B) b0 (a:array A (S n)) :
  fold_left_ltN f b0 a = f (shead a) (fold_left_ltN f b0 (stail a)).
Proof. reflexivity. Qed.

Lemma spop_ltN_S {A n} (a:array A(S(S n))) k : spop (ltN_S k) a = scons (shead a) (spop k (stail a)).
Proof with curry1.
rewrite array_head_expansion...
split...
- unfold shead, spop...
- apply functional_extensionality; intro x...
  unfold stail, spop...
Qed.

Lemma fold_left_ltN_scons {A B n} (f:A -> B -> B) b0 a0 (a:array A n) :
  fold_left_ltN f b0 (scons a0 a) = f a0 (fold_left_ltN f b0 a).
Proof. rewrite fold_left_ltN_S; curry1. Qed.

Lemma stail_comp_array_S {A n} (a:array A (S n)) {B} (f:A -> B) : stail (comp a f) = comp (stail a) f.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @stail_comp_array_S : curry1_rewrite.

Lemma is_max_positive_stail_iff_is_max_positive_ltN_S {n} (a:array bool (S n)) (k:ltN n) :
  is_max_positive cmp_ltN a (ltN_S k) <-> is_max_positive cmp_ltN (stail a) k.
Proof with curry1.
unfold is_max_positive...
split; intros H1 a1...
- specialize(H1(ltN_S a1))...
- generalize dependent a1.
  rewrite forall_ltN_S...
  unfold shead...
Qed.

Lemma stail_eq_fun_cst {A n} (F:array A(S n)) a0 :
  stail F = (fun _ : ltN n => a0) <-> F = scons (shead F) (fun _ => a0).
Proof. rewrite array_head_expansion; curry1. Qed.
#[export] Hint Rewrite @stail_eq_fun_cst : curry1_rewrite.

Lemma scons_eval_0 {A n} a0 (a:array A n) p : scons a0 a (ltN_of_nat 0 (S n) p) = a0.
Proof. unfold scons; curry1. Qed.
#[export] Hint Rewrite @scons_eval_0 : curry1_rewrite.

Lemma is_max_positive_cmp_ltN_0 {n} F p :
  is_max_positive cmp_ltN F (ltN_of_nat 0 (S n) p) <-> F = scons true (fun _ => false).
Proof with curry1.
unfold is_max_positive...
dependent destruction p...
rewrite (array_head_expansion F).
unfold shead...
split...
- rewrite <- stail_eq_fun_cst.
  apply functional_extensionality...
  specialize(H0(ltN_S x))...
- rewrite H0 in *...
  destruct(ltN_pred a1) as [k'|] eqn:E0...
Qed.
#[export] Hint Rewrite @is_max_positive_cmp_ltN_0 : curry1_rewrite.

Lemma scons_x_fun_x {A n} v : @scons A n v (fun _ => v) = (fun _ => v).
Proof. rewrite array_head_expansion; curry1. Qed.
#[export] Hint Rewrite @scons_x_fun_x : curry1_rewrite.

Lemma maxsat_weak_maxsat_ltN n : maxsat_weak (@cmp_ltN n) maxsat_ltN.
Proof with curry1.
unfold maxsat_weak...
induction n...
- rewrite (sempty_unique F)...
- specialize(IHn(stail F)).
  dest_match_step...
  + rewrite is_max_positive_stail_iff_is_max_positive_ltN_S...
  + setoid_rewrite stail_eq_fun_cst in IHn...
    rewrite IHn in *...
    dest_match_step...
Qed.

Lemma maxsat_positive_maxsat_ltN n : maxsat_positive (@cmp_ltN n) maxsat_ltN.
Proof with curry1.
rewrite <- maxsat_weak_iff_maxsat_positive.
- apply maxsat_weak_maxsat_ltN.
- apply cmp_P_cmp_ltN.
Qed.

(* Order on Arrays *)

Definition cmp_array {A} (cA:cmp A) {n} (a1 a2:array A n) : ord :=
  let ca := (fun k => cA (a1 k) (a2 k)) in
  match minsat_ltN (fun k => negb(beq_ord (ca k) Eq)) with
  | Some k => ca k
  | None   => Eq
  end.

Lemma cmp_array_scons {A} (cmpA:cmp A) {n} x0 y0 (x y:array A n) :
  cmp_array cmpA (scons x0 x) (scons y0 y) = cmp_comp (cmpA x0 y0) (cmp_array cmpA x y).
Proof with curry1.
unfold cmp_array.
assert(R: (fun k : ltN n => beq_ord (cmpA (x k) (y k)) Eq) =
            stail (fun k : ltN (S n) => beq_ord (cmpA (scons x0 x k) (scons y0 y k)) Eq)).
{
  apply functional_extensionality...
  unfold stail.
  repeat rewrite scons_ltN_S...
}
dest_match_step...
- dest_if...
  + unfold cmp_comp...
    destruct(cmpA x0 y0)eqn:E...
  + rewrite (beq_iff_true_beq_ord _ _) in D0...
    rewrite D0...
    rewrite R. clear R.
    dest_match_step.
    * simpl in D. inversion D. subst.
      repeat rewrite scons_ltN_S...
    * simpl...
- rewrite R...
  rewrite H0... 
  rewrite (beq_iff_true_beq_ord _ _) in H.
  rewrite H...
Qed.

Lemma rewrite_cmp_array_S {A} (cmpA:cmp A) {n} (x y:array A(S n)) :
  cmp_array cmpA x y = cmp_comp (cmpA (shead x) (shead y)) (cmp_array cmpA (stail x) (stail y)).
Proof with curry1.
rewrite <- (scons_shead_stail x).
rewrite <- (scons_shead_stail y).
rewrite cmp_array_scons...
Qed.

Lemma cmp_Eq_iff_eq_cmp_array {A cA} (PA:cmp_P cA) n : cmp_Eq_iff_eq (@cmp_array A cA n).
Proof with curry1.
assert(PA':=PA).
destruct PA' as [RA TA AA].
unfold cmp_Eq_iff_eq.
induction n...
- rewrite (sempty_unique x)...
- rewrite rewrite_cmp_array_S...
  rewrite cmp_comp_Eq...
  rewrite (RA _ _).
  rewrite IHn.
  rewrite (array_head_expansion x y)...
Qed.

Lemma cmp_Lt_trans_cmp_array {A cA} (PA:cmp_P cA) n : cmp_Lt_trans (@cmp_array A cA n).
Proof with curry1.
assert(PA':=PA).
destruct PA' as [RA TA AA].
intros x y z.
induction n.
- rewrite (sempty_unique x), (sempty_unique y), (sempty_unique z)...
- repeat rewrite rewrite_cmp_array_S.
  rewrite rewrite_cmp_comp_trans...
  + split; [apply TA|].
    intros H1 H2; apply IHn.
  + apply(cmp_Eq_iff_eq_cmp_array PA).
Qed.

Lemma cmp_asym_cmp_array {A cA} (PA:cmp_P cA) n : cmp_asym (@cmp_array A cA n).
Proof with curry1.
assert(PA':=PA).
destruct PA' as [RA TA AA].
intros x y.
unfold cmp_array.
assert(R:(fun k : ltN n => negb (beq_ord (cA (x k) (y k)) Eq)) = 
         (fun k : ltN n => negb (beq_ord (cA (y k) (x k)) Eq))).
{
  apply functional_extensionality...
  rewrite eq_iff_eq_true...
  repeat rewrite (beq_iff_true_beq_ord _)...
  repeat rewrite (RA _ _)...
}
rewrite R.
dest_match_step...
Qed.

Lemma cmp_P_cmp_array {A cA} (PA:cmp_P cA) n : cmp_P (@cmp_array A cA n).
Proof with curry1.
constructor.
- apply (cmp_Eq_iff_eq_cmp_array PA).
- apply (cmp_Lt_trans_cmp_array PA).
- apply (cmp_asym_cmp_array PA).
Qed.

(* Boolean Case *)


Definition cmp_array_bool {n} (a1 a2:array bool n) : ord := cmp_array cmp_bool a1 a2.

Lemma cmp_P_cmp_array_bool n : cmp_P (@cmp_array_bool n).
Proof. apply (cmp_P_cmp_array cmp_P_bool). Qed.

Lemma cmp_array_bool_scons {n} x0 y0 (x y:array bool n) :
  cmp_array_bool (scons x0 x) (scons y0 y) = cmp_comp (cmp_bool x0 y0) (cmp_array_bool x y).
Proof. apply cmp_array_scons. Qed.

Lemma rewrite_cmp_array_bool_S {n} (x y:array bool (S n)) :
  cmp_array_bool x y = cmp_comp (cmp_bool (shead x) (shead y)) (cmp_array_bool (stail x) (stail y)).
Proof. apply rewrite_cmp_array_S. Qed.


Definition axor {n} (a1 a2:array bool n) : array bool n := (fun k => xorb(a1 k)(a2 k)).

Definition ascal {n} (b:bool) (a:array bool n) : array bool n := (fun k => andb b (a k)).

Definition asum {n} (a:array bool n) : bool := fold_left_ltN xorb false a.

Lemma asum_S {n} (a:array bool(S n)) : asum a = xorb (shead a) (asum(stail a)).
Proof. unfold asum; curry1. Qed.

Lemma asum_spop {n} (a:array bool(S n)) (k:ltN(S n)): asum a = xorb (a k) (asum(spop k a)).
Proof with curry1.
unfold asum...
induction n...
rewrite fold_left_ltN_S.
generalize dependent k.
rewrite forall_ltN_S; split...
+ unfold shead...
+ unfold stail...
  fold (stail a).
  specialize(IHn(stail a) k).
  rewrite IHn.
  fold (stail a k).
  rewrite spop_ltN_S.
  rewrite fold_left_ltN_scons.
  ring.
Qed.

Definition array_set {A n} (k0:ltN n) (a0:A) (a:array A n) : array A n :=
  (fun k => if beq_ltN k k0 then a0 else a k).

Definition array_dirac {A n} (k0:ltN n) (a0 a1:A): array A n :=
  (fun k => if beq_ltN k k0 then a0 else a1).

Lemma beq_ltN_ltN_intro_r {n} k1 k2 : @beq_ltN (S n) k1 (ltN_intro k1 k2) = false.
Proof. apply reverse_bool_eq; intro H; rewrite (beq_iff_true_beq_ltN _ _) in H; curry1. Qed.
#[export] Hint Rewrite @beq_ltN_ltN_intro_r : curry1_rewrite.

Lemma beq_ltN_comm {n} (k1 k2:ltN n) : beq_ltN k1 k2 = beq_ltN k2 k1.
Proof. rewrite bool_eq_iff_iff; curry1. Qed.

Lemma beq_ltN_ltN_intro_l {n} k1 k2 : @beq_ltN (S n) (ltN_intro k1 k2) k1 = false.
Proof. rewrite beq_ltN_comm; apply beq_ltN_ltN_intro_r. Qed.
#[export] Hint Rewrite @beq_ltN_ltN_intro_l : curry1_rewrite.

Lemma array_set_eval_same {A n} (k0:ltN n) (a0:A) (a:array A n) : array_set k0 a0 a k0 = a0.
Proof. unfold array_set; curry1. Qed.
#[export] Hint Rewrite @array_set_eval_same : curry1_rewrite.

Lemma spop_array_set_same {A n} (k0:ltN(S n)) (a0:A) (a:array A(S n)) : spop k0 (array_set k0 a0 a) = spop k0 a.
Proof with curry1.
apply functional_extensionality...
unfold array_set, spop...
Qed.
#[export] Hint Rewrite @spop_array_set_same : curry1_rewrite.

Lemma rewrite_array_dirac_as_array_set {A n} (k0:ltN n) (a0 a1:A) :
  array_dirac k0 a0 a1 = array_set k0 a0 (fun _ => a1).
Proof. reflexivity. Qed.

Lemma array_dirac_eval_same {A n} (k0:ltN n) (a0 a1:A) : array_dirac k0 a0 a1 k0 = a0.
Proof. rewrite rewrite_array_dirac_as_array_set; curry1. Qed.
#[export] Hint Rewrite @array_dirac_eval_same : curry1_rewrite.

Lemma spop_array_dirac_same {A n} (k0:ltN(S n)) (a0 a1:A) : spop k0 (array_dirac k0 a0 a1) = fun _ => a1.
Proof. rewrite rewrite_array_dirac_as_array_set; curry1. Qed.
#[export] Hint Rewrite @spop_array_dirac_same : curry1_rewrite.

Lemma array_expansion_u {A n} (k:ltN(S n)) (a1 a2:array A(S n)) :
  a1 = a2 <-> a1 k = a2 k /\ spop k a1 = spop k a2.
Proof with curry1.
split...
rename k into k1.
apply functional_extensionality; intro k2.
destruct(ltN_pop k1 k2) as [|k2'] eqn:E0...
specialize(f_equal (fun f => f k2') H0)...
Qed.

Lemma spop_eq_fun_cst {A n} (k0:ltN(S n)) (a1:A) a : spop k0 a = (fun _ => a1) <-> a = array_dirac k0 (a k0) a1.
Proof. rewrite (array_expansion_u k0); curry1. Qed.
#[export] Hint Rewrite @spop_eq_fun_cst : curry1_rewrite.

Definition array_cdirac (c:bool) {n} (k0:ltN n) : array bool n := array_dirac k0 c false.

Definition aand {n} (a1 a2:array bool n) : array bool n := (fun k => (a1 k) && (a2 k)).

Definition adot {n} (a1 a2:array bool n) : bool := asum(aand a1 a2).

Lemma spop_axor {n} k a1 a2 : @spop _ n k (axor a1 a2) = axor (spop k a1) (spop k a2).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @spop_axor : curry1_rewrite.

Lemma axor_same {n} x : @axor n x x = (fun _ => false).
Proof. apply functional_extensionality; unfold axor; curry1. Qed.
#[export] Hint Rewrite @axor_same : curry1_rewrite.

Lemma axor_assoc {n} x y z : axor (axor x y) z = @axor n x (axor y z).
Proof. apply functional_extensionality; unfold axor; curry1. Qed.
#[export] Hint Rewrite @axor_assoc : curry1_rewrite.

Lemma axor_false_r {n} x : @axor n x (fun _ => false) = x.
Proof. apply functional_extensionality; unfold axor; curry1. Qed.
#[export] Hint Rewrite @axor_false_r : curry1_rewrite.

Lemma axor_false n : @axor n (fun _ => false) = id.
Proof. unfold axor; rewrite xorb_false; reflexivity. Qed.
#[export] Hint Rewrite @axor_false : curry1_rewrite.

Lemma aand_eval {n} (a1 a2:array _ n) (k:ltN n) : aand a1 a2 k = (a1 k) && (a2 k).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @aand_eval : curry1_rewrite.

Lemma spop_aand {n} (k:ltN(S n)) (a1 a2:array _(S n)) : spop k (aand a1 a2) = aand (spop k a1) (spop k a2).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @spop_aand : curry1_rewrite.

Lemma adot_spush_both_same {n} (k:ltN(S n)) b1 a1 b2 a2 :
  adot (spush k b1 a1) (spush k b2 a2) = xorb (b1 && b2) (adot a1 a2).
Proof. unfold adot; rewrite (asum_spop _ k); curry1. Qed.
#[export] Hint Rewrite @adot_spush_both_same : curry1_rewrite.

Lemma spop_ascal {n} (k:ltN(S n)) b v : spop k (ascal b v) = ascal b (spop k v).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @spop_ascal : curry1_rewrite.

Lemma rewrite_adot_using_spop {n} (k:ltN(S n)) a1 a2 : adot a1 a2 = xorb(a1 k && a2 k)(adot(spop k a1)(spop k a2)).
Proof with curry1.
rewrite <- (spush_spop_same k a1).
rewrite <- (spush_spop_same k a2).
rewrite adot_spush_both_same...
Qed.

Lemma axor_shift {n} (x y z:array bool n) : axor x y = z <-> y = axor x z.
Proof. split; intro H; (rewrite H || rewrite <- H); clear H; rewrite <- axor_assoc, axor_same; curry1. Qed.

Lemma stail_axor {n} x y : @stail _ n (axor x y) = axor (stail x) (stail y).
Proof. reflexivity. Qed.

Lemma rewrite_adot_using_shead {n} (x y:array _ (S n)) :
  adot x y = xorb ((shead x) && (shead y)) (adot (stail x) (stail y)).
Proof. rewrite(rewrite_adot_using_spop(ltN_of_nat 0 (S n) eq_refl)); curry1. Qed.

Lemma shead_axor {n} (x y: array _ (S n)) : shead(axor x y) = xorb(shead x)(shead y).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @shead_axor : curry1_rewrite.

Lemma adot_axor_l {n} (x y z:array _ n) : adot (axor x y) z = xorb (adot x z) (adot y z).
Proof with curry1.
induction n...
rewrite rewrite_adot_using_shead...
rewrite stail_axor.
rewrite IHn.
repeat rewrite rewrite_adot_using_shead...
BPS.MicroBPSolver...
Qed.

Lemma x_eq_x_xorb_y x y : x = xorb x y <-> y = false.
Proof. BPS.MicroBPSolver; reflexivity. Qed.
#[export] Hint Rewrite x_eq_x_xorb_y : curry1_rewrite.

Lemma adot_diag n x : @adot n x x = asum x.
Proof with curry1.
induction n...
rewrite rewrite_adot_using_shead; rewrite IHn...
Qed.
#[export] Hint Rewrite @adot_diag : curry1_rewrite.

Lemma x_andb_eq_andb_x x y z : (x && y = z && x) <-> (x = true -> y = z).
Proof. BPS.MicroBPSolver; reflexivity. Qed.
#[export] Hint Rewrite x_andb_eq_andb_x : curry1_rewrite.

Lemma aand_comm n x y : @aand n x y = aand y x.
Proof. apply functional_extensionality; curry1. Qed.

Lemma adot_comm n x y : @adot n x y = adot y x.
Proof. unfold adot; rewrite aand_comm; curry1. Qed.

Definition adot' {n} x y : bool := xorb(@adot n x y)(andb(asum x)(asum y)).

Lemma adot'_diag n x : @adot' n x x = false.
Proof. unfold adot'; curry1. Qed.
#[export] Hint Rewrite adot'_diag : curry1_rewrite.

Lemma adot'_comm n x y : @adot' n x y = adot' y x .
Proof with curry1. unfold adot'; rewrite adot_comm... Qed.

Lemma asum_axor n x y : asum (@axor n x y) = xorb (asum x) (asum y).
Proof with curry1.
induction n...
repeat rewrite asum_S...
rewrite stail_axor...
rewrite IHn...
Qed.
#[export] Hint Rewrite asum_axor : curry1_rewrite.

Lemma adot'_axor_l n x1 x2 y : @adot' n (axor x1 x2) y = xorb (adot' x1 y) (adot' x2 y).
Proof with curry1.
unfold adot'...
rewrite adot_axor_l...
BPS.MicroBPSolver...
Qed.

Lemma adot'_axor_r n x1 x2 y : @adot' n y (axor x1 x2) = xorb (adot' y x1) (adot' y x2).
Proof. rewrite adot'_comm, adot'_axor_l; rewrite adot'_comm; curry1; rewrite adot'_comm; curry1. Qed.

Lemma ascal_false_l n x : @ascal n false x = (fun _ => false).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @ascal_false_l : curry1_rewrite.

Lemma aand_false_l n y : @aand n (fun _ => false) y = (fun _ => false).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @aand_false_l : curry1_rewrite.

Lemma aand_false_r n y : @aand n y (fun _ => false) = (fun _ => false).
Proof. rewrite aand_comm; apply aand_false_l. Qed.
#[export] Hint Rewrite @aand_false_r : curry1_rewrite.

Lemma asum_false n : @asum n (fun _ => false) = false.
Proof. induction n; curry1; rewrite asum_S; curry1. Qed.
#[export] Hint Rewrite @asum_false : curry1_rewrite.

Lemma adot_false_l n y : @adot n (fun _ => false) y = false.
Proof. unfold adot; curry1. Qed.
#[export] Hint Rewrite adot_false_l : curry1_rewrite.

Lemma adot_ascal b {n} (x y:array _ n) : adot (ascal b x) y = b && (adot x y).
Proof. destruct b; curry1. Qed.
#[export] Hint Rewrite @adot_ascal : curry1_rewrite.

Lemma adot'_false_l {n} (y:array _ n) : adot' (fun _ => false) y = false.
Proof. unfold adot'; curry1. Qed.
#[export] Hint Rewrite @adot'_false_l : curry1_rewrite.

Lemma adot'_false_r {n} (y:array _ n) : adot' y (fun _ => false) = false.
Proof. rewrite adot'_comm; curry1. Qed.
#[export] Hint Rewrite @adot'_false_r : curry1_rewrite.

Lemma adot'_ascal_l b {n} (x y:array _ n) : adot' (ascal b x) y = b && (adot' x y).
Proof. destruct b; curry1. Qed.
#[export] Hint Rewrite @adot'_ascal_l : curry1_rewrite.

Lemma adot'_ascal_r b {n} (x y:array _ n) : adot' y (ascal b x) = b && (adot' y x).
Proof. destruct b; curry1. Qed.
#[export] Hint Rewrite @adot'_ascal_r : curry1_rewrite.

Lemma ascal_eval n b v k : @ascal n b v k = b && v k.
Proof. reflexivity. Qed.
#[export] Hint Rewrite ascal_eval : curry1_rewrite.

Lemma ascal_true n a : @ascal n true a = a.
Proof. reflexivity. Qed.
#[export] Hint Rewrite ascal_true : curry1_rewrite.

Lemma axor_eval {n} x y k : @axor n x y k = xorb(x k)(y k).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @axor_eval : curry1_rewrite.

Lemma x_xorb_eq_xorb_x x y z : xorb x y = xorb z x <-> y = z.
Proof. BPS.MicroBPSolver; reflexivity. Qed.
#[export] Hint Rewrite x_xorb_eq_xorb_x : curry1_rewrite.

Lemma axor_comm {n} x y : @axor n x y = axor y x.
Proof. apply functional_extensionality; curry1. Qed.

(* [REMOVE]
Lemma proj1_sig_ltN_intro_beq_proj1_sig {n} (k:ltN(S n)) x :
  proj1_sig (ltN_intro k x) =? proj1_sig k = false.
Proof. curry1; rewrite_if_as_Prop; for_lia1. Qed.
#[export] Hint Rewrite @proj1_sig_ltN_intro_beq_proj1_sig : curry1_rewrite.
 *)

Lemma aand_array_dirac_l {n} k a b0 b1 :
  @aand n (array_dirac k b0 b1) a = array_set k (a k && b0) (ascal b1 a).
Proof with curry1.
destruct n...
rewrite (array_expansion_u k); curry1.
Qed.
#[export] Hint Rewrite @aand_array_dirac_l : curry1_rewrite.

Lemma asum_spush b {n} k a : asum (spush k b a) = xorb b (@asum n a).
Proof. rewrite (asum_spop _ k); curry1. Qed.
#[export] Hint Rewrite @asum_spush : curry1_rewrite.

Lemma adot_fun_cst_l b1 {n} (a2:array bool n) : adot (fun _ => b1) a2 = b1 && asum a2.
Proof. destruct b1; curry1. Qed.
#[export] Hint Rewrite @adot_fun_cst_l : curry1_rewrite.
Lemma adot_fun_cst_r b1 {n} (a2:array bool n) : adot a2 (fun _ => b1) = asum a2 && b1.
Proof. rewrite adot_comm, adot_fun_cst_l, andb_comm; reflexivity. Qed.
#[export] Hint Rewrite @adot_fun_cst_r : curry1_rewrite.

Lemma adot_array_dirac_l {n} (k:ltN(S n)) a b0 b1 :
  adot (array_dirac k b0 b1) a = xorb (b0 && (a k)) (b1 && (asum (spop k a))).
Proof. rewrite (rewrite_adot_using_spop k); curry1. Qed.
#[export] Hint Rewrite @adot_array_dirac_l : curry1_rewrite.

Lemma adot_array_dirac_false_l {n} (k:ltN n) a b0 : adot (array_dirac k b0 false) a = b0 && (a k).
Proof. destruct n; curry1. Qed.
#[export] Hint Rewrite @adot_array_dirac_false_l : curry1_rewrite.

Lemma ascal_false_r n b : @ascal n b (fun _ : ltN n => false) = (fun _ => false).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @ascal_false_r : curry1_rewrite.

Lemma array_cdirac_eval_same b {n} k : @array_cdirac b n k k = b.
Proof. unfold array_cdirac; curry1. Qed.
#[export] Hint Rewrite @array_cdirac_eval_same : curry1_rewrite.

Lemma spop_array_cdirac_same b {n} k : @spop _ n k (array_cdirac b k) = (fun _ => false).
Proof. unfold array_cdirac; curry1. Qed.
#[export] Hint Rewrite @spop_array_cdirac_same : curry1_rewrite.

Lemma aand_array_cdirac_l b {n} k a :
  @aand (S n) (array_cdirac b k) a = array_cdirac (b && (a k)) k.
Proof. rewrite (array_expansion_u k); curry1. Qed.
#[export] Hint Rewrite @aand_array_cdirac_l : curry1_rewrite.

Lemma asum_array_cdirac {n} b k : asum (@array_cdirac b n k) = b.
Proof with curry1.
destruct n...
rewrite (asum_spop _ k)...
Qed.
#[export] Hint Rewrite @asum_array_cdirac : curry1_rewrite.

Lemma adot_array_cdirac_l b {n} k a : @adot n (array_cdirac b k) a = b && (a k).
Proof with curry1.
destruct n...
rewrite (rewrite_adot_using_spop k)...
Qed.
#[export] Hint Rewrite @adot_array_cdirac_l : curry1_rewrite.

Lemma axor_v_ascal_b_v {n} b v : @axor n v (ascal b v) = ascal (negb b) v.
Proof with curry1.
apply functional_extensionality...
BPS.MicroBPSolver...
Qed.
#[export] Hint Rewrite @axor_v_ascal_b_v : curry1_rewrite.

Lemma axor_ascal_b_v_v {n} b v : @axor n (ascal b v) v = ascal (negb b) v.
Proof. rewrite axor_comm; curry1. Qed.
#[export] Hint Rewrite @axor_ascal_b_v_v : curry1_rewrite.

Lemma axor_x_axor_x_y {n} x y : @axor n x (axor x y) = y.
Proof. rewrite axor_shift; curry1. Qed.
#[export] Hint Rewrite @axor_x_axor_x_y : curry1_rewrite.

Lemma axor_same_l_injective {n} x y1 y2 : @axor n x y1 = axor x y2 <-> y1 = y2.
Proof. rewrite axor_shift; curry1. Qed.
#[export] Hint Rewrite @axor_same_l_injective : curry1_rewrite.


Lemma scons0 {A} a0 a : @scons A 0 a0 a = (fun _ => a0).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @scons0 : curry1_rewrite.

Lemma spush0 {A} a0 (k:ltN 1) a : @spush A 0 k a0 a = (fun _ => a0).
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @spush0 : curry1_rewrite.

Lemma ltN_pop_ltN_S_both {n} x y : ltN_pop (ltN_S x) (ltN_S y) = mapAB id ltN_S (@ltN_pop n x y).
Proof. destruct(ltN_pop x y)eqn:E0; curry1. Qed.

Lemma spush_ltN_S_scons {A n} k x y (a:array A n)  : spush (ltN_S k) x (scons y a) = scons y (spush k x a).
Proof with curry1.
unfold spush, scons.
apply functional_extensionality...
destruct(ltN_pred x0) as [x0'|]eqn:E0...
- rewrite ltN_pop_ltN_S_both.
  destruct(ltN_pop k x0')eqn:E0...
- destruct x0...
Qed.

Lemma minsat_ltN_true_lemma1 {n} (p:ltN n -> bool) (P:p <> (fun _ => false)) : minsat_ltN p <> None.
Proof. curry1. Qed.

Definition minsat_ltN_true {n} (p:ltN n -> bool) (P:p <> (fun _ => false)) : ltN n :=
  unop (minsat_ltN p) (minsat_ltN_true_lemma1 p P).

Lemma minsat_ltN_iff_is_min_positive {n} a p (k:ltN n) :
  minsat_ltN_true a p = k <-> is_min_positive cmp_ltN a k.
Proof with curry1.
unfold minsat_ltN_true.
rewrite unop_rewrite.
rewrite (minsat_alternative_minsat_ltN _ _)...
Qed.
