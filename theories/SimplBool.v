Require Import Arith.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.

Lemma tf_False : true = false <-> False.
Proof. solve_by_invert. Qed.

Lemma ft_False : false = true <-> False.
Proof. solve_by_invert. Qed.

Ltac bool_eq_rewrite := match goal with
  | H : true = false |- _ => inversion H
  | H : false = true |- _ => inversion H
  | |- true = false => exfalso
  | |- false = true => exfalso
  | H : context [true = false] |- _ =>
    rewrite tf_False in H
  | |- context [true = false] =>
    rewrite tf_False
  | H : context [false = true] |- _ =>
    rewrite ft_False in H
  | |- context [false = true] =>
    rewrite ft_False
  | H : true = _ |- _ => symmetry in H; try bool_eq_rewrite
  | H : false = _ |- _ => symmetry in H; try bool_eq_rewrite
  | H1 : ?E = true, H2 : context [?E] |- _ => rewrite H1 in H2
  | H1 : ?E = false, H2 : context [?E] |- _ => rewrite H1 in H2
  | H1 : ?E = true |- context [?E] => rewrite H1
  | H1 : ?E = false |- context [?E] => rewrite H1

end.

Ltac dest_bool_step := match goal with
  | |- _ => bool_eq_rewrite
  | b : bool |- _ => destruct b
end.

Ltac dest_bool := intros; repeat split; repeat (dest_bool_step; simpl; auto; try solve_by_invert; try intros).
Ltac dead_bool := dest_bool.

Lemma neq_to_beq b1 b2 : b1 <> b2 <-> b1 = negb b2.
Proof. dest_bool. Qed.

Lemma replace_if_with_logic (b b1 b0:bool) :
  (if b then b1 else b0) = (b && b1) || (negb b && b0).
Proof. dest_bool. Qed.

Lemma eqb_to_xorb b0 b1 : eqb b0 b1 = negb(xorb b0 b1).
Proof. dest_bool. Qed.

Lemma orb_to_andb b0 b1 : orb b0 b1 = negb(andb(negb b0)(negb b1)).
Proof. dest_bool. Qed.

Lemma xorb_negb_l b0 b1 : xorb(negb b0)b1 = negb(xorb b0 b1).
Proof. dest_bool. Qed.

Lemma xorb_negb_r b0 b1 : xorb b0(negb b1) = negb(xorb b0 b1).
Proof. dest_bool. Qed.

Lemma xorb_to_eq b0 b1 : xorb b0 b1 = false <-> b0 = b1.
Proof. dest_bool. Qed.

Lemma xorb_to_neq b0 b1 : xorb b0 b1 = true <-> b0 = negb b1.
Proof. dest_bool. Qed.

Lemma negb_migration b0 b1 : negb b0 = b1 <-> b0 = negb b1.
Proof. dest_bool. Qed.

Lemma negb_no_fixpoint b : (b = negb b) <-> False.
Proof. dest_bool. Qed.

Lemma xorb_elim_l bl br1 br2 : xorb bl br1 = xorb bl br2 <-> br1 = br2.
Proof. dest_bool. Qed.

Lemma xorb_elim_r bl1 bl2 br: xorb bl1 br = xorb bl2 br <-> bl1 = bl2.
Proof. dest_bool. Qed.

Lemma negb_injective b0 b1 : negb b0 = negb b1 <-> b0 = b1.
Proof. dest_bool. Qed.

Lemma xorb_b_xorb_b_b0 b b0 : xorb b (xorb b b0) = b0.
Proof. dest_bool. Qed.

Lemma xorb_b_xorb_b0_b b b0 : xorb b (xorb b0 b) = b0.
Proof. dest_bool. Qed.

Lemma xorb_assoc a b c : xorb (xorb a b) c = xorb a (xorb b c).
Proof. dest_bool. Qed.

Lemma andb_negb_l b : negb b && b = false.
Proof. dest_bool. Qed.

Lemma andb_negb_r b : b && negb b = false.
Proof. dest_bool. Qed.

(* functional partial evalution *)
Lemma eqb_true : eqb true = id.
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite eqb_true : simplprop1_rewrite.

Lemma eqb_false : eqb false = negb.
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite eqb_false : simplprop1_rewrite.

Lemma xorb_true : xorb true = negb.
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite xorb_true : simplprop1_rewrite.

Lemma xorb_false : xorb false = id.
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite xorb_false : simplprop1_rewrite.

Lemma andb_true : andb true = id.
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite andb_true : simplprop1_rewrite.

Lemma andb_false : andb false = (fun _ => false).
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite andb_false : simplprop1_rewrite.

Lemma orb_true : orb true = (fun _ => true).
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite orb_true : simplprop1_rewrite.

Lemma orb_false : orb false = id.
Proof. apply functional_extensionality; dest_bool. Qed.
#[export] Hint Rewrite orb_false : simplprop1_rewrite.

Lemma bool_disjunction b : b = true \/ b = false.
Proof. dest_bool. Qed.

Lemma bool_disjunction' b : b = false \/ b = true.
Proof. dest_bool. Qed.

Lemma if_elim {A} (c:bool) (a:A) : (if c then a else a) = a.
Proof. destruct c; reflexivity. Qed.

Lemma if_rewrite (b1 b2 b3:bool) :
  (if b1 then b2 else b3) =
    if b2
    then if b3
      then true
      else b1
    else if b3
      then negb b1
      else false.
Proof.
dest_bool.
Qed.

Lemma eq_or_neq (b0 b1:bool) : (b0 = b1) \/ (b0 = negb b1).
Proof. dest_bool. Qed.

Ltac eq_or_neq b0 b1 := destruct (eq_or_neq b0 b1); subst.

Definition impb (x y:bool) := if x then y else true.

Lemma impb_rewrite x y : impb x y = orb (negb x) y.
Proof. dest_bool. Qed.

Lemma goal_andb_same_ll a b c :
  a && b = a && c <-> (a = true -> b = c).
Proof. dest_bool. Qed.

Lemma rewrite_then_true (a b:bool) : (if a then true else b) = a || b.
Proof. dest_bool. Qed.
Lemma rewrite_else_true (a b:bool) : (if a then b else true) = (negb a) || b.
Proof. dest_bool. Qed.
Lemma rewrite_then_false (a b:bool) : (if a then false else b) = (negb a) && b.
Proof. dest_bool. Qed.
Lemma rewrite_else_false (a b:bool) : (if a then b else false) = a && b.
Proof. dest_bool. Qed.

Lemma ITE_to_circuit (x y z:bool) :
  (if x then y else z) = xorb (x && y) ((negb x) && z).
Proof. dest_bool. Qed.

Lemma if_b_then_true_else_false (b:bool) :
  (if b then true else false) = b.
Proof. dest_bool. Qed.

Lemma if_b_then_false_else_true (b:bool) :
  (if b then false else true) = negb b.
Proof. dest_bool. Qed.

Lemma if_negb_b b {A} (x y:A) : (if (negb b) then x else y) = (if b then y else x).
Proof. dest_bool. Qed.

Lemma rewrite_bool_imp_False x y :
  (x = y -> False) <-> (x = negb y).
Proof. dest_bool. Qed.

Lemma rewrite_bool_iff_False x y :
  (x = y <-> False) <-> (x = negb y).
Proof. dest_bool. Qed.

Lemma xorb_shift_l b0 b1 b2 : xorb b0 b1 = b2 <-> b1 = xorb b0 b2.
Proof. dest_bool. Qed.

Ltac simplbool_step := match goal with
  | _ => bool_eq_rewrite
  | H : context [?b1 <> ?b2] |- _ => rewrite neq_to_beq in H
  | |- context [?b1 <> ?b2] => rewrite neq_to_beq
  | H : context [negb true] |- _ => simpl in H
  | H : context [negb false] |- _ => simpl in H
  | HT : ?A = true, HF : ?A = false |- _ =>
    rewrite HF in HT; inversion HT
  | H : context [?b = true \/ ?b = false] |- _ =>
    rewrite bool_disjunction in H
  | H : context [?b = false \/ ?b = true] |- _ =>
    rewrite bool_disjunction' in H
  | |- context [?b = true \/ ?b = false] =>
    rewrite bool_disjunction
  | |- context [?b = false \/ ?b = true] =>
    rewrite bool_disjunction'
  | H : context [impb _ _] |- _ => rewrite impb_rewrite in H
  | |- context [impb _ _] => rewrite impb_rewrite
  | H : ?a && ?b = true |- _ => rewrite andb_true_iff in H
  | H : ?a || ?b = false |- _ => rewrite orb_false_iff in H
  | H : context[?x = ?y -> False] |- _ =>
    rewrite (rewrite_bool_imp_False x y) in H
  | |- context[?x = ?y -> False] =>
    rewrite (rewrite_bool_imp_False x y)
  | H : context[?x = ?y <-> False] |- _ =>
    rewrite (rewrite_bool_iff_False x y) in H
  | |- context[?x = ?y <-> False] =>
    rewrite (rewrite_bool_iff_False x y)
(* CONVERSION *)
  | H : context [eqb _ _] |- _ => rewrite eqb_to_xorb in H
  | |- context [eqb _ _] => rewrite eqb_to_xorb
  | H : context [orb _ _] |- _ => rewrite orb_to_andb in H
  | |- context [orb _ _] => rewrite orb_to_andb
(* XORB vs NEGB *)
  | H : context [xorb (negb _) _] |- _ => rewrite xorb_negb_l in H
  | |- context [xorb (negb _) _] => rewrite xorb_negb_l
  | H : context [xorb _ (negb _)] |- _ => rewrite xorb_negb_r in H
  | |- context [xorb _ (negb _)] => rewrite xorb_negb_r
(* NEGB vs NEGB *)
  | H : context [?b = negb ?b] |- _ => rewrite negb_no_fixpoint in H
  | |- context [?b = negb ?b] => rewrite negb_no_fixpoint
  | H : context [negb(negb _)] |- _ => rewrite negb_involutive in H; subst
  | |- context [negb(negb _)] => rewrite negb_involutive
  | H : context [negb _ = negb _] |- _ => rewrite negb_injective in H
  | |- context [negb _ = negb _] => rewrite negb_injective
(* simpl NEGB *)
  | H : true  = negb _ |- _ => symmetry in H; rewrite negb_true_iff  in H
  | |- true  = negb _ => symmetry; rewrite negb_true_iff
  | H : false = negb _ |- _ => symmetry in H; rewrite negb_false_iff in H
  | |- false = negb _ => symmetry; rewrite negb_false_iff
  | H : context [negb _ = true]  |- _ => rewrite negb_true_iff  in H
  | |- context [negb _ = true] => rewrite negb_true_iff
  | H : context [negb _ = false] |- _ => rewrite negb_false_iff in H
  | |- context [negb _ = false] => rewrite negb_false_iff
  | H : context [negb _ = _] |- _ => rewrite negb_migration in H
  | |- context [negb _ = _] => rewrite negb_migration
(* simpl XORB *)
  | H : context [xorb ?b ?b] |- _ => rewrite xorb_nilpotent in H
  | |- context [xorb ?b ?b] => rewrite xorb_nilpotent
  | H : context [xorb false _] |- _ => rewrite xorb_false_l in H
  | |- context [xorb false _] => rewrite xorb_false_l
  | H : context [xorb _ false] |- _ => rewrite xorb_false_r in H
  | |- context [xorb _ false] => rewrite xorb_false_r
  | H : context [xorb true ?b] |- _ => rewrite (xorb_true_l b) in H
  | |- context [xorb true ?b] => rewrite (xorb_true_l b)
  | H : context [xorb _ true] |- _ => rewrite xorb_true_r in H
  | |- context [xorb _ true] => rewrite xorb_true_r
  | H : context [xorb _ _ = false] |- _ => rewrite xorb_to_eq in H
  | |- context [xorb _ _ = false] => rewrite xorb_to_eq
  | H : context [xorb _ _ = true] |- _ => rewrite xorb_to_neq in H
  | |- context [xorb _ _ = true] => rewrite xorb_to_neq
  | H : context [xorb (xorb _ _) _] |- _ => rewrite xorb_assoc in H
  | |- context [xorb (xorb _ _) _] => rewrite xorb_assoc
(* XORB vs XORB *)
  | H : context [xorb ?b _ = xorb ?b _] |- _ => rewrite xorb_elim_l in H
  | |- context [xorb ?b _ = xorb ?b _] => rewrite xorb_elim_l
  | H : context [xorb _ ?b = xorb _ ?b] |- _ => rewrite xorb_elim_r in H
  | |- context [xorb _ ?b = xorb _ ?b] => rewrite xorb_elim_r
  | H : context [xorb ?b (xorb ?b _)] |- _ => rewrite xorb_b_xorb_b_b0 in H
  | |- context [xorb ?b (xorb ?b _)] => rewrite xorb_b_xorb_b_b0
  | H : context [xorb ?b (xorb _ ?b)] |- _ => rewrite xorb_b_xorb_b0_b in H
  | |- context [xorb ?b (xorb _ ?b)] => rewrite xorb_b_xorb_b0_b
(* simpl ANDB *)
  | H : context [?b && ?b] |- _ => rewrite andb_diag in H
  | |- context [?b && ?b] => rewrite andb_diag
  | H : context [andb _ true] |- _ => rewrite andb_true_r in H
  | |- context [andb _ true] => rewrite andb_true_r
  | H : context [andb true _] |- _ => rewrite andb_true_l in H
  | |- context [andb true ] => rewrite andb_true_l
  | H : context [andb _ false] |- _ => rewrite andb_false_r in H
  | |- context [andb _ false] => rewrite andb_false_r
  | H : context [andb false _] |- _ => rewrite andb_false_l in H
  | |- context [andb false _] => rewrite andb_false_l
(* ANDB vs NEGB *)
  | H : context [?b && (negb ?b)] |- _ => rewrite andb_negb_r in H
  | |- context [?b && (negb ?b)] => rewrite andb_negb_r
  | H : context [(negb ?b) && ?b] |- _ => rewrite andb_negb_l in H
  | |- context [(negb ?b) && ?b] => rewrite andb_negb_l
(* IF to logic *)
  | H : context[match ?a with true => ?b | false => ?c end] |- _ =>
    match b with
    | c => rewrite if_elim in H
    | true  => rewrite (rewrite_then_true  a c) in H
    | false => rewrite (rewrite_then_false a c) in H
    | _ =>
      match c with
      | true  => rewrite (rewrite_else_true  a b) in H
      | false => rewrite (rewrite_else_false a b) in H
      end
    end
  | |- context[match ?a with true => ?b | false => ?c end] =>
    match b with
    | c => rewrite if_elim
    | true  => rewrite (rewrite_then_true  a c)
    | false => rewrite (rewrite_then_false a c)
    | _ =>
      match c with
      | true  => rewrite (rewrite_else_true  a b)
      | false => rewrite (rewrite_else_false a b)
      end
    end
(* Annexes *)
  | |- context[?a && ?b = ?a && ?c] =>
    rewrite goal_andb_same_ll
end.

Ltac simplbool := simplprop; repeat (simplbool_step; simplprop).

Lemma bool_eq_iff_iff (b1 b2:bool) : b1 = b2 <-> (b1 = true <-> b2 = true).
Proof. dest_bool. Qed.

Lemma xorb_a_b_eq_c_l a b c :
  xorb a b = c <-> b = xorb a c.
Proof. dest_bool. Qed.

Lemma reverse_bool_eq a b : (a = negb b -> False) -> a = b.
Proof. dest_bool. Qed.

Lemma xorb_migration a b c : xorb a b = c <-> b = xorb a c.
Proof. dest_bool. Qed.

Lemma xorb_eq_negb_xorb_same_l b0 b1 b2 : xorb b0 b1 = negb (xorb b0 b2) <-> b1 = negb b2.
Proof. rewrite xorb_shift_l; simplbool. Qed.
#[export] Hint Rewrite xorb_eq_negb_xorb_same_l : simplprop1_rewrite.

Lemma xorb_lemma2 b0 b1 b2 b3 :
  xorb b0 (xorb b1 b2) = xorb b1 b3 <-> xorb b0 b2 = b3.
Proof. dest_bool. Qed.
#[export] Hint Rewrite xorb_lemma2 : simplprop1_rewrite.

Lemma rewrite_fun_bool {A} (f:bool -> A) : f = (fun x => if x then f true else f false).
Proof with simplbool.
apply functional_extensionality...
destruct x...
Qed.

(* $2019-04-23 *)
Ltac rewrite_fun_bool_step := match goal with
  | H : context[fun (x:bool) => @?f x] |- _ =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_fun_bool f) in H; simpl in H);
    idtac "normalized :" f
  | |- context[fun (x:bool) => @?f x] =>
    match type of f with
    | _ -> ?C => match C with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of C with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_fun_bool f); simpl);
    idtac "normalized :" f
  end.

Ltac rewrite_fun_bool := repeat rewrite_fun_bool_step.

Lemma fun_match_bool_converge {A} (cc:A) :
  (fun x : bool => if x then cc else cc) = (fun _ => cc).
Proof with simplbool.
apply functional_extensionality...
Qed.

Lemma bool_disjunction_as_Prop (b:bool) (P:bool -> Prop) :
  P b <->  (b = true -> P true) /\ (b = false -> P false).
Proof. dest_bool. Qed.

Lemma rewrite_if_as_Prop (A:Type) (P:A -> Prop) (b:bool) (if1 if0:A) :
  P (if b then if1 else if0) <-> (b = true -> P if1) /\ (b = false -> P if0).
Proof. dest_bool. Qed.

Ltac rewrite_if_as_Prop :=
  match goal with
  | H : context[if ?b then ?if1 else ?if0] |- _ =>
    pattern (if b then if1 else if0) in H;
    rewrite rewrite_if_as_Prop in H
  | |- context[if ?b then ?if1 else ?if0] =>
    pattern (if b then if1 else if0);
    rewrite rewrite_if_as_Prop
  end.

Lemma rewrite_if_as_Prop_indirect (A:Type) (P:A -> Prop) (b:bool) (if1 if0:A) :
  P (if b then if1 else if0) <-> (forall (a:A), ((b = true -> a = if1) /\ (b = false -> a = if0)) -> P a).
Proof with simplbool.
split...
- destruct b...
- apply H...
  destruct b...
Qed.

Ltac rewrite_if_as_Prop_indirect :=
match goal with
| |- context [if ?term then _ else _ ] =>
  let inner := get_inner_match term in
  match goal with
  | |- context [if inner then ?if1 else ?if0 ] =>
    pattern (if inner then if1 else if0);
    rewrite (rewrite_if_as_Prop_indirect _ _ inner if1 if0);
    intros
  end
| H: context [match ?term with _ => _ end] |- _ =>
  let inner := get_inner_match term in
  generalize dependent H;
  repeat (match goal with h : context[inner] |- _ => generalize dependent h end);
  match goal with
  | |- context [if inner then ?if1 else ?if0 ] =>
    pattern (if inner then if1 else if0);
    rewrite (rewrite_if_as_Prop_indirect _ _ inner if1 if0);
    intros
  end
end.

(* Section 3. Quantifier *)

Lemma neg_exist {A} (P:A -> Prop) : ~ (exists a, P a) -> forall a, ~(P a).
Proof with simplprop.
intros H a Pa...
apply H.
exists a...
Qed.

Lemma forall_true_as_neg_exist {A} (p:A -> bool) : (forall a, p a = true) <-> ~(exists a, p a = false).
Proof with simplbool.
split...
- intros [a0 H0].
  rewrite H in H0...
- specialize (neg_exist (fun a => p a = false) H)...
  specialize(H0 a)...
Qed.

Lemma forall_neg_eq {A} (p1 p2:A -> bool) : (forall a, negb(p1 a) = p2 a) <-> (forall a, p1 a = negb(p2 a)).
Proof. split; intros H a; specialize(H a); simplbool. Qed.

Lemma exist_neg_eq {A} (p1 p2:A -> bool) : (exists a, negb(p1 a) = p2 a) <-> (exists a, p1 a = negb(p2 a)).
Proof. split; intros [a Ha]; exists a;  simplbool. Qed.

Lemma forall_as_neg_exists {A} (p:A -> bool) b : (forall a, p a = b) <-> ~(exists a, p a = negb b).
Proof with simplprop.
destruct b.
- apply forall_true_as_neg_exist.
- specialize (forall_true_as_neg_exist (fun a => negb(p a)))...
  rewrite forall_neg_eq, exist_neg_eq in H...
Qed.