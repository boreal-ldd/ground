Require Import Arith.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.

Lemma fun_negb {A} f :
  (fun x : A => negb(f x)) = comp f negb.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @fun_negb : simplprop1_rewrite.

Lemma fun_fold_negb {A} f :
  (fun x : A => if (f x:bool) then false else true) = comp f negb.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @fun_fold_negb : simplprop1_rewrite.

Lemma fun_fold_ITE_id {A} f :
  (fun x : A => if (f x:bool) then true else false) = f.
Proof. apply functional_extensionality. simplbool. Qed.
#[export] Hint Rewrite @fun_fold_ITE_id : simplprop1_rewrite.

Lemma comp_negb_negb : comp negb negb = id.
Proof. apply functional_extensionality; lazy; simplbool. Qed.
#[export] Hint Rewrite comp_negb_negb : simplprop1_rewrite.

Lemma comp_f_negb_negb {A} (f:A->bool) :
  comp (comp f negb) negb = f.
Proof.
rewrite <- comp_assoc, comp_negb_negb, comp_id_r.
reflexivity.
Qed.
#[export] Hint Rewrite @comp_f_negb_negb : simplprop1_rewrite.
