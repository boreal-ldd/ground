Require Import SimplProp.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplBool.

Lemma lemma1 p f : p || f = f <-> ( p = true -> f = true).
Proof. dest_bool. Qed.

Lemma lemma2 p f : p && f = f <-> ( f = true -> p = true).
Proof. destruct f; simplbool. Qed.

(*
Definition min_or (p f:bool) : bool := xorb p f.

Lemma carac_min_or (p f:bool) : ( p = true -> f = true ) -> p || f = p || (min_or p f).
Proof. compute; simplbool. Qed.

Definition max_and (p f:bool) : bool := eqb p f.

Lemma carac_min_and (p f:bool) : ( f = true -> p = true ) -> p && f = p && (max_and p f).
Proof. compute; simplbool. Qed.

Lemma prop2_min_or (p f:bool) (H1: p = true -> f = true ) (H2: f = true) : ( p = true <-> min_or p f = false).
Proof. compute; simplbool. Qed.

Lemma prop2_max_and (p f:bool) (H1: f = true -> p = true ) (H2: f = false) : ( p = true <-> max_and p f = false).
Proof. compute; simplbool. Qed.

Lemma prop1_min_or (p f:bool) (H1:p = true -> f = true) : min_or p f = true -> f = true.
Proof. dest_bool. Qed.
 *)

Definition min_or (p f:bool) : bool :=  f && (negb p).

Lemma carac_min_or (p f:bool) : ( p = true -> f = true ) -> p || f = p || (min_or p f).
Proof. compute; simplbool. Qed.

Definition max_and (p f:bool) : bool := f || (negb p).

Lemma carac_min_and (p f:bool) : ( f = true -> p = true ) -> p && f = p && (max_and p f).
Proof. compute; simplbool. Qed.

Lemma prop2_min_or (p f:bool) (H1: p = true -> f = true ) (H2: f = true) : ( p = true <-> min_or p f = false).
Proof. compute; simplbool. Qed.

Lemma prop2_max_and (p f:bool) (H1: f = true -> p = true ) (H2: f = false) : ( p = true <-> max_and p f = false).
Proof. compute; simplbool. Qed.

Lemma prop1_min_or (p f:bool) (H1:p = true -> f = true) : min_or p f = true -> f = true.
Proof. dest_bool. Qed.

Lemma prop1_max_and (p f:bool) (H1:f = true -> p = true) : max_and p f = false -> f = false.
Proof. dest_bool. Qed.