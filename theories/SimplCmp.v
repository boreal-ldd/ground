Require Import Arith.
Require Import Coq.Logic.JMeq.
Require Import Coq.Bool.Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplEq.
Require Import SimplOption.

(* Definition of ord, cmp and cmp_P
  - cmp_P : states that a comparisons [cmp] is
    + compatible with =
    + reflexive
    + transitive
    + asymmetric
 *)

Inductive ord : Type := Lt | Eq | Gt.

Definition cmp A : Type := A -> A -> ord.

Definition cmp_Eq_iff_eq {A} (cmpA:cmp A) :=
  forall x y, cmpA x y = Eq <-> x = y.

Definition cmp_refl {A} (cmpA:cmp A) := forall x, cmpA x x = Eq.

Lemma cmp_Eq_iff_eq_implies_cmp_refl {A} (cmpA:cmp A) : cmp_Eq_iff_eq cmpA -> cmp_refl cmpA.
Proof with simplprop.
unfold cmp_Eq_iff_eq, cmp_refl...
specialize(H x x)...
Qed.

Definition cmp_Lt_trans {A} (cmpA:cmp A) :=
  forall x y z, cmpA x y = Lt -> cmpA y z = Lt -> cmpA x z = Lt.

Definition asym x := match x with
| Lt => Gt
| Eq => Eq
| Gt => Lt
end.

Definition cmp_asym {A} (cmpA:cmp A) := forall x y, asym(cmpA x y) = cmpA y x.

Record cmp_P {A} (cmpA:cmp A) : Prop :=
{
  cmp_p_eq       : cmp_Eq_iff_eq cmpA;
  cmp_p_lt_trans : cmp_Lt_trans cmpA;
  cmp_p_asym     : cmp_asym cmpA
}.

(* compare composition *)

Definition cmp_comp x1 x2 := match x1 with
| Lt => Lt
| Eq => x2
| Gt => Gt
end.


Lemma Lt_eq_Lt : Lt = Lt <-> True. Proof. simplprop. Qed.
Lemma Eq_eq_Eq : Eq = Eq <-> True. Proof. simplprop. Qed.
Lemma Gt_eq_Gt : Gt = Gt <-> True. Proof. simplprop. Qed.
Lemma Lt_eq_Eq : Lt = Eq <-> False. Proof. solve_by_invert. Qed.
Lemma Lt_eq_Gt : Lt = Gt <-> False. Proof. solve_by_invert. Qed.
Lemma Eq_eq_Lt : Eq = Lt <-> False. Proof. solve_by_invert. Qed.
Lemma Eq_eq_Gt : Eq = Gt <-> False. Proof. solve_by_invert. Qed.
Lemma Gt_eq_Lt : Gt = Lt <-> False. Proof. solve_by_invert. Qed.
Lemma Gt_eq_Eq : Gt = Eq <-> False. Proof. solve_by_invert. Qed.

#[export] Hint Rewrite Lt_eq_Lt : simplprop1_rewrite.
#[export] Hint Rewrite Eq_eq_Eq : simplprop1_rewrite.
#[export] Hint Rewrite Gt_eq_Gt : simplprop1_rewrite.
#[export] Hint Rewrite Lt_eq_Eq : simplprop1_rewrite.
#[export] Hint Rewrite Lt_eq_Gt : simplprop1_rewrite.
#[export] Hint Rewrite Eq_eq_Lt : simplprop1_rewrite.
#[export] Hint Rewrite Eq_eq_Gt : simplprop1_rewrite.
#[export] Hint Rewrite Gt_eq_Lt : simplprop1_rewrite.
#[export] Hint Rewrite Gt_eq_Eq : simplprop1_rewrite.

Lemma cmp_asym_comp x1 x2 : asym (cmp_comp x1 x2) = cmp_comp(asym x1)(asym x2).
Proof. destruct x1, x2; reflexivity. Qed.

Lemma cmp_comp_rewrite x1 x2 x :
cmp_comp x1 x2 = x <-> match x with
| Lt => x1 = Lt \/ (x1 = Eq /\ x2 = Lt)
| Eq => x1 = Eq /\ x2 = Eq
| Gt => x1 = Gt \/ (x1 = Eq /\ x2 = Gt)
end.
Proof with simplprop.
unfold cmp_comp.
dest_match; split; simplprop; try solve_by_invert; destruct H; try solve_by_invert.
Qed.

Lemma cmp_comp_Eq x1 x2 : cmp_comp x1 x2 = Eq <-> x1 = Eq /\ x2 = Eq.
Proof with simplprop.
split...
unfold cmp_comp in H.
dest_match.
Qed.

(* w.l.o.g Lt *)

Theorem wlog_Lt {A} (cmpA:cmp A)
  (CEq:cmp_Eq_iff_eq cmpA)
  (CAsym:cmp_asym cmpA)
  (P:A->A->Prop)
  (S:forall a1 a2, P a1 a2 -> P a2 a1)
  (PLt:forall a1 a2, cmpA a1 a2 = Lt -> P a1 a2)
  (PEq:forall a, P a a) : forall a1 a2, P a1 a2.
Proof with auto.
intros.
destruct (cmpA a1 a2) eqn:E.
- apply PLt...
- rewrite (CEq _ _) in E; subst.
  apply PEq.
- apply (apply_f asym) in E.
  rewrite (CAsym _ _) in E.
  simpl in E.
  apply S.
  apply PLt...
Qed.

(* comparisons [bool] *)
Require Import SimplBool.

Definition cmp_bool : cmp bool := (fun b1 b2 =>
match b1, b2 with
  | false, false => Eq
  | false, true  => Lt
  | true , false => Gt
  | true , true  => Eq
end).

Lemma cmp_bool_diag b : cmp_bool b b = Eq.
Proof. dest_bool. Qed.

Lemma cmp_bool_asym t1 t2 : asym(cmp_bool t1 t2) = cmp_bool t2 t1.
Proof. dest_bool. Qed.

Lemma cmp_P_bool : cmp_P cmp_bool.
Proof. constructor; intro x; dest_bool. Qed.

Lemma cmp_bool_rewrite b1 b2 x :
cmp_bool b1 b2 = x <-> match x with
  | Lt => b1 = false /\ b2 = true
  | Eq => b1 = b2
  | Gt => b1 = true /\ b2 = false
end.
Proof.
unfold cmp_bool.
dest_match; simplbool; solve_by_invert.
Qed.

(* Section 2. Partial Order *)

Definition pcmp_t (A:Type) : Type := A -> A -> option ord.
Definition pcmp_reflexive {A} (p:pcmp_t A) : Prop :=
  forall x y, p x y = Some Eq <-> x = y.
Definition pcmp_transitive {A} (p:pcmp_t A) : Prop :=
  forall x y z, p x y = Some Lt -> p y z = Some Lt -> p x z = Some Lt.
Definition opasym (oo:option ord) : option ord := opmap asym oo.
Definition pcmp_asymmetric {A} (p:pcmp_t A) : Prop :=
  forall x y, p y x = opasym (p x y).

Record pcmp_P {A} (p:pcmp_t A) : Prop :=
{
  pcmp_refl  : pcmp_reflexive p;
  pcmp_trans : pcmp_transitive p;
  pcmp_asym  : pcmp_asymmetric p;
}.

(* Section 3. Order on Order *)

Definition beq_ord (o1 o2:ord) : bool :=
match o1, o2 with
| Lt, Lt => true
| Eq, Eq => true
| Gt, Gt => true
|  _, _  => false
end.

Definition cmp_ord (o1 o2:ord) : ord :=
match o1, o2 with
| Lt, Lt => Eq
| Lt, _  => Lt
|  _, Lt => Gt
| Eq, Eq => Eq
| Eq, _  => Lt
|  _, Eq => Gt
| Gt, Gt => Eq
end.

Lemma beq_iff_true_beq_ord : beq_iff_true beq_ord.
Proof with simploption1.
intros x y...
destruct x, y...
Qed.

Lemma cmp_Eq_iff_eq_cmp_ord : cmp_Eq_iff_eq cmp_ord.
Proof with simploption1.
intros x y.
destruct x, y...
Qed.

Lemma cmp_Lt_trans_cmp_ord : cmp_Lt_trans cmp_ord.
Proof with simploption1.
intros x y z.
destruct x, y, z...
Qed.

Lemma cmp_asym_cmp_ord : cmp_asym cmp_ord.
Proof with simploption1.
intros x y.
destruct x, y...
Qed.

Lemma cmp_P_cmp_ord : cmp_P cmp_ord.
Proof with simploption1.
constructor.
- apply cmp_Eq_iff_eq_cmp_ord.
- apply cmp_Lt_trans_cmp_ord.
- apply cmp_asym_cmp_ord.
Qed.

Lemma beq_ord_cmp_using_cmp_P {A} (cmp:cmp A) (H:cmp_P cmp) x y :
  beq_ord (cmp x y) Eq = false <-> x <> y.
Proof with simploption1.
destruct H as [RA TA AA].
split...
- apply cmp_Eq_iff_eq_implies_cmp_refl in RA.
  rewrite (RA _) in H...
- apply reverse_bool_eq...
  rewrite (beq_iff_true_beq_ord _ _) in H0...
  rewrite (RA _ _) in H0...
Qed.

Lemma not_neq_using_cmp_P {A} (cmp:cmp A) (C:cmp_P cmp) (x y:A) : (x <> y -> False) <-> x = y.
Proof with simploption1.
split...
destruct(beq_ord (cmp x y) Eq) eqn:E...
- rewrite (beq_iff_true_beq_ord _ _) in E.
  destruct C as [RA TA AA].
  rewrite (RA _ _) in E...
- rewrite(beq_ord_cmp_using_cmp_P cmp C) in E...
Qed.

Definition cmp_prod {A B} (cA:cmp A) (cB:cmp B) (x y:(A * B)) : ord :=
  cmp_comp (cA(fst x)(fst y)) (cB(snd x)(snd y)).

Lemma rewrite_cmp_comp_trans {A B} (cA:cmp A) (cB:cmp B) a0 a1 a2 b0 b1 b2 :
  cmp_Eq_iff_eq cA -> cmp_asym cA -> cmp_Eq_iff_eq cB ->
  (cmp_comp (cA a0 a1) (cB b0 b1) = Lt ->
    cmp_comp (cA a1 a2) (cB b1 b2) = Lt ->
    cmp_comp (cA a0 a2) (cB b0 b2) = Lt) <->
  ((cA a0 a1 = Lt -> cA a1 a2 = Lt -> cA a0 a2 = Lt) /\
   (cA a0 a1 = Eq -> cA a1 a2 = Eq ->
      cB b0 b1 = Lt -> cB b1 b2 = Lt -> cB b0 b2 = Lt)).
Proof with simploption1.
intros RA AA RB.
destruct(cA a0 a1)eqn:A01...
- destruct(cA a1 a2)eqn:A12...
  + destruct(cA a0 a2)eqn:A02...
    rewrite (RA _) in A02...
    rewrite <- (AA _) in A12...
    rewrite_subst.
  + rewrite (RA _) in A12...
    rewrite A01...
- rewrite (RA _) in A01...
  destruct(cA a1 a2)eqn:A12...
Qed.

Lemma cmp_P_cmp_prod {A B} cA cB (HA:cmp_P cA) (HB:cmp_P cB) : cmp_P(@cmp_prod A B cA cB).
Proof with simploption1.
split...
- unfold cmp_Eq_iff_eq, cmp_prod...
  destruct x as [xA xB], y as [yA yB]...
  rewrite cmp_comp_Eq...
  destruct HA as [RA TA AA], HB as [RB TB AB]...
  rewrite (RA _ _), (RB _ _)...
- unfold cmp_Lt_trans, cmp_prod; intros x y z H1 H2.
  destruct HA as [RA TA AA], HB as [RB TB AB]...
  generalize dependent H2.
  generalize dependent H1.
  rewrite rewrite_cmp_comp_trans...
  destruct x as [xA xB], y as [yA yB], z as [zA zB]...
  specialize(TA xA yA zA)...
  specialize(TB xB yB zB)...
- unfold cmp_asym, cmp_prod...
  destruct x as [xA xB], y as [yA yB]...
  rewrite cmp_asym_comp.
  destruct HA as [RA TA AA], HB as [RB TB AB]...
  rewrite (AA _ _), (AB _ _)...
Qed.


Lemma asym_shift o1 o2 : asym o1 = o2 <-> o1 = asym o2.
Proof. destruct o1, o2; simplprop. Qed.
#[export] Hint Rewrite asym_shift : simplprop1_rewrite.

Lemma neg_neq_ord (o1 o2:ord) : (o1 <> o2 -> False) <-> o1 = o2.
Proof with simplprop.
destruct o1, o2...
Qed.
#[export] Hint Rewrite neg_neq_ord : simplprop1_rewrite.

Lemma cmp_eq_Lt_implies_neq {A} (cmp:cmp A) (cmpP:cmp_P cmp) x y : cmp x y = Lt -> x <> y.
Proof with simploption1.
intros H1 H2...
destruct cmpP.
rewrite (cmp_Eq_iff_eq_implies_cmp_refl _ cmp_p_eq0 y) in H1...
Qed.

Lemma cmp_eq_Gt_implies_neq {A} (cmp:cmp A) (cmpP:cmp_P cmp) x y : cmp x y = Gt -> x <> y.
Proof with simploption1.
intros H1 H2...
destruct cmpP.
rewrite (cmp_Eq_iff_eq_implies_cmp_refl _ cmp_p_eq0 y) in H1...
Qed.

Lemma cmp_P_implies_eq_dec {A} (cmp:cmp A) (cmpP:cmp_P cmp) (x y:A) : x = y \/ x <> y.
Proof with simploption1.
destruct cmpP.
specialize(cmp_Eq_iff_eq_implies_cmp_refl _ cmp_p_eq0 x) as cmp_refl.
destruct(cmp x y)eqn:E0...
- right; intro E1...
  rewrite_subst.
- rewrite (cmp_p_eq0 _) in E0...
- right; intro E1...
  rewrite_subst.
Qed.

Lemma neg_neq_with_cmp_P {A} cmp (cmpP:@cmp_P A cmp) (x y : A) : (x <> y -> False) <-> x = y.
Proof with simploption1.
split...
destruct(cmp_P_implies_eq_dec cmp cmpP x y)...
Qed.

(* Section 4. minsat *)

Definition is_min_positive {A} (cmp:cmp A) (F:A -> bool) (a0:A) : Prop :=
  F a0 = true /\ (forall a1, F a1 = true -> a1 <> a0 -> (* a1 > a0 \/ a1 = a0 *) cmp a0 a1 = Lt).

Definition is_min_negative {A} (cmp:cmp A) (F:A -> bool) (a0:A) : Prop :=
  F a0 = true /\ (forall a1, (* a1 < a0 -> F a1 = false *) cmp a1 a0 = Lt -> F a1 = false).

Lemma is_min_positive_iff_is_min_negative {A} (cmp:cmp A) (cmpP:cmp_P cmp) F a0 :
  is_min_positive cmp F a0 <-> is_min_negative cmp F a0.
Proof with simplbool.
unfold is_min_positive, is_min_negative...
split...
- destruct(F a1)eqn:E0...
  specialize(H0 _ E0)...
  destruct cmpP...
  rewrite <- cmp_p_asym0 in H1...
  rewrite H1 in H0...
  rewrite <- (cmp_p_eq0 a1 a0) in H0...
  rewrite <- cmp_p_asym0 in H1...
  rewrite H0 in H1...
- destruct(cmp a0 a1)eqn:E0...
  + destruct cmpP...
    apply cmp_p_eq0 in E0...
  + rewrite <- cmp_p_asym in E0...
    specialize(H0 _ E0)...
Qed.

Definition minsat_positive {A} (cmp:cmp A) (minsat:(A -> bool) -> option A) : Prop :=
  (forall (F:A -> bool) a0, minsat F = Some a0 <-> is_min_positive cmp F a0) /\
  (forall (F:A -> bool) a0, F a0 = true -> isSome(minsat F) = true).

Definition minsat_alternative {A} (cmp:cmp A) (minsat:(A -> bool) -> option A) : Prop :=
  forall (F:A -> bool) v, minsat F = v <->
    match v with
    | None => F = fun _ => false
    | Some a0 => is_min_positive cmp F a0
    end.

Lemma is_min_positive_false {A} (cmp:cmp A) a : is_min_positive cmp (fun _ : A => false) a <-> False.
Proof. unfold is_min_positive; simploption1. Qed.
#[export] Hint Rewrite @is_min_positive_false : simplprop1_rewrite.

Lemma minsat_positive_iff_minsat_alternative {A} (cmp:cmp A) (minsat:(A -> bool) -> option A) :
  minsat_positive cmp minsat <-> minsat_alternative cmp minsat.
Proof with simploption1.
unfold minsat_positive, minsat_alternative...
split...
- split...
  + dest_match_step...
    * rewrite H in D...
    * apply functional_extensionality...
      generalize dependent x.
      rewrite forall_as_neg_exists; intros [a0 Ha0]...
      apply H0 in Ha0...
  + dest_match_step...
    * rewrite <- H in H1...
    * destruct(minsat (fun _ => false))eqn:E0...
      rewrite H in E0...
- split...
  + rewrite H...
  + destruct(minsat F)eqn:E0...
    rewrite H in E0...
Qed.

Definition minsat_weak {A} (cmp:cmp A) (minsat:(A -> bool) -> option A) : Prop :=
  forall (F:A -> bool),
    match minsat F with
    | None => F = fun _ => false
    | Some a0 => is_min_positive cmp F a0
    end.

Lemma is_min_positive_unique {A} cmp (cmpP:cmp_P cmp) F x y :
  @is_min_positive A cmp F x -> is_min_positive cmp F y -> x = y.
Proof with simploption1.
unfold is_min_positive...
specialize(H1 x)...
specialize(H2 y)...
apply (neg_neq_with_cmp_P cmp cmpP)...
rewrite neq_sym in H2...
destruct cmpP.
rewrite <- cmp_p_asym0 in H1...
rewrite_subst.
Qed.

Lemma minsat_weak_iff_minsat_alternative {A} (cmp:cmp A) (cmpP:cmp_P cmp) minsat :
  minsat_weak cmp minsat <-> minsat_alternative cmp minsat.
Proof with simploption1.
unfold minsat_alternative, minsat_weak...
split...
- specialize(H F) as HF.
  split...
  destruct(minsat F)eqn:E...
  + dest_match_step...
    specialize(is_min_positive_unique _ cmpP _ _ _ HF H0)...
  + dest_match_step...
- remember (minsat F) as v eqn:E.
  symmetry in E.
  rewrite H in E...
Qed.

Lemma minsat_weak_iff_minsat_positive {A} (cmp:cmp A) (cmpP:cmp_P cmp) minsat :
  minsat_weak cmp minsat <-> minsat_positive cmp minsat.
Proof with simploption1.
rewrite minsat_weak_iff_minsat_alternative...
rewrite minsat_positive_iff_minsat_alternative...
Qed.

Definition minsat_t (A:Type) := (A -> bool) -> option A.

Definition minsat_prod {A B} (minsat_A:minsat_t A) (minsat_B:minsat_t B) (p:(A * B) -> bool) : option(A * B) :=
  match minsat_A (fun a => isSome(minsat_B (fun b => p(a, b)))) with
  | Some a0 =>
    match minsat_B (fun b => p(a0, b)) with
    | Some b0 => Some(a0, b0)
    | None => None (* assert false *)
    end
  | None => None
  end.

Lemma minsat_weak_minsat_prod {A B} (minsat_A:minsat_t A) (minsat_B:minsat_t B)
  (cmpA:cmp A) (cmpA_P:cmp_P cmpA) (cmpB:cmp B) (cmpB_P:cmp_P cmpB)
  (minsat_A_positive:minsat_positive cmpA minsat_A)
  (minsat_B_positive:minsat_positive cmpB minsat_B) :
  let cmpAB := cmp_prod cmpA cmpB in
  minsat_weak cmpAB (minsat_prod minsat_A minsat_B).
Proof with simploption1.
intro cmpAB...
assert(cmpAB_P:cmp_P cmpAB).
{ unfold cmpAB. apply cmp_P_cmp_prod... }
unfold minsat_weak, minsat_prod...
destruct minsat_A_positive as [SA NA].
destruct minsat_B_positive as [SB NB].
dest_match_step; [ dest_match_step | ]...
- rewrite SA in D...
  rewrite SB in D0...
  unfold is_min_positive in *...
  destruct a1 as [a' b']...
  specialize(H2 a')...
  destruct(cmpAB (a, b) (a', b'))eqn:E1...
  + destruct cmpAB_P as [RAB TAB AAB].
    rewrite (RAB _ _) in E1...
  + unfold cmpAB, cmp_prod in E1...
    specialize(NB (fun b => F(a', b)) _ H3)...
    destruct(cmpA a a')eqn:E2...
    * destruct cmpA_P as [RA TA AA].
      rewrite (RA _ _) in E2...
      specialize(H0 b')...
      rewrite H0 in E1...
    * rewrite (not_neq_using_cmp_P _ cmpA_P) in H2...
      destruct cmpA_P as [RA TA AA].
      rewrite (cmp_Eq_iff_eq_implies_cmp_refl _ RA) in E2...
- rewrite SA in D. unfold is_min_positive in D...
- apply functional_extensionality...
  apply reverse_bool_eq...
  destruct x as [xa xb].
  specialize(NA (fun a : A => isSome (minsat_B (fun b : B => F (a, b)))) xa)...
  specialize(NB (fun b : B => F (xa, b)) xb)...
Qed.

Theorem minsat_positive_minsat_prod {A B} (minsat_A:minsat_t A) (minsat_B:minsat_t B)
  (cmpA:cmp A) (cmpA_P:cmp_P cmpA) (cmpB:cmp B) (cmpB_P:cmp_P cmpB)
  (minsat_A_positive:minsat_positive cmpA minsat_A)
  (minsat_B_positive:minsat_positive cmpB minsat_B) :
  let cmpAB := cmp_prod cmpA cmpB in
  minsat_positive cmpAB (minsat_prod minsat_A minsat_B).
Proof with simploption1.
intro.
rewrite <- minsat_weak_iff_minsat_positive.
- apply minsat_weak_minsat_prod; assumption.
- unfold cmpAB; apply cmp_P_cmp_prod; assumption.
Qed.

(* Section 4. maxsat *)

Definition is_max_positive {A} (cmp:cmp A) (F:A -> bool) (a0:A) : Prop :=
  F a0 = true /\ (forall a1, F a1 = true -> ~(cmp a0 a1 = Lt)).

Definition is_max_negative {A} (cmp:cmp A) (F:A -> bool) (a0:A) : Prop :=
  F a0 = true /\ (forall a1, (* a1 > a0 -> F a1 = false *) cmp a1 a0 = Gt -> F a1 = false).

Definition cmp_rev {A} (cmpA:cmp A) : cmp A := (fun x y => asym(cmpA x y)).

Lemma asym_asym o : asym(asym o) = o. Proof. destruct o; reflexivity. Qed.
#[export] Hint Rewrite @asym_asym : simplprop1_rewrite.

Lemma cmp_rev_cmp_rev {A} (cmpA:cmp A) : cmp_rev(cmp_rev cmpA) = cmpA.
Proof. repeat (apply functional_extensionality; intro); unfold cmp_rev; simploption1. Qed.
#[export] Hint Rewrite @cmp_rev_cmp_rev : simplprop1_rewrite.

Lemma cmp_P_cmp_rev {A} (cmpA:cmp A) : cmp_P cmpA -> cmp_P (cmp_rev cmpA).
Proof with simploption1.
intros [RA TA AA]; constructor.
- intros x y; unfold cmp_rev...
- intros x y z; unfold cmp_rev in *...
  simpl in *.
  specialize(TA z y x).
  rewrite <- (AA z x).
  rewrite <- (AA y x) in *.
  rewrite <- (AA z y) in *...
- unfold cmp_rev, cmp_asym...
Qed.

Lemma is_max_positive_iff_is_min_positive_cmp_asym {A} (cmpA:cmp A) (cmpP:cmp_P cmpA) (F:A -> bool) (a0:A) :
  is_max_positive cmpA F a0 <-> is_min_positive (cmp_rev cmpA) F a0.
Proof with simploption1.
unfold is_max_positive, is_min_positive...
unfold cmp_rev...
split; intros H1 a1 HF1; simploption1; specialize(H1 a1)...
- destruct(cmpA a0 a1)eqn:E0...
  rewrite ((cmp_p_eq _ cmpP) _ _) in E0...
- destruct(cmpA a0 a1)eqn:E0...
  rewrite (neg_neq_with_cmp_P _ cmpP) in H1...
  rewrite (cmp_Eq_iff_eq_implies_cmp_refl _ (cmp_p_eq _ cmpP) _) in E0...
Qed.

Lemma is_min_positive_iff_is_max_positive_cmp_asym {A} (cmpA:cmp A) (cmpP:cmp_P cmpA) (F:A -> bool) (a0:A) :
  is_min_positive cmpA F a0 <-> is_max_positive (cmp_rev cmpA) F a0.
Proof. rewrite is_max_positive_iff_is_min_positive_cmp_asym; simploption1; apply (cmp_P_cmp_rev _ cmpP). Qed.

Lemma is_max_negative_iff_is_min_negative_cmp_asym {A} (cmpA:cmp A) (cmpP:cmp_P cmpA) (F:A -> bool) (a0:A) :
  is_max_negative cmpA F a0 <-> is_min_negative (cmp_rev cmpA) F a0.
Proof with simploption1.
unfold is_max_negative, is_min_negative...
unfold cmp_rev...
split; intros H1 a1 HF1; simploption1; specialize(H1 a1)...
Qed.

Lemma is_min_negative_iff_is_max_negative_cmp_asym {A} (cmpA:cmp A) (cmpP:cmp_P cmpA) (F:A -> bool) (a0:A) :
  is_min_negative cmpA F a0 <-> is_max_negative (cmp_rev cmpA) F a0.
Proof. rewrite is_max_negative_iff_is_min_negative_cmp_asym; simploption1; apply (cmp_P_cmp_rev _ cmpP). Qed.


Lemma is_max_positive_iff_is_max_negative {A} (cmp:cmp A) (cmpP:cmp_P cmp) F a0 :
  is_max_positive cmp F a0 <-> is_max_negative cmp F a0.
Proof with simplbool.
rewrite is_max_positive_iff_is_min_positive_cmp_asym...
rewrite is_max_negative_iff_is_min_negative_cmp_asym...
apply is_min_positive_iff_is_min_negative...
apply (cmp_P_cmp_rev _ cmpP).
Qed.

Definition maxsat_positive {A} (cmp:cmp A) (maxsat:(A -> bool) -> option A) : Prop :=
  (forall (F:A -> bool) a0, maxsat F = Some a0 <-> is_max_positive cmp F a0) /\
  (forall (F:A -> bool) a0, F a0 = true -> isSome(maxsat F) = true).

Definition maxsat_alternative {A} (cmp:cmp A) (maxsat:(A -> bool) -> option A) : Prop :=
  forall (F:A -> bool) v, maxsat F = v <->
    match v with
    | None => F = fun _ => false
    | Some a0 => is_max_positive cmp F a0
    end.

Lemma is_max_positive_false {A} (cmp:cmp A) a : is_max_positive cmp (fun _ : A => false) a <-> False.
Proof. unfold is_max_positive; simploption1. Qed.
#[export] Hint Rewrite @is_max_positive_false : simplprop1_rewrite.

Lemma maxsat_positive_iff_maxsat_alternative {A} (cmp:cmp A) (maxsat:(A -> bool) -> option A) :
  maxsat_positive cmp maxsat <-> maxsat_alternative cmp maxsat.
Proof with simploption1.
unfold maxsat_positive, maxsat_alternative...
split...
- split...
  + dest_match_step...
    * rewrite H in D...
    * apply functional_extensionality...
      generalize dependent x.
      rewrite forall_as_neg_exists; intros [a0 Ha0]...
      apply H0 in Ha0...
  + dest_match_step...
    * rewrite <- H in H1...
    * destruct(maxsat (fun _ => false))eqn:E0...
      rewrite H in E0...
- split...
  + rewrite H...
  + destruct(maxsat F)eqn:E0...
    rewrite H in E0...
Qed.

Definition maxsat_weak {A} (cmp:cmp A) (maxsat:(A -> bool) -> option A) : Prop :=
  forall (F:A -> bool),
    match maxsat F with
    | None => F = fun _ => false
    | Some a0 => is_max_positive cmp F a0
    end.

Lemma is_max_positive_unique {A} cmp (cmpP:cmp_P cmp) F x y :
  @is_max_positive A cmp F x -> is_max_positive cmp F y -> x = y.
Proof with simploption1.
(repeat rewrite is_max_positive_iff_is_min_positive_cmp_asym)...
apply (is_min_positive_unique _ (cmp_P_cmp_rev _ cmpP) _ _ _ H H0).
Qed.

Lemma maxsat_weak_iff_maxsat_alternative {A} (cmp:cmp A) (cmpP:cmp_P cmp) maxsat :
  maxsat_weak cmp maxsat <-> maxsat_alternative cmp maxsat.
Proof with simploption1.
unfold maxsat_alternative, maxsat_weak...
split...
- specialize(H F) as HF.
  split...
  destruct(maxsat F)eqn:E...
  + dest_match_step...
    specialize(is_max_positive_unique _ cmpP _ _ _ HF H0)...
  + dest_match_step...
- remember (maxsat F) as v eqn:E.
  symmetry in E.
  rewrite H in E...
Qed.

Lemma maxsat_weak_iff_maxsat_positive {A} (cmp:cmp A) (cmpP:cmp_P cmp) minsat :
  maxsat_weak cmp minsat <-> maxsat_positive cmp minsat.
Proof with simploption1.
rewrite maxsat_weak_iff_maxsat_alternative...
rewrite maxsat_positive_iff_maxsat_alternative...
Qed.

Lemma rewrite_match_ord_eq (o:ord) (P:ord -> Prop) :
  P o <-> ((o = Lt -> P Lt) /\ (o = Eq -> P Eq) /\ (o = Gt -> P Gt)).
Proof. destruct o; simploption1. Qed.

Definition cmp_sig {A} (P:A -> Prop) (cA:cmp A) (x y:sig P) : ord := cA(proj1_sig x)(proj1_sig y).

Lemma cmp_P_sig {A} (P:A -> Prop) (cA:cmp A) (PA:@cmp_P A cA) : @cmp_P (sig P) (cmp_sig P cA).
Proof with simploption1.
unfold cmp_sig.
destruct PA as [RA TA AA].
constructor; intros [x Hx] [y Hy]; try (intros [z Hz])... 
apply (TA x y z)...
Qed.