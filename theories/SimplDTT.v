Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Ltac simpldtt1 :=
  simplmore;
  repeat (simpldtt1_step ||
    (progress (autorewrite with simpldtt1_rewrite in * )); simplmore).

Ltac elim_exist := match goal with
  | HH : context[exist ?A ?B ?P1] |- context[exist ?A ?B ?P2] =>
    match P1 with
    | P2 => fail
    | _ =>
      idtac "Elim Exist{HH:="HH"; A:="A"; B:="B"; P1:="P1"; P2:=" P2"}";
      let PI := fresh "PI" in
      specialize(proof_irrelevance _ P1 P2) as PI; subst
    end
  end.

Ltac eq_rect_unify :=
  match goal with
  | H: context [eq_rect ?A ?P ?px0 ?py0 ?E1]
    |- context [eq_rect ?A ?P ?px0 ?py0 ?E2] =>
    match E2 with
    | E1 => fail 1
    | _ =>
      destruct (proof_irrelevance _ E1 E2)
    end
  | |- context [eq_rect ?A ?P ?px0 ?py0 ?E1] =>
    match goal with |- context [eq_rect ?A ?P ?px0 ?py0 ?E2] =>
      match E2 with
      | E1 => fail 1
      | _ =>
        destruct (proof_irrelevance _ E1 E2)
      end
    end
  end.

Lemma apply_both A B (f1 f2:A -> B) (x:A) : f1 = f2 -> f1 x = f2 x.
Proof. intros; subst; reflexivity. Qed.

Ltac apply_both_as H x HH :=
  specialize (apply_both _ _ _ _ x H) as HH.

Ltac apply_both H x :=
  let HH := fresh H in
  apply_both_as H x HH.

Lemma fun_PI (P:Prop) (p:P) A (T : P -> A) :
  T = (fun _ => T p).
Proof with simpldtt1.
apply functional_extensionality...
specialize(proof_irrelevance _ p x)...
Qed.

Lemma reject_false_with_true
  (b:bool) (H:b = true) (B:Type) (T: b = false -> B) (X:B) :
    T = (fun _ => X).
Proof with simpldtt1.
apply functional_extensionality...
Qed.

Lemma reject_true_with_false
  (b:bool) (H:b = false) (B:Type) (T: b = true -> B) (X:B) :
    T = (fun _ => X).
Proof with simpldtt1.
apply functional_extensionality...
Qed.

Lemma if_dtt (c:bool) (R1:Type) (si1: c = true -> R1) (si0: c = false -> R1)
  (CTX: R1 -> Prop) :
    CTX ((if c as b return (c = b -> R1) then si1 else si0) eq_refl) <->
      (forall (p:(c = true)), CTX (si1 p)) /\
      (forall (p:(c = false)), CTX (si0 p)).
Proof with simpldtt1.
destruct c eqn:C; repeat split; simpldtt1;
  assert(p = eq_refl); simpldtt1;
  apply proof_irrelevance...
Qed.


Lemma if_dtt_R1 (c:bool) (R1:bool -> Type) (si1:R1 true) (si0:R1 false) (CTX: forall b, R1 b -> Prop) :
    CTX c (if c as b return R1 b then si1 else si0) <->
      (c = true -> CTX true si1) /\
      (c = false -> CTX false si0).
Proof with simpldtt1.
destruct c...
Qed.


Lemma assumed_hypothesis (P:Prop) (H:P) (F:P -> Prop) :
  (forall p:P, F p) <-> F H.
Proof with simpldtt1.
split...
assert(p = H)...
apply proof_irrelevance.
Qed.

Lemma disgard_false_with_true b (H:b = true) (F:b = false -> Prop) :
  (forall p:(b = false), F p) <-> True.
Proof. simpldtt1. Qed.

Lemma disgard_true_with_false b (H:b = false) (F:b = true -> Prop) :
  (forall p:(b = true), F p) <-> True.
Proof. simpldtt1. Qed.

Ltac simpldtt2_step :=
  match goal with
  | H : ?b = true |- context[(forall p:(?b = false), @?F p)] =>
    rewrite (disgard_false_with_true b H F)
  | H : ?b = false |- context[(forall p:(?b = true), @?F p)] =>
    rewrite (disgard_true_with_false b H F)
  | H1 : ?P, H2 : ?P |- _ =>
    match type of P with
    | Prop =>
      let PI := fresh "PI" in
      specialize(proof_irrelevance P H1 H2) as PI;
      subst
    end
  | H : ?P |- context[(forall p:?P, @?F p)] =>
    match type of P with
    | Prop => rewrite (assumed_hypothesis P H F)
    end
  end.

Ltac simpldtt2 := simpldtt1; repeat (simpldtt2_step; simpldtt1).

Ltac rewrite_dtt_if :=
  match goal with
  | |- context[(if ?c
          then (fun (E1: ?c = true ) => @?si1 E1)
          else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] =>
      idtac "rewrite if_dtt";
      idtac "if"c;
      idtac "then"si1;
      idtac "else"si0;
      pattern((if c as _ return _ then si1 else si0) eq_refl);
      rewrite if_dtt
  | H: context[(if ?c
          then (fun (E1: ?c = true ) => @?si1 E1)
          else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] |- _ =>
      generalize dependent H;
      idtac "rewrite if_dtt";
      idtac "if"c;
      idtac "then"si1;
      idtac "else"si0;
      pattern((if c as _ return _ then si1 else si0) eq_refl);
      rewrite if_dtt
(*
  | H: context[(if ?c
          then (fun (E1: ?c = true ) => @?si1 E1)
          else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] |- _ =>
      idtac "rewrite if_dtt";
      idtac "if"c;
      idtac "then"si1;
      idtac "else"si0;
      pattern((if c as _ return _ then si1 else si0) eq_refl) in H;
      rewrite if_dtt in H
 *)
  end.

Lemma if_true_dtt (c:bool) (R:Type) (si1: c = true -> R) (si0: c = false -> R)
  (H:c = true) : (if c as b return (c = b -> R) then si1 else si0) eq_refl = si1 H.
Proof. simpldtt2. Qed.

Lemma if_false_dtt (c:bool) (R:Type) (si1: c = true -> R) (si0: c = false -> R)
  (H:c = false) : (if c as b return (c = b -> R) then si1 else si0) eq_refl = si0 H.
Proof. simpldtt2. Qed.

Lemma match_nat_dtt (n:nat) (R1:Type)
   (si0: n = 0 -> R1)
   (siS: forall m, n = S m -> R1)
   (CTX: R1 -> Prop) :
      CTX ((match n as n' return (n = n' -> R1) with
            | 0 => si0 | S m => siS m end eq_refl)) <->
        (forall (p:(n = 0)), CTX (si0 p)) /\
        (forall m (p:(n = S m)), CTX (siS m p)).
Proof with simpldtt1.
destruct n eqn:C; repeat split; simpldtt1; dependent destruction p...
Qed.

Lemma match_nat_dtt_indirect (n:nat) (R1:Type)
   (si0: n = 0 -> R1)
   (siS: forall m, n = S m -> R1)
   M
   (EM:M = (match n as n' return (n = n' -> R1) with
            | 0 => si0 | S m => siS m end eq_refl))
   (CTX: R1 -> Prop) :
      CTX M <->
        (forall (p:(n = 0)), CTX (si0 p)) /\
        (forall m (p:(n = S m)), CTX (siS m p)).
Proof with simpldtt1.
subst.
apply match_nat_dtt.
Qed.

Lemma match_0_dtt (n:nat) (R1:Type)
   (p:n = 0)
   (si0: n = 0 -> R1)
   (siS: forall m, n = S m -> R1) :
   (match n as n' return (n = n' -> R1) with
    | 0 => si0 | S m => siS m end eq_refl) = si0 p.
Proof with simpldtt1.
destruct n...
dependent destruction p...
Qed.

Lemma match_S_dtt (n:nat) (R1:Type)
   m (p:n = S m)
   (si0: n = 0 -> R1)
   (siS: forall m, n = S m -> R1) :
   (match n as n' return (n = n' -> R1) with
    | 0 => si0 | S m => siS m end eq_refl) = siS m p.
Proof with simpldtt1.
destruct n...
dependent destruction p...
Qed.

Ltac rewrite_dtt_nat :=
  match goal with
  | |- context[(match ?n as n0 in nat return (?n = n0 -> _) with
                | 0 => (fun (E0: ?n = 0) => @?si0 E0)
                | S m => (fun (ES: ?n = S m) => @?siS m ES) end eq_refl)] =>
      idtac "rewrite match_nat_dtt";
      idtac "match"n"with";
      idtac "| 0 =>"si0;
      idtac "| S m =>"siS;
      let M := fresh "M" in
      let EM := fresh "EM" in
      remember ((match n as n0 return (n = n0 -> _) with
                  0 => si0 | S m => siS m end) eq_refl) as M eqn:EM;
      pattern M;
      rewrite EM; clear EM;
      rewrite match_nat_dtt
  | |- context[(match ?n as n0 in nat return (?n = n0 -> _) with
                | 0 => (fun (E0: ?n = 0) => @?si0 E0)
                | S m => (fun (ES: ?n = S m) => @?siS m ES) end eq_refl)] =>
      idtac "rewrite match_nat_dtt";
      idtac "match"n"with";
      idtac "| 0 =>"si0;
      idtac "| S m =>"siS;
      let M := fresh "M" in
      let EM := fresh "EM" in
      remember ((match n as n0 return (n = n0 -> _) with
                  0 => si0 | S m => siS m end) eq_refl) as M eqn:EM;
      pattern M;
      rewrite (match_nat_dtt_indirect n _ si0 siS M EM _);
      clear EM
  end.

Ltac simpldtt3_step :=
match goal with
| H : ?c = true
  |- context[(if ?c
        then (fun (E1: ?c = true ) => @?si1 E1)
        else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] =>
    idtac "simplified dependent [if true]";
    idtac H":"c" = true";
    idtac "if"c;
    idtac "then"si1;
    idtac "else"si0;
    (rewrite (if_true_dtt c _ si1 si0 H)) ||
    (setoid_rewrite (if_true_dtt c _ si1 si0 H)) ||
    (idtac "failed at rewriting." )
| H : ?c = false
  |- context[(if ?c
        then (fun (E1: ?c = true ) => @?si1 E1)
        else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] =>
    idtac "simplified dependent [if false]";
    idtac H":"c" = true";
    idtac "if"c;
    idtac "then"si1;
    idtac "else"si0;
    (rewrite (if_false_dtt c _ si1 si0 H)) ||
    (setoid_rewrite (if_false_dtt c _ si1 si0 H))  ||
    (idtac "failed at rewriting." )
| H : ?c = true,
  HH : context[(if ?c
        then (fun (E1: ?c = true ) => @?si1 E1)
        else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] |- _ =>
    idtac "simplified dependent [if true] in"HH;
    idtac H":"c" = true";
    idtac "if"c;
    idtac "then"si1;
    idtac "else"si0;
    rewrite (if_true_dtt c _ si1 si0 H) in HH  ||
    (idtac "failed at rewriting." )
| H : ?c = false,
  HH : context[(if ?c
        then (fun (E1: ?c = true ) => @?si1 E1)
        else (fun (E2: ?c = false) => @?si0 E2)) eq_refl] |- _ =>
    idtac "simplified dependent [if false] in"HH;
    idtac H":"c" = true";
    idtac "if"c;
    idtac "then"si1;
    idtac "else"si0;
    rewrite (if_false_dtt c _ si1 si0 H) in HH  ||
    (idtac "failed at rewriting." )
| H : ?n = 0,
  HH : context[(match ?n as n0 in nat return (?n = n0 -> _) with
                | 0 => (fun (E0: ?n = 0) => @?si0 E0)
                | S m => (fun (ES: ?n = S m) => @?siS m ES) end eq_refl)] |- _ =>
      idtac "rewrite match_nat_dtt_with_0";
      idtac "p:"H;
      idtac "match"n"with";
      idtac "| 0 =>"si0;
      idtac "| S m =>"siS;
      rewrite (match_0_dtt n _ H si0 siS) in HH  ||
    (idtac "failed at rewriting." )
| H : ?n = 0
  |- context[(match ?n as n0 in nat return (?n = n0 -> _) with
                | 0 => (fun (E0: ?n = 0) => @?si0 E0)
                | S m => (fun (ES: ?n = S m) => @?siS m ES) end eq_refl)] =>
      idtac "rewrite match_nat_dtt_with_0";
      idtac "p:"H;
      idtac "match"n"with";
      idtac "| 0 =>"si0;
      idtac "| S m =>"siS;
      rewrite (match_0_dtt n _ H si0 siS)  ||
    (idtac "failed at rewriting." )
| H : ?n = S ?m,
  HH : context[(match ?n as n0 in nat return (?n = n0 -> _) with
                | 0 => (fun (E0: ?n = 0) => @?si0 E0)
                | S m => (fun (ES: ?n = S m) => @?siS m ES) end eq_refl)] |- _ =>
      idtac "rewrite match_nat_dtt_with_0";
      idtac "p:"H;
      idtac "match"n"with";
      idtac "| 0 =>"si0;
      idtac "| S m =>"siS;
      rewrite (match_S_dtt n _ m H si0 siS) in HH  ||
    (idtac "failed at rewriting." )
| H : ?n = S ?m
  |- context[(match ?n as n0 in nat return (?n = n0 -> _) with
                | 0 => (fun (E0: ?n = 0) => @?si0 E0)
                | S m => (fun (ES: ?n = S m) => @?siS m ES) end eq_refl)] =>
      idtac "rewrite match_nat_dtt_with_0";
      idtac "p:"H;
      idtac "match"n"with";
      idtac "| 0 =>"si0;
      idtac "| S m =>"siS;
      rewrite (match_S_dtt n _ m H si0 siS)  ||
    (idtac "failed at rewriting." )
end.

Ltac simpldtt3 := simpldtt2; repeat (simpldtt3_step; simpldtt2).

Ltac simpldtt := simpldtt3.

Lemma comm_dtt_iff {A B} (f:A -> B) (b:bool) if1 if0 :
  f((if b as b0 return b = b0 -> A then if1 else if0) eq_refl) =
    (if b as b0 return b = b0 -> B then (fun E => f (if1 E)) else (fun E => f (if0 E))) eq_refl.
Proof. dest_bool. Qed.

Lemma dtt_if_nodtt (b:bool) T (if1 if0:T) :
  (if b as b0 return b = b0 -> T then (fun _ => if1) else (fun _ => if0)) eq_refl =
    if b then if1 else if0.
Proof. dest_bool. Qed.
#[export] Hint Rewrite dtt_if_nodtt : simpldtt1_rewrite.

(* Option *)

Lemma match_option_dtt {T} (x:option T) (R1:Type)
   (siNone: x = None -> R1)
   (siSome: forall v, x = Some v -> R1)
   (CTX: R1 -> Prop) :
      CTX ((match x as x' return (x = x' -> R1) with
            | None => siNone | Some v => siSome v end eq_refl)) <->
        (forall (p:(x = None)), CTX (siNone p)) /\
        (forall v (p:(x = Some v)), CTX (siSome v p)).
Proof with simpldtt1.
destruct x eqn:C; repeat split; simpldtt1; dependent destruction p...
Qed.

Lemma match_option_dtt_indirect {T} (x:option T) (R1:Type)
   (siNone: x = None -> R1)
   (siSome: forall v, x = Some v -> R1)
   M
   (EM:M = (match x as x' return (x = x' -> R1) with
            | None => siNone | Some v => siSome v end eq_refl))
   (CTX: R1 -> Prop) :
      CTX M <->
        (forall (p:(x = None)), CTX (siNone p)) /\
        (forall v (p:(x = Some v)), CTX (siSome v p)).
Proof with simpldtt1.
subst.
apply match_option_dtt.
Qed.

Lemma match_None_dtt {T} (x:option T) (R1:Type)
   (p:x = None)
   (siNone: x = None -> R1)
   (siSome: forall v, x = Some v -> R1) :
   (match x as x' return (x = x' -> R1) with
    | None => siNone | Some v => siSome v end eq_refl) = siNone p.
Proof with simpldtt1.
destruct x...
dependent destruction p...
Qed.

Ltac rewrite_dtt_option :=
  match goal with
  | |- context[(match ?x as x0 in option _ return (?x = x0 -> _) with
                | None => (fun (E0: ?n = None) => @?siNone E0)
                | Some v => (fun (ES: ?n = Some v) => @?siSome v ES) end eq_refl)] =>
      idtac "rewrite match_option_dtt";
      idtac "match"x"with";
      idtac "| None =>"siNone;
      idtac "| Some m =>"siSome;
      let M := fresh "M" in
      let EM := fresh "EM" in
      remember ((match x as x0 return (x = x0 -> _) with
                  None => siNone | Some v => siSome v end) eq_refl) as M eqn:EM;
      pattern M;
      rewrite EM; clear EM;
      rewrite match_option_dtt
  | |- context[(match ?x as x0 in option _ return (?x = x0 -> _) with
                | None => (fun (E0: ?n = None) => @?siNone E0)
                | Some v => (fun (ES: ?n = Some v) => @?siSome v ES) end eq_refl)] =>
      idtac "rewrite match_option_dtt";
      idtac "match"x"with";
      idtac "| None =>"siNone;
      idtac "| Some m =>"siSome;
      let M := fresh "M" in
      let EM := fresh "EM" in
      remember ((match x as x0 return (x = x0 -> _) with
                  None => siNone | Some v => siSome v end) eq_refl) as M eqn:EM;
      pattern M;
      rewrite (match_option_dtt_indirect n _ siNone siSome M EM _);
      clear EM
  end.

(*
Lemma match_option_dtt {T} (x:option T) (R1:Type)
   (siNone: x = None -> R1)
   (siSome: forall v, x = Some v -> R1)
   (CTX: R1 -> Prop) :
      CTX ((match x as x' return (x = x' -> R1) with
            | None => siNone | Some v => siSome v end eq_refl)) <->
        ((forall (p:(x = None)), CTX (siNone p)) /\
        (forall v (p:(x = Some v)), CTX (siSome v p))).
Proof with simpldtt1.
destruct x eqn:C; repeat split; simpldtt1; dependent destruction p...
Qed.

Lemma match_option_dtt_indirect {T} (x:option T) (R1:Type)
   (siNone: x = None -> R1)
   (siSome: forall v, x = Some v -> R1)
   M
   (EM:M = (match x as x' return (x = x' -> R1) with
            | None => siNone | Some v => siSome v end eq_refl))
   (CTX: R1 -> Prop) :
      CTX M <->
        (forall (p:(x = None)), CTX (siNone p)) /\
        (forall v (p:(x = Some v)), CTX (siSome v p)).
Proof with simpldtt1.
subst.
apply match_option_dtt.
Qed.

Lemma match_None_dtt {T} (x:option T) (R1:Type)
   (p:x = None)
   (siNone: x = None -> R1)
   (siSome: forall v, x = Some v -> R1) :
   (match x as x' return (x = x' -> R1) with
    | None => siNone | Some v => siSome v end eq_refl) = siNone p.
Proof with simpldtt1.
destruct x...
dependent destruction p...
Qed.

Ltac rewrite_dtt_option :=
  match goal with
  | |- context[(match ?x as x0 in option _ return (?x = x0 -> _) with
                | None => (fun (E0: ?n = None) => @?siNone E0)
                | Some v => (fun (ES: ?n = Some v) => @?siSome v ES) end eq_refl)] =>
      idtac "rewrite match_option_dtt";
      idtac "match"x"with";
      idtac "| Some v =>"siSome;
      idtac "| None =>"siNone;
      let M := fresh "M" in
      let EM := fresh "EM" in
      pattern((match x as x0 return (x = x0 -> _) with
                  Some v => siSome v | None => siNone end) eq_refl);
      rewrite match_option_dtt
  end.
*)




(* AB *)

Lemma match_AB_dtt {A B} (x:AB A B) (R1:Type)
   (siAA: forall a, x = @AA A B a -> R1)
   (siBB: forall b, x = @BB A B b -> R1)
   (CTX: R1 -> Prop) :
      CTX ((match x as x' return (x = x' -> R1) with
            | AA a => siAA a | BB b => siBB b end eq_refl)) <->
    (forall a (p:x = @AA A B a), CTX(siAA a p)) /\
    (forall b (p:x = @BB A B b), CTX(siBB b p)).
Proof with simpldtt1.
destruct x eqn:C; repeat split; simpldtt1; dependent destruction p...
Qed.

(* [BUGGY?] *)

Ltac rewrite_dtt_AB :=
  match goal with
  | |- context[(match ?x as x0 in AB _ _ return (?x = x0 -> _) with
                | AA a => (fun (EA: ?x = AA a) => @?siAA a EA)
                | BB b => (fun (EB: ?x = BB b) => @?siBB b EB) end eq_refl)] =>
      idtac "rewrite match_AB_dtt";
      idtac "match"x"with";
      idtac "| AA a =>"siAA;
      idtac "| BB b =>"siBB;
      let M := fresh "M" in
      let EM := fresh "EM" in
      remember (match x as x0 in AB _ _ return (x = x0 -> _) with
                | @AA _ _ a => (fun (EA: x = AA a) => siAA a EA)
                | @BB _ _ b => (fun (EB: x = BB b) => siBB b EB) end eq_refl) as M eqn:EM;
      pattern M;
      rewrite EM; clear EM;
      rewrite match_AB_dtt
  end.
