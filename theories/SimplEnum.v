Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
Require Import Coq.Logic.Decidable.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.
Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

(* Section 1. Finite Type Definition *)

Definition explicit_finite (T:Type) (n:nat) (pi:ltN n -> T) : Prop := @explicit_bijection (ltN n) T pi.
Definition explicit_finite_constr (T:Type) (n:nat) (idx:T -> ltN n) (pi:ltN n -> T) : Prop :=
  @explicit_bijection_constr T (ltN n) idx pi.

Lemma explicit_finite_constr_implies_explicit_finite T n idx pi :
  @explicit_finite_constr T n idx pi -> @explicit_finite T n pi.
Proof with curry1.
unfold explicit_finite_constr, explicit_finite.
rewrite comm_explicit_bijection_constr.
apply explicit_bijection_constr_implies_explicit_bijection.
Qed.

Lemma reverse_surjection_ltN_lemma1 {T} beq (beqP:@beq_iff_true T beq)
  n (f:ltN n -> T) (P:surjection f) t0 :
  (fun k : ltN n => beq t0 (f k)) <> (fun _ : ltN n => false).
Proof with curry1.
curry1.
destruct (P t0)...
specialize(f_equal (fun f => f x) H)...
rewrite (beq_reflexive_of_iff_true beqP) in H0...
Qed.

Definition reverse_surjection_ltN {T} beq (beqP:@beq_iff_true T beq)
  n (f:ltN n -> T) (P:surjection f) : (T -> ltN n) :=
  (fun t0 => minsat_ltN_true (fun k : ltN n => beq t0 (f k)) (reverse_surjection_ltN_lemma1 beq beqP n f P t0)).

Lemma surjection_of_explicit_finite {T} n pi : @explicit_finite T n pi -> surjection pi.
Proof with curry1.
unfold explicit_finite, explicit_bijection...
Qed.


Lemma comp_reverse_surjection_ltN_left {T} beq (beqP:@beq_iff_true T beq) n pi (S:surjection pi) :
  comp (reverse_surjection_ltN beq beqP n pi S) pi = id.
Proof with curry1.
apply functional_extensionality...
unfold reverse_surjection_ltN.
generalize (reverse_surjection_ltN_lemma1 beq beqP n pi S x); intro p0.
remember (minsat_ltN_true (fun k : ltN n => beq x (pi k)) p0) as k0 eqn:E0.
symmetry in E0.
rewrite minsat_ltN_iff_is_min_positive in E0.
unfold is_min_positive in E0...
rewrite (beqP _ _) in H...
Qed.

Lemma comp_reverse_surjection_ltN_right {T} beq (beqP:@beq_iff_true T beq) n pi (I:injection pi) (S:surjection pi) :
  comp pi (reverse_surjection_ltN beq beqP n pi S) = id.
Proof with curry1.
apply functional_extensionality...
unfold reverse_surjection_ltN...
rewrite minsat_ltN_iff_is_min_positive.
unfold is_min_positive...
rewrite (beqP _ _)...
rewrite (beqP _ _) in H...
specialize(I x a1)...
Qed.

Lemma explicit_bijection_and_beq_implies_bijection_finite_constr T n pi beq (beqP:beq_iff_true beq)
  (H:@explicit_bijection (ltN n) T pi) :
    @explicit_bijection_constr T (ltN n)
      (reverse_surjection_ltN beq beqP n pi
        (surjection_of_explicit_finite _ _ H)) pi.
Proof with curry1.
unfold explicit_bijection, explicit_bijection_constr in *...
split.
- apply comp_reverse_surjection_ltN_left.
- apply comp_reverse_surjection_ltN_right...
Qed.

Lemma explicit_finite_and_beq_implies_explicit_finite_constr T n pi beq (beqP:beq_iff_true beq)
  (H:@explicit_finite T n pi) :
    @explicit_finite_constr T n
      (reverse_surjection_ltN beq beqP n pi
        (surjection_of_explicit_finite _ _ H)) pi.
Proof with curry1.
unfold explicit_finite, explicit_finite_constr in *.
apply explicit_bijection_and_beq_implies_bijection_finite_constr.
Qed.

Definition dec_quant {T} (P:T -> Prop) : Prop := (forall k, P k) \/ (exists k, ~(P k)).

Lemma dec_quant_implies_decidable_forall {T} P (D:@dec_quant T P) : decidable (forall t, P t).
Proof with curry1.
unfold decidable.
destruct D...
Qed.

Lemma dec_quant_using_decidable_ltN {n} (P:ltN n -> Prop) :
  (forall k, decidable (P k)) -> dec_quant P.
Proof with curry1.
curry1.
unfold dec_quant.
induction n...
destruct(H(ltN_of_nat 0 (S n) eq_refl))...
+ specialize(IHn (stail P))...
  destruct IHn...
  * specialize(H(ltN_S k))...
  * left; rewrite forall_ltN_S...
  * unfold stail in H1...
    right; exists (ltN_S k)...
+ right; exists (ltN_of_nat 0 (S n) eq_refl)...
Qed.

Lemma forall_shift_is_finite {T n idx pi} (F:explicit_finite_constr T n idx pi) (P:T -> Prop) :
  (forall (t:T), P t) <-> (forall k, P(pi k)).
Proof with curry1.
split...
specialize(H(idx t))...
destruct F...
fold(comp idx pi t) in H.
rewrite H0 in H...
Qed.

Lemma decidable_using_iff {P Q:Prop} : (P <-> Q) -> decidable P -> decidable Q.
Proof with curry1.
intros R E.
unfold decidable in *.
destruct E...
rewrite R in H...
Qed.

Lemma decidable_iff_using_iff {P Q:Prop} : (P <-> Q) -> (decidable P <-> decidable Q).
Proof with curry1.
intros.
split; intro HH.
- refine(decidable_using_iff _ HH)...
- refine(decidable_using_iff _ HH)...
  symmetry...
Qed.

Lemma dec_quant_using_explicit_finite_constr {T n idx pi}
  (F:explicit_finite_constr T n idx pi) (P:T -> Prop) :
    (forall t, decidable(P t)) -> dec_quant P.
Proof with curry1.
specialize(dec_quant_using_decidable_ltN (fun (k:ltN n) => P(pi k)))...
assert(forall k : ltN n, decidable (P (pi k)))...
destruct H...
- rewrite <- (forall_shift_is_finite F P) in H...
  unfold dec_quant...
- unfold dec_quant...
  right.
  exists(pi k)...
Qed.

Definition is_cmp_compatible {A B} (f:A -> B) (cmpA:cmp A) (cmpB:cmp B) : Prop :=
  forall a1 a2, cmpA a1 a2 = cmpB(f a1)(f a2).

Definition idx_is_cmp_compatible {A} (cmp:cmp A) {n} (idx:A -> ltN n) : Prop :=
  @is_cmp_compatible A (ltN n) idx cmp cmp_ltN.

Definition pi_is_cmp_compatible {A} (cmp:cmp A) {n} (pi:ltN n -> A) : Prop :=
  @is_cmp_compatible (ltN n) A pi cmp_ltN cmp.

(*

Axiom axiom_of_choice_dep : forall {A:Type} {B:A -> Type} (P:forall (a:A), B a -> Prop),
  (forall (a:A), (exists (b:B a), P a b)) -> (exists (f:forall a, B a), (forall a, P a(f a))).

Lemma axiom_of_choice {A B:Type} (P:A -> B -> Prop) :
  (forall (a:A), (exists b, P a b)) -> (exists (f:A -> B), (forall a, P a (f a))).
Proof. apply axiom_of_choice_dep. Qed.

 *)

Definition bijection_type_sig_ltN_implied_cast_down {n} (p:ltN n -> bool)
  (m:nat) (P:forall k, p k = true -> proj1_sig k <? m = true) (H : m <= n) :
    bijection_type {k : ltN n | p k = true} {k : ltN m | p (ltN_up_cast k n H) = true}.
Proof.
constructor.
- intros [k pk].
  refine (exist _ (ltN_down_cast k m (P _ pk)) _).
  rewrite ltN_up_cast_ltN_down_cast.
  apply pk.
- intros [k pk].
  refine (exist _ _ pk).
Defined.

Lemma bijection_prop_sig_ltN_implied_cast_down {n} p m P H :
  bijection_prop (@bijection_type_sig_ltN_implied_cast_down n p m P H).
Proof with curry1.
split...
- apply functional_extensionality...
  dest_match_step...
- apply functional_extensionality...
  dest_match_step...
Defined.

Definition bijection_sig_ltN_implied_cast_down {n} (p:ltN n -> bool)
  (m:nat) (P:forall k, p k = true -> proj1_sig k <? m = true) (H : m <= n) :
    bijection {k : ltN n | p k = true} {k : ltN m | p (ltN_up_cast k n H) = true} :=
      exist _ _ (bijection_prop_sig_ltN_implied_cast_down p m P H).

(* Addition *)

Definition bijection_type_AB_ltN_ltN nA nB : bijection_type (@AB (ltN nA) (ltN nB)) (ltN (nA + nB)).
Proof.
constructor.
- apply ltN_unsplit.
- apply ltN_split.
Defined.

Definition bijection_prop_AB_ltN_ltN nA nB : bijection_prop (bijection_type_AB_ltN_ltN nA nB).
Proof. constructor; apply functional_extensionality; curry1. Qed.

Definition bijection_AB_ltN_ltN nA nB : bijection (@AB (ltN nA) (ltN nB)) (ltN (nA + nB)) :=
  exist _ _ (bijection_prop_AB_ltN_ltN nA nB).

Definition bijection_type_AB {A1 A2 B1 B2} (BA:bijection A1 A2) (BB:bijection B1 B2) :
  bijection_type (@AB A1 B1) (@AB A2 B2) :=
    pair (mapAB (bijection_fst BA) (bijection_fst BB)) (mapAB (bijection_snd BA) (bijection_snd BB)).

(* [MOVEME] *)
Lemma comp_mapAB_mapAB (A0 A1 A2 B0 B1 B2 : Type) (a01 : A0 -> A1) (a12 : A1 -> A2) (b01 : B0 -> B1) (b12 : B1 -> B2) :
  comp (mapAB a01 b01) (mapAB a12 b12) = mapAB (comp a01 a12) (comp b01 b12).
Proof with curry1.
apply functional_extensionality...
apply mapAB_mapAB.
Qed.

Lemma comp_bijection_fst_bijection_snd {A B} (BIJ:bijection A B) :
  comp (bijection_fst BIJ) (bijection_snd BIJ) = id.
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @comp_bijection_fst_bijection_snd : curry1_rewrite.

Lemma comp_bijection_snd_bijection_fst {A B} (BIJ:bijection A B) :
  comp (bijection_snd BIJ) (bijection_fst BIJ) = id.
Proof. apply functional_extensionality; curry1. Qed.
#[export] Hint Rewrite @comp_bijection_snd_bijection_fst : curry1_rewrite.

Lemma bijection_prop_AB {A1 A2 B1 B2} (BA:bijection A1 A2) (BB:bijection B1 B2) :
  bijection_prop (bijection_type_AB BA BB).
Proof. split; curry1; rewrite comp_mapAB_mapAB; curry1. Qed.

Definition bijection_AB {A1 A2 B1 B2} (BA:bijection A1 A2) (BB:bijection B1 B2) :
  bijection (@AB A1 B1) (@AB A2 B2) := 
    exist _ _ (bijection_prop_AB BA BB).

Definition bijection_type_sig_AB {A B} (pA:A -> bool) (pB:B -> bool) :
  bijection_type
    (@AB (sig (fun a => pA a = true)) (sig (fun b => pB b = true)))
    { ab :@AB A B | match_AB pA pB ab = true }.
Proof.
constructor.
- intros [[a pa]|[b pb]].
  + apply (exist _ (AA a) pa).
  + apply (exist _ (BB b) pb).
- intros [[a|b] pab].
  + refine (AA(exist _ a pab)).
  + refine (BB(exist _ b pab)).
Defined.

Lemma bijection_prop_sig_AB {A B} pA pB : bijection_prop (@bijection_type_sig_AB A B pA pB).
Proof with curry1.
split...
- apply functional_extensionality...
  dest_match_step...
  + dest_match_step...
  + dest_match_step...
- apply functional_extensionality...
  dest_match_step...
  dest_match_step...
Qed.

Definition bijection_sig_AB {A B} (pA:A -> bool) (pB:B -> bool) :
  bijection
    (@AB (sig (fun a => pA a = true)) (sig (fun b => pB b = true)))
    { ab :@AB A B | match_AB pA pB ab = true } :=
      exist _ _ (bijection_prop_sig_AB pA pB).

Lemma explicit_finite_constr_bijection_fst_bijection_snd A B BIJ :
  explicit_finite_constr A B (bijection_fst BIJ) (bijection_snd BIJ).
Proof. destruct BIJ; curry1. Qed.

Lemma explicit_finite_constr_bijection_fst_bijection_snd_iff_True A B BIJ :
  explicit_finite_constr A B (bijection_fst BIJ) (bijection_snd BIJ) <-> True.
Proof. destruct BIJ; curry1. Qed.
#[export] Hint Rewrite @explicit_finite_constr_bijection_fst_bijection_snd_iff_True : curry1_rewrite.


(* [MOVEME] *)
Lemma match_AB_AA_BB {A B C} (f:@AB A B -> C) : match_AB (fun k : A => f (AA k)) (fun k : B => f (BB k)) = f.
Proof with curry1.
apply functional_extensionality...
unfold match_AB...
Qed.
#[export] Hint Rewrite @match_AB_AA_BB : curry1_rewrite.

Definition bijection_sig_AB_AB_sig {A B} (p:@AB A B -> bool) :
  bijection (sig (fun (ab:@AB A B) => p ab = true)) (AB {a:A|p(AA a) = true} {b:B|p(BB b)=true}).
Proof.
specialize(bijection_sym (bijection_sig_AB (fun a => p(AA a)) (fun b => p(BB b)))) as B1.
rewrite match_AB_AA_BB in B1.
apply B1.
Defined.

Definition bijection_AB_ltN_using_bijection_A_ltN_and_B_ltN (A B:Type) (p:@AB A B -> bool)
  nA (BA : bijection (sig (fun a => p(AA a) = true)) (ltN nA))
  nB (BB : bijection (sig (fun b => p(BB b) = true)) (ltN nB)) :
    bijection {a | p a = true} (ltN (nA + nB)) :=
      bijection_comp (bijection_sig_AB_AB_sig p)
     (bijection_comp (bijection_AB BA BB)
                     (bijection_AB_ltN_ltN nA nB)).

Definition split_type_on_predicate {A:Type} (p:A -> bool) (a:A) : AB {a : A | p a = false} {a : A | p a = true} :=
  (if p a as b0 return (p a = b0 -> AB {a0 : A | p a0 = false} {a0 : A | p a0 = true})
  then fun E => BB (exist _ a E)
  else fun E => AA (exist _ a E)) eq_refl.

Definition merge_type_on_predicate {A : Type} {PA PB:A -> Prop} (x:AB (sig PA) (sig PB)) : A :=
  match x with AA a => proj1_sig a | BB b => proj1_sig b end.

Definition bijection_type_disjunction {A:Type} (p:A -> bool) : bijection_type A (@AB (sig (fun a => p a = false)) (sig (fun a => p a = true))) :=
  pair (@split_type_on_predicate A _) (@merge_type_on_predicate A _ _).

Lemma merge_split_on_predicate {A:Type} (p:A -> bool) (x:A) : merge_type_on_predicate (split_type_on_predicate p x) = x.
Proof with curry1.
unfold split_type_on_predicate, merge_type_on_predicate...
rewrite_dtt_if...
Qed.
#[export] Hint Rewrite @merge_split_on_predicate : curry1_rewrite.

Lemma split_merge_on_predicate {A:Type} (p:A -> bool) (x:(@AB (sig (fun a => p a = false)) (sig (fun a => p a = true)))) :
  split_type_on_predicate p (merge_type_on_predicate x) = x.
Proof with curry1.
unfold split_type_on_predicate, merge_type_on_predicate...
destruct x as [[x px]|[x px]]...
Qed.
#[export] Hint Rewrite @split_merge_on_predicate : curry1_rewrite.

Lemma bijection_prop_disjunction A p : bijection_prop (@bijection_type_disjunction A p).
Proof. constructor; apply functional_extensionality; curry1. Qed.

Definition bijection_disjunction {A:Type} (p:A -> bool) : bijection A (@AB (sig (fun a => p a = false)) (sig (fun a => p a = true))) :=
  exist _ (@bijection_type_disjunction A p) (@bijection_prop_disjunction A p).

(* [WIP]
Lemma bijection_sig_ltN_le_ltN (A:Type) (p:A -> bool) n nP :
  bijection A (ltN n) -> bijection (sig (fun a => p a = true)) (ltN nP) -> nP <= n.
Proof with curry1.
intros BAn BpAnP.
 *)




