Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.
Require Import FunInd.
Require Import Recdef.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_VAX.
Require Import MiniBool_Logic.

Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import SimplEnum.
Require Import SimplSizedList.

#[export] Hint Rewrite Nat.mul_0_r : curry1_rewrite.
#[export] Hint Rewrite Nat.mul_1_r : curry1_rewrite.

Lemma mult_le_using_le s x y : x <= y -> s * x <= s * y.
Proof. induction s; simplnat; lia. Qed.

Lemma multmod_lemma0 r n q m : r < n -> q < m -> r + n * q < n * m.
Proof with curry1.
intros Hr Hq.
destruct n...
unfold lt in *.
specialize(mult_le_using_le n _ _ Hq) as nHq...
rewrite Nat.mul_succ_r in nHq.
lia.
Qed.

Require Import Coq.Init.Nat.

Lemma mod_ltN_lemma1 (n m:nat) (H:m =? 0 = false) : (n mod m <? m) = true.
Proof. curry1; apply Nat.mod_upper_bound; assumption. Qed.

Definition mod_ltN (n m:nat) (H:m =? 0 = false) : ltN m :=
  (exist _ (n mod m) (mod_ltN_lemma1 n m H)).

(* Quotient Rest Pair *)
Definition QR (n:nat) : Type := nat * ltN n.

Lemma QR_0_empty : QR 0 -> False.
Proof. intros [q r]; curry1. Qed.

Lemma QR_n_neq_0 {n} (qr:QR n) : 0 < n.
Proof. destruct n; curry1; destruct(QR_0_empty qr). Qed.

Definition QR_r {n} (r:ltN n) : QR n := (0, r).

Lemma ltN_S_mod_lemma1 {n} (k:ltN n) (E:(n =? S (proj1_sig k)) = true) : (0 <? n) = true.
Proof. curry1. Qed.

Lemma ltN_S_mod_lemma2 {n} (k:ltN n) (E:(n =? S (proj1_sig k)) = false) : (S (proj1_sig k) <? n) = true.
Proof with curry1.
destruct k as [k Hk]...
lia.
Qed.

Definition ltN_S_mod {n} (k:ltN n) : ltN n :=
  (if n =? S(proj1_sig k) as b0 return n =? S(proj1_sig k) = b0 -> ltN n
  then (fun E => ltN_of_nat 0 _ (ltN_S_mod_lemma1 _ E))
  else (fun E => ltN_of_nat (S(proj1_sig k)) _ (ltN_S_mod_lemma2 _ E))) eq_refl.

Definition ltN_S_quo {n} (k:ltN n) : bool := n =? S(proj1_sig k).

Definition QR_S {n} (qr:QR n) : QR n :=
  let q := fst qr in
  let r := snd qr in
  ((if ltN_S_quo r then 1 else 0) + q, ltN_S_mod r).

Definition nat_of_QR {n} (qr:QR n) : nat := (proj1_sig (snd qr)) + (fst qr) * n .

Lemma nat_of_QR_QR_r {n} (k:ltN n) : nat_of_QR(QR_r k) = proj1_sig k.
Proof. unfold nat_of_QR, QR_r; curry1. Qed.
#[export] Hint Rewrite @nat_of_QR_QR_r : curry1_rewrite.

Lemma nat_of_QR_QR_S {n} (qr:QR n) : nat_of_QR (QR_S qr) = S(nat_of_QR qr).
Proof with curry1.
unfold QR_S, nat_of_QR...
unfold ltN_S_mod...
destruct qr as [q [r Hr]]...
rewrite_dtt_if...
rewrite_if_as_Prop...
unfold ltN_S_quo...
lia.
Qed.
#[export] Hint Rewrite @nat_of_QR_QR_S : curry1_rewrite.

Fixpoint QR_of_nat (k:nat) {n} (r:ltN n) : QR n :=
match k with
| 0 => QR_r r
| S k => QR_S(QR_of_nat k r)
end.

Lemma nat_of_QR_QR_of_nat n k : nat_of_QR (QR_of_nat k (ltN_of_nat 0 (S n) eq_refl)) = k.
Proof. induction k; curry1. Qed.
#[export] Hint Rewrite nat_of_QR_QR_of_nat : curry1_rewrite.

Lemma QR_r_0_eq_QR_S n qr : QR_r (ltN_of_nat 0 (S n) eq_refl) = QR_S qr <-> False.
Proof with curry1.
unfold QR_r, QR_S...
dest_match_step...
destruct qr as [q r]...
unfold ltN_S_quo, ltN_S_mod in *...
rewrite_dtt_if...
Qed.
#[export] Hint Rewrite QR_r_0_eq_QR_S : curry1_rewrite.

Lemma QR_S_QR_r_0 n qr : QR_S qr = QR_r (ltN_of_nat 0 (S n) eq_refl) <-> False.
Proof. rewrite eq_sym_iff; curry1. Qed.
#[export] Hint Rewrite QR_S_QR_r_0 : curry1_rewrite.

Lemma proj1_sig_ltN_S_mod {n} (k:ltN n) :
  proj1_sig (ltN_S_mod k) = if n =? S (proj1_sig k) then 0 else (S(proj1_sig k)).
Proof with curry1.
unfold ltN_S_mod.
rewrite_dtt_if...
rewrite_if_as_Prop...
Qed.
#[export] Hint Rewrite @proj1_sig_ltN_S_mod : curry1_rewrite.

Lemma QR_S_injective {n} (qr1 qr2:QR n) : QR_S qr1 = QR_S qr2 <-> qr1 = qr2.
Proof with curry1.
split...
unfold QR_S in *...
rewrite ltN_PI in H0...
destruct qr1 as [q1 [r1 H1]]...
destruct qr2 as [q2 [r2 H2]]...
unfold ltN_S_quo in H...
repeat rewrite_if_as_Prop...
lia.
Qed.
#[export] Hint Rewrite @QR_S_injective : curry1_rewrite.

Lemma QR_of_nat_injective {n} (k1 k2:nat) :
  QR_of_nat k1 (ltN_of_nat 0 (S n) eq_refl) = QR_of_nat k2 (ltN_of_nat 0 (S n) eq_refl) <-> k1 = k2.
Proof with curry1.
generalize dependent k2.
induction k1 as [|k1 IH1]; intros [|k2]; try specialize(IH1 k2)...
Qed.
#[export] Hint Rewrite @QR_of_nat_injective : curry1_rewrite.

Lemma nat_of_QR_injective {n} (qr1 qr2:QR n) : nat_of_QR qr1 = nat_of_QR qr2 <-> qr1 = qr2.
Proof with curry1.
unfold nat_of_QR...
destruct qr1 as [q1 [r1 H1]].
destruct qr2 as [q2 [r2 H2]]...
split...
assert(r1 = r2)...
{
  specialize(f_equal(fun v => v mod n)H) as Hn...
  rewrite <- (Nat.add_mod_idemp_r r1) in Hn...
  rewrite <- (Nat.add_mod_idemp_r r2) in Hn...
  rewrite Nat.mod_mul in Hn...
  rewrite Nat.mod_mul in Hn...
  rewrite Nat.mod_small in Hn...
  rewrite Nat.mod_small...
}
rewrite Nat.add_cancel_l in H...
rewrite Nat.mul_cancel_r in H...
Qed.

Lemma QR_of_nat_nat_of_QR {n} (qr:QR(S n)) : QR_of_nat (nat_of_QR qr) (ltN_of_nat 0 (S n) eq_refl) = qr.
Proof. rewrite <- nat_of_QR_injective; curry1. Qed.
#[export] Hint Rewrite @QR_of_nat_nat_of_QR : curry1_rewrite.

Lemma ltN_plus_mod_lemma1 {n} (k1 k2:ltN n) :
  let k12 := proj1_sig k1 + proj1_sig k2 in
  (k12 <? n) = false -> (k12 - n <? n) = true.
Proof with curry1.
curry1.
destruct k1 as [k1 H1], k2 as [k2 H2]...
lia.
Qed.

Definition ltN_plus_mod {n} (k1 k2:ltN n) : ltN n :=
let k12 := proj1_sig k1 + proj1_sig k2 in
(if k12 <? n as b0 return ((k12 <? n) = b0 -> ltN n)
 then fun E => ltN_of_nat k12 n E
 else fun E => ltN_of_nat (k12 - n) n (ltN_plus_mod_lemma1 k1 k2 E))
  eq_refl.

Lemma le_diag_iff n : n <= n <-> True.
Proof. lia. Qed.
#[export] Hint Rewrite le_diag_iff : curry1_rewrite.

Lemma minus_n_k_le_n n k : n - k <= n <-> True.
Proof. lia. Qed.
#[export] Hint Rewrite minus_n_k_le_n : curry1_rewrite.

Lemma QR_nat_lemma1 (n k:nat) :
  let qr := let qu := divmod k n 0 n in (fst qu, n - snd qu) in
  (snd qr <? S n) = true.
Proof. curry1. Qed.


Definition QR_nat (n:nat) (H:n =? 0 = false) (k:nat) : QR n :=
match n as n0 return ((n0 =? 0) = false -> QR n0) with
| 0 => fun H0 => match proj1 tf_False H0 return (QR 0) with end
| S n0 => fun _ =>
    let qu := divmod k n0 0 n0 in
    (fst qu, ltN_of_nat (n0 - snd qu) (S n0) (QR_nat_lemma1 n0 k))
end H.

Lemma QR_nat_eq_QR_of_nat n H k p : QR_nat n H k = QR_of_nat k (ltN_of_nat 0 n p).
Proof with curry1.
unfold QR_nat.
destruct n...
dependent destruction p...
rewrite <- nat_of_QR_injective...
unfold nat_of_QR...
specialize(Nat.divmod_spec k n 0 n)...
rewrite Nat.mul_succ_r...
lia.
Qed.
#[export] Hint Rewrite QR_nat_eq_QR_of_nat : curry1_rewrite.

Lemma QR_nat_eq n H k qr : QR_nat n H k = qr <-> k = nat_of_QR qr.
Proof with curry1.
destruct n.
- curry1.
- rewrite (QR_nat_eq_QR_of_nat (S n) _ _ eq_refl)...
  split...
Qed.
#[export] Hint Rewrite QR_nat_eq : curry1_rewrite.


Lemma mult_le_mult_same_l n x y : (n * x <= n * y) <-> (n <> 0 -> x <= y).
Proof with curry1.
generalize dependent y.
generalize dependent x.
induction n; destruct x, y...
rewrite Nat.mul_succ_r...
specialize(IHn x y)...
lia.
Qed.

Lemma ltN_prod_lemma1 {n1} (k1:ltN n1) {n2} (k2:ltN n2) : (proj1_sig k1 + n1 * proj1_sig k2 <? n1 * n2) = true.
Proof with curry1.
destruct k1 as [k1 H1], k2 as [k2 H2]...
unfold lt in *.
destruct n1, n2...
rewrite Nat.mul_succ_r...
assert(n1 * k2 <= n1 * n2).
- rewrite mult_le_mult_same_l...
- lia.
Qed.

Definition ltN_prod {n1} (k1:ltN n1) {n2} (k2:ltN n2) : ltN(n1 * n2) :=
  (ltN_of_nat (proj1_sig k1 + n1 * proj1_sig k2) (n1 * n2) (ltN_prod_lemma1 k1 k2)).

Lemma ltN_proj1_lemma1 {n1 n2} (k:ltN(n1 * n2)) : (proj1_sig k mod n1 <? n1) = true.
Proof. curry1; apply Nat.mod_upper_bound; curry1. Qed.

Definition ltN_proj1 {n1 n2} (k:ltN(n1 * n2)) : ltN n1 :=
  ltN_of_nat ((proj1_sig k) mod n1) n1 (ltN_proj1_lemma1 k).

Lemma ltN_proj2_lemma1 {n1 n2} (k:ltN(n1 * n2)) : (proj1_sig k / n1 <? n2) = true.
Proof with curry1.
destruct k as [k H]...
apply Nat.div_lt_upper_bound...
Qed.

Definition ltN_proj2 {n1 n2} (k:ltN(n1 * n2)) : ltN n2 :=
  ltN_of_nat ((proj1_sig k) / n1) n2 (ltN_proj2_lemma1 k).

Definition ltN_to_pair {n1 n2} (k:ltN(n1 * n2)) : (ltN n1) * (ltN n2) :=
  (ltN_proj1 k, ltN_proj2 k).

Lemma proj1_sig_ltN_proj2_as_fst_QR_nat n1 n2 H k :
  proj1_sig (@ltN_proj2 n1 n2 k) = fst(QR_nat n1 H (proj1_sig k)).
Proof with curry1.
unfold ltN_proj2, QR_nat...
destruct n1...
Qed.

Lemma proj1_sig_ltN_proj1_as_fst_QR_nat n1 n2 H k :
  @ltN_proj1 n1 n2 k = snd(QR_nat n1 H (proj1_sig k)).
Proof with curry1.
unfold ltN_proj1, QR_nat...
destruct n1...
Qed.

Lemma fold_nat_of_QR {n} (k:ltN n) k' : proj1_sig k + n * k' = @nat_of_QR n (k', k).
Proof. unfold nat_of_QR; rewrite mult_comm; curry1. Qed.

Lemma ltN_to_pair_ltN_prod n1 k1 n2 k2 : ltN_to_pair (@ltN_prod n1 k1 n2 k2) = (k1, k2).
Proof with curry1.
unfold ltN_to_pair...
rewrite (ltN_PI _ _ k2).
assert(H : (n1 =? 0) = false).
- curry1.
- rewrite (proj1_sig_ltN_proj2_as_fst_QR_nat n1 n2 H).
  rewrite (proj1_sig_ltN_proj1_as_fst_QR_nat n1 n2 H).
  remember(QR_nat n1 H (proj1_sig (ltN_prod k1 k2))) as qr eqn:E.
  symmetry in E.
  rewrite QR_nat_eq in E...
  rewrite fold_nat_of_QR, nat_of_QR_injective in E...
Qed.

Lemma ltN_prod_injective {n1 n2} (k1 k1':ltN n1) (k2 k2':ltN n2) :
  ltN_prod k1 k2 = ltN_prod k1' k2' <-> k1 = k1' /\ k2 = k2'.
Proof with curry1.
unfold ltN_prod...
repeat rewrite (@fold_nat_of_QR n1).
rewrite nat_of_QR_injective...
rewrite ltN_PI...
Qed.

Lemma n1_neq_0_of_ltN_mult {n1 n2} (k:ltN(n1 * n2)) : n1 =? 0 = false.
Proof. curry1. Qed.

Lemma n2_neq_0_of_ltN_mult {n1 n2} (k:ltN(n1 * n2)) : n2 =? 0 = false.
Proof. curry1. Qed.

Lemma ltN_to_pair_as_QR_nat {n1 n2} (k:ltN(n1 * n2)) :
  let k1k2 := ltN_to_pair k in
  let qr := QR_nat n1 (n1_neq_0_of_ltN_mult k) (proj1_sig k) in
  fst k1k2 = snd qr /\ proj1_sig(snd k1k2) = fst qr.
Proof with curry1.
simpl...
unfold ltN_proj1, ltN_proj2, QR_nat...
destruct n1...
Qed.

Lemma QR_nat_injective n H1 k1 H2 k2 : QR_nat n H1 k1 = QR_nat n H2 k2 <-> k1 = k2.
Proof with curry1.
rewrite <- nat_of_QR_injective...
assert(p : (0 <? n) = true). { curry1. lia. }
rewrite (QR_nat_eq_QR_of_nat n H2 k1 p).
rewrite (QR_nat_eq_QR_of_nat n H2 k2 p).
clear H2.
destruct n...
- exfalso...
- dependent destruction p...
Qed.
#[export] Hint Rewrite QR_nat_injective : curry1_rewrite.

Lemma pair_expansion {A B} (p1 p2:A * B) : p1 = p2 <-> fst p1 = fst p2 /\ snd p1 = snd p2.
Proof. destruct p1, p2; curry1. Qed.

Lemma ltN_to_pair_injective {n1 n2} (k k':ltN(n1 * n2)) : ltN_to_pair k = ltN_to_pair k' <-> k = k'.
Proof with curry1.
split...
specialize(ltN_to_pair_as_QR_nat k)...
specialize(ltN_to_pair_as_QR_nat k')...
unfold ltN_to_pair in H...
unfold ltN_proj2 in H4...
simpl in *.
rewrite H, H4 in *.
rewrite H0 in H2.
rewrite H1 in H3.
assert(HH:QR_nat n1 (n1_neq_0_of_ltN_mult k) (proj1_sig k) = QR_nat n1 (n1_neq_0_of_ltN_mult k') (proj1_sig k')).
- setoid_rewrite pair_expansion...
- rewrite QR_nat_injective in HH...
  rewrite ltN_PI...
Qed.

Lemma ltN_prod_ltN_proj1_ltN_proj2 {n1 n2} (k:ltN(n1 * n2)) : ltN_prod (ltN_proj1 k) (ltN_proj2 k) = k.
Proof with curry1.
rewrite <- ltN_to_pair_injective.
rewrite ltN_to_pair_ltN_prod...
Qed.
#[export] Hint Rewrite @ltN_prod_ltN_proj1_ltN_proj2 : curry1_rewrite.

Lemma ltN_n_pow_m_implies_n_neq_0 {n m} (k:ltN(n ^ m)) : m =? 0 = false -> n =? 0 = false.
Proof. curry1; rewrite (Nat.pow_0_l _ H) in k; curry1. Qed.

Lemma ltN_n_pow_S_m_implies_n_neq_0 {n m} (k:ltN(n ^(S m))) : n =? 0 = false.
Proof. apply (@ltN_n_pow_m_implies_n_neq_0 n (S m) k); reflexivity. Qed.

Lemma quomod_ltN_lemma1 {n m} (k:ltN(n ^ (S m))) :
  fst (QR_nat n (ltN_n_pow_S_m_implies_n_neq_0 k) (proj1_sig k)) <? n ^ m = true.
Proof with curry1.
generalize (ltN_n_pow_S_m_implies_n_neq_0 k); intro H...
remember(QR_nat n H (proj1_sig k)) as qr eqn:E.
symmetry in E.
rewrite QR_nat_eq in E...
destruct k as [k Hk]...
destruct qr as [q [r Hr]]...
unfold nat_of_QR in Hk...
rewrite (Nat.mul_lt_mono_pos_l n)...
- rewrite (mult_comm n _) in *.
  lia.
- destruct n...
Qed.


Fixpoint ltN_of_array_ltN {n m} (a:array (ltN n) m) {struct m} : ltN(n ^ m) :=
  match m as m0 return array (ltN n) m0 -> ltN (n ^ m0) with
  | 0 => fun _ => ltN_of_nat 0 1 eq_refl
  | S m => fun a => ltN_prod (shead a) (ltN_of_array_ltN (stail a))
  end a.

Fixpoint array_ltN_of_ltN {n} {m} (k:ltN(n ^ m)) {struct m} : array (ltN n) m :=
  match m as m0 return ltN(n ^ m0) -> array (ltN n) m0 with
  | 0 => fun _ => sempty
  | S m => fun k =>
    let ht := ltN_to_pair k in
    scons (fst ht) (array_ltN_of_ltN (snd ht))
  end k.

Definition modN (n:nat) (H:n =? 0 = false) (k:nat) : ltN n := snd(QR_nat n H k).

Definition modN_plus {n} (k1 k2:ltN n) : ltN n := ltN_plus_mod k1 k2.

Lemma proj1_sig_modN n H k : proj1_sig (modN n H k) = k mod n.
Proof with curry1.
unfold modN, QR_nat.
destruct n...
Qed.

Lemma proj1_sig_modN_proj {n} (k1 k2:ltN n) : proj1_sig (modN_plus k1 k2) = (proj1_sig k1 + proj1_sig k2) mod n.
Proof with curry1.
unfold modN_plus, ltN_plus_mod...
rewrite_dtt_if...
split...
- rewrite Nat.mod_small...
- rewrite Nat.mod_eq...
  destruct k1 as [k1 H1], k2 as [k2 H2]...
  assert(((k1 + k2) / n) = 1)...
  unfold div.
  destruct n...
  specialize(Nat.divmod_spec (k1 + k2) n 0 n)...
  destruct n0...
  * lia.
  * destruct n0...
    repeat rewrite Nat.mul_succ_r in *.
    curry1.
    lia.
Qed.

Lemma modN_plus_distrib n H k1 k2: modN n H (k1 + k2) = modN_plus (modN n H k1) (modN n H k2).
Proof with curry1.
rewrite ltN_PI.
rewrite proj1_sig_modN.
rewrite proj1_sig_modN_proj.
repeat rewrite proj1_sig_modN.
rewrite Nat.add_mod_idemp_r...
rewrite Nat.add_mod_idemp_l...
Qed.

Lemma fold_modN n H k : snd (QR_nat n H k) = modN n H k.
Proof. reflexivity. Qed.

Lemma ltN_proj1_ltN_prod {n1} (k1:ltN n1) {n2} (k2:ltN n2) : ltN_proj1 (ltN_prod k1 k2) = k1.
Proof with curry1.
specialize(ltN_to_pair_ltN_prod _ k1 _ k2)...
unfold ltN_to_pair in H...
Qed.
#[export] Hint Rewrite @ltN_proj1_ltN_prod : curry1_rewrite.

Lemma ltN_proj2_ltN_prod {n1} (k1:ltN n1) {n2} (k2:ltN n2) : ltN_proj2 (ltN_prod k1 k2) = k2.
Proof with curry1.
specialize(ltN_to_pair_ltN_prod _ k1 _ k2)...
unfold ltN_to_pair in H...
Qed.
#[export] Hint Rewrite @ltN_proj2_ltN_prod : curry1_rewrite.

Lemma ltN_prod_expansion {n1 n2} (k k':ltN(n1 * n2)) :
  k = k' <-> ltN_proj1 k = ltN_proj1 k' /\ ltN_proj2 k = ltN_proj2 k'.
Proof with curry1.
split...
rewrite <- (ltN_prod_ltN_proj1_ltN_proj2 k).
rewrite <- (ltN_prod_ltN_proj1_ltN_proj2 k').
rewrite_subst.
Qed.

Lemma array_ltN_of_ltN_of_array_ltN {n m} a : @array_ltN_of_ltN n m (ltN_of_array_ltN a) = a.
Proof with curry1.
induction m...
rewrite array_head_expansion...
Qed.
#[export] Hint Rewrite @array_ltN_of_ltN_of_array_ltN : curry1_rewrite.

Lemma ltN_of_array_ltN_of_ltN {n m} k : @ltN_of_array_ltN n m (array_ltN_of_ltN k) = k.
Proof with curry1.
induction m...
rewrite IHm...
Qed.
#[export] Hint Rewrite @ltN_of_array_ltN_of_ltN : curry1_rewrite.

Lemma array_ltN_of_ltN_iff_ltN_of_array_ltN {n m} a k : @array_ltN_of_ltN n m k = a <-> k = ltN_of_array_ltN a.
Proof. split; curry1. Qed.

Lemma ltN_of_array_ltN_iff_array_ltN_of_ltN {n m} a k : @ltN_of_array_ltN n m k = a <-> k = array_ltN_of_ltN a.
Proof. split; curry1. Qed.

Theorem explicit_finite_constr_array_ltN {n m} :
  explicit_finite_constr (array (ltN n) m) (n ^ m) ltN_of_array_ltN array_ltN_of_ltN.
Proof with curry1.
constructor...
- apply functional_extensionality...
- apply functional_extensionality...
Qed.

