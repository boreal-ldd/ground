Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
Require Import Coq.Logic.Decidable.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.
Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.
Require Import SimplEnum.

Definition minsat_map {A B} (map:A -> B) (minsatA:minsat_t A) : minsat_t B :=
  (fun p => opmap map (minsatA (comp map p))).

Lemma rewrite_is_min_positive_map
  {A B:Type} (map:A -> B) (rec:B -> A)
  (HBij:explicit_bijection_constr map rec)
  (cA:cmp A) (cB:cmp B)
  (HC:is_cmp_compatible map cA cB) :
    forall F a, is_min_positive cA (comp map F) a <-> is_min_positive cB F (map a).
Proof with curry1.
unfold is_min_positive in *...
unfold is_cmp_compatible in HC.
split...
- specialize(H0(rec a1))...
  rewrite HC in H0.
  fold (comp rec map a1) in *.
  rewrite (proj2 HBij) in H0...
  apply H0...
  fold (comp rec map a1) in *.
  rewrite (proj2 HBij) in *...
- specialize(H0(map a1))...
  rewrite <- HC in H0...
  rewrite (injection_of_explicit_bijection_constr HBij _) in H0...
Qed.

Lemma opmap_shift_using_explicit_bijection_constr
  {A B:Type} (map:A -> B) (rec:B -> A)
  (H:explicit_bijection_constr map rec) opa opb:
    opmap map opa = opb <-> opa = opmap rec opb.
Proof with curry1.
split...
- rewrite (proj1 H)...
- rewrite (proj2 H)...
Qed.

Lemma comp_shift_using_explicit_bijection_constr
  {A B:Type} (map:A -> B) (rec:B -> A)
  (H:explicit_bijection_constr map rec) {C} (F:B -> C) (G:A -> C):
    comp map F = G <-> F = comp rec G.
Proof with curry1.
split...
- rewrite (proj2 H)...
- rewrite (proj1 H)...
Qed.

Lemma minsat_positive_minsat_map_using_is_cmp_compatible_and_bijection
  {A B:Type} (map:A -> B) (rec:B -> A)
  (HBij:explicit_bijection_constr map rec)
  (cA:cmp A) (cB:cmp B) (HC:is_cmp_compatible map cA cB)
  (minsatA:minsat_t A) (HMA:minsat_positive cA minsatA) :
    minsat_positive cB (minsat_map map minsatA).
Proof with curry1.
rewrite minsat_positive_iff_minsat_alternative in HMA.
rewrite minsat_positive_iff_minsat_alternative.
unfold minsat_alternative in *...
unfold minsat_map...
rewrite (opmap_shift_using_explicit_bijection_constr _ _ HBij).
rewrite HMA.
destruct v...
- rewrite (rewrite_is_min_positive_map map rec HBij cA cB HC)...
  fold(comp rec map b).
  rewrite(proj2 HBij)...
- rewrite (comp_shift_using_explicit_bijection_constr _ _ HBij)...
Qed.

Lemma minsat_predicate_lemma0
  {A} (pA: A -> bool) cA minsat (H:minsat_positive cA minsat)
  (p : {a : A | pA a = true} -> bool)
  (opv := minsat
     (fun a : A =>
      (if pA a as p0 return (pA a = p0 -> bool)
       then fun E : pA a = true => p (exist (fun a0 : A => pA a0 = true) a E)
       else fun _ : pA a = false => false) eq_refl) : option A)
  (a : A) (E : opv = Some a) : pA a = true.
Proof with curry1.
unfold opv in *. clear opv.
rewrite minsat_positive_iff_minsat_alternative in H.
rewrite (H _) in E.
unfold is_min_positive in E...
rewrite_dtt_if...
Qed.

Definition sig_map {A} (pA:A -> bool) {B} (F1:{a:A|pA a = true} -> B) (F0:{a:A|pA a = false} -> B) : A -> B :=
  (fun a : A => (if pA a as p0 return (pA a = p0 -> B)
      then fun E => F1 (exist (fun a0 : A => pA a0 = true ) a E)
      else fun E => F0 (exist (fun a0 : A => pA a0 = false) a E)) eq_refl).

Definition minsat_sig
  {A} (pA: A -> bool) cA minsat (H:@minsat_positive A cA minsat) : minsat_t { a : A | pA a = true } :=
  (fun (p:{ a : A | pA a = true} -> bool) =>
    let opv := minsat (sig_map pA p (fun _ => false)) in
    (match opv as o0 return opv = o0 -> option { a : A | pA a = true} with
    | Some a => (fun E => Some(exist _ a (minsat_predicate_lemma0 pA _ _ H p _ E)))
    | None   => (fun _ => None)
    end) eq_refl).

Lemma sig_map_eq_fun_cst {A B} pA p1 p0 vB :
  sig_map pA p1 p0 = ((fun _ => vB) : A -> B) <-> p1 = (fun _ => vB) /\ p0 = (fun _ => vB).
Proof with curry1.
split; intro C...
- split...
  + apply functional_extensionality; intros [a Ha].
    specialize(f_equal(fun f => f a)C) as Ca...
    unfold sig_map...
  + apply functional_extensionality; intros [a Ha].
    specialize(f_equal(fun f => f a)C) as Ca...
    unfold sig_map...
- apply functional_extensionality...
  unfold sig_map...
Qed.
#[export] Hint Rewrite @sig_map_eq_fun_cst : curry1_rewrite.

Lemma is_min_positive_cmp_sig {A} (pA: A -> bool) cA (HCA:cmp_P cA) F sa :
is_min_positive (cmp_sig (fun a : A => pA a = true) cA) F sa <->
  is_min_positive cA (sig_map pA F (fun _ => false)) (proj1_sig sa).
Proof with curry1.
unfold is_min_positive...
unfold sig_map...
destruct sa as [a Ha]...
split...
- rewrite_dtt_if...
  specialize(H0 _ H1)...
- destruct a1 as [a' Ha']...
  specialize(H0 a')...
Qed.

Lemma minsat_positive_minsat_sig
  {A} (pA: A -> bool) cA (HCA:cmp_P cA) minsat (H:@minsat_positive A cA minsat) :
    minsat_positive (cmp_sig (fun a => pA a = true) cA) (minsat_sig pA cA minsat H).
Proof with curry1.
assert(H0:=H).
rewrite minsat_positive_iff_minsat_alternative in H0.
rewrite <- (minsat_weak_iff_minsat_positive _ (cmp_P_sig _ _ HCA)).
unfold minsat_alternative in H0.
unfold minsat_weak, minsat_sig...
pattern(match
  minsat (sig_map pA F (fun _  => false)) as o0
  return (minsat (sig_map pA F (fun _ => false)) = o0 -> option {a : A | pA a = true})
with
| Some a => fun E  => Some (exist _ a (minsat_predicate_lemma0 pA cA minsat H F a E))
| None => fun _ => None
end eq_refl).
rewrite match_option_dtt...
split...
- rewrite H0 in p...
- generalize (minsat_predicate_lemma0 pA cA minsat H F v p) as HH...
  rewrite H0 in p.
  rewrite is_min_positive_cmp_sig...
Qed.