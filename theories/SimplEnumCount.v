Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
Require Import Coq.Logic.Decidable.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.
Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

Require Import SimplEnum.

Fixpoint ltN_count {n} (p:array bool n) : nat :=
  match n as n0 return array bool n0 -> nat with
  | 0 => fun _ => 0
  | S n => fun p => (if shead p then 1 else 0) + (ltN_count (stail p))
  end p.

Lemma ltN_count_le_n {n} (p:array bool n) : ltN_count p <= n.
Proof with curry1.
induction n...
specialize(IHn(stail p))...
rewrite_if_as_Prop...
Qed.

Lemma ltN_count_ltN_lemma0 {n} (p:array bool n) : ltN_count p <?(S n) = true.
Proof. curry1; apply ltN_count_le_n. Qed.

Definition ltN_count_ltN {n} (p:array bool n) : ltN(S n) :=
  ltN_of_nat (ltN_count p) (S n) (ltN_count_ltN_lemma0 p).

Lemma ltN_bS_lemma0 (b:bool) {n} (k:ltN n) : ((if b then 1 else 0) + (proj1_sig k)) <? (S n) = true.
Proof with curry1.
destruct k...
rewrite_if_as_Prop...
Qed.

Definition ltN_bS (b:bool) {n} (k:ltN n) : ltN(S n) :=
  ltN_of_nat ((if b then 1 else 0) + (proj1_sig k)) (S n) (ltN_bS_lemma0 b k).

Lemma rewrite_ltN_count_ltN {n} (p:array bool n) :
  ltN_count_ltN p =
  match n as n0 return array bool n0 -> ltN(S n0) with
  | 0 => fun _ => ltN_of_nat 0 1 eq_refl
  | S n => fun p => ltN_bS (shead p) (ltN_count_ltN (stail p))
  end p.
Proof with curry1.
destruct n...
unfold ltN_count_ltN...
Qed.


Fixpoint find_nth {n} (p:array bool n) (k:nat) {struct n} : nat :=
  match n as n0 return array bool n0 -> nat with
  | 0 => fun _ => 0 (* assert false *)
  | S n => fun p =>
    if shead p
    then
      match k with
      | 0 => 0
      | S k => S(find_nth(stail p) k)
      end
    else S(find_nth(stail p) k)
  end p.

Lemma find_nth_lt_n {n} (p:array bool n) (k:ltN(ltN_count p)) : find_nth p (proj1_sig k) < n.
Proof with curry1.
induction n...
dest_match_step...
dest_match_step...
destruct k...
rewrite <- Nat.ltb_lt in e.
specialize(IHn(stail p) (ltN_of_nat n0 (ltN_count(stail p)) e))...
Qed.

Lemma ltN_find_nth_lemma0 {n} (p:array bool n) (k:ltN(ltN_count p)) : find_nth p (proj1_sig k) <? n = true.
Proof. curry1; apply find_nth_lt_n. Qed.

Definition ltN_find_nth {n} (p:array bool n) (k:ltN(ltN_count p)) : ltN n :=
  ltN_of_nat (find_nth p (proj1_sig k)) n (ltN_find_nth_lemma0 p k).

Lemma rewrite_ltN_find_nth {n} (p:array bool n) (k:ltN(ltN_count p)) :
  ltN_find_nth p k =
  match n as n0 return (forall p0 : array bool n0, ltN (ltN_count p0) -> ltN n0) with
  | 0 => fun _ k => match ltN_0_empty k return (ltN 0) with end
  | S n0 => fun p0 k0 =>
      (if shead p0 as b0 return (ltN ((if b0 then 1 else 0) + ltN_count (stail p0)) -> ltN (S n0))
       then fun k1 =>
        match ltN_pred k1 with
        | Some k' => ltN_S (ltN_find_nth (stail p0) k')
        | None => ltN_of_nat 0 (S n0) eq_refl
        end
       else fun k1 => ltN_S (ltN_find_nth (stail p0) k1)) k0
  end p k.
Proof with curry1.
induction n...
specialize(IHn(stail p)).
unfold ltN_find_nth...
dest_match_step...
unfold ltN_pred.
rewrite_dtt_nat...
split...
Qed.

Lemma reverse_nth_lemma0 {n} (p:array bool(S n)) (k:ltN(S n)) (H:p k = true)
  (k' : ltN n) (E : ltN_pred k = Some k') : stail p k' = true.
Proof. rewrite ltN_pred_eq_Some in E; curry1. Qed.

Lemma ltN_count_using_spop {n} (p:array bool (S n)) (k:ltN(S n)) :
  ltN_count p = (if p k then 1 else 0) + ltN_count(spop k p).
Proof with curry1.
simpl.
generalize dependent k.
generalize dependent p.
induction n; intros; [ curry1 | ].
destruct(ltN_pred k) as [k'|]eqn:E0...
- specialize(IHn (stail p) k')...
  rewrite spop_ltN_S...
  dest_match_step...
- destruct k...
Qed.

Lemma reverse_nth_lemma1 {n} (p:array bool(S n)) (k:ltN(S n)) (H:p k = true)
  (E : ltN_pred k = None) : (0 <? ltN_count p) = true.
Proof. rewrite (ltN_count_using_spop _ k); curry1. Qed.

Fixpoint reverse_nth {n} (p:array bool n) (k:nat) {struct n} : nat :=
  match n as n0 return array bool n0 -> nat with
  | 0 => fun _ => 0
  | S n => fun p =>
    match k with
    | 0 => 0
    | S k' => (if shead p then 1 else 0) + reverse_nth (stail p) k'
    end
  end p.

Lemma reverse_nth_lt_ltN_count {n} (p:array bool n) (k:ltN n) (H:p k = true) :
  reverse_nth p (proj1_sig k) < ltN_count p.
Proof with curry1.
induction n...
specialize(IHn(stail p)).
destruct(ltN_pred k) as [k'|] eqn:E0...
- specialize(IHn k')...
  dest_match_step...
- destruct k...
  unfold shead...
Qed.

Lemma ltN_reverse_nth_lemma0 {n} (p:array bool n) (k:ltN n) (H:p k = true) :
  reverse_nth p (proj1_sig k) <? ltN_count p = true.
Proof. curry1; apply reverse_nth_lt_ltN_count; assumption. Qed.

Definition ltN_reverse_nth {n} (p:array bool n) (k:ltN n) (H:p k = true) : ltN(ltN_count p) :=
  ltN_of_nat (reverse_nth p (proj1_sig k)) (ltN_count p) (ltN_reverse_nth_lemma0 p k H).

Lemma rewrite_ltN_reverse_nth {n} (p:array bool n) (k:ltN n) (H:p k = true) :
  ltN_reverse_nth p k H =
    match n as n0 return (forall (p:array bool n0) (k:ltN n0) (H:p k = true), ltN(ltN_count p)) with
    | 0 => fun p k H => match ltN_0_empty k with end
    | S n => fun p k H =>
      match ltN_pred k as e0 return ltN_pred k = e0 -> ltN (ltN_count p) with
      | Some k' => (fun E =>
        let k'' := ltN_reverse_nth (stail p) k' (reverse_nth_lemma0 p k H k' E) in
        if shead p as b0 return ltN((if b0 then 1 else 0) + ltN_count(stail p))
        then (ltN_S k'') else k''
      )
      | None => fun E => ltN_of_nat 0 (ltN_count p) (reverse_nth_lemma1 p k H E)
      end eq_refl
    end p k H.
Proof with curry1.
rewrite ltN_PI...
induction n...
specialize(IHn(stail p))...
destruct k...
destruct x...
dest_match_step...
Qed.

Definition ltN_of_sig_ltN {n} (p:ltN n -> bool) : (sig (fun k => p k = true) -> ltN(ltN_count p)) :=
  (fun (kp : {k : ltN n | p k = true}) => ltN_reverse_nth p (proj1_sig kp) (proj2_sig kp)).

Lemma ltN_find_nth_spec0 {n} p (k:ltN (@ltN_count n p)) : p (ltN_find_nth p k) = true.
Proof with curry1.
induction n...
rewrite rewrite_ltN_find_nth...
destruct(shead p)eqn:E0...
- dest_match_step...
  specialize(IHn(stail p) l)...
- specialize(IHn(stail p) k)...
Qed.

Definition sig_ltN_of_ltN {n} (p:ltN n -> bool) : (ltN(ltN_count p) -> sig (fun k => p k = true)) :=
  (fun (k:ltN(ltN_count p)) => exist _ (ltN_find_nth p k) (ltN_find_nth_spec0 p k)).

Lemma sig_ltN_of_ltN_of_sig_ltN {n} (p:ltN n -> bool) k : sig_ltN_of_ltN p (ltN_of_sig_ltN p k) = k.
Proof with curry1.
unfold sig_ltN_of_ltN, ltN_of_sig_ltN...
rewrite ltN_PI...
destruct k as [k pk]...
induction n...
specialize(IHn(stail p)).
destruct(ltN_pred k) as [k'|] eqn:E0...
- specialize(IHn k')...
  dest_match_step...
- destruct k...
  unfold shead...
Qed.
#[export] Hint Rewrite @sig_ltN_of_ltN_of_sig_ltN : curry1_rewrite.

Lemma ltN_of_sig_ltN_of_ltN {n} (p:ltN n -> bool) k : ltN_of_sig_ltN p (sig_ltN_of_ltN p k) = k.
Proof with curry1.
rewrite ltN_PI...
induction n...
specialize(IHn(stail p))...
dest_match_step...
destruct(ltN_pred k) as [k'|] eqn:E0...
Qed.
#[export] Hint Rewrite @ltN_of_sig_ltN_of_ltN : curry1_rewrite.

Lemma explicit_finite_constr_ltN_sig_bool {n} (p:ltN n -> bool) :
  explicit_finite_constr (sig (fun k => p k = true)) (ltN_count p) (ltN_of_sig_ltN p) (sig_ltN_of_ltN p).
Proof with curry1.
constructor...
- apply functional_extensionality...
- apply functional_extensionality...
Qed.

Definition count_t (A:Type) : Type := (A -> bool) -> nat.

Lemma injection_ltN_ltN_implies_le_lemma0 n1 n2 (f:ltN(S n1) -> ltN(S n2)) (I:injection f) :
  let z1 := ltN_of_nat 0 (S n1) eq_refl : ltN (S n1) in
  let z2 := f z1 : ltN (S n2) in
  forall k1, ltN_pop z2 (f(ltN_intro z1 k1)) = AA tt -> False.
Proof with curry1.
simpl...
rewrite (I _ _) in H...
Qed.

Lemma injection_ltN_ltN_implies_le n1 n2 (f:ltN n1 -> ltN n2) (I:injection f) : n1 <= n2.
Proof with curry1.
generalize dependent n2.
induction n1...
destruct n2...
- destruct(ltN_0_empty(f(ltN_of_nat 0 (S n1) eq_refl))).
- pose (ltN_of_nat 0 (S n1) eq_refl) as z1.
  pose (f z1) as z2.
  pose (fun (k1:ltN n1) =>
    match ltN_pop z2 (f(ltN_intro z1 k1)) as r0 return ltN_pop z2 (f(ltN_intro z1 k1)) = r0 -> ltN n2 with
    | AA tt => fun E =>
      match injection_ltN_ltN_implies_le_lemma0 n1 n2 f I k1 E return ltN n2 with end
    | BB k2 => fun _ => k2
    end eq_refl) as f'.
  assert(injection f')...
  + unfold injection...
    unfold f'. clear f'.
    pattern(match ltN_pop z2 (f (ltN_intro z1 x)) as r0 return (ltN_pop z2 (f (ltN_intro z1 x)) = r0 -> ltN n2) with
| AA u =>
    match u as u0 return (ltN_pop z2 (f (ltN_intro z1 x)) = AA u0 -> ltN n2) with
    | tt =>
        fun E : ltN_pop z2 (f (ltN_intro z1 x)) = AA tt =>
        match injection_ltN_ltN_implies_le_lemma0 n1 n2 f I x E return (ltN n2) with
        end
    end
| BB k2 => fun _ : ltN_pop z2 (f (ltN_intro z1 x)) = BB k2 => k2
end eq_refl).
    rewrite match_AB_dtt...
    split...
    * dest_match_step...
    * pattern(match ltN_pop z2 (f (ltN_intro z1 y)) as r0 return (ltN_pop z2 (f (ltN_intro z1 y)) = r0 -> ltN n2) with
| AA u =>
    match u as u0 return (ltN_pop z2 (f (ltN_intro z1 y)) = AA u0 -> ltN n2) with
    | tt =>
        fun E : ltN_pop z2 (f (ltN_intro z1 y)) = AA tt =>
        match injection_ltN_ltN_implies_le_lemma0 n1 n2 f I y E return (ltN n2) with
        end
    end
| BB k2 => fun _ : ltN_pop z2 (f (ltN_intro z1 y)) = BB k2 => k2
end eq_refl).
      rewrite match_AB_dtt...
      split...
      -- dest_match_step...
      -- symmetry in p0.
         split; curry1; rewrite_subst...
         rewrite (I _ _) in p0...
  + specialize(IHn1 _ _ H)...
Qed.

Lemma explicit_bijection_constr_ltN_unique n1 n2 idx pi :
  @explicit_bijection_constr (ltN n1) (ltN n2) idx pi -> n1 = n2.
Proof with curry1.
intros H.
specialize(injection_of_explicit_bijection_constr H) as Iidx.
rewrite comm_explicit_bijection_constr in H.
specialize(injection_of_explicit_bijection_constr H) as Ipi.
apply injection_ltN_ltN_implies_le in Iidx.
apply injection_ltN_ltN_implies_le in Ipi.
curry1.
Qed.

Lemma explicit_finite_constr_unique (A:Type) n1 n2 i1 i2 p1 p2 :
  explicit_finite_constr A n1 i1 p1 ->
  explicit_finite_constr A n2 i2 p2 -> n1 = n2.
Proof with curry1.
intros H1 H2.
unfold explicit_finite_constr in *.
rewrite comm_explicit_bijection_constr in H1.
specialize(explicit_bijection_constr_trans H1 H2) as H12.
apply explicit_bijection_constr_ltN_unique in H12...
Qed.

Definition is_counting (A:Type) (count:count_t A) : Prop :=
  forall (p:A -> bool), exists idx pi, explicit_finite_constr (sig (fun k => p k = true)) (count p) idx pi.

Lemma is_counting_unique (A:Type) (c1 c2:count_t A) : is_counting A c1 -> is_counting A c2 -> c1 = c2.
Proof with curry1.
intros H1 H2.
unfold is_counting in *.
apply functional_extensionality; intros p...
specialize(H1 p). specialize(H2 p).
destruct H1 as [i1 [p1 H1]].
destruct H2 as [i2 [p2 H2]].
specialize(explicit_finite_constr_unique _ _ _ _ _ _ _ H1 H2)...
Qed.


(* ltN_count array_app *)

Lemma ltN_count_split {n} (p d:ltN n -> bool) :
  ltN_count p = ltN_count (fun k => p k && d k) + ltN_count (fun k => p k && (negb(d k))).
Proof with curry1.
induction n...
specialize(IHn(stail p)(stail d))...
unfold shead, stail in *...
dest_match_step...
dest_match_step...
Qed.

Lemma compose_explicit_finite_constr_and_explicit_bijection_constr {A B nB iAB pAB iB pB} :
  @explicit_bijection_constr A B iAB pAB ->
  @explicit_finite_constr B nB iB pB ->
  @explicit_finite_constr A nB (comp iAB iB) (comp pB pAB).
Proof with curry1.
unfold explicit_finite_constr.
apply explicit_bijection_constr_trans.
Qed.

Lemma ltN_count_unique {n1} (p1:ltN n1 -> bool) {n2} (p2:ltN n2 -> bool) i12 p12 :
  @explicit_bijection_constr (sig (fun k => p1 k = true)) (sig(fun k => p2 k = true)) i12 p12 ->
      ltN_count p1 = ltN_count p2.
Proof with curry1.
intros H12.
specialize(explicit_finite_constr_ltN_sig_bool p1) as H1.
specialize(explicit_finite_constr_ltN_sig_bool p2) as H2.
specialize(compose_explicit_finite_constr_and_explicit_bijection_constr H12 H2) as H2'.
specialize(explicit_finite_constr_unique _ _ _ _ _ _ _ H1 H2')...
Qed.

Lemma ltN_count_unique_exists {n1} (p1:ltN n1 -> bool) {n2} (p2:ltN n2 -> bool) :
  (exists i12 p12, @explicit_bijection_constr (sig (fun k => p1 k = true)) (sig(fun k => p2 k = true)) i12 p12) ->
      ltN_count p1 = ltN_count p2.
Proof with curry1.
curry1.
apply(ltN_count_unique p1 p2 i12 p12)...
Qed.

Lemma ltN_count_eq_using_bijection {n1} (p1:ltN n1 -> bool) {n2} (p2:ltN n2 -> bool) :
  bijection (sig (fun k => p1 k = true)) (sig(fun k => p2 k = true)) -> ltN_count p1 = ltN_count p2.
Proof with curry1.
intros [T P].
apply (ltN_count_unique p1 p2 (fst T) (snd T) P).
Qed.






Lemma ltN_split_ltN_up_cast_case00 n1 (k:ltN n1) n2 H :
  ltN_split n1 n2 (ltN_up_cast k (n1 + n2) H) = AA k.
Proof with curry1.
rewrite rewrite_ltN_split_eq...
apply ltN_PI...
Qed.

Lemma ltN_count_array_app_def1_bijection_type_lemma0 {n1} (a1:array bool n1) {n2} (a2:array bool n2)
  (k : ltN (n1 + n2)) (pk : array_app a1 a2 k && negb (proj1_sig k <? n1) = true) :
    proj1_sig k - n1 <? n2 = true.
Proof with curry1.
destruct k...
clear H; for_lia1.
Qed.

Lemma ltN_count_array_app_def1_bijection_type_lemma1  {n1} (a1:array bool n1) {n2} (a2:array bool n2)
  (k : ltN (n1 + n2)) (pk : array_app a1 a2 k && negb (proj1_sig k <? n1) = true) :
    a2 (ltN_of_nat (proj1_sig k - n1) n2 (ltN_count_array_app_def1_bijection_type_lemma0 a1 a2 k pk)) = true.
Proof with curry1.
generalize(ltN_count_array_app_def1_bijection_type_lemma0 a1 a2 k pk)...
unfold array_app in H...
dest_match_step; rewrite rewrite_ltN_split_eq in D...
- destruct l as [k p]; curry1.
  exfalso...
  clear H...
- rename l into k.
  assert(R: k = ltN_of_nat (n1 + proj1_sig k - n1) n2 e).
  {
    apply ltN_PI...
    lia.
  }
  rewrite R in H...
Qed.

Lemma ltN_count_array_app_def1_bijection_type_lemma2 {n1} (a1:array bool n1) {n2} (a2:array bool n2)
  (k : ltN n2) (a2k : a2 k = true) :
    array_app a1 a2 (plus_ltN n1 k) && negb (proj1_sig (plus_ltN n1 k) <? n1) = true.
Proof with curry1.
unfold array_app...
for_lia1.
Qed.

Definition ltN_count_array_app_def1_bijection_type {n1} (a1:array bool n1) {n2} (a2:array bool n2) :
  bijection_type
    {k : ltN (n1 + n2) | array_app a1 a2 k && negb (proj1_sig k <? n1) = true}
    {k : ltN n2 | a2 k = true}.
Proof.
constructor.
- intros [k pk].
  refine (exist _ (ltN_of_nat (proj1_sig k - n1) n2 (ltN_count_array_app_def1_bijection_type_lemma0 a1 a2 k pk)) _).
  apply ltN_count_array_app_def1_bijection_type_lemma1.
- intros [k a2k].
  refine (exist _ (plus_ltN n1 k) _).
  apply ltN_count_array_app_def1_bijection_type_lemma2.
  assumption.
Defined.

Lemma proj1_sig_ltN_of_nat k n p : proj1_sig(ltN_of_nat k n p) = k.
Proof. reflexivity. Qed.

Lemma ltN_count_array_app_def1_bijection_prop  {n1} (a1:array bool n1) {n2} (a2:array bool n2) :
  bijection_prop (ltN_count_array_app_def1_bijection_type a1 a2).
Proof with curry1.
split...
- apply functional_extensionality...
  dest_match_step...
  apply ltN_PI...
- apply functional_extensionality...
  dest_match_step...
  lia.
Qed.

Definition ltN_count_array_app_def1_bijection {n1} (a1:array bool n1) {n2} (a2:array bool n2) :
  bijection
    {k : ltN (n1 + n2) | array_app a1 a2 k && negb (proj1_sig k <? n1) = true}
    {k : ltN n2 | a2 k = true} :=
      exist _ _ (ltN_count_array_app_def1_bijection_prop a1 a2).

Lemma ltN_count_array_app {n1} (a1:array bool n1) {n2} (a2:array bool n2) :
  ltN_count (array_app a1 a2) = ltN_count a1 + ltN_count a2.
Proof with curry1.
rewrite (ltN_count_split (array_app a1 a2) (fun k => proj1_sig k <? n1)). 
assert(R1:ltN_count (fun k : ltN (n1 + n2) => array_app a1 a2 k && (proj1_sig k <? n1)) = ltN_count a1).
{
  apply ltN_count_eq_using_bijection.
  specialize(@bijection_sig_ltN_implied_cast_down (n1 + n2) (fun k => array_app a1 a2 k && (proj1_sig k <? n1)) n1) as H.
  simpl in H.
  assert_hypothesis H...
  assert_hypothesis H...
  { lia. }
  refine (bijection_comp H _)...
  refine (bijection_cast_sig _ _ _)...
  apply functional_extensionality...
  unfold array_app.
  rewrite ltN_split_ltN_up_cast_case00...
}
assert(R2:ltN_count (fun k : ltN (n1 + n2) => array_app a1 a2 k && negb (proj1_sig k <? n1)) = ltN_count a2).
{
  apply ltN_count_eq_using_bijection.
  apply ltN_count_array_app_def1_bijection.
}
rewrite R1, R2...
Qed.

Definition AB_count {A B:Type} (cA:count_t A) (cB:count_t B) : count_t (@AB A B) :=
  (fun p => cA (fun x => p(AA x)) + cB (fun x => p(BB x))).

(* [MOVEME] *)

Lemma is_counting_as_bijection {A} cA :
  is_counting A cA <-> forall p, exists (b:bijection {a : A | p a = true} (ltN(cA p))), True.
Proof with curry1.
split...
- destruct (H p) as [IA [SA PA]].
  exists (exist _ (pair IA SA) PA)...
- unfold is_counting...
  specialize(H p)...
  destruct b as [[IA SA] PA]...
  unfold bijection_prop in PA...
  exists IA.
  exists SA...
Qed.


Lemma is_counting_AB_count {A B} cA (pA:is_counting A cA) cB (pB:is_counting B cB) :
  is_counting _ (AB_count cA cB).
Proof with curry1.
rewrite is_counting_as_bijection...
rewrite is_counting_as_bijection in pA.
rewrite is_counting_as_bijection in pB.
specialize (pA (fun x => p(AA x))) as [BijA _].
specialize (pB (fun x => p(BB x))) as [BijB _].
exists (bijection_AB_ltN_using_bijection_A_ltN_and_B_ltN A B p _ BijA _ BijB)...
Qed.
