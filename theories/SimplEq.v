Require Import Arith.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.

Definition beqt {A} := A -> A -> bool.

Definition beq_reflexive {A} (beq:@beqt A) := forall a, beq a a = true.
Definition beq_iff_false {A} (beq:@beqt A) := forall a b,
  beq a b = false <-> (a = b -> False).
Definition beq_iff_true  {A} (beq:@beqt A) := forall a b,
  beq a b = true  <-> (a = b).
Definition beq_is_eq {A} (beq:@beqt A) := forall a b,
  (beq a b = true /\ a = b) \/ (beq a b = false /\ a <> b).
Definition beq_sym {A} (beq:@beqt A) := forall a b,
  (beq a b = beq b a).

Lemma beq_iff_false_of_iff_true {A} {beq:@beqt A} :
  beq_iff_true beq -> beq_iff_false beq.
Proof.
intros H a b.
specialize (H a b).
apply not_iff_compat in H.
simplbool.
Qed.

Lemma beq_reflexive_of_iff_true {A} {beq:@beqt A} :
  beq_iff_true beq -> beq_reflexive beq.
Proof.
intros H a.
specialize(H a a).
simplprop.
Qed.

Lemma beq_is_eq_of_iff_true {A} {beq:@beqt A} :
  beq_iff_true beq -> beq_is_eq beq.
Proof.
intros HT a b.
specialize (beq_iff_false_of_iff_true HT) as HF.
specialize (HT a b).
specialize (HF a b).
unfold not.
rewrite <- HF, <- HT.
simplbool.
apply bool_disjunction.
Qed.

Lemma beq_sym_of_iff_true {A} {beq} :
  @beq_iff_true A beq -> forall x y, beq x y = beq y x.
Proof with simplprop.
intros.
rewrite bool_eq_iff_iff; split...
- rewrite (H _ _) in H0...
  rewrite (H _ _)...
- rewrite (H _ _) in H0...
  rewrite (H _ _)...
Qed.

Lemma beq_rewrite_false {A} {beq} :
  @beq_iff_true A beq -> forall x y, x <> y -> beq x y = false.
Proof.
intros.
apply (proj2 (beq_iff_false_of_iff_true H _ _) H0).
Qed.

Lemma beq_rewrite_true {A} {beq}  :
  @beq_iff_true A beq -> forall x y, beq x y = true <-> x = y.
Proof.
intros.
split; simplprop; apply H; simplprop.
Qed.

Lemma beq_rewrite {A} {beq} :
  @beq_iff_true A beq -> forall x y, beq x y = true -> x = y.
Proof.
intros.
rewrite (beq_rewrite_true H x y) in H0; auto.
Qed.

Ltac simpleq_main_step beq EQ := match goal with
| H : context[beq ?x ?x] |- _ =>
  rewrite ((beq_reflexive_of_iff_true EQ) x) in H
| |- context[beq ?x ?x] =>
  rewrite ((beq_reflexive_of_iff_true EQ) x)
| S : ?x = ?y, H : context [beq ?x ?y] |- _ =>
  rewrite S in H; rewrite ((beq_reflexive_of_iff_true EQ) y) in H
| S : ?x = ?y, H : context [beq ?y ?x] |- _ =>
  rewrite S in H; rewrite ((beq_reflexive_of_iff_true EQ) y) in H
| S : ?x = ?y |- context [beq ?x ?y] =>
  rewrite S; rewrite ((beq_reflexive_of_iff_true EQ) y)
| S : ?x = ?y |- context [beq ?y ?x] =>
  rewrite S; rewrite ((beq_reflexive_of_iff_true EQ) y)
| H1 : ?x <> ?y, H2 : context[beq ?x ?y] |- _ =>
  rewrite (beq_rewrite_false EQ x y H1) in H2
| H1 : ?x <> ?y |- context[beq ?x ?y] =>
  rewrite (beq_rewrite_false EQ x y H1)
| S : context [beq ?x ?y = true],
  H : context [beq ?z ?x = true] |- _ =>
  rewrite (beq_rewrite EQ x y S) in H
| S : context [beq ?x ?y = true],
  H : context [?x] |- _ =>
  match y with
  | context[x] => fail 1
  | _ => rewrite (beq_rewrite EQ x y S) in H
  end
| S : context [beq ?x ?y = true]
  |- context [?x] =>
  match y with
  | context[x] => fail 1
  | _ => rewrite (beq_rewrite EQ x y S)
  end
| |- beq ?x ?y = false =>
  let E := fresh "E" in destruct(beq x y) eqn:E
end.

Ltac simpleq_main beq EQ :=
  simplbool; repeat(simpleq_main_step beq EQ; simplbool).

Ltac simpleq_auto_step := match goal with
  | H : beq_iff_true ?beq |- _ => simpleq_main_step beq H
end.

Ltac simpleq_auto := simplbool; repeat(simpleq_auto_step; simplbool).

Definition beq_unit : unit -> unit -> bool.
intros.
apply true.
Defined.

Lemma beq_unit_iff_true : beq_iff_true beq_unit.
Proof.
intros [] [].
simplbool.
Qed.

#[export] Hint Resolve beq_unit_iff_true : core.

(* Section nat1_beq *)

Definition j1beqt {A:nat->Type} :=
  forall n n', A n -> A n' -> bool.

Definition j1eq {A:nat->Type} {n n'} (x:A n) (y:A n') :=
  n = n' /\ x =J y.

Definition j1neq {A:nat->Type} {n n'} (x:A n) (y:A n') :=
  j1eq x y -> False.

Definition j1beq_reflexive {A} (beq:@j1beqt A) :=
  forall n a, beq n n a a = true.
Definition j1beq_iff_false {A} (beq:@j1beqt A) :=
  forall n n' a b,
    beq n n' a b = false <-> j1neq a b.
Definition j1beq_iff_true  {A} (beq:@j1beqt A) :=
  forall n n' a b,
    beq n n' a b = true <-> j1eq a b.
Definition j1beq_is_eq {A} (beq:@j1beqt A) :=
  forall n n' a b,
    (beq n n' a b = true /\ j1eq a b) \/
      (beq n n' a b = false /\ j1neq a b).

Lemma j1beqc_of_beqc {A} (beq:@j1beqt A) :
  (forall n, beq_iff_true (beq n n)) ->
  (forall n n' a b, beq n n' a b = true -> n = n') ->
    (j1beq_iff_true beq).
Proof with simplbool.
intros He Hn n n'.
unfold j1eq.
destruct (Nat.eqb_spec n n').
- subst...
  rewrite (He n' a b).
  split...
- intros a b.
  split...
  apply Hn in H...
Qed.

Lemma j1beq_iff_false_of_iff_true {A} {beq:@j1beqt A} :
  j1beq_iff_true beq -> j1beq_iff_false beq.
Proof.
intros H n n' a b.
specialize (H _ _ a b).
apply not_iff_compat in H.
simplbool.
Qed.

Lemma j1beq_reflexive_of_iff_true {A} {beq:@j1beqt A} :
  j1beq_iff_true beq -> j1beq_reflexive beq.
Proof.
intros H n a.
rewrite (H n n a a).
unfold j1eq.
simplprop.
Qed.

Lemma j1beq_is_eq_of_iff_true {A} {beq:@j1beqt A} :
  j1beq_iff_true beq -> j1beq_is_eq beq.
Proof.
intros HT n n' a b.
specialize (j1beq_iff_false_of_iff_true HT) as HF.
specialize (HT _ _ a b).
specialize (HF _ _ a b).
rewrite <- HF, <- HT.
simplbool.
apply bool_disjunction.
Qed.

Lemma j1beq_rewrite_false {A} {beq} {n n' x y} :
  @j1beq_iff_true A beq -> j1neq x y -> beq n n' x y = false.
Proof.
intros.
apply (proj2 (j1beq_iff_false_of_iff_true H _ _ _ _) H0).
Qed.

Ltac simplj1eq_main_step beq EQ := match goal with
| H : context[beq ?x ?x] |- _ =>
  rewrite ((j1beq_reflexive_of_iff_true EQ) x) in H
| |- context[beq ?x ?x] =>
  rewrite ((j1beq_reflexive_of_iff_true EQ) x)
| H1 : j1neq ?x ?y, H2 : context[beq ?x ?y] |- _ =>
  rewrite (j1beq_rewrite_false EQ H1) in H2
| H1 : j1neq ?x ?y |- context[beq ?x ?y] =>
  rewrite (j1beq_rewrite_false EQ H1)
| H : context [beq ?x ?y = true] |- _ =>
  rewrite (EQ x y) in H
| |- context [beq ?x ?y = true] =>
  rewrite (EQ x y)
| H : context [beq ?x ?y] |- _ =>
  destruct (j1beq_is_eq_of_iff_true EQ x y)
| |- context [beq ?x ?y] =>
  destruct (j1beq_is_eq_of_iff_true EQ x y)
end.

Ltac simplj1eq_main beq EQ :=
  simplbool; repeat(simplj1eq_main_step beq EQ; simplbool).

Ltac simplj1eq_auto_step := match goal with
  | H : beq_iff_true ?beq |- _ => simpleq_main_step beq H
  | H : j1beq_iff_true ?beq |- _ => simplj1eq_main_step beq H
end.

Ltac simplj1eq_auto := simplbool; repeat(simplj1eq_auto_step; simplbool).

(* Section nat2_beq *)

Definition j2beqt {A:nat->nat->Type} :=
  forall n n' m m', A n m -> A n' m' -> bool.

Definition j2eq {A:nat->nat->Type} {n n' m m'} (x:A n m) (y:A n' m') :=
  n = n' /\ m = m' /\ x =J y.

Definition j2neq {A:nat->nat->Type} {n n' m m'} (x:A n m) (y:A n' m') :=
  j2eq x y -> False.

Definition j2beq_reflexive {A} (beq:@j2beqt A) :=
  forall m n a, beq n n m m a a = true.
Definition j2beq_iff_false {A} (beq:@j2beqt A) :=
  forall m m' n n' a b,
    beq n n' m m' a b = false <-> j2neq a b.
Definition j2beq_iff_true  {A} (beq:@j2beqt A) :=
  forall m m' n n' a b,
    beq n n' m m' a b = true <-> j2eq a b.
Definition j2beq_is_eq {A} (beq:@j2beqt A) :=
  forall m m' n n' a b,
    (beq n n' m m' a b = true /\ j2eq a b) \/
      (beq n n' m m' a b = false /\ j2neq a b).

Lemma j2beq_iff_false_of_iff_true {A} {beq:@j2beqt A} :
  j2beq_iff_true beq -> j2beq_iff_false beq.
Proof.
intros H m m' n n' a b.
specialize (H m m' n n' a b).
apply not_iff_compat in H.
simplbool.
Qed.

Lemma j2beq_reflexive_of_iff_true {A} {beq:@j2beqt A} :
  j2beq_iff_true beq -> j2beq_reflexive beq.
Proof.
intros H m n a.
rewrite (H m m n n a a).
unfold j2eq.
simplprop.
Qed.

Lemma j2beq_is_eq_of_iff_true {A} {beq:@j2beqt A} :
  j2beq_iff_true beq -> j2beq_is_eq beq.
Proof.
intros HT m m' n n' a b.
specialize (j2beq_iff_false_of_iff_true HT) as HF.
specialize (HT _ _ _ _ a b).
specialize (HF _ _ _ _ a b).
rewrite <- HF, <- HT.
simplbool.
apply bool_disjunction.
Qed.

Lemma j2beq_rewrite_false {A} {beq} {m m' n n' x y} :
  @j2beq_iff_true A beq -> j2neq x y -> beq n n' m m' x y = false.
Proof.
intros.
apply (proj2 (j2beq_iff_false_of_iff_true H _ _ _ _ _ _) H0).
Qed.

Ltac simplj2eq_main_step beq EQ := match goal with
| H : context[beq ?x ?x] |- _ =>
  rewrite ((j2beq_reflexive_of_iff_true EQ) x) in H
| |- context[beq ?x ?x] =>
  rewrite ((j2beq_reflexive_of_iff_true EQ) x)
| H1 : j1neq ?x ?y, H2 : context[beq ?x ?y] |- _ =>
  rewrite (j2beq_rewrite_false EQ H1) in H2
| H1 : j1neq ?x ?y |- context[beq ?x ?y] =>
  rewrite (j2beq_rewrite_false EQ H1)
| H : context [beq ?x ?y = true] |- _ =>
  rewrite (EQ x y) in H
| |- context [beq ?x ?y = true] =>
  rewrite (EQ x y)
| H : context [beq ?x ?y] |- _ =>
  destruct (j2beq_is_eq_of_iff_true EQ x y)
| |- context [beq ?x ?y] =>
  destruct (j2beq_is_eq_of_iff_true EQ x y)
end.

Ltac simplj2eq_main beq EQ :=
  simplbool; repeat(simplj2eq_main_step beq EQ; simplbool).

Ltac simplj2eq_auto_step := match goal with
  | H : beq_iff_true ?beq |- _ => simpleq_main_step beq H
  | H : j1beq_iff_true ?beq |- _ => simplj1eq_main_step beq H
  | H : j2beq_iff_true ?beq |- _ => simplj2eq_main_step beq H
end.

Ltac simplj2eq_auto := simplbool; repeat(simplj2eq_auto_step; simplbool).

(* Section nat3_beq *)

Definition j3beqt {A:nat->nat->nat->Type} :=
  forall n n' m m' k k', A n m k -> A n' m' k' -> bool.

Definition j3eq {A:nat->nat->nat->Type} {n n' m m' k k'} (x:A n m k) (y:A n' m' k') :=
  n = n' /\ m = m' /\ k = k' /\ x =J y.

Lemma j3eq_sym {A:nat->nat->nat->Type} {n n' m m' k k'} (x:A n m k) (y:A n' m' k') :
  j3eq x y <-> j3eq y x.
Proof. unfold j3eq. split; simplprop. Qed.

Definition j3neq {A:nat->nat->nat->Type} {n n' m m' k k'} (x:A n m k) (y:A n' m' k') :=
  j3eq x y -> False.

Lemma j3neq_sym {A:nat->nat->nat->Type} {n n' m m' k k'} (x:A n m k) (y:A n' m' k') :
  j3neq x y <-> j3neq y x.
Proof. unfold j3neq. rewrite j3eq_sym. reflexivity. Qed.

Definition j3beq_reflexive {A} (beq:@j3beqt A) :=
  forall k m n a, beq n n m m k k a a = true.
Definition j3beq_iff_false {A} (beq:@j3beqt A) :=
  forall k k' m m' n n' a b,
    beq n n' m m' k k' a b = false <-> j3neq a b.
Definition j3beq_iff_true  {A} (beq:@j3beqt A) :=
  forall k k' m m' n n' a b,
    beq n n' m m' k k' a b = true <-> j3eq a b.
Definition j3beq_is_eq {A} (beq:@j3beqt A) :=
  forall k k' m m' n n' a b,
    (beq n n' m m' k k' a b = true /\ j3eq a b) \/
      (beq n n' m m' k k' a b = false /\ j3neq a b).

Lemma j3beq_iff_false_of_iff_true {A} {beq:@j3beqt A} :
  j3beq_iff_true beq -> j3beq_iff_false beq.
Proof.
intros H k k' m m' n n' a b.
specialize (H _ _ _ _ _ _ a b).
apply not_iff_compat in H.
simplbool.
Qed.

Lemma j3beq_reflexive_of_iff_true {A} {beq:@j3beqt A} :
  j3beq_iff_true beq -> j3beq_reflexive beq.
Proof.
intros H k m n a.
rewrite (H _ _ _ _ _ _ a a).
unfold j3eq.
simplprop.
Qed.

Lemma j3beq_is_eq_of_iff_true {A} {beq:@j3beqt A} :
  j3beq_iff_true beq -> j3beq_is_eq beq.
Proof.
intros HT k k' m m' n n' a b.
specialize (j3beq_iff_false_of_iff_true HT) as HF.
specialize (HT _ _ _ _ _ _ a b).
specialize (HF _ _ _ _ _ _ a b).
rewrite <- HF, <- HT.
simplbool.
apply bool_disjunction.
Qed.

Lemma j3beq_rewrite_false {A} {beq} {k k' m m' n n' x y} :
  @j3beq_iff_true A beq -> j3neq x y -> beq n n' m m' k k' x y = false.
Proof.
intros.
apply (proj2 (j3beq_iff_false_of_iff_true H _ _ _ _ _ _ _ _) H0).
Qed.

Ltac simplj3eq_main_step beq EQ := match goal with
| H : context[beq ?x ?x] |- _ =>
  rewrite ((j3beq_reflexive_of_iff_true EQ) x) in H
| |- context[beq ?x ?x] =>
  rewrite ((j3beq_reflexive_of_iff_true EQ) x)
| H1 : j3neq ?x ?y, H2 : context[beq ?x ?y] |- _ =>
  rewrite (j3beq_rewrite_false EQ H1) in H2
| H1 : j3neq ?x ?y |- context[beq ?x ?y] =>
  rewrite (j3beq_rewrite_false EQ H1)
| H : context [beq ?x ?y = true] |- _ =>
  rewrite (EQ x y) in H
| |- context [beq ?x ?y = true] =>
  rewrite (EQ x y)
| H : context [beq ?x ?y] |- _ =>
  destruct (j3beq_is_eq_of_iff_true EQ x y)
| |- context [beq ?x ?y] =>
  destruct (j3beq_is_eq_of_iff_true EQ x y)
end.

Ltac simplj3eq_main beq EQ :=
  simplbool; repeat(simplj3eq_main_step beq EQ; simplbool).

Ltac simplj3eq_auto_step := match goal with
  | H : beq_iff_true ?beq |- _ => simpleq_main_step beq H
  | H : j1beq_iff_true ?beq |- _ => simplj1eq_main_step beq H
  | H : j2beq_iff_true ?beq |- _ => simplj2eq_main_step beq H
  | H : j3beq_iff_true ?beq |- _ => simplj3eq_main_step beq H
end.

Ltac simplj3eq_auto := simplbool; repeat(simplj3eq_auto_step; simplbool).
