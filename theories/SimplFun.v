Require Import Arith.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplEq.

(* Section 1. Comp *)

Definition comp {A B C} (f:A->B) (g:B->C) := (fun x => g(f x)).

Lemma apply_id A x : @id A x = x.
Proof. reflexivity. Qed.
#[export] Hint Rewrite apply_id : simplprop1_rewrite.

Lemma apply_comp A B C f g x : @comp A B C f g x = g(f(x)).
Proof. reflexivity. Qed.
#[export] Hint Rewrite apply_comp : simplprop1_rewrite.

Lemma comp_assoc {A B C D} (f1:A->B) (f2:B->C) (f3:C->D) :
  comp f1 (comp f2 f3) = comp (comp f1 f2) f3.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @comp_assoc : simplprop1_rewrite.

Lemma comp_id_l {A B} (f:A->B) : comp id f = f.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @comp_id_l : simplprop1_rewrite.

Lemma comp_id_r {A B} (f:A->B) : comp f id = f.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @comp_id_l : simplprop1_rewrite.

Lemma comp_cst {A B C} f0 g : @comp A B C (fun _ => f0) g = (fun _ => g f0).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @comp_cst : simplprop1_rewrite.

(* Section 2. Bijection *)

Definition injection {T1 T2:Type} (f:T1 -> T2) : Prop := forall (x y:T1), f x = f y <-> x = y.
Definition surjection {T1 T2:Type} (f:T1 -> T2) : Prop := forall (y:T2), exists (x:T1), f x = y.
Definition explicit_bijection {T1 T2:Type} (f:T1 -> T2) : Prop := injection f /\ surjection f.
Definition explicit_bijection_constr {T1 T2:Type} (f:T1 -> T2) (g:T2 -> T1) : Prop :=
  (comp f g = id) /\ (comp g f = id).

Lemma explicit_bijection_constr_implies_explicit_bijection {T1 T2 f g} :
  @explicit_bijection_constr T1 T2 f g -> explicit_bijection f.
Proof with simplprop.
unfold explicit_bijection_constr.
unfold explicit_bijection...
unfold injection, surjection...
split...
- split...
  specialize(f_equal (fun v => g v) H1)...
  specialize(f_equal (fun fg => fg x) H)...
  specialize(f_equal (fun fg => fg y) H)...
  rewrite H3, H4 in H2...
- exists (g y)...
  specialize(f_equal (fun gf => gf y) H0)...
Qed.

Lemma comm_explicit_bijection_constr {T1 T2} f g :
  @explicit_bijection_constr T1 T2 f g <-> explicit_bijection_constr g f.
Proof. unfold explicit_bijection_constr; simplprop. Qed.

Lemma injection_of_explicit_bijection_constr {T1 T2 f g} :
  @explicit_bijection_constr T1 T2 f g -> injection f.
Proof with simplprop.
intro H.
destruct(explicit_bijection_constr_implies_explicit_bijection H)...
Qed.

Lemma surjection_of_explicit_bijection_constr {T1 T2 f g} :
  @explicit_bijection_constr T1 T2 f g -> surjection f.
Proof with simplprop.
intro H.
destruct(explicit_bijection_constr_implies_explicit_bijection H)...
Qed.

Lemma explicit_bijection_constr_trans {A B C:Type} {iAB pAB iBC pBC} :
  @explicit_bijection_constr A B iAB pAB ->
  @explicit_bijection_constr B C iBC pBC ->
  @explicit_bijection_constr A C (comp iAB iBC) (comp pBC pAB).
Proof with simplprop.
intros [AB1 AB2] [BC1 BC2].
unfold explicit_bijection_constr in *...
rewrite <- (comp_assoc iAB iBC pBC), BC1...
rewrite <- (comp_assoc pBC pAB iAB), AB2...
Qed.


(* Bijection Type *)

Definition bijection_type (T1 T2:Type) : Type := (T1 -> T2) * (T2 -> T1).
Definition bijection_prop {T1 T2} (b:bijection_type T1 T2) : Prop := explicit_bijection_constr (fst b) (snd b).

Definition bijection (T1 T2:Type) : Type := { b : bijection_type T1 T2 | bijection_prop b}.

Definition bijection_fst {T1 T2} (b:bijection T1 T2) := fst(proj1_sig b).
Definition bijection_snd {T1 T2} (b:bijection T1 T2) := snd(proj1_sig b).

Definition bijection_type_comp {A B C} (ab:bijection_type A B) (bc:bijection_type B C) : bijection_type A C :=
  pair (comp (fst ab) (fst bc)) (comp (snd bc) (snd ab)).
Lemma bijection_prop_comp {A B C} {ab} (pab:@bijection_prop A B ab) {bc} (pbc:@bijection_prop B C bc) :
  @bijection_prop A C (bijection_type_comp ab bc).
Proof with simplprop.
unfold bijection_prop, bijection_type_comp...
unfold bijection_prop in *.
apply (explicit_bijection_constr_trans pab pbc).
Qed.

Definition bijection_comp {A B C} (AB:bijection A B) (BC:bijection B C) : bijection A C :=
  exist _ _ (bijection_prop_comp (proj2_sig AB) (proj2_sig BC)).

Definition bijection_refl (A:Type) : bijection A A.
Proof.
refine (exist _ (pair id id) _).
split; reflexivity.
Qed.


Lemma bijection_sym_lemma0 A B (F : bijection_type A B) (P : bijection_prop F) : bijection_prop (snd F, fst F).
Proof with simplprop.
destruct F, P...
unfold bijection_prop,explicit_bijection_constr in *...
Qed.

Definition bijection_sym {A B:Type} : bijection A B -> bijection B A.
Proof.
intros [F P].
refine (exist _ (pair (snd F) (fst F)) (bijection_sym_lemma0 _ _ F P)).
Defined.

Lemma sig_cast_predicate_using_eq_lemma0 {A} (F G:A -> bool) (H:F = G) : forall x, F x = true -> G x = true.
Proof. simplprop. Qed.

Definition sig_cast_predicate {A} (F G:A -> bool) (H:forall x, F x = true -> G x = true) :
  {x : A | F x = true} -> {x : A | G x = true} :=
    (fun kp => exist _ (proj1_sig kp) (H (proj1_sig kp) (proj2_sig kp))).

Definition sig_cast_predicate_using_eq {A} (F G:A -> bool) (H:F = G) :=
  sig_cast_predicate F G (sig_cast_predicate_using_eq_lemma0 F G H).

Lemma sig_cast_predicate_using_eq_refl {A} (F:A -> bool) : sig_cast_predicate_using_eq F F eq_refl = id.
Proof with simplprop.
apply functional_extensionality...
destruct x...
Qed.
#[export] Hint Rewrite @sig_cast_predicate_using_eq_refl : simplprop1_rewrite.

Lemma bijection_sig_cast_eq_predicate {A} (F G:A -> bool) (H:F = G) :
  @explicit_bijection_constr {x : A | F x = true} {x : A | G x = true}
    (sig_cast_predicate_using_eq F G H)
    (sig_cast_predicate_using_eq G F (eq_sym H)).
Proof. unfold explicit_bijection_constr; simplprop.  Qed.

Definition bijection_type_cast_sig {A:Type} (P Q:A -> bool) (H : P = Q) :
  bijection_type (sig (fun x => P x = true)) (sig (fun x => Q x = true)) :=
  pair (sig_cast_predicate_using_eq P Q H) (sig_cast_predicate_using_eq Q P (eq_sym H)).
Lemma bijection_prop_cast_sig {A:Type} (P Q:A -> bool) (H : P = Q) :
  bijection_prop (bijection_type_cast_sig P Q H).
Proof. apply bijection_sig_cast_eq_predicate. Qed.

Definition bijection_cast_sig {A:Type} (P Q:A -> bool) (H : P = Q) :
  bijection (sig (fun x => P x = true)) (sig (fun x => Q x = true)) :=
    (exist _ _ (bijection_prop_cast_sig P Q H)).

Definition bijection_type_split_andb {A:Type} (p q:A -> bool) :
  bijection_type {x : A | p x && q x = true} { xp : { x : A | p x = true } | q (proj1_sig xp) = true }.
Proof.
constructor.
- intros [x pqx].
  destruct(andb_prop _ _ pqx) as [px qx].
  apply (exist _ (exist _ x px) qx).
- intros [[x px] qx].
  simpl in qx.
  refine (exist _ x _).
  apply (proj2 (andb_true_iff _ _) (conj px qx)).
Defined.

Lemma bijection_prop_split_andb {A:Type} (p q:A -> bool) :
  bijection_prop (bijection_type_split_andb p q).
Proof with simplprop.
split...
- apply functional_extensionality...
  dest_match.
- apply functional_extensionality...
  dest_match.
Qed.

Definition bijection_split_andb {A:Type} (p q:A -> bool) :
  bijection {x : A | p x && q x = true} { xp : { x : A | p x = true } | q (proj1_sig xp) = true } :=
    exist _ _ (bijection_prop_split_andb p q).

Lemma bijection_snd_bijection_fst {A B} (BIJ:bijection A B) a : bijection_snd BIJ (bijection_fst BIJ a) = a.
Proof with simplprop.
destruct BIJ as [[I S] [IS SI]]...
unfold bijection_snd, bijection_fst...
unfold comp in *...
specialize(apply_f (fun f => f a) IS)...
Qed.
#[export] Hint Rewrite @bijection_snd_bijection_fst : simplprop1_rewrite.

Lemma bijection_fst_bijection_snd {A B} (BIJ:bijection A B) a : bijection_fst BIJ (bijection_snd BIJ a) = a.
Proof with simplprop.
destruct BIJ as [[I S] [IS SI]]...
unfold bijection_snd, bijection_fst...
unfold comp in *...
specialize(apply_f (fun f => f a) SI)...
Qed.
#[export] Hint Rewrite @bijection_fst_bijection_snd : simplprop1_rewrite.

Definition bijection_type_sig_bijection {A B:Type} (p:A -> bool) (BIJ:bijection A B) :
  bijection_type {a : A | p a = true} {b : B | p (bijection_snd BIJ b) = true}.
Proof.
constructor.
- intros [a pa].
  refine (exist _ (bijection_fst BIJ a) _).
  rewrite bijection_snd_bijection_fst.
  apply pa.
- intros [b pb].
  refine (exist _ (bijection_snd BIJ b) _).
  apply pb.
Defined.

Lemma bijection_prop_sig_bijection {A B:Type} (p:A -> bool) (BIJ:bijection A B) :
  bijection_prop (bijection_type_sig_bijection p BIJ).
Proof with simplprop.
constructor...
- apply functional_extensionality...
  dest_match_step...
- apply functional_extensionality...
  dest_match_step...
Qed.

Definition bijection_sig_bijection {A B:Type} (p:A -> bool) (BIJ:bijection A B) :
  bijection {a : A | p a = true} {b : B | p (bijection_snd BIJ b) = true} :=
    exist _ _ (bijection_prop_sig_bijection p BIJ).


Definition is_bij (A B:Type) : Prop := exists (B:bijection A B), True.

Lemma is_bij_refl (A:Type) : is_bij A A.
Proof. exists (bijection_refl A). trivial. Qed.
Lemma is_bij_sym (A B:Type) : is_bij A B -> is_bij B A.
Proof. intros [R _]; exists (bijection_sym R). trivial. Qed.
Lemma is_bij_trans (A B C:Type) : is_bij A B -> is_bij B C -> is_bij A C.
Proof. intros [R1 _] [R2 _]. exists (bijection_comp R1 R2). trivial. Qed.


Require Import Coq.Setoids.Setoid.

Add Parametric Relation: (Type) (is_bij)
    reflexivity proved by (is_bij_refl)
    symmetry proved by (is_bij_sym)
    transitivity proved by (is_bij_trans)
      as is_bij_eq_rel.

