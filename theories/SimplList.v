Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplEq.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplSimpl.

Require Import SimplCmp.
Require Import SimplEq.
Require Import SimplNat.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.


(* Section. To Be Sorted *)

