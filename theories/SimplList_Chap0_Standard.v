Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplCmp.
Require Import SimplEq.
Require Import SimplSimpl.
Require Import SimplNat.

(* Chapter 0. List (StdLib and Extensions) *)

(* Outline :
  - Section 0. Already Existing List Related Theorems/Lemmas (StdLib exclusive)
  - Section 1. New List Related Theorems/Lemma (StdLib exclusive)
  - Section 2. Definitions/Fixpoints
  - Section 3. Immediate Proof on Section 2. (single def/fix (+ stdlib) at once)
  - Section 4. Advanced Proof on Section 2. (combined def/fix (+ stdlib))
  - Section 5. More Proofs (To Be Sorted)
 *)

Ltac simpllist1 := simplnat; repeat (progress (autorewrite with simpllist1_rewrite in *); simplnat).

(* Section 0. Already Existing List related theorems *)
#[export] Hint Rewrite List.length_zero_iff_nil : simpllist1_rewrite.
#[export] Hint Rewrite List.nth_error_None: simpllist1_rewrite.
(* forall (A : Type) (l : list A) (n : nat), List.nth_error l n = None <-> length l <= n *)
#[export] Hint Rewrite List.nth_error_Some: simpllist1_rewrite.
(* forall (A : Type) (l : list A) (n : nat), List.nth_error l n <> None <-> n < length l *)
#[export] Hint Rewrite List.app_nil_r: simpllist1_rewrite. (* forall (A : Type) (l : list A), (l ++ nil)%list = l *)
#[export] Hint Rewrite List.app_nil_l: simpllist1_rewrite.
#[export] Hint Rewrite List.map_length: simpllist1_rewrite.

#[export] Hint Rewrite List.firstn_length : simpllist1_rewrite.
#[export] Hint Rewrite List.skipn_length : simpllist1_rewrite.
#[export] Hint Rewrite @List.firstn_app : simpllist1_rewrite.
#[export] Hint Rewrite List.skipn_all : simpllist1_rewrite.

(* Section 1. New List Related Theorems/Lemma (StdLib exclusive) *)

Lemma cons_injective {A} (x0 x1:A) l0 l1 :
  cons x0 l0 = cons x1 l1 <-> x0 = x1 /\ l0 = l1.
Proof. split; solve_by_invert. Qed.
#[export] Hint Rewrite @cons_injective : simpllist1_rewrite.

Lemma cons_eq_nil {A} (x:A) l :
  cons x l = nil <-> False.
Proof. split; solve_by_invert. Qed.
#[export] Hint Rewrite @cons_eq_nil : simpllist1_rewrite.

Lemma nil_eq_cons {A} (x:A) l :
  nil = cons x l <-> False.
Proof. split; solve_by_invert. Qed.
#[export] Hint Rewrite @nil_eq_cons : simpllist1_rewrite.

Lemma list_eq_rewrite {A} (l1 l2:list A) :
  l1 = l2 <-> forall i, List.nth_error l1 i = List.nth_error l2 i.
Proof with simpllist1.
generalize dependent l2.
induction l1; destruct l2;
  split...
- specialize(H 0); simpl in H...
- specialize(H 0); simpl in H...
- rewrite rewrite_forall_nat in H; simpl in *...
  firstorder.
Qed.

Lemma forallb_true {A} l : @List.forallb A (fun _ => true) l = true.
Proof. induction l; simpllist1. Qed.
#[export] Hint Rewrite @forallb_true : simpllist1_rewrite.

Lemma forallb_true_TODO {A} p (H : p = (fun _ => true)) l : @List.forallb A p l = true.
Proof. subst; simpllist1. Qed.

Theorem rewrite_listSn {A} {P:list A -> Prop} (n:nat) :
  (forall (l:list A), length l = S n -> P l) <->
    (forall (a:A) (l:list A), length l = n -> P (cons a l)).
Proof with simplprop.
split; intros...
destruct l...
inversion H0.
Qed.
#[export] Hint Rewrite @rewrite_listSn : simpllist1_rewrite.

Lemma list_nth_In_neq {A} (x d:A) (D: x <> d) l i :
  List.nth i l d = x -> List.In x l.
Proof with simplbool.
generalize dependent l.
induction i; auto; destruct l...
Qed.
(* NO REWRITE *)

Lemma nth_forallb_isNone {A} {x:A} {i l} :
  List.nth i l None = Some x -> List.forallb isNone l = true -> False.
Proof with simpllist1.
generalize dependent l.
unfold isNone.
induction i; intros [|l0 l]...
destruct(IHi _ H H1).
Qed.

Lemma map_iff_nil {A B} (f : A -> B) l : List.map f l = nil <-> l = nil.
Proof with simplbool.
split...
apply List.map_eq_nil in H...
Qed.
#[export] Hint Rewrite @map_iff_nil : simpllist1_rewrite.

Lemma list_nth_eq_rewrite {A} l1 l2 t1 t2 :
  length l1 = length l2 ->
  (forall i : nat, @List.nth A i l1 t1 = List.nth i l2 t2) <-> (l1 = l2 /\ t1 = t2).
Proof with simpllist1.
generalize dependent l2.
induction l1; destruct l2; split...
- specialize(H0 0)...
- specialize (IHl1 l2)...
  simpl in *...
  specialize(H0 0) as H00; simpl in *...
  rewrite <- IHl1...
  specialize(H0(S i))...
Qed.

Lemma list_nth_error_nil A i : @List.nth_error A nil i = None.
Proof. destruct i; reflexivity. Qed.
#[export] Hint Rewrite @list_nth_error_nil : simpllist1_rewrite.

Lemma existsb_iff_true {A} (p:A->bool) l : List.existsb p l = true <->
  exists i, match List.nth_error l i with
  | None => False
  | Some x => p x = true
end.
Proof with simpllist1.
induction l; simpl in *...
destruct(List.existsb p l) eqn:E0...
+ exists (S i)...
+ destruct(p a) eqn:E1...
  * exists 0...
  * rewrite IHl.
    destruct i; simpl in *...
    exists i...
Qed.

Lemma forallb_negb_existsb {A} p l :
  @List.forallb A p l = negb(List.existsb (comp p negb) l).
Proof with simpllist1.
induction l...
Qed.

Lemma forallb_iff_false {A} (p:A->bool) l : List.forallb p l = false <->
  exists i, match List.nth_error l i with
  | None => False
  | Some x => p x = false
end.
Proof with simpllist1.
rewrite forallb_negb_existsb...
rewrite existsb_iff_true...
split; intros [i H]; exists i;
  dest_match...
Qed.

Lemma forallb_eq_forallb {A} (f1 f2:A->bool) :
  (forall l, List.forallb f1 l = List.forallb f2 l) <-> f1 = f2.
Proof with simpllist1.
split...
apply functional_extensionality...
specialize(H(cons x nil)); simpl in *...
Qed.

Lemma forallb_with_forallb {A} p1 p2
  (HB:forall (a:A), p1 a = true -> p2 a = true)
  {lA}
  (HA:List.forallb p1 lA = true) :
  List.forallb p2 lA = true.
Proof with simpllist1.
induction lA; simpl in *...
Qed.

Lemma forallb_map {A B} (f:A->B) (p:B->bool) l :
  List.forallb p (List.map f l) = List.forallb (fun x => p(f x)) l.
Proof with simplbool.
induction l...
Qed.

Lemma list_forallb_to_forall {A} (p:A->bool) :
  (forall l, List.forallb p l = true) <-> (forall (x:A), p x = true).
Proof with simplbool.
split...
- specialize(H(cons x nil)); simpl in *...
- induction l...
Qed.

Lemma nth_to_nth_error {A} i (l:list A) d :
  List.nth i l d = match List.nth_error l i with
    | Some v => v
    | None   => d
  end.
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
Qed.

Lemma nth_error_vs_nth {A} {l:list A} {x d v1}
  (H1:List.nth_error l x = Some v1) : List.nth x l d = v1.
Proof with simpllist1.
rewrite nth_to_nth_error...
Qed.

Lemma nth_error_map {A B} (f:A->B) l i :
  List.nth_error (List.map f l) i = opmap f (List.nth_error l i).
Proof.
generalize dependent l.
induction i; destruct l; simpllist1.
Qed.
#[export] Hint Rewrite @nth_error_map : simpllist1_rewrite.

Lemma nth_map {A B} (f:A->B) (l:list A) (d:B) (i:nat) :
  List.nth i (List.map f l) d = match List.nth_error l i with
    | Some v => f v
    | None   => d
  end.
Proof with simpllist1.
rewrite nth_to_nth_error, nth_error_map.
dest_match.
Qed.
#[export] Hint Rewrite @nth_map : simpllist1_rewrite.

Lemma list_match_id {A} (l:list A) :
 (match l with nil => nil | cons _ _ => l end) = l.
Proof. dest_match. Qed.
#[export] Hint Rewrite @list_match_id : simpllist1_rewrite.

Lemma list_match_id_v2 {A} (l:list A) :
  match l with nil => nil | cons h t => cons h t end = l.
Proof. dest_match. Qed.
#[export] Hint Rewrite @list_match_id_v2 : simpllist1_rewrite.

Lemma list_match_map {A B} f l :
  match l with
  | nil => nil
  | cons _ _ => @List.map A B f l
  end = List.map f l.
Proof. dest_match. Qed.
#[export] Hint Rewrite @list_match_map : simpllist1_rewrite.

Lemma list_map_id {A} l : @List.map A A id l = l.
Proof. apply List.map_id. Qed.
#[export] Hint Rewrite @list_map_id : simpllist1_rewrite.

Lemma nth_error_minus A x l i m :
  @List.nth_error A (cons x l) (i - m) =
    if Nat.leb i m then Some x else List.nth_error l (i - S m).
Proof with simpllist1.
destruct(i - m) eqn:D...
dest_if...
assert(R:i - S m = n)... lia.
Qed.

Lemma map_map {A B C} (f:A->B) (g:B->C) l : List.map g (List.map f l) = List.map (comp f g) l.
Proof. induction l; simpllist1. Qed.
#[export] Hint Rewrite @map_map : simpllist1_rewrite.

Lemma list_skipn_0 {A} l : @List.skipn A 0 l = l.
Proof. destruct l; reflexivity. Qed.
#[export] Hint Rewrite @list_skipn_0 : simpllist1_rewrite.

Lemma list_firstn_0 {A} l : @List.firstn A 0 l = nil.
Proof. destruct l; reflexivity. Qed.
#[export] Hint Rewrite @list_firstn_0 : simpllist1_rewrite.

Lemma list_nth_error_with_list_forallb
  {A p l} (H1:@List.forallb A p l = true)
  {i x} (H2:List.nth_error l i = Some x) : p x = true.
Proof with simpllist1.
generalize dependent i.
induction l; intros [|i]...
apply(IHl _ H2).
Qed.

Lemma list_nth_last_eq {A} l1 t1 l2 t2 :
  (forall i : nat, @List.nth A i l1 t1 = List.nth i l2 t2) -> t1 = t2.
Proof with simpllist1.
intro H.
specialize(H(max(length l1)(length l2))).
specialize(List.nth_error_Some l1 (max(length l1)(length l2))) as H1.
specialize(List.nth_error_Some l2 (max(length l1)(length l2))) as H2.
rewrite max_to_if_leb in *.
repeat rewrite nth_to_nth_error in H.
dest_match; simpllist1.
Qed.

Lemma list_nth_list_firstn i n {A} (l:list A) d :
  List.nth i (List.firstn n l) d = if i <? n then List.nth i l d else d.
Proof with simpllist1.
generalize dependent i.
generalize dependent l.
induction n...
destruct l...
destruct i...
Qed.

Fixpoint list_Forall {A} (p:A->Prop) (l:list A) :=
match l with
| nil => True
| cons x l => p x /\ list_Forall p l
end.

Lemma list_Forall_iff_list_nth_error {A} P l :
  @list_Forall A P l <-> (forall i a, List.nth_error l i = Some a -> P a).
Proof with simpllist1.
induction l...
rewrite IHl; clear IHl.
symmetry; rewrite rewrite_forall_nat...
Qed.

Lemma list_skipn_nil A n : @List.skipn A n nil = nil.
Proof. destruct n; reflexivity. Qed.
#[export] Hint Rewrite @list_skipn_nil : simpllist1_rewrite.

Lemma list_nth_error_list_skipn i n {A} (l:list A) :
  List.nth_error (List.skipn n l) i = List.nth_error l (n+i).
Proof with simpllist1.
generalize dependent i.
generalize dependent n.
induction l...
destruct n...
Qed.
#[export] Hint Rewrite @list_nth_error_list_skipn : simpllist1_rewrite.

Lemma list_nth_error_eq_Some_implies_lt_length {A} l i x :
  @List.nth_error A l i = Some x -> i < length l.
Proof with simpllist1.
destruct(i <? length l)eqn:E0...
rewrite <- List.nth_error_None in E0...
Qed.


Lemma list_firstn_nil A n : @List.firstn A n nil = nil.
Proof. destruct n; reflexivity. Qed.
#[export] Hint Rewrite @list_firstn_nil : simpllist1_rewrite.

Lemma list_nth_error_list_firstn n i {A} (l:list A) :
  List.nth_error (List.firstn n l) i = if i <? n then List.nth_error l i else None.
Proof with simpllist1.
generalize dependent i.
generalize dependent n.
induction l...
destruct n...
destruct i...
Qed.
#[export] Hint Rewrite @list_nth_error_list_firstn : simpllist1_rewrite.

Lemma forall_index_list_nth_eq_lemma0 {A} l1 t1 l2 t2 :
  length l1 <= length l2 ->
  (forall i : nat, @List.nth A i l1 t1 = List.nth i l2 t2) <-> (
    let n := min(length l1)(length l2) in
    List.firstn n l1 = List.firstn n l2 /\
    t1 = t2 /\
    (list_Forall (fun x => x = t1) (List.skipn n l1)) /\
    (list_Forall (fun x => x = t1) (List.skipn n l2))).
Proof with simpllist1.
intros...
rewrite list_eq_rewrite.
split...
- specialize(list_nth_last_eq _ _ _ _ H0)...
  constructor...
  + dest_match_step...
    specialize(H0 i).
    repeat rewrite nth_to_nth_error in H0.
    dest_match_step...
    dest_match_step...
    lia.
  + repeat rewrite list_Forall_iff_list_nth_error...
    specialize(H0(min(length l1)(length l2)+i)) as Hi...
    repeat rewrite nth_to_nth_error in Hi...
    dest_match_step...
    apply list_nth_error_eq_Some_implies_lt_length in D...
- repeat rewrite nth_to_nth_error.
  specialize(H0 i)...
  destruct(i <? length l1) eqn:E0;
    destruct(i <? length l2) eqn:E1...
  + rewrite H0...
  + lia.
  + rewrite <- List.nth_error_None in E0...
    dest_match_step...
    rewrite list_Forall_iff_list_nth_error in H2.
    specialize(H2(i-(length l1)))...
  + rewrite <- List.nth_error_None in E0, E1...
Qed.

Lemma forall_index_list_nth_eq {A} l1 t1 l2 t2 :
  (forall i : nat, @List.nth A i l1 t1 = List.nth i l2 t2) <-> (
    let n := min(length l1)(length l2) in
    List.firstn n l1 = List.firstn n l2 /\
    t1 = t2 /\
    (list_Forall (fun x => x = t1) (List.skipn n l1)) /\
    (list_Forall (fun x => x = t1) (List.skipn n l2))).
Proof with simpllist1.
destruct(length l1 <=? length l2)eqn:E0...
- rewrite (forall_index_list_nth_eq_lemma0 l1 t1 l2 t2 E0)...
- specialize (forall_index_list_nth_eq_lemma0 l2 t2 l1 t1)...
  split...
  + symmetry in H0.
    rewrite H in H0...
  + symmetry.
    generalize dependent i.
    rewrite H...
Qed.

Lemma hd_error_list_skipn n {A} (l:list A) :
  List.hd_error (List.skipn n l) = List.nth_error l n.
Proof with simpllist1.
generalize dependent n.
induction l...
dest_match...
destruct n...
Qed.

Lemma list_skipn_list_skipn x y {A} (l:list A):
  List.skipn x (List.skipn y l) = List.skipn (y+x) l.
Proof with simpllist1.
generalize dependent y.
generalize dependent x.
induction l; destruct y...
Qed.

Lemma merge_list_find_last_lemma1 {A}
  {p n1 l} (H11: @List.forallb A p (List.skipn (S n1) l) = true)
    {y1}   (H12: List.nth_error l n1 = Some y1)
    {n2}   (H21: List.forallb p (List.skipn (S n2) l) = true)
    {y2}   (H22: List.nth_error l n2 = Some y2)
           (H13: p y1 = false)
           (H23: p y2 = false) : n1 < n2 -> False.
Proof with simpllist1.
intro.
assert(HH:List.nth_error (List.skipn (S n1) l) (n2 - (S n1)) = Some y2).
{ rewrite list_nth_error_list_skipn.
  assert(R:S n1 + (n2 - S n1) = n2). { lia. }
  rewrite R... }
specialize(list_nth_error_with_list_forallb H11 HH)...
Qed.


Lemma merge_list_find_last {A}
  {p n1 l} (H11: @List.forallb A p (List.skipn (S n1) l) = true)
    {y1}   (H12: List.nth_error l n1 = Some y1)
    {n2}   (H21: List.forallb p (List.skipn (S n2) l) = true)
    {y2}   (H22: List.nth_error l n2 = Some y2)
           (H13: p y1 = false)
           (H23: p y2 = false) : n1 = n2 /\ y1 = y2.
Proof with simpllist1.
destruct(n1 =? n2)eqn:B0...
destruct(n1 <=? n2)eqn:B1...
- assert(n1 < n2). { lia. }
  apply (merge_list_find_last_lemma1 H11 H12 H21 H22 H13 H23 H).
- apply (merge_list_find_last_lemma1 H21 H22 H11 H12 H23 H13 B1).
Qed.

Lemma list_skipn_eq_nil {A} n l :
  @List.skipn A n l = nil <-> length l <= n.
Proof with simpllist1.
generalize dependent l.
induction n...
destruct l...
Qed.
#[export] Hint Rewrite @list_skipn_eq_nil : simpllist1_rewrite.


Lemma list_skipn_with_ge_length {A l n} (H:length l <= n) : @List.skipn A n l = nil.
Proof. rewrite list_skipn_eq_nil; assumption. Qed.

Lemma list_firstn_eq_same {A} n l : @List.firstn A n l = l <-> length l <= n.
Proof with simpllist1.
generalize dependent l.
induction n...
destruct l...
Qed.
#[export] Hint Rewrite @list_firstn_eq_same : simpllist1_rewrite.

Lemma list_firstn_with_ge_length {A n l} (H:length l <= n) : @List.firstn A n l = l.
Proof. rewrite list_firstn_eq_same; assumption. Qed.

Lemma same_eq_list_firstn {A} n l : l = @List.firstn A n l <-> length l <= n.
Proof. rewrite <- list_firstn_eq_same; simplprop. Qed.
#[export] Hint Rewrite @same_eq_list_firstn : simpllist1_rewrite.

Lemma list_app_eq_nil {A} l1 l2 :
  @List.app A l1 l2 = nil <-> l1 = nil /\ l2 = nil.
Proof. induction l1; simpllist1. Qed.
#[export] Hint Rewrite @list_app_eq_nil : simpllist1_rewrite.

Lemma list_skipn_app n {A} l1 l2:
  @List.skipn A n (List.app l1 l2) = List.app (List.skipn n l1) (List.skipn (n-(length l1)) l2).
Proof with simpllist1.
generalize dependent l1.
induction n...
destruct l1...
Qed.

Lemma list_skipn_list_firstn n1 n2 {A} l :
  @List.skipn A n1 (List.firstn n2 l) = List.firstn (n2-n1) (List.skipn n1 l).
Proof with simpllist1.
generalize dependent n2.
generalize dependent n1.
induction l...
destruct n2...
destruct n1...
Qed.

Lemma list_skipn_list_firstn_same n {A} (l:list A):
  List.skipn n (List.firstn n l) = nil.
Proof. rewrite list_skipn_list_firstn; simpllist1. Qed.
#[export] Hint Rewrite @list_skipn_list_firstn_same : simpllist1_rewrite.

Lemma list_app_elim_same_length {A}
  l1 l1' (H1:length l1 = length l1')
  l2 l2' (H2:length l2 = length l2') :
    @List.app A l1 l2 = List.app l1' l2' <-> l1 = l1' /\ l2 = l2'.
Proof with simpllist1.
generalize dependent l1'.
induction l1; destruct l1'...
autospec...
rewrite IHl1...
firstorder.
Qed.

Lemma list_app_elim_same_r {A} l1 l2 l3 :
  @List.app A l1 l3 = List.app l2 l3 <-> l1 = l2.
Proof with simpllist1.
split...
rewrite list_app_elim_same_length in H...
specialize(apply_f (@length _) H)...
repeat rewrite List.app_length in H0...
lia.
Qed.
#[export] Hint Rewrite @list_app_elim_same_r : simpllist1_rewrite.

Lemma list_app_elim_same_l {A} l1 l2 l3 :
  @List.app A l1 l2 = List.app l1 l3 <-> l2 = l3.
Proof with simpllist1.
split...
rewrite list_app_elim_same_length in H...
specialize(apply_f (@length _) H)...
repeat rewrite List.app_length in H0...
lia.
Qed.
#[export] Hint Rewrite @list_app_elim_same_l : simpllist1_rewrite.

Lemma list_map_list_firstn {A B} f n l :
  @List.map A B f (List.firstn n l) = List.firstn n (List.map f l).
Proof with simpllist1.
rewrite list_eq_rewrite...
dest_match_step...
Qed.

Lemma list_skipn_map {A B} n f l :
  List.skipn n (@List.map A B f l) = List.map f (List.skipn n l).
Proof. rewrite list_eq_rewrite; simpllist1. Qed.

Lemma list_forallb_list_skipn {A} p n l :
  @List.forallb A p (List.skipn n l) = List.forallb id (List.skipn n (List.map p l)).
Proof. rewrite list_skipn_map, forallb_map; simpllist1. Qed.

Lemma list_firstn_eq_nil {A} n l :
  @List.firstn A n l = nil <-> n = 0 \/ l = nil.
Proof with simpllist1.
destruct l...
destruct n...
Qed.

Lemma list_firstn_list_firstn {A} n1 n2 l :
  @List.firstn A n1 (List.firstn n2 l) = List.firstn (min n1 n2) l.
Proof with simpllist1.
rewrite list_eq_rewrite...
repeat rewrite list_nth_error_list_firstn.
dest_if...
for_lia1.
Qed.

Lemma reverse_equality_map {A B} (f:A -> B)
  (HH1:forall x y, f x = f y <-> x = y) l1 l2 :
  List.map f l1 = List.map f l2 <-> l1 = l2.
Proof with simpllist1.
generalize dependent l2.
induction l1...
- destruct l2...
- destruct l2...
  rewrite IHl1...
Qed.