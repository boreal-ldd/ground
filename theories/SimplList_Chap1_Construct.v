Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplCmp.
Require Import SimplEq.
Require Import SimplSimpl.
Require Import SimplNat.

Require Import SimplList_Chap0_Standard.

(* Section 1. Definitions *)

Fixpoint ntimes (n:nat) : forall {A} (a:A) (l:list A), list A.
Proof.
destruct n; intros.
- apply l.
- apply (cons a (ntimes n _ a l)).
Defined.

Definition make (n:nat) {A} (a:A) := ntimes n a nil.

Fixpoint pop_nth {A} (l:list A) (n:nat) : list A :=
match l with
| nil => nil
| cons a l =>
  match n with
  | 0 => l
  | S n => cons a (pop_nth l n)
  end
end.

Fixpoint reverse_intro_index_tailrec {A} (l:list(option A)) (n0:nat) (n:nat) : option nat.
Proof.
destruct l as [|opa l].
- apply (Some(n0+n)).
- destruct opa as [a|], n as [|n].
  + apply None.
  + apply (reverse_intro_index_tailrec A l    n0  n).
  + apply (Some n0).
  + apply (reverse_intro_index_tailrec A l (S n0) n).
Defined.

Fixpoint reverse_intro_index {A} (l:list(option A)) (n:nat) : option nat.
Proof.
destruct l as [|opa l].
- apply (Some n).
- destruct opa as [a|], n as [|n].
  + apply None.
  + apply (reverse_intro_index          A l n).
  + apply (Some 0).
  + apply (opmap S (reverse_intro_index A l n)).
Defined.

Fixpoint list_last_error {A} (l:list A) : option A :=
match l with
| nil => None
| cons h t => Some(
  match list_last_error t with
  | None    => h
  | Some h' => h'
  end)
end.


Fixpoint beq_list {A} (eqa:@beqt A) (l1 l2:list A) :=
match l1, l2 with
| nil       , nil        => true
| cons x1 l1, cons x2 l2 => eqa x1 x2 && beq_list eqa l1 l2
| _         , _          => false
end.

Fixpoint cmp_list {A} (cA:cmp A) (l1 l2:list A) : ord :=
match l1, l2 with
| nil, nil => Eq
| nil, cons _ _ => Lt
| cons _ _, nil => Gt
| cons h1 t1, cons h2 t2 =>
  match cA h1 h2 with
  | Eq => cmp_list cA t1 t2
  | o  => o
  end
end.

(* Section 2. Basic Proofs *)

(* SubSection 1. Order *)
Lemma cmp_Eq_iff_eq_cmp_list {A} cA (PA:@cmp_P A cA) : cmp_Eq_iff_eq (cmp_list cA).
Proof with simpllist1.
intros x; induction x as [|x0 x IHx]; intros [|y0 y]... 
specialize(IHx y)...
fold (cmp_comp (cA x0 y0) (cmp_list cA x y))...
rewrite cmp_comp_Eq...
rewrite IHx...
destruct PA as [RA TA AA].
rewrite (RA _)...
Qed.

Lemma cmp_Lt_trans_cmp_list {A} cA (PA:@cmp_P A cA) : cmp_Lt_trans (cmp_list cA).
Proof with simpllist1.
intros x; induction x as [|x0 x IHx]; intros [|y0 y] [|z0 z]...
fold (cmp_comp (cA x0 y0) (cmp_list cA x y)) in *.
fold (cmp_comp (cA y0 z0) (cmp_list cA y z)) in *.
fold (cmp_comp (cA x0 z0) (cmp_list cA x z)) in *.
specialize(cmp_Eq_iff_eq_cmp_list cA PA) as ELA.
generalize dependent H0.
generalize dependent H.
destruct PA as [RA TA AA].
rewrite rewrite_cmp_comp_trans...
specialize(TA x0 y0 z0)...
specialize(IHx y z)...
Qed.

Lemma cmp_asym_cmp_list {A} cA (PA:@cmp_P A cA) :cmp_asym (cmp_list cA).
Proof with simpllist1.
intros x; induction x as [|x0 x IHx]; intros [|y0 y]...
fold (cmp_comp (cA x0 y0) (cmp_list cA x y)) in *.
fold (cmp_comp (cA y0 x0) (cmp_list cA y x)) in *.
destruct PA as [RA TA AA].
rewrite cmp_asym_comp, (AA _), <- (IHx _)...
Qed.

Lemma cmp_P_cmp_list {A} cA (PA:@cmp_P A cA) : cmp_P (@cmp_list A cA).
Proof with simpllist1.
constructor.
- apply (cmp_Eq_iff_eq_cmp_list _ PA).
- apply (cmp_Lt_trans_cmp_list _ PA).
- apply (cmp_asym_cmp_list _ PA).
Qed.

(* SubSection 2. Rewritting Lemmas *)

Lemma make_S A n x : @make (S n) A x = cons x (make n x).
Proof. reflexivity. Qed.
#[export] Hint Rewrite @make_S : simpllist1_rewrite.
Lemma make_0 A x : @make 0 A x = nil.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @make_0 : simpllist1_rewrite.

Lemma make_eq_nil {A} n x : @make n A x = nil <-> n = 0.
Proof. destruct n; simpllist1. Qed.
#[export] Hint Rewrite @make_eq_nil : simpllist1_rewrite.

Lemma nil_eq_make {A} n x : nil = @make n A x <-> n = 0.
Proof. destruct n; simpllist1. Qed.
#[export] Hint Rewrite @make_eq_nil : simpllist1_rewrite.

Lemma cons_eq_make {A} x l n y :
  @cons A x l = make n y <-> n > 0 /\ x = y /\ l = make(pred n) y.
Proof. destruct n; simpllist1. Qed.
#[export] Hint Rewrite @cons_eq_make : simpllist1_rewrite.


Lemma ntimes_eq_nil n {A} (d:A) l :
  ntimes n d l = nil <-> n = 0 /\ l = nil.
Proof. destruct n; simpllist1. Qed.
#[export] Hint Rewrite @ntimes_eq_nil : simpllist1_rewrite.

Lemma list_last_error_eq_None {A} (l:list A) : list_last_error l = None <-> l = nil.
Proof. destruct l; simpllist1. Qed.
#[export] Hint Rewrite @list_last_error_eq_None : simpllist1_rewrite.

Lemma list_last_error_vs_nth_error {A} l :
  list_last_error l = List.nth_error l (pred(@length A l)).
Proof with simpllist1.
induction l...
rewrite IHl.
dest_match...
destruct l...
Qed.

Lemma map_cst_to_make {A B} b l :
  @List.map A B (fun _ => b) l = make (length l) b.
Proof with simpllist1.
induction l...
Qed.
#[export] Hint Rewrite @map_cst_to_make : simpllist1_rewrite.

Lemma length_ntimes (n:nat) {A} (a:A) l : length(ntimes n a l) = n + length l.
Proof. induction n; simpllist1. Qed.
#[export] Hint Rewrite @length_ntimes : simpllist1_rewrite.

Lemma length_make (n:nat) {A} (a:A) : length(make n a) = n.
Proof.
unfold make.
rewrite length_ntimes.
simplnat.
Qed.
#[export] Hint Rewrite @length_make : simpllist1_rewrite.

Lemma nth_error_ntimes_same {A} l n (a:A) :
  List.nth_error (ntimes n a l) n = List.nth_error l 0.
Proof. induction n; auto. Qed.
#[export] Hint Rewrite @nth_error_ntimes_same : simpllist1_rewrite.

Lemma nth_ntimes {A} l n (a a0:A) :
  List.nth n (ntimes n a l) a0 = List.nth 0 l a0.
Proof. induction n; auto. Qed.
#[export] Hint Rewrite @nth_ntimes : simpllist1_rewrite.

Lemma elim_eq_make A (x:A) (P:list A -> Prop) f :
  (forall l, l = make (f l) x -> P l) <-> (forall n, f(make n x) = n -> P(make n x)).
Proof with simplprop.
split...
- apply H...
  rewrite H0...
- rewrite H0.
  apply H...
  rewrite <- H0...
Qed.
#[export] Hint Rewrite @elim_eq_make : simpllist1_rewrite.

Lemma elim_eq_make_length A (x:A) (P:list A -> Prop) :
  (forall l, l = make(length l)x -> P l) <-> (forall n, P(make n x)).
Proof with simplprop.
rewrite (elim_eq_make A x P (@length A)).
split...
apply H...
rewrite length_make...
Qed.
#[export] Hint Rewrite @elim_eq_make_length : simpllist1_rewrite.

Lemma ntimes_ntimes_same A n1 n2 x l :
  @ntimes n1 A x (ntimes n2 x l) = ntimes (n1+n2) x l.
Proof with simpllist1.
induction n1...
Qed.
#[export] Hint Rewrite ntimes_ntimes_same : simpllist1_rewrite.

Lemma ntimes_make_same A n1 n2 x :
  @ntimes n1 A x (make n2 x) = make (n1+n2) x.
Proof with simpllist1.
unfold make.
apply ntimes_ntimes_same.
Qed.
#[export] Hint Rewrite ntimes_make_same : simpllist1_rewrite.

Lemma app_ntimes n {A} (x:A) l1 l2 :
  List.app (ntimes n x l1) l2 = ntimes n x (List.app l1 l2).
Proof with simpllist1.
induction n...
Qed.
#[export] Hint Rewrite @app_ntimes : simpllist1_rewrite.

Lemma app_make n {A} (x:A) l :
  List.app (make n x) l = ntimes n x l.
Proof with simpllist1.
specialize (app_ntimes n x nil l)...
Qed.
#[export] Hint Rewrite @app_make : simpllist1_rewrite.

Lemma app_eq_make {A} (l1 l2:list A) n x :
  List.app l1 l2 = make n x <-> l1 = make(length l1)x /\ l2 = make(length l2)x /\ length l1 + length l2 = n.
Proof with simpllist1.
split...
- generalize dependent n.
  induction l1; destruct n...
- rewrite H, H0...
Qed.
#[export] Hint Rewrite @app_eq_make : simpllist1_rewrite.

Lemma ntimes_None_cons_Some_nil_eq_cons_Some {A} n (x y:A) l :
  ntimes n None (cons(Some x)nil) = cons(Some y)l <-> n = 0 /\ l = nil /\ x = y.
Proof with simpllist1.
destruct n...
Qed.
#[export] Hint Rewrite @ntimes_None_cons_Some_nil_eq_cons_Some : simpllist1_rewrite.

Lemma nth_error_ntimes {A} n (d:A) l i :
  List.nth_error (ntimes n d l) i = if i <? n then Some d else List.nth_error l (i-n).
Proof with simpllist1.
generalize dependent n.
induction i; destruct n...
Qed.
#[export] Hint Rewrite @nth_error_ntimes : simpllist1_rewrite.

Lemma length_pop_nth {A} (l:list A) i :
  length (pop_nth l i) = if length l <=? i then length l else pred(length l).
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
rewrite IHi...
dest_match...
Qed.
#[export] Hint Rewrite @length_pop_nth : simpllist1_rewrite.

Lemma list_last_error_map {A B} (f:A->B) l :
  list_last_error (List.map f l) = opmap f (list_last_error l).
Proof with simpllist1.
induction l...
dest_match...
Qed.
#[export] Hint Rewrite @list_last_error_map : simpllist1_rewrite.

Lemma list_forallb_ntimes {A} p n x l :
  @List.forallb A p (ntimes n x l) = ((n =? 0)||(p x)) && (List.forallb p l).
Proof with simpllist1.
destruct n...
induction n...
Qed.
#[export] Hint Rewrite @list_forallb_ntimes : simpllist1_rewrite.

Lemma list_forallb_make {A} p n x :
  @List.forallb A p (make n x) = ((n =? 0)||(p x)).
Proof with simpllist1.
unfold make.
rewrite list_forallb_ntimes...
Qed.
#[export] Hint Rewrite @list_forallb_make : simpllist1_rewrite.


Lemma nth_error_make {A} n d i :
  @List.nth_error A (make n d) i = (if i <? n then Some d else None).
Proof with simpllist1.
unfold make.
rewrite nth_error_ntimes...
Qed.
#[export] Hint Rewrite @nth_error_make : simpllist1_rewrite.

Lemma ntimes_cons_d {A} n (d:A) l :
  ntimes n d (d :: l) = ntimes (S n) d l.
Proof with simpllist1.
induction n...
Qed.
#[export] Hint Rewrite @ntimes_cons_d : simpllist1_rewrite.

(* SubSection 3. Non Rewritting Lemmas *)

Lemma pop_nth_with_le {A l i} (LE:@length A l <= i) : pop_nth l i = l.
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
Qed.

Lemma list_forall_with_forallb_nth {A} p l i :
  @List.forallb A p l =
    (match List.nth_error l i with Some li => p li | None => true end) && List.forallb p (pop_nth l i).
Proof with simpllist1.
generalize dependent l.
induction i; destruct l; dest_match; rewrite IHi...
ring.
Qed.

Lemma reverse_intro_index_coherent {A} (l:list(option A)) (n0 n:nat) :
  reverse_intro_index_tailrec l n0 n = opmap (Nat.add n0) (reverse_intro_index l n).
Proof with simpllist1.
generalize dependent l.
generalize dependent n0.
induction n; simplbool;
  destruct l as [|[]]...
rewrite IHn...
unfold comp.
repeat apply apply_intro...
apply functional_extensionality...
Qed.

Lemma pop_nth_ntimes {A} n1 (x:A) l n2 :
  pop_nth (ntimes n1 x l) n2 =
    if n2 <? n1 then ntimes (pred n1) x l
                else ntimes n1 x (pop_nth l (n2-n1)).
Proof with simpllist1.
generalize dependent n1.
induction n2; destruct n1...
rewrite IHn2...
dest_if...
destruct n1...
Qed.
#[export] Hint Rewrite @pop_nth_ntimes : simpllist1_rewrite.

Lemma pop_nth_make {A} (x:A) n i :
  pop_nth (make n x) i = make (if i <? n then pred n else n) x.
Proof with simpllist1.
unfold make...
dest_if...
Qed.
#[export] Hint Rewrite @pop_nth_make : simpllist1_rewrite.

Lemma list_last_error_with_forallb {A}
  {l x} (H1:@list_last_error A l = Some x)
  { p } (H2:List.forallb p l = true) : p x = true.
Proof with simpllist1.
generalize dependent x.
induction l...
dest_match...
Qed.

Lemma list_last_error_of_forallb {A p l} (H:@List.forallb A p l = true) :
  match list_last_error l with
  | Some x => p x = true
  | None  => l = nil
  end.
Proof with simpllist1.
dest_match...
apply(list_last_error_with_forallb D H).
Qed.

Lemma map_ntimes {A B} (f:A->B) n x l : List.map f (ntimes n x l) = ntimes n (f x) (List.map f l).
Proof. induction n; simpllist1. Qed.
#[export] Hint Rewrite @map_ntimes : simpllist1_rewrite.

Lemma list_last_error_ntimes {A} n x l :
  @list_last_error A (ntimes n x l) = match list_last_error l with
    | Some y => Some y
    | None   => (if n =? 0 then None else Some x)
    end.
Proof with simpllist1.
induction n...
dest_match...
Qed.
#[export] Hint Rewrite @list_last_error_ntimes : simpllist1_rewrite.

Lemma nth_error_cons_minus {A} h t i n :
  @List.nth_error A (cons h t) (i - n) =
    if (i - n) =? 0 then Some h else List.nth_error t (i - S n).
Proof with simpllist1.
generalize dependent n.
generalize dependent t.
generalize dependent h.
induction i...
destruct n...
Qed.

Lemma list_nth_ntimes A n1 n2 x l d :
  @List.nth A n1 (ntimes n2 x l) d = if n1 <? n2 then x else List.nth (n1-n2) l d.
Proof with simpllist1.
repeat rewrite nth_to_nth_error...
dest_match_step...
Qed.
#[export] Hint Rewrite list_nth_ntimes : simpllist1_rewrite.

Lemma list_nth_make A n1 n2 x d :
  @List.nth A n1 (make n2 x) d = if n1 <? n2 then x else d.
Proof. unfold make; simpllist1. Qed.
#[export] Hint Rewrite list_nth_make : simpllist1_rewrite.

Lemma exists_list {A} (x1 x2 d1 d2:A) n1 n2 (N:n1<>n2) :
  exists l, List.nth n1 l d1 = x1 /\ List.nth n2 l d2 = x2.
Proof with simpllist1.
generalize dependent n2.
induction n1; destruct n2...
- exists (cons x1 (ntimes n2 x2 (cons x2 nil)))...
- exists (cons x2 (ntimes n1 x1 (cons x1 nil)))...
- destruct (IHn1 _ N) as [l0 p0].
  exists (cons x1 l0)...
Qed.

Lemma forall_list_nth_eq {A} (x0 y0:A) (N:x0<>y0) n1 (d1:A) n2 d2 :
  (forall l, List.nth n1 l d1 = List.nth n2 l d2) <-> n1 = n2 /\ d1 = d2.
Proof with simpllist1.
split...
specialize(H nil) as H0... (* assert(d1 = d2) *)
specialize(H(ntimes n1 x0 (cons y0 (make(n2-n1-1)x0)))) as H1... (* assert(n1 <= n2) *)
specialize(H(ntimes n2 x0 (cons y0 (make(n1-n2-1)x0)))) as H2... (* assert(n2 <= n1) *)
dest_match_step...
dest_match_step...
dest_match_step...
Qed.

Lemma forall_list_bool n1 (d1:bool) n2 d2 :
  (forall l, List.nth n1 l d1 = List.nth n2 l d2) <-> n1 = n2 /\ d1 = d2.
Proof with simpllist1.
apply (forall_list_nth_eq false true)...
Qed.

Lemma list_forallb_pop_nth {A p l} (H:@List.forallb A p l = true) i :
  @List.forallb A p (pop_nth l i) = true.
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
Qed.

Lemma list_forallb_list_firstn_rewrite {A} p i l :
  @List.forallb A p (List.firstn i l) = true <->
  (forall j : nat, j < (min i (length l)) -> match List.nth_error l j with
                          | Some x => p x = true
                          | None => False
                          end).
Proof with simpllist1.
generalize dependent l.
induction i...
destruct l...
rewrite andb_true_iff.
rewrite IHi...
symmetry.
rewrite rewrite_forall_nat...
split...
- specialize(H0 j)...
- specialize(H0 n)...
Qed.


Lemma list_Forall_ntimes {A} p n (x:A) l:
  list_Forall p (ntimes n x l) <-> (n = 0 \/ p x) /\ list_Forall p l.
Proof. induction n; simpllist1. Qed.

Lemma list_Forall_make {A} p n (x:A):
  list_Forall p (make n x) <-> (n = 0 \/ p x).
Proof with simpllist1.
specialize(list_Forall_ntimes p n x nil)...
Qed.

Lemma list_pop_last A (l:list A) :
 l = match list_last_error l with
  | Some x => List.app(pop_nth l (pred(length l)))(cons x nil)
  | None   => nil
  end.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma beq_list_iff_true {A} {eqa:@beqt A} (HA:beq_iff_true eqa) :
  beq_iff_true (beq_list eqa).
Proof with simpllist1.
intros l1.
induction l1; dest_match...
rewrite andb_true_iff...
firstorder.
Qed.


Lemma map_make {A B} (f:A->B) n x : List.map f (make n x) = make n (f x).
Proof with simpllist1.
unfold make.
rewrite map_ntimes...
Qed.

Lemma list_firstn_eq_make_case0 {A n l n' x} (H : List.firstn n l = make n' x) :
  l = @ntimes (min n n') A x (List.skipn n l).
Proof with simpllist1.
generalize dependent l.
generalize dependent n'.
induction n...
destruct l , n'...
Qed.

Lemma list_firstn_eq_make {A n l n' x} (H : List.firstn n l = make n' x) :
  l = @ntimes n' A x (List.skipn n l) /\ n' <= n.
Proof with simpllist1.
specialize(list_firstn_eq_make_case0 H) as H0...
specialize(apply_f (@length A) H) as H1...
rewrite min_to_if_leb in *...
dest_match... 
Qed.

Lemma list_firstn_eq_make_rewrite A n l n' x :
  List.firstn n l = make n' x <-> l = @ntimes n' A x (List.skipn n l) /\ n' <= n.
Proof with simpllist1.
split...
- specialize(list_firstn_eq_make H)...
- generalize dependent l.
  generalize dependent n'.
  induction n...
  destruct n', l...
  specialize(apply_f (@length A) H)...
  lia.
Qed.

Lemma list_skipn_ntimes n1 n2 {A} (x:A) l :
  List.skipn n1 (ntimes n2 x l) = if n1 <=? n2 then ntimes (n2-n1) x l else List.skipn (n1-n2) l.
Proof with simpllist1.
generalize dependent n2.
induction n1; destruct n2...
Qed.

Lemma list_last_error_make n {A} (x:A) :
  list_last_error (make n x) = if n =? 0 then None else Some x.
Proof with simpllist1.
induction n...
dest_match...
Qed.
#[export] Hint Rewrite @list_last_error_make : simpllist1_rewrite.

Lemma list_skipn_eq_make {A} n l n' x :
  List.skipn n l = @make n' A x <-> l = List.app (List.firstn n l) (make n' x).
Proof with simpllist1.
generalize dependent n'.
generalize dependent l.
induction n; destruct l...
Qed.
#[export] Hint Rewrite @list_skipn_eq_make : simpllist1_rewrite.

Lemma list_skipn_make {A} n m x :
  @List.skipn A n (make m x) = make (m-n) x.
Proof with simpllist1.
unfold make.
rewrite list_skipn_ntimes.
dest_match_step...
Qed.

Lemma list_firstn_ntimes n m {A} (x:A) l:
  List.firstn n (ntimes m x l) = ntimes (min n m) x (List.firstn(n-m) l).
Proof with simpllist1.
generalize dependent m.
induction n; destruct m...
Qed.
#[export] Hint Rewrite @list_firstn_ntimes : simpllist1_rewrite.

Lemma list_firstn_make n m {A} (x:A) :
  List.firstn n (make m x) = make (min n m) x.
Proof. setoid_rewrite list_firstn_ntimes; simpllist1. Qed.
#[export] Hint Rewrite @list_firstn_make : simpllist1_rewrite.

Fixpoint set_nth {A} (x d:A) n (l:list A) : list A :=
match l with
| nil => ntimes n d (cons x nil)
| cons h l => match n with
  | 0 => cons x l
  | S n => cons h (set_nth x d n l)
  end
end.

Fixpoint subst_list {A} (x:A) (n:nat) (l:list A) (d:A) :=
  match l with
  | nil => ntimes n d (cons x nil)
  | cons v l => match n with
    | 0 => cons x l
    | S n => cons v (subst_list x n l d)
    end
  end.

Lemma nth_subst_list_eq {A} n (x:A) l d1 d2 :
  List.nth n (subst_list x n l d1) d2 = x.
Proof with simpllist1.
generalize dependent l.
induction n; destruct l...
Qed.
#[export] Hint Rewrite @nth_subst_list_eq : simpllist1_rewrite.

Lemma list_nth_ntimes_cons_nil {A} n1 n2 (d:A) x :
  List.nth n1 (ntimes n2 d (x :: nil)) d = (if n1 =? n2 then x else d).
Proof with simpllist1.
generalize dependent n2.
induction n1; destruct n2...
Qed.

Lemma nth_subst_list {A} n2 n1 (x:A) l d :
  List.nth n1 (subst_list x n2 l d) d =
    if Nat.eqb n1 n2 then x
    else List.nth n1 l d.
Proof with simpllist1.
generalize dependent l.
generalize dependent n2.
induction n1; destruct l, n2...
dest_match...
Qed.

Lemma subst_list_nth {A} n l (d:A) :
  subst_list (List.nth n l d) n l d = List.app l (make ((S n) - (length l)) d).
Proof with simpllist1.
unfold make.
generalize dependent l.
induction n; destruct l...
Qed.

Lemma list_nth_make_d_d {A} n k (d:A) :
  List.nth n (make k d) d = d.
Proof with simpllist1.
unfold make.
generalize dependent k.
induction n; destruct k...
Qed.

Lemma nth_app_make_d {A} n l k (d:A) :
  List.nth n (l ++ make k d) d = List.nth n l d.
Proof with simpllist1.
generalize dependent l.
induction n; destruct l; simpllist1;
  rewrite list_nth_make_d_d...
Qed.

Lemma subst_list_with_nth {A} n l (v:A) (H:List.nth_error l n = Some v) d :
  subst_list v n l d = List.app l (make (S n - length l) d).
Proof with simpllist1.
specialize(subst_list_nth n l d)...
rewrite nth_to_nth_error in H0...
Qed.

Lemma forallb_with_forallb_v2
  {A lA p1} (HA:@List.forallb A p1 lA = true)
        p2 (HB:forall (a:A), p1 a = true -> p2 a = true) :
    List.forallb p2 lA = true.
Proof. eauto using forallb_with_forallb. Qed.

Lemma list_last_error_make_eq_Some {A} n x y :
  @list_last_error A (make n x) = Some y <-> 1 <= n /\ x = y.
Proof with simpllist1.
rewrite list_last_error_make...
dest_match...
for_lia1.
Qed.

Lemma list_forallb_twice_same_list {A} p1 p2 l
  (H1:@List.forallb A p1 l = true) (H2:List.forallb p2 l = true) :
  List.forallb (fun x => p1 x && p2 x) l = true.
Proof. induction l; simpllist1. Qed.

Lemma list_skipn_pred_length {A} l :
  @List.skipn A (pred(length l)) l = match list_last_error l with Some x => (cons x nil) | None => nil end.
Proof with simpllist1.
induction l...
destruct(length l)eqn:E0...
dest_match_step...
Qed.
#[export] Hint Rewrite @list_skipn_pred_length : simpllist1_rewrite.

Lemma list_last_error_list_skipn {A} n l :
  @list_last_error A (List.skipn n l) = if length l <=? n then None else list_last_error l.
Proof with simpllist1.
repeat rewrite list_last_error_vs_nth_error.
rewrite list_nth_error_list_skipn.
dest_match...
apply apply_intro...
lia.
Qed.

Lemma list_last_error_app {A} l1 l2 :
  @list_last_error A (List.app l1 l2) =
  match list_last_error l2 with
    | Some x => Some x
    | None => list_last_error l1
  end.
Proof with simpllist1.
induction l1...
dest_match...
Qed.

Lemma list_nth_error_cons_length {A} x l :
  @List.nth_error A (cons x l) (length l) = list_last_error (cons x l).
Proof. rewrite list_last_error_vs_nth_error; simpllist1. Qed.

Lemma forallb_eq_to_make {A} {eq:@beqt A} (EQ:beq_iff_true eq) l x :
  List.forallb (eq x) l = true <-> l = make (length l) x.
Proof with simpllist1.
unfold make.
induction l...
rewrite andb_true_iff...
firstorder.
Qed.

Lemma rewrite_ntimes_eq_same_n A n x1 l1 x2 l2 :
  @ntimes n A x1 l1 = ntimes n x2 l2 <-> (n <> 0 -> x1 = x2) /\ l1 = l2.
Proof. induction n; simpllist1. Qed.

Lemma rewrite_ntimes_eq_same_length_tail A n1 x1 l1 n2 x2 l2 (H:length l1 = length l2) :
  @ntimes n1 A x1 l1 = ntimes n2 x2 l2 <-> n1 = n2 /\ (n1 <> 0 -> x1 = x2) /\ l1 = l2.
Proof with simpllist1.
split...
- specialize(@apply_f (list A) _ (@length A) _ _ H0) as HH...
  rewrite_subst...
  assert(n1 = n2)... lia.
  rewrite rewrite_ntimes_eq_same_n in H0...
- destruct n2...
Qed.

Lemma pop_nth_last_list_firstn {A} l n :
  @pop_nth A (List.firstn (S n) l) n = List.firstn n l.
Proof with simpllist1.
generalize dependent l.
induction n; destruct l...
Qed.

Lemma nth_error_pop_nth_with_gt {A} l i j (H:j < i) :
  @List.nth_error A (pop_nth l i) j =  List.nth_error l j.
Proof with simpllist1.
generalize dependent l.
generalize dependent j.
induction i; destruct j, l...
Qed.

Lemma single_eq_cons_Some {A} n d x y l (H1 : x <> d) (H2: y <> d) :
  ntimes n d (cons x nil) = @cons A y l <-> x = y /\ l = nil /\ n = 0.
Proof with simpllist1.
split...
induction n...
Qed.

Lemma cons_d_ntimes {A} d n l :
  @cons A d (ntimes n d l) = ntimes (S n) d l.
Proof with simpllist1. reflexivity. Qed.

Definition ophd {A} (l:list A) : option A :=
  match l with
  | nil => None
  | cons x _ => Some x
  end.

Lemma fold_ophd {A} l :  match l with nil => None | cons x _ => Some x end = @ophd A l.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @fold_ophd : simpllist1_rewrite.

Lemma ophd_opmap {A B} f l : ophd (@List.map A B f l) = opmap f (ophd l).
Proof. induction l; simpllist1. Qed.
#[export] Hint Rewrite @ophd_opmap : simpllist1_rewrite.

Lemma make_eq_rewrite {A} n1 x1 n2 x2 :
  @make n1 A x1 = make n2 x2 <-> n1 = n2 /\ (n1 <> 0 -> x1 = x2).
Proof with simpllist1.
split...
- specialize(apply_f (@length _) H)...
  destruct n2...
- destruct n2...
Qed.
#[export] Hint Rewrite @make_eq_rewrite : simpllist1_rewrite.

Lemma eq_list_firstn_same {A} n (l1 l2:list A) : l1 = List.app (List.firstn n l1) l2 <-> List.skipn n l1 = l2.
Proof with simpllist1.
generalize dependent n.
induction l1...
destruct n...
Qed.
#[export] Hint Rewrite @eq_list_firstn_same : simpllist1_rewrite.

Lemma list_forallb_isNone_vs_make_None {A} (l:list(option A)) :
  List.forallb isNone l = true <-> l = make (length l) None.
Proof with simpllist1.
split...
- induction l...
- rewrite H...
Qed.

Lemma pop_nth_eq_make {A} l i n x :
  @pop_nth A l i = make n x <-> match List.nth_error l i with
    | Some y => i <= n /\ l = ntimes i x (cons y (make (n-i) x))
    | None   => l = make n x
  end.
Proof with simpllist1.
generalize dependent n.
generalize dependent l.
induction i; destruct l...
destruct n...
- dest_match...
- rewrite IHi...
  dest_match...
  firstorder.
Qed.

Lemma pop_nth_ntimes_same A i x y l :
  @pop_nth A (ntimes i x (cons y l)) i = ntimes i x l.
Proof with simpllist1.
induction i...
Qed.

Lemma nth_error_pop_nth_lt {A} l i j (H : i < j) :
  @List.nth_error A (pop_nth l i) (pred j) = List.nth_error l j.
Proof with simpllist1.
generalize dependent l.
generalize dependent i.
induction j...
destruct i...
- destruct l...
- destruct l...
  specialize(IHj _ H l)...
  destruct j...
Qed.

Lemma list_firstn_pop_nth {A} n l m :
  List.firstn n (@pop_nth A l m) = pop_nth (List.firstn ((if m <=? n then 1 else 0) + n) l) m.
Proof with simpllist1.
generalize dependent l.
generalize dependent m.
induction n; destruct m, l...
Qed.

Lemma pop_nth_pop_nth {A} l n m :
  @pop_nth A (pop_nth l n) m =
    if n <=? m then pop_nth (pop_nth l (S m)) n
               else pop_nth (pop_nth l m) (pred n).
Proof with simpllist1.
generalize dependent m.
generalize dependent n.
induction l...
destruct n...
destruct m...
rewrite IHl. clear IHl.
dest_match_step...
dest_match_step...
Qed.