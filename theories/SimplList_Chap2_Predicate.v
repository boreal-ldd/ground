Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplCmp.
Require Import SimplEq.
Require Import SimplSimpl.
Require Import SimplNat.

Require Import SimplList_Chap0_Standard.


Fixpoint list_count {A} (p:A->bool) (l:list A) : nat :=
match l with
| nil => 0
| cons h t => (if p h then 1 else 0) + list_count p t
end.

(* p : when to decrement the result *)
Fixpoint list_decount {A} (p:A->bool) (l:list A) (n:nat) : nat :=
match n with
| 0 => 0
| S n =>
  match l with
  | nil => S n
  | cons x l => (if p x then 0 else 1) + list_decount p l n
  end
end.

Fixpoint rm_trail {A} (p:A->bool) (l:list A) : list A :=
match l with
| nil => nil
| cons h t => match rm_trail p t with
  | nil => if p h then nil else cons h nil
  | t'  => cons h t'
  end
end.

(* pU : predicate indicating when to decrement nU *)
(* p  : predicate indicating when to increase result *)
Fixpoint list_count_until {A} (pU p:A->bool) (l:list A) (nU:nat) : nat :=
match nU with
| 0 => 0
| S nU' => match l with
  | nil => 0
  | cons h t =>
    (if p h then 1 else 0) + list_count_until pU p t (if pU h then nU' else nU)
  end
end.

Definition blit {A} pos len (l:list A) := List.firstn len (List.skipn pos l).

Fixpoint list_find_last {A} (p:A->bool) (l:list A) : option(nat*A) :=
match l with
| nil => None
| cons x l =>
  match list_find_last p l with
  | Some(i, x) => Some(S i, x)
  | None => if p x then Some(0, x) else None
  end
end.

Fixpoint list_find {A} (p:A->bool) (l:list A) : option(nat*A) :=
match l with
| nil => None
| cons x l =>
  if p x then Some(0, x) else
  match list_find p l with
  | Some(i, x) => Some(S i, x)
  | None       => None
  end
end.

(* Section 2. Rewritting Lemmas *)

Lemma list_decount_0 {A} p l : @list_decount A p l 0 = 0.
Proof.
induction l; simplbool.
Qed.
#[export] Hint Rewrite @list_decount_0 : simpllist1_rewrite.

Lemma list_count_eq_0 {A} p l :
  @list_count A p l = 0 <-> List.forallb (fun x => negb(p x)) l = true.
Proof with simpllist1.
induction l...
dest_if...
Qed.
#[export] Hint Rewrite @list_count_eq_0 : simpllist1_rewrite.


Lemma rm_trail_eq_nil {A} p l :
  @rm_trail A p l = nil <-> List.forallb p l = true.
Proof with simpllist1.
induction l...
dest_match...
Qed.
#[export] Hint Rewrite @rm_trail_eq_nil : simpllist1_rewrite.

Lemma list_count_app {A} (p:A->bool) l1 l2 :
  list_count p (List.app l1 l2) = list_count p l1 + list_count p l2.
Proof with simpllist1.
induction l1...
dest_if.
Qed.
#[export] Hint Rewrite @list_count_app : simpllist1_rewrite.

Lemma list_count_beq_0 {A} p l :
  (list_count p l =? 0) = List.forallb (fun x : A => negb (p x)) l.
Proof with simpllist1.
apply eq_true_iff_eq...
Qed.
#[export] Hint Rewrite @list_count_beq_0 : simpllist1_rewrite.

(* Section 3. Non Rewritting Lemmas *)

Lemma list_count_leb_length {A} p l :
  list_count p l <= @List.length A l.
Proof with simpllist1.
induction l...
dest_if...
Qed.

Lemma list_count_0_list_nth_error
  {A p l} (H1 : @list_count A p l = 0)
  {a i}   (H2 : List.nth_error l i = Some a) : p a = false.
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
- dest_match...
- autospec...
Qed.

Lemma list_count_until_le {A} pU p l n1 n2 (LT:n1<=n2) :
  @list_count_until A pU p l n1 <= @list_count_until A pU p l n2.
Proof with simpllist1.
generalize dependent n1.
generalize dependent n2.
induction l...
dest_match...
apply IHl...
Qed.

Lemma list_count_until_0 {A} pU p l : @list_count_until A pU p l 0 = 0.
Proof. destruct l; reflexivity. Qed.
#[export] Hint Rewrite @list_count_until_0 : simpllist1_rewrite.

Lemma list_count_until_with_length_le {A l i} (LE:@length A l <= i) pU p :
  list_count_until pU p l i = list_count p l.
Proof with simpllist1.
generalize dependent i.
induction l; destruct i...
rewrite IHl...
dest_if...
Qed.

Lemma length_rm_trail_le_length {A} p l :
  length(@rm_trail A p l) <= length l.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma max_length_rm_trail_length {A} p l : max(length(@rm_trail A p l))(length l) = length l.
Proof. specialize(length_rm_trail_le_length p l); simpllist1. Qed.
#[export] Hint Rewrite @max_length_rm_trail_length : simpllist1_rewrite.

Lemma min_length_rm_trail_length {A} p l : min(length(@rm_trail A p l))(length l) = length(@rm_trail A p l).
Proof. specialize(length_rm_trail_le_length p l); simpllist1. Qed.
#[export] Hint Rewrite @min_length_rm_trail_length : simpllist1_rewrite.

Lemma list_nth_error_rm_trail {A} p l i :
  List.nth_error (@rm_trail A p l) i = match List.nth_error l i with
  | Some x => if i <? length(rm_trail p l) then Some x else None
  | None   => None
  end.
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
- dest_match.
- specialize(IHi l).
  dest_match...
Qed.

Lemma list_count_until_true_le {T} p l i : list_count_until (fun _ : T => true) p l i <= i.
Proof with simpllist1.
generalize dependent i.
induction l; destruct i...
specialize(IHl i).
dest_match_step...
Qed.

Lemma list_decount_as_list_count_until {T} (l:list T) p i :
  list_decount p l i = i - list_count_until (fun _ => true) p l i.
Proof with simpllist1.
generalize dependent i.
induction l; destruct i; try repeat rewrite S_eq_SS...
rewrite IHl.
specialize(list_count_until_true_le p l i)...
destruct (p a)eqn:B; rewrite <- S_eq_SS; lia.
Qed.

Lemma list_decount_rm_trail_with_disjoint {A} p1 p2 l i (D:forall x, p1 x && p2 x = false) :
  @list_decount A p1 (rm_trail p2 l) i = list_decount p1 l i.
Proof with simpllist1.
generalize dependent i.
induction l...
dest_match_step; simpl in *...
- dest_match...
  + specialize(D a)...
  + specialize(IHl n)...
- dest_match_step; simpl in *...
Qed.

Lemma nth_error_vs_rm_trail {A}
  l i x (H1:@List.nth_error A l i = Some x)
      p (H2:p x = false) : i < length (rm_trail p l).
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
- dest_match...
- specialize(IHi _ H1).
  dest_match...
Qed.

Lemma list_find_eq_Some {A} p l i x :
  @list_find A p l = Some (i, x) <->
    (List.nth_error l i = Some x /\ p x = true /\ List.forallb (comp p negb) (List.firstn i l) = true).
Proof with simpllist1.
generalize dependent i.
induction l...
destruct i...
- split...
  dest_match...
- specialize(IHl i)...
  dest_match...
Qed.


Lemma list_count_until_true_r {A} p l n :
  min (length l) n <= @list_count_until A p (fun _ => true) l n.
Proof with simpllist1.
generalize dependent n.
induction l...
dest_match...
specialize(IHl n0).
specialize(@list_count_until_le A p (fun _ => true) l n0 (S n0))...
assert(n0 <= S n0)...
lia.
Qed.

Lemma list_count_until_le_list_count {A} pU p l i :
  list_count_until pU p l i <= @list_count A p l.
Proof with simpllist1.
generalize dependent i.
induction l...
dest_match...
Qed.

Lemma list_count_until_eq_list_count {A} p l i:
  list_count_until (fun _ => true) p l i = @list_count A p l <->
    list_count_until p (fun _ => true) l (list_count p l) <= i.
Proof with simpllist1.
generalize dependent i.
induction l...
dest_match...
specialize(list_count_until_le_list_count (fun _ : A => true) p l n)...
rewrite IHl...
Qed.
#[export] Hint Rewrite @list_count_until_eq_list_count.

Lemma list_nth_error_beyond_last {A p l i}
  (H1:@list_count_until A p (fun _ => true) l (list_count p l) <= i) {x}
  (H2:List.nth_error l i = Some x) : p x = false.
Proof with simpllist1.
generalize dependent i.
induction l; destruct i...
- dest_match...
- dest_match...
  + autospec.
  + specialize(list_nth_error_with_list_forallb D0 H2)...
  + autospec.
Qed.

Lemma list_forallb_rewrite_nth_error {A} p l :
  @List.forallb A p l = true <-> forall i,
    match List.nth_error l i with
    | Some x => p x = true
    | None   => True
    end.
Proof with simpllist1.
induction l...
rewrite andb_true_iff.
rewrite rewrite_forall_nat...
Qed.

Lemma list_nth_error_app {A} n l1 l2:
  @List.nth_error A (List.app l1 l2) n =
    match List.nth_error l1 n with
    | Some x => Some x
    | None => List.nth_error l2 (n-length l1)
    end.
Proof with simpllist1.
generalize dependent l1.
induction n; destruct l1...
Qed.
#[export] Hint Rewrite @list_nth_error_app : simpllist1_rewrite.

Lemma list_count_with_list_forallb {A p l} (H:@List.forallb A p l = true) :
  list_count p l = length l.
Proof with simpllist1.
induction l; simpl in *...
Qed.

Lemma list_count_until_le_length {A} pU p l i :
  @list_count_until A pU p l i <= length l.
Proof with simpllist1.
generalize dependent i.
induction l...
dest_match...
Qed.

Lemma list_count_until_le_case1 {A} pU p l i :
  @list_count_until A pU p l i <= @list_count_until A pU (fun _ => true) l i.
Proof with simpllist1.
generalize dependent i.
induction l...
dest_match...
Qed.

Lemma list_count_rm_trail_le_list_count {A} p1 p2 l :
  @list_count A p1 (rm_trail p2 l) <= list_count p1 l.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_count_map {A B} p f l :
  list_count p (@List.map A B f l) = list_count (comp f p) l.
Proof with simpllist1.
induction l...
Qed.

Lemma forallb_rm_trail_with_forallb {A p1 l}
  (H:@List.forallb A p1 l = true) p2 :
    List.forallb p1 (rm_trail p2 l) = true.
Proof with simpllist1.
rewrite list_forallb_rewrite_nth_error...
rewrite list_forallb_rewrite_nth_error in H.
specialize(H i).
rewrite list_nth_error_rm_trail...
dest_match...
Qed.

Lemma list_count_until_true_true {A} (l:list A) n:
  list_count_until (fun _ => true) (fun _ => true) l n = min(length l) n.
Proof with simpllist1.
generalize dependent l.
induction n...
destruct l...
Qed.

Lemma diff_list_count_until_case0 {A} p (l:list A) n1 n2 :
  let n1' := list_count_until (fun _ => true) p l n1 in
  let n2' := list_count_until (fun _ => true) p l n2 in
  n1 <= n2 -> n1' <= n2' /\ n2' - n1' <= n2 - n1.
Proof with simpllist1.
specialize(list_count_until_le (fun _ : A => true) p l n1 n2) as H0... clear H0...
generalize dependent l.
generalize dependent n1.
induction n2...
destruct n1...
- specialize(list_count_until_le_case1 (fun _ => true) p l (S n2))...
  rewrite list_count_until_true_true in H...
- specialize(IHn2 _ H)...
  destruct l...
  specialize(IHn2 l)...
  dest_match...
Qed.

Lemma diff_list_count_until {A} p (l:list A) n1 n2 :
  let n1' := list_count_until (fun _ => true) p l n1 in
  let n2' := list_count_until (fun _ => true) p l n2 in
    if n1 <=? n2 then (n1' <= n2' /\ n2' - n1' <= n2 - n1)
                 else (n2' <= n1' /\ n1' - n2' <= n1 - n2).
Proof with simpllist1.
dest_match...
- specialize(diff_list_count_until_case0 p l n1 n2)...
- specialize(diff_list_count_until_case0 p l n2 n1)...
Qed.

Lemma list_find_last_spec {A} p l :
  match @list_find_last A p l with
  | Some(i, x) => p x && List.forallb (comp p negb) (List.skipn (S i) l) = true
    /\ List.nth_error l i = Some x
  | None => List.forallb (comp p negb) l = true
  end.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma rm_trail_rewrite {A} p l :
  @rm_trail A p l = List.firstn
    (match list_find_last (comp p negb) l with
     | Some (i, x) => (S i)
     | None        => 0
     end) l.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Fixpoint assoc_find {A} (x:nat) (vals:list(nat*A)) : option A :=
match vals with
| nil => None
| cons (y, v) vals => if Nat.eqb x y then (Some v) else assoc_find x vals
end.
Lemma rewrite_forallb_p_as_forallb_id_map_p {A} p l : @List.forallb A p l = List.forallb id (List.map p l).
Proof. rewrite forallb_map; simpllist1. Qed.

Lemma rewrite_list_count_p_as_list_count_id_map_p {A} p l :
  @list_count A p l = list_count id (List.map p l).
Proof. rewrite list_count_map; simpllist1. Qed.

Fixpoint list_find_last_index {A} (p:A->bool) (l:list A) : option nat :=
match l with
| nil => None
| cons x l =>
  match list_find_last_index p l with
  | Some i => Some(S i)
  | None => if p x then Some 0 else None
  end
end.

Lemma list_find_last_index_map {A B} p f l :
  list_find_last_index p (@List.map A B f l) = list_find_last_index (comp f p) l.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_find_last_index_false {A} l :
  @list_find_last_index A (fun _ => false) l = None.
Proof with simpllist1.
induction l...
Qed.
#[export] Hint Rewrite @list_find_last_index_false : simpllist1_rewrite.

Lemma rewrite_list_find_last_index_as_list_find_last {A} (p:A -> bool) (l:list A) :
  list_find_last_index p l = opmap fst (list_find_last p l).
Proof with simpllist1.
induction l...
dest_match...
Qed.


Lemma map_of_list_find_last_index {A} p l : @list_find_last_index A p l = list_find_last_index id (List.map p l).
Proof. rewrite list_find_last_index_map; simpllist1. Qed.

Lemma list_find_last_app {A} p l1 l2 :
  @list_find_last A p (List.app l1 l2) =
    match list_find_last p l2 with
    | Some(i, x) => Some(length l1 + i, x)
    | None => list_find_last p l1
    end.
Proof with simpllist1.
induction l1...
- dest_match...
- rewrite IHl1.
  dest_match_step...
Qed.

Lemma nth_error_with_forallb_list_skipn
  {A p n l} (H1:@List.forallb A p (List.skipn n l) = true)
  {m x}     (H2:List.nth_error l m = Some x) : p x = false -> m < n.
Proof with simpllist1.
simpl...
rewrite list_forallb_rewrite_nth_error in H1...
specialize(H1 (m-n))...
destruct(n <=? m)eqn:E0...
Qed.

Lemma rm_trail_map {A B} p f l :
  rm_trail p (@List.map A B f l) = List.map f (rm_trail (comp f p) l).
Proof with simpllist1.
induction l...
rewrite IHl.
dest_match...
Qed.

Lemma split_list_count_until {A} pU p l i :
  @list_count_until A pU p l i =
    list_count_until (fun _ => true) p l (list_count_until pU (fun _ => true) l i).
Proof with simpllist1.
generalize dependent i.
induction l...
dest_match...
Qed.

Fixpoint list_filter {A} (p:A->bool) (l:list A) :=
match l with
| nil => nil
| cons x l => if p x
  then         list_filter p l
  else cons x (list_filter p l)
end.

Fixpoint list_filter_by {A} (lb:list bool) (l:list A) :=
match lb with
| nil => l
| cons b lb => match l with
  | nil => nil
  | cons x l => (if b
    then cons x (list_filter_by lb l)
    else (list_filter_by lb l))
  end
end.

Lemma list_filter_by_nil_r A lb : @list_filter_by A lb nil = nil.
Proof with simpllist1.
destruct lb...
Qed.
#[export] Hint Rewrite list_filter_by_nil_r : simpllist1_rewrite.

Lemma list_filter_by_map {A B} lb f l :
  list_filter_by lb (@List.map A B f l) = List.map f (list_filter_by lb l).
Proof with simpllist1.
generalize dependent l.
induction lb...
destruct l...
dest_match_step...
Qed.

Lemma forallb_list_filter_by_map_rm_trail {A} p l :
  @List.forallb A p (list_filter_by (List.map p (rm_trail p l)) l) = true.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_forallb_false_iff_eq_nil {A} l :
  @List.forallb A (fun _ => false) l = true <-> l = nil.
Proof with simpllist1.
destruct l...
Qed.
#[export] Hint Rewrite @list_forallb_false_iff_eq_nil : simpllist1_rewrite.

Lemma map_of_list_find_last {A} p l :
  @list_find_last A p l = match list_find_last fst (List.map (fun x => (p x, x)) l) with
    | Some(i, (_, x)) => Some(i, x)
    | None => None
    end.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_count_true {A} l : @list_count A (fun _ => true) l = length l.
Proof. induction l; simpllist1. Qed.
#[export] Hint Rewrite @list_count_true : simpllist1_rewrite.

Lemma rm_trail_app {A} p l1 l2 :
  @rm_trail A p (List.app l1 l2) =
    match rm_trail p l2 with
    | nil => rm_trail p l1
    | l2' => List.app l1 l2'
    end.
Proof with simpllist1.
induction l1...
dest_match...
symmetry in IHl1...
Qed.


Lemma nth_error_rm_trail_eq_Some {A} p l i x :
  List.nth_error (@rm_trail A p l) i = Some x <->
    List.nth_error l i = Some x /\ i < length(rm_trail p l).
Proof with simpllist1.
rewrite list_nth_error_rm_trail.
dest_match_step...
Qed.

Lemma list_count_vs_list_firstn_list_skipn {A} p n l :
  @list_count A p (List.firstn n l) + list_count p (List.skipn n l) = list_count p l.
Proof. rewrite <- list_count_app, List.firstn_skipn; reflexivity. Qed.

Lemma list_count_list_firstn {A} p n l :
  @list_count A p (List.firstn n l) = list_count p l - list_count p (List.skipn n l).
Proof. specialize(list_count_vs_list_firstn_list_skipn p n l). lia. Qed.

Lemma rewrite_list_count_until_A_B_with_ge_list_count
  {A} pU l n (H:@list_count A pU l < n) p :
    list_count_until pU p l n = list_count p l.
Proof with simpllist1.
generalize dependent n.
induction l...
dest_match...
Qed.


Lemma rm_trail_rm_trail_same {A} p l :
  @rm_trail A p (rm_trail p l) = rm_trail p l.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_forallb_rm_trail_same {A} p l :
  @List.forallb A p (rm_trail p l) = List.forallb p l.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_find_eq_None {A} p l :
  @list_find A p l = None <-> List.forallb (comp p negb) l = true.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_count_until_with_nth_error_case1 {A} (l:list A) n x
  (H1:List.nth_error l n = Some x) p (H2:p x = true) :
  list_count_until p (fun _ => true) l (S n) = S(list_count_until p (fun _ => true) l n).
Proof with simpllist1.
Abort.

Lemma list_count_until_true_plus {A} p l n i :
  @list_count_until A (fun _ => true) p l (n + i) =
    list_count_until (fun _ => true) p l n +
    list_count_until (fun _ => true) p (List.skipn n l) i.
Proof with simpllist1.
generalize dependent l.
induction n...
destruct l...
rewrite (IHn l).
dest_match_step...
Qed.

Lemma list_count_until_true_vs_list_count_list_firstn {A} p l n :
  @list_count_until A (fun _ => true) p l n = list_count p (List.firstn n l).
Proof with simpllist1.
generalize dependent l.
induction n...
destruct l...
Qed.

Lemma diff_list_count {A} p l n m (H : n <= m) :
  n + @list_count A p (List.firstn m l) <= m + list_count p (List.firstn n l).
Proof with simpllist1.
generalize dependent l.
rewrite le_iff_le0 in H.
induction H...
- specialize(list_count_leb_length p (List.firstn n l))...
- rewrite <- le_iff_le0 in H.
  destruct l...
  specialize(IHle0 l)...
  dest_match_step...
Qed. 

Lemma map_of_rm_trail {A} p l :
  @rm_trail A p l = List.map snd (rm_trail fst (List.map (fun x => (p x, x)) l)).
Proof with simpllist1.
induction l...
rewrite IHl...
dest_match...
Qed.

Lemma list_count_until_map {A B} pU p l f n :
  list_count_until pU p (@List.map A B f l) n =
    list_count_until (comp f pU) (comp f p) l n.
Proof with simpllist1.
generalize dependent n.
induction l...
destruct n...
Qed.

Lemma list_find_last_rm_trail_with_disjoint {A} p1 p2 (H:forall x, p1 x && p2 x = false) l :
  @list_find_last A p1 (rm_trail p2 l) = list_find_last p1 l.
Proof with simpllist1.
induction l...
dest_match...
specialize(H a)...
Qed.

Lemma rm_trail_rewrite_v2 {A} p l :
  @rm_trail A p l = List.firstn
    (match list_find_last (comp p negb) l with
      | Some(i, x) => S i
      | None       => 0
     end) l.
Proof with simpllist1.
rewrite rm_trail_rewrite.
dest_match...
Qed.

Fixpoint list_find_index {A : Type} (p : A -> bool) (l : list A) : option nat :=
  match l with 
  | nil     => None
  | cons x t => if p x then Some 0 
                     else opmap S (list_find_index p t)
  end.