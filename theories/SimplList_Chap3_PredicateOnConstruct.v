Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplFun.
Require Import SimplBoolFun.
Require Import SimplOption.
Require Import SimplCmp.
Require Import SimplEq.
Require Import SimplSimpl.
Require Import SimplNat.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.


Lemma list_count_ntimes {A} p n d l :
  @list_count A p (ntimes n d l) = (if p d then n else 0) + list_count p l.
Proof with simpllist1.
induction n...
dest_match...
Qed.
#[export] Hint Rewrite @list_count_ntimes : simpllist1_rewrite.

Lemma list_count_with_list_count_pop_nth {A} i p l :
  @list_count A p l =
    (match List.nth_error l i with Some x => if p x then 1 else 0 | None => 0 end)
      + list_count p (pop_nth l i).
Proof with simpllist1.
generalize dependent l.
induction i; destruct l...
dest_if...
Qed.

Lemma list_last_error_rm_trail {A} p l :
  match @list_last_error A (rm_trail p l) with
  | Some x => p x = false
  | None   => List.forallb p l = true
  end.
Proof with simpllist1.
induction l...
gen_dest_match simpllist1; simpl in *...
Qed.


Lemma list_count_pop_nth {A} p l n :
  @list_count A p (pop_nth l n) =
    list_count p l -
      match List.nth_error l n with
      | Some x => if p x then 1 else 0
      | None   => 0
      end.
Proof with simpllist1.
generalize dependent n.
induction l...
destruct n...
- dest_match_step...
- rewrite(IHl n)... clear IHl.
  gen_dest_match simpllist1.
  destruct(list_count p l)eqn:E0...
  specialize(list_nth_error_with_list_forallb E0 D0)...
Qed.

Lemma list_count_make {A} p n d :
  @list_count A p (make n d) = (if p d then n else 0).
Proof with simpllist1.
unfold make.
rewrite list_count_ntimes...
Qed.
#[export] Hint Rewrite @list_count_make : simpllist1_rewrite.

Lemma rm_trail_ntimes {A} p n x l :
  @rm_trail A p (ntimes n x l) = match rm_trail p l with
    | nil => if p x then nil else (make n x)
    | l'  => ntimes n x l'
    end.
Proof with simpllist1.
unfold make.
induction n...
rewrite IHn...
dest_match...
Qed.
#[export] Hint Rewrite @rm_trail_ntimes : simpllist1_rewrite.


Lemma rm_trail_make {A} p n x :
  @rm_trail A p (make n x) = if p x then nil else (make n x).
Proof with simpllist1.
unfold make.
rewrite rm_trail_ntimes...
Qed.
#[export] Hint Rewrite @rm_trail_make : simpllist1_rewrite.

Lemma list_count_rm_trail_with_disjoint {A} p1 p2 l (H:forall x, p1 x && p2 x = false) :
  @list_count A p1 (rm_trail p2 l) = list_count p1 l.
Proof with simpllist1.
induction l...
dest_match...
specialize(H a)...
Qed.

Lemma list_count_eq_S {A} p l n :
  @list_count A p l = S n <->
    match list_find p l with
    | Some(i, x) => p x = true /\ list_count p (pop_nth l i) = n
    | None => False
    end.
Proof with simpllist1.
generalize dependent n.
induction l...
dest_if...
rewrite IHl. clear IHl.
dest_match...
Qed.

Lemma list_find_eq_Some_v2 {A} p l i x :
  @list_find A p l = Some (i, x) <->
    (List.nth_error l i = Some x /\ p x = true /\
      (forall j, j < i -> match List.nth_error l j with
        | Some x => p x = false
        | None   => False end)).
Proof with simpllist1.
rewrite list_find_eq_Some.
rewrite list_forallb_list_firstn_rewrite.
split...
- specialize(list_nth_error_eq_Some_implies_lt_length _ _ _ H)...
  specialize(H1 j)...
  dest_match...
- specialize(list_nth_error_eq_Some_implies_lt_length _ _ _ H)...
  specialize(H1 j)...
  dest_match...
Qed.


Lemma list_find_ntimes {A} p n x l :
  @list_find A p (ntimes n x l) =
    if p x && (negb(n=?0)) then Some(0, x) else
    match list_find p l with
    | Some (i, x) => Some(n+i, x)
    | None => None
    end.
Proof with simpllist1.
induction n...
- dest_match.
- rewrite IHn...
  dest_match...
Qed.
#[export] Hint Rewrite @list_find_ntimes : simpllist1_rewrite.

Lemma list_find_make {A} p n x :
  @list_find A p (make n x) = if p x && (negb(n=?0)) then Some(0, x) else None.
Proof with simpllist1.
unfold make.
rewrite list_find_ntimes...
Qed.
#[export] Hint Rewrite @list_find_make : simpllist1_rewrite.

Lemma list_count_until_make {A} pU p nx x nu :
  @list_count_until A pU p (@make nx A x) nu =
    if p x && (negb(nu =? 0)) then (if pU x then min nu nx else nx) else 0.
Proof with simpllist1.
unfold make.
generalize dependent nu.
induction nx...
dest_match; simpllist1; rewrite IHnx...
dest_if...
Qed.
#[export] Hint Rewrite @list_count_until_make : simpllist1_rewrite.

Lemma list_find_last_eq_None {A} p l :
  @list_find_last A p l = None <-> List.forallb (comp p negb) l = true.
Proof with simpllist1.
induction l...
dest_match_step...
Qed.

Lemma list_find_last_eq_Some {A} p l i x :
  @list_find_last A p l = Some(i, x) <->
    p x && List.forallb (comp p negb) (List.skipn (S i) l) = true
      /\ List.nth_error l i = Some x.
Proof with simpllist1.
rewrite S_eq_SS.
specialize(list_find_last_spec p l).
dest_match_step.
- dest_match_step.
  rewrite S_eq_SS...
  split...
  rewrite <- S_eq_SS in *.
  specialize(merge_list_find_last H4 H3 H1 H0)...
- rewrite list_find_last_eq_None in D...
  specialize(list_nth_error_with_list_forallb D H0)...
Qed.

Lemma rm_trail_eq_rewrite {A} p l l' :
  @rm_trail A p l = l' <-> match list_last_error l' with
    | Some x => (negb(p x)) && (List.forallb p (List.skipn (length l') l)) = true /\
      l = List.app l' (List.skipn (length l') l)
    | None => List.forallb p l = true
    end.
Proof with simpllist1.
split...
- dest_match...
  specialize(list_last_error_rm_trail p l)...
  induction l...
  dest_match...
- generalize dependent l'.
  induction l...
  + destruct l'...
  + destruct l'...
    * dest_match...
      rewrite <- rm_trail_eq_nil in H0.
      rewrite H0 in D...
    * specialize(IHl l')...
      dest_match...
Qed.

Lemma rm_trail_eq_make {A} p l n (x:A) :
rm_trail p l = make n x <-> if p x
  then (n = 0 /\ List.forallb p l = true)
  else (l = ntimes n x (List.skipn n l) /\ List.forallb p (List.skipn n l) = true).
Proof with simpllist1.
split...
- rewrite rm_trail_eq_rewrite in H...
  dest_match... 
- dest_match...
  rewrite H...
  dest_match...
  rewrite (proj2(rm_trail_eq_nil _ _)H0) in D0...
Qed.
#[export] Hint Rewrite @rm_trail_eq_make : simpllist1_rewrite.

Lemma list_find_last_ntimes {A} p n x l :
  @list_find_last A p (ntimes n x l) =
  match list_find_last p l with
    | Some(i, x) => Some(n+i, x)
    | None => if (n =? 0) || (negb(p x))
      then None
      else Some(pred n, x)
  end.
Proof with simpllist1.
induction n...
- dest_match...
- dest_match...
Qed.

Lemma list_find_last_make {A} p n x :
  list_find_last p (@make n A x) =
    if (n =? 0) || negb (p x) then None else Some (Init.Nat.pred n, x).
Proof with simpllist1.
unfold make.
rewrite list_find_last_ntimes...
Qed.

Lemma list_count_until_list_count_vs_list_find_last {A} p l :
  @list_count_until A p (fun _ => true) l (list_count p l) = match list_find_last p l with
    | Some(i, _) => S i
    | None       => 0
    end.
Proof with simpllist1.
induction l...
dest_match...
rewrite list_find_last_eq_None in D1...
unfold comp in D1.
rewrite <- list_count_eq_0 in D1...
Qed.

Lemma list_count_rm_trail_eq_same {A} p l : @list_count A p (rm_trail p l) = list_count p l <-> rm_trail p l = l.
Proof with simpllist1.
split; rewrite_subst...
induction l...
dest_match...
Qed.

Lemma rm_trail_eq_same {A} p l :
  @rm_trail A p l = l <->
    match list_last_error l with
    | Some x => p x = false
    | None   => True
    end.
Proof with simpllist1.
rewrite rm_trail_eq_rewrite.
dest_match_step...
Qed.
#[export] Hint Rewrite @rm_trail_eq_same : simpllist1_rewrite.

Lemma list_last_error_eq_Some {A} l a :
  @list_last_error A l = Some a <-> List.nth_error l (pred(length l)) = Some a.
Proof with simpllist1.
generalize dependent a.
induction l...
dest_match_step...
specialize(IHl a0)...
destruct(length l)eqn:E0...
Qed.

Lemma list_find_last_true {A} l :
  @list_find_last A (fun _ => true) l = opmap (fun x => (pred(length l), x)) (list_last_error l).
Proof with simpllist1.
unfold opmap; dest_match_step...
rewrite list_find_last_eq_Some.
rewrite list_last_error_eq_Some in D.
rewrite S_eq_SS.
destruct(length l)eqn:E0...
rewrite <- S_eq_SS...
Qed.

Lemma list_find_last_with_forallb {A p l} (H:@List.forallb A p l = true) :
  list_find_last p l =
    match list_last_error l with
    | Some x => Some (pred(length l), x)
    | None   => None
    end.
Proof with simpllist1.
induction l...
rewrite IHl...
dest_match...
Qed.

Lemma pop_nth_list_firstn_last n {A} (l:list A) : pop_nth (List.firstn (S n) l) n = List.firstn n l.
Proof with simpllist1.
rewrite S_eq_SS.
generalize dependent n.
induction l...
destruct n.
- reflexivity.
- rewrite S_eq_SS, <- S_eq_SS...
  rewrite IHl...
  rewrite <- S_eq_SS...
Qed.
#[export] Hint Rewrite @pop_nth_list_firstn_last : simpllist1_rewrite.


Lemma list_count_list_firstn_S {A} p i l:
  list_count p (@List.firstn A (S i) l) =
    match List.nth_error l i with
    | Some x => (if p x then 1 else 0)
    | None   => 0
    end + list_count p (List.firstn i l).
Proof with simpllist1.
rewrite S_eq_SS.
rewrite (list_count_with_list_count_pop_nth i)...
rewrite <- S_eq_SS, pop_nth_list_firstn_last...
Qed.

Lemma rewrite_list_find_last_eq_Some_as_list_find_last_index {A} (p:A -> bool) (l:list A) ix :
  list_find_last p l = Some ix <-> list_find_last_index p l = Some(fst ix) /\ List.nth_error l (fst ix) = Some(snd ix).
Proof with simpllist1.
rewrite rewrite_list_find_last_index_as_list_find_last.
split...
- generalize dependent ix.
  induction l...
  dest_match...
- unfold opmap in H.
  dest_match...
  destruct p0.
  rewrite list_find_last_eq_Some in D...
  destruct ix...
Qed.

Lemma rm_trail_with_list_last_error {A} p l x (H1:list_last_error l = Some x) (H2:p x = false) :
  @rm_trail A p l = l.
Proof. rewrite rm_trail_eq_rewrite; simpllist1. Qed.

Lemma length_rm_trail {A} p1 l :
  @length A (rm_trail p1 l) = match list_find_last (comp p1 negb) l with
    | Some p => S (fst p)
    | None => 0
    end.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma rm_trail_with_forallb_same {A p l} (H:@List.forallb A p l = true) : rm_trail p l = nil.
Proof. simpllist1. Qed.


Lemma rm_trail_with_forallb_list_skipn {A p n l} (H:@List.forallb A p (List.skipn n l) = true) :
  rm_trail p l = rm_trail p (List.firstn n l).
Proof with simpllist1.
generalize dependent n.
induction l...
destruct n...
- rewrite(rm_trail_with_forallb_same H0)...
- autospec...
  rewrite_subst...
Qed.

Lemma list_last_error_list_firstn {A} n l :
  @list_last_error A (List.firstn n l) =
    if n =? 0 then None
    else List.nth_error l (min (pred n) (pred(length l))).
Proof with simpllist1.
rewrite list_last_error_vs_nth_error.
rewrite list_nth_error_list_firstn.
dest_match...
symmetry...
lia.
Qed.

Lemma list_find_last_index_ntimes {A} p n x l :
  @list_find_last_index A p (ntimes n x l) =
    match list_find_last_index p l with
    | Some i => Some (n + i)
    | None => if p x && (0 <? n) then Some (pred n) else None
    end.
Proof with simpllist1.
induction n...
rewrite IHn.
dest_match_step...
dest_match_step...
dest_match_step...
Qed.
#[export] Hint Rewrite @list_find_last_index_ntimes : simpllist1_rewrite.

Lemma list_find_last_fst_map_false {A B} f l :
  list_find_last fst (@List.map A (bool*B) (fun x : A => (false, f x)) l) = None.
Proof with simpllist1.
rewrite list_find_last_eq_None...
rewrite forallb_map...
Qed.

Lemma list_last_error_rm_trail_eq_Some {A p l x} :
  @list_last_error A (rm_trail p l) = Some x -> p x = false.
Proof with simpllist1.
specialize(list_last_error_rm_trail p l)...
Qed.

Lemma list_find_last_as_list_last_error_rm_trail {A} p l :
  @list_find_last A p l =
  match list_last_error(rm_trail (comp p negb) l) with
    | Some x => Some(pred(length(rm_trail (comp p negb) l)), x)
    | None => None
  end.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_last_error_rm_trail_as_list_find_last {A} p l :
  @list_last_error A (rm_trail p l) =
    match list_find_last (comp p negb) l with
    | Some(i, x) => Some x
    | None       => None
    end.
Proof with simpllist1.
induction l...
dest_match...
Qed.

Lemma list_find_last_index_make {A} p n x :
  @list_find_last_index A p (make n x) = if p x && (0 <? n) then Some (pred n) else None.
Proof. unfold make; rewrite list_find_last_index_ntimes; reflexivity. Qed.
#[export] Hint Rewrite @list_find_last_index_make : simpllist1_rewrite.

Lemma list_find_last_index_and_list_nth_error
  {A p l n} (H1:@list_find_last_index A p l = Some n)
  {x} (H2:List.nth_error l n = Some x) :
  p x = true.
Proof with simpllist1.
rewrite rewrite_list_find_last_index_as_list_find_last in H1...
unfold opmap in H1.
dest_match_step...
destruct p0.
rewrite list_find_last_eq_Some in D...
Qed.

Lemma list_find_last_with_list_last_error {A l x} (H:@list_last_error A l = Some x) p :
  p x = true -> list_find_last p l = Some(pred(length l), x).
Proof with simpllist1.
simpl...
rewrite list_find_last_as_list_last_error_rm_trail.
rewrite (rm_trail_with_list_last_error _ _ _ H)...
Qed.

Lemma list_find_last_fst_map_true {A B} f l :
  list_find_last fst (@List.map A (bool * B) (fun x => (true, f x)) l) =
    opmap (fun x => (pred(length l), (true, f x))) (list_last_error l).
Proof with simpllist1.
unfold opmap; dest_match_step...
rewrite list_find_last_eq_Some.
rewrite S_eq_SS.
unfold opmap.
rewrite list_last_error_eq_Some in D...
rewrite list_forallb_list_skipn...
unfold comp...
rewrite list_skipn_make...
destruct(length l)eqn:E0...
unfold SS...
Qed.