Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.
Require Import SimplDTT.


Set Loose Hint Behavior "Lax".

(* Bounded Integers *)

(* [ltN n] : the set of integers lower than [n], that is {0, ..., n-1}. *)
(* In particular, the cardinal of [ltN n] is n, thus [ltN 0] is empty *)

Definition ltN (n:nat) : Set := {k:nat | k <? n = true}.

Definition ltN_of_nat (k n:nat) p : ltN n := exist _ k p.

(* [TODO] make tactic *)
Lemma ltN_0_empty (k:ltN 0) : False.
Proof. destruct k; simpldtt. Qed.

Lemma ltN_1_unique (x:ltN 1) : x = ltN_of_nat 0 1 eq_refl.
Proof. destruct x as [x Hx]; setoid_rewrite exist_eq_l; simpldtt. Qed.

Lemma forall_ltN_0 (P:ltN 0 -> Prop) : (forall (k:ltN 0), P k) <-> True.
Proof. simpldtt. destruct(ltN_0_empty k). Qed.
#[export] Hint Rewrite forall_ltN_0 : curry1_rewrite.

Lemma forall_ltN_1 (P:ltN 1 -> Prop) : (forall (k:ltN 1), P k) <-> (P(ltN_of_nat 0 1 eq_refl)).
Proof with simpldtt.
split...
rewrite (ltN_1_unique k)...
Qed.
#[export] Hint Rewrite forall_ltN_1 : curry1_rewrite.

Ltac curry1_step :=
match goal with
| H : context[{k:nat | Nat.ltb k ?n = true}] |- _ => fold(ltN n) in H
| |- context[{k:nat | Nat.ltb k ?n = true}] => fold(ltN n) in *
| k : ltN 0 |- _ =>
  generalize dependent k;
  rewrite forall_ltN_0
| k : ltN 1 |- _ =>
  generalize dependent k;
  rewrite forall_ltN_1
end.

Ltac curry1 :=
  simpldtt;
  repeat (curry1_step || (progress (autorewrite with curry1_rewrite in * )); simpldtt).

Lemma ltN_PI n (x1 x2:ltN n) : x1 = x2 <-> proj1_sig x1 = proj1_sig x2.
Proof with curry1.
split...
destruct x1, x2...
Qed.

Definition ltN_S {n} (k:ltN n) : ltN (S n) :=
  ltN_of_nat (S(proj1_sig k)) (S n) (proj2_sig k).

Lemma proj1_sig_ltN_lt_N {n} (k:ltN n) : proj1_sig k <? n = true.
Proof. destruct k; curry1. Qed.
#[export] Hint Rewrite @proj1_sig_ltN_lt_N : curry1_rewrite.

Lemma ltN_of_nat_elim x n p y : ltN_of_nat x n p = y <-> x = proj1_sig y.
Proof with curry1.
destruct y...
unfold ltN_of_nat...
Qed.
#[export] Hint Rewrite ltN_of_nat_elim : curry1_rewrite.

Lemma ltN_of_nat_elim_r x n p y : y = ltN_of_nat x n p <-> proj1_sig y = x.
Proof with curry1.
destruct y...
unfold ltN_of_nat...
Qed.
#[export] Hint Rewrite ltN_of_nat_elim_r : curry1_rewrite.

Lemma ltN_split_lemma1 {k n1 n2} :
  (k <? n1 + n2) = true -> (k <? n1) = false -> (k - n1 <? n2) = true.
Proof. for_lia1. Qed.

Definition ltN_split n1 n2 (k:ltN (n1+n2)) : AB (ltN n1) (ltN n2) :=
  (if proj1_sig k <? n1 as b0 return ((proj1_sig k <? n1) = b0 -> _)
  then
    fun E0 : (proj1_sig k <? n1) = true  =>
    AA (ltN_of_nat (proj1_sig k) n1 E0)
  else
    fun E0 : (proj1_sig k <? n1) = false =>
    BB (ltN_of_nat (proj1_sig k - n1) n2 (ltN_split_lemma1 (proj2_sig k) E0))
  ) (eq_refl _).

Lemma ltN_plus_lemma1 {x n1 n2} : (x <? n1) = true -> (x <? n1 + n2) = true.
Proof. for_lia1. Qed.

Definition ltN_plus {n1} (k:ltN n1) n2 : ltN (n1+n2) :=
  (ltN_of_nat (proj1_sig k) (n1+n2) (ltN_plus_lemma1 (proj2_sig k))).

Lemma plus_ltN_lemma1 {x n1 n2} : (x <? n2) = true -> (n1 + x <? n1 + n2) = true.
Proof. for_lia1. Qed.

Definition plus_ltN n1 {n2} (k:ltN n2) : ltN (n1+n2) :=
  (ltN_of_nat (n1+(proj1_sig k)) (n1+n2) (plus_ltN_lemma1 (proj2_sig k))).

Lemma ltN_split_ltN_plus n1 n2 k : ltN_split n1 n2 (ltN_plus k n2) = AA k.
Proof with curry1.
destruct k...
unfold ltN_split, ltN_plus...
Qed.
#[export] Hint Rewrite ltN_split_ltN_plus : curry1_rewrite.

Lemma ltN_split_plus_ltN n1 n2 k : ltN_split n1 n2 (plus_ltN n1 k) = BB k.
Proof with curry1.
destruct k...
unfold ltN_split, plus_ltN...
rewrite_dtt_if...
split...
lia.
Qed.
#[export] Hint Rewrite ltN_split_plus_ltN : curry1_rewrite.

Definition ltN_unsplit {nA nB} (ab:AB (ltN nA) (ltN nB)) : ltN (nA + nB) :=
match ab with
| AA kA => ltN_plus kA nB
| BB kB => plus_ltN nA kB
end.

Lemma ltN_split_ltN_unsplit {nA nB} (ab:AB (ltN nA) (ltN nB)) : ltN_split nA nB (ltN_unsplit ab) = ab.
Proof. destruct ab; curry1. Qed.
#[export] Hint Rewrite @ltN_split_ltN_unsplit : curry1_rewrite.

Lemma match_ltN_split_with_ltN_plus_plus_ltN n1 n2 x :
match ltN_split n1 n2 x with
| AA k1 => ltN_plus k1 n2
| BB k2 => plus_ltN n1 k2
end = x.
Proof with curry1.
destruct x...
unfold ltN_split, ltN_plus, plus_ltN, ltN_of_nat in *...
rewrite_dtt_if...
split...
Qed.

Lemma rewrite_ltN_split_eq_AA n1 n2 x k1 :
  ltN_split n1 n2 x = AA k1 <-> x = ltN_plus k1 n2.
Proof with curry1.
specialize(match_ltN_split_with_ltN_plus_plus_ltN n1 n2 x)...
split...
rewrite_subst.
Qed.
#[export] Hint Rewrite @rewrite_ltN_split_eq_AA : curry1_rewrite.

Lemma rewrite_ltN_split_eq_BB n1 n2 x k2 :
  ltN_split n1 n2 x = BB k2 <-> x = plus_ltN n1 k2.
Proof with curry1.
specialize(match_ltN_split_with_ltN_plus_plus_ltN n1 n2 x)...
split...
rewrite_subst.
Qed.
#[export] Hint Rewrite @rewrite_ltN_split_eq_BB : curry1_rewrite.

Lemma rewrite_ltN_split_eq n1 n2 x v :
  ltN_split n1 n2 x = v <->
    match v with
    | AA k1 => x = ltN_plus k1 n2
    | BB k2 => x = plus_ltN n1 k2
    end.
Proof. dest_match_step; curry1. Qed.

Lemma ltN_unsplit_ltN_split nA nB (k:ltN(nA + nB)) : ltN_unsplit (ltN_split nA nB k) = k.
Proof. destruct(ltN_split nA nB k) as [kA|kB] eqn:E; curry1. Qed.
#[export] Hint Rewrite @ltN_unsplit_ltN_split : curry1_rewrite.

Lemma plus_ltN_0 {n} (k:ltN n) : plus_ltN 0 k = k.
Proof. apply ltN_PI; curry1. Qed.
#[export] Hint Rewrite @plus_ltN_0 : curry1_rewrite.

Lemma ltN_split_0_l n x : ltN_split 0 n x = BB x.
Proof. curry1. Qed.
#[export] Hint Rewrite ltN_split_0_l : curry1_rewrite.

Lemma ltN_split_0_r n x :
  ltN_split n 0 x = AA (eq_rect _ _ x n (Nat.add_0_r n)).
Proof with curry1.
destruct x...
apply ltN_PI...
Qed.
#[export] Hint Rewrite ltN_split_0_r : curry1_rewrite.

Lemma ltN_split_S_ltN_S n1 n2 (x:ltN(n1+n2)) :
  ltN_split (S n1) n2 (ltN_S x) =
    match ltN_split n1 n2 x with
    | AA k1 => AA (ltN_S k1)
    | BB k2 => BB k2
    end.
Proof. dest_match_step; curry1; apply ltN_PI; curry1. Qed.

(* Uniform Operators *)

Lemma ltN_intro_lemma1 n (k:ltN (S n)) (x:ltN n) :
  (proj1_sig x <? proj1_sig k) = true -> (proj1_sig x <? S n) = true.
Proof. destruct k, x; simpldtt. Qed.

Lemma ltN_intro_lemma2 n (k:ltN (S n)) (x:ltN n) :
  (proj1_sig x <? proj1_sig k) = false -> (S (proj1_sig x) <? S n) = true.
Proof. destruct k, x; simpldtt. Qed.

(* returns [x] if [x < k], otherwise [x+1] *)
Definition ltN_intro {n} (k:ltN (S n)) (x:ltN n) : ltN (S n) :=
(if proj1_sig x <? proj1_sig k as b0 return ((proj1_sig x <? proj1_sig k) = b0 -> ltN (S n))
 then fun E1 => ltN_of_nat (proj1_sig x) (S n) (ltN_intro_lemma1 n k x E1)
 else fun E1 => ltN_of_nat (S (proj1_sig x)) (S n) (ltN_intro_lemma2 n k x E1)) (eq_refl _).

Lemma proj1_sig_ltN_intro n k x :
  proj1_sig (@ltN_intro n k x) = if proj1_sig x <? proj1_sig k then (proj1_sig x) else (S(proj1_sig x)).
Proof with curry1.
unfold ltN_intro...
rewrite comm_dtt_iff...
Qed.
#[export] Hint Rewrite proj1_sig_ltN_intro : curry1_rewrite.

Lemma ltN_pop_lemma1 n (k:ltN (S n)) (x:ltN(S n)) :
  (proj1_sig x <? proj1_sig k) = true -> (proj1_sig x <? n) = true.
Proof. destruct k, x; simpldtt. lia. Qed.

Lemma ltN_pop_lemma2 n (k:ltN (S n)) (x:ltN(S n)) :
  (proj1_sig x <? proj1_sig k) = false ->
  (proj1_sig x =? proj1_sig k) = false -> (pred (proj1_sig x) <? n) = true.
Proof. destruct k, x; simpldtt. lia. Qed. 

Definition ltN_pop {n} (k:ltN (S n)) (x:ltN (S n)) : AB unit (ltN n) :=
(if proj1_sig x =? proj1_sig k as b0 return ((proj1_sig x =? proj1_sig k) = b0 -> _)
 then fun _ => AA tt
 else fun E0 =>
  BB
    ((if proj1_sig x <? proj1_sig k as b1 return ((proj1_sig x <? proj1_sig k) = b1 -> ltN n )
      then fun E1 => (ltN_of_nat (proj1_sig x) n (ltN_pop_lemma1 n k x E1))
      else fun E1 => (ltN_of_nat (pred (proj1_sig x)) n (ltN_pop_lemma2 n k x E1 E0))) (eq_refl _))
) (eq_refl _).

Lemma ltN_rev_intro_lemma1 n (k:ltN n) (x:ltN n) :
  (proj1_sig x <? proj1_sig k) = true -> (proj1_sig x <? S n) = true.
Proof. destruct k, x; simpldtt. Qed.

Lemma ltN_rev_intro_lemma2 n (k:ltN n) (x:ltN n) :
  (proj1_sig x <? proj1_sig k) = false -> (S (proj1_sig x) <? S n) = true.
Proof. destruct k, x; simpldtt. Qed.

(* returns [x] if [x < k], otherwise [x+1] *)
Definition ltN_rev_intro {n} (k:ltN n) (x:ltN n) : ltN (S n) :=
(if proj1_sig x <? proj1_sig k as b0 return ((proj1_sig x <? proj1_sig k) = b0 -> ltN (S n))
 then fun E1 => ltN_of_nat (proj1_sig x) (S n) (ltN_rev_intro_lemma1 n k x E1)
 else fun E1 => ltN_of_nat (S (proj1_sig x)) (S n) (ltN_rev_intro_lemma2 n k x E1)) (eq_refl _).

Lemma ltN_rev_elim_lemma1 {n} (k x:ltN n) :
  (proj1_sig k <? proj1_sig x) = true -> (S(proj1_sig x) <? S n) = true.
Proof with curry1. destruct x, k... Qed.

Lemma ltN_rev_elim_lemma2 {n} (k x:ltN n) :
  (proj1_sig k <? proj1_sig x) = false -> (proj1_sig x <? S n) = true.
Proof with curry1. destruct x, k... Qed.

Lemma proj1_sig_ltN_rev_intro n k x :
  proj1_sig (@ltN_rev_intro n k x) = if proj1_sig x <? proj1_sig k then (proj1_sig x) else (S(proj1_sig x)).
Proof with curry1.
unfold ltN_rev_intro.
rewrite comm_dtt_iff...
Qed.
#[export] Hint Rewrite proj1_sig_ltN_rev_intro : curry1_rewrite.

Definition ltN_rev_elim {n} (k x:ltN n) : ltN(S n) :=
  (if proj1_sig k <? proj1_sig x as b0 return proj1_sig k <? proj1_sig x = b0 ->  ltN(S n)
    then (fun E0 => ltN_of_nat (S(proj1_sig x)) (S n) (ltN_rev_elim_lemma1 k x E0))
    else (fun E0 => ltN_of_nat (proj1_sig x) (S n) (ltN_rev_elim_lemma2 k x E0))) (eq_refl _).

Lemma proj1_sig_ltN_rev_elim n k x :
  proj1_sig (@ltN_rev_elim n k x) = if proj1_sig k <? proj1_sig x then (S (proj1_sig x)) else (proj1_sig x).
Proof with curry1.
unfold ltN_rev_elim.
rewrite comm_dtt_iff...
Qed.
#[export] Hint Rewrite proj1_sig_ltN_rev_elim : curry1_rewrite.


Lemma dtt_free_ltN_pop n k x opy :
  @ltN_pop n k x = opy <->
    match opy with
    | AA _ => k = x
    | BB y => k <> x /\ proj1_sig y = (if proj1_sig x <? proj1_sig k then (proj1_sig x) else (pred(proj1_sig x)))
    end.
Proof with curry1.
unfold ltN_pop.
rewrite_dtt_if...
split...
- split...
  + rewrite ltN_PI...
  + dest_match_step...
    rewrite ltN_PI in *...
- split...
  + rewrite comm_dtt_iff...
  + dest_match_step...
    rewrite ltN_PI, comm_dtt_iff...
Qed.

Lemma ltN_pop_same n (k:ltN(S n)) : ltN_pop k k = AA tt.
Proof. rewrite dtt_free_ltN_pop; reflexivity. Qed.
#[export] Hint Rewrite ltN_pop_same : curry1_rewrite.

Lemma ltN_pop_eq_AA n k1 k2 : @ltN_pop n k2 k1 = AA tt <-> k2 = k1.
Proof. rewrite dtt_free_ltN_pop; reflexivity. Qed.
#[export] Hint Rewrite ltN_pop_eq_AA : curry1_rewrite.

Lemma ltN_pop_iff_ltN_intro {n} k0 k1 k2 : ltN_pop k0 k1 = BB k2 <-> @ltN_intro n k0 k2 = k1.
Proof with curry1.
rewrite ltN_PI, proj1_sig_ltN_intro...
rewrite dtt_free_ltN_pop...
repeat rewrite_if_as_Prop...
destruct k0, k1, k2...
lia.
Qed.
#[export] Hint Rewrite @ltN_pop_iff_ltN_intro : curry1_rewrite.

Lemma ltN_intro_ltN_rev_intro_k1_k2_k1 {n} (k1 k2:ltN(S n)) :
  ltN_intro (ltN_rev_intro k2 k1) k2 = ltN_rev_elim k1 k2.
Proof with curry1.
rewrite ltN_PI, proj1_sig_ltN_intro...
repeat rewrite_if_as_Prop...
lia.
Qed.
#[export] Hint Rewrite @ltN_intro_ltN_rev_intro_k1_k2_k1 : curry1_rewrite.

Lemma ltN_pop_ltN_rev_intro_ltN_rev_elim n (k1 k2:ltN(S n)) :
  ltN_pop (ltN_rev_intro k2 k1) (ltN_rev_elim k1 k2) = BB k2.
Proof. curry1. Qed.
#[export] Hint Rewrite ltN_pop_ltN_rev_intro_ltN_rev_elim : curry1_rewrite.

Lemma ltN_pop_ltN_intro_same n k (x:ltN n) : ltN_pop k (ltN_intro k x) = BB x.
Proof. curry1. Qed.
#[export] Hint Rewrite ltN_pop_ltN_intro_same : curry1_rewrite.

Lemma ltN_intro_eq_same n k x : k = @ltN_intro n k x <-> False.
Proof with curry1.
rewrite ltN_PI...
rewrite_if_as_Prop_indirect...
lia.
Qed.
#[export] Hint Rewrite ltN_intro_eq_same : curry1_rewrite.

Lemma ltN_pop_ltN_rev_intro_ltN_intro_ltN_rev_elim n (k1 k2:ltN(S n)) x0 :
  ltN_pop (ltN_rev_intro k2 k1) (ltN_intro (ltN_rev_elim k1 k2) x0) =
     match ltN_pop k1 x0 with
     | AA tt => AA tt
     | BB x1 => BB (ltN_intro k2 x1)
     end.
Proof with curry1.
dest_match_step...
- rewrite ltN_PI.
  rewrite proj1_sig_ltN_rev_intro...
  dest_match_step...
- rewrite ltN_PI.
  rewrite proj1_sig_ltN_intro...
  repeat rewrite_if_as_Prop...
  destruct k1, k2, l...
  lia.
Qed.

Lemma ltN_comm_intro_lemma1 {n} (k1:ltN n) (k2:ltN (S n)) :
  proj1_sig k1 <? proj1_sig k2 = true -> pred(proj1_sig k2) <? n = true.
Proof. destruct k1, k2; for_lia1. Qed.

Lemma ltN_comm_intro_lemma2 {n} (k1:ltN n) (k2:ltN (S n)) :
  proj1_sig k1 <? proj1_sig k2 = false -> proj1_sig k2 <? n = true.
Proof. destruct k1, k2; for_lia1. Qed.

Definition ltN_comm_intro {n} (k1:ltN n) (k2:ltN (S n)) : ltN n (* returns k2' *) :=
  (if proj1_sig k1 <? proj1_sig k2 as b0 return proj1_sig k1 <? proj1_sig k2 = b0 -> ltN n
   then (fun E => ltN_of_nat (pred(proj1_sig k2)) n (ltN_comm_intro_lemma1 k1 k2 E))
   else (fun E => ltN_of_nat      (proj1_sig k2)  n (ltN_comm_intro_lemma2 k1 k2 E))) eq_refl.

Lemma proj1_sig_ltN_comm_intro {n} (k1:ltN n) (k2:ltN (S n)) :
  proj1_sig (ltN_comm_intro k1 k2) =
    if proj1_sig k1 <? proj1_sig k2 then (pred(proj1_sig k2)) else (proj1_sig k2).
Proof with curry1.
unfold ltN_comm_intro...
rewrite comm_dtt_iff...
Qed.
#[export] Hint Rewrite @proj1_sig_ltN_comm_intro : curry1_rewrite.

Lemma ltN_intro_ltN_intro_comm {n} (k1:ltN(S n)) (k2:ltN(S(S n))) (x:ltN n) :
  ltN_intro k2 (ltN_intro k1 x) = ltN_intro (ltN_intro k2 k1) (ltN_intro (ltN_comm_intro k1 k2) x).
Proof with curry1.
rewrite ltN_PI.
rewrite proj1_sig_ltN_intro...
destruct k1, k2, x...
repeat rewrite_if_as_Prop.
for_lia1.
Qed.

Lemma ltN_intro_ltN_intro_ltN_comm_intro {n} (k1:ltN(S n)) (k2:ltN(S(S n))) :
  ltN_intro (ltN_intro k2 k1) (ltN_comm_intro k1 k2) = k2.
Proof with curry1.
rewrite ltN_PI.
rewrite proj1_sig_ltN_intro...
repeat rewrite_if_as_Prop...
lia.
Qed.
#[export] Hint Rewrite @ltN_intro_ltN_intro_ltN_comm_intro : curry1_rewrite.

Lemma ltN_comm_intro_ltN_comm_intro_ltN_intro {n} (k1:ltN(S n)) (k2:ltN(S(S n))) :
  ltN_comm_intro (ltN_comm_intro k1 k2) (ltN_intro k2 k1) = k1.
Proof with curry1.
rewrite ltN_PI.
rewrite proj1_sig_ltN_comm_intro...
repeat rewrite_if_as_Prop...
lia.
Qed.
#[export] Hint Rewrite @ltN_comm_intro_ltN_comm_intro_ltN_intro : curry1_rewrite.

Example ltN_intro_ltN_intro_comm_comm {n} (k1:ltN(S n)) (k2:ltN(S(S n))) (x:ltN n) :
  ltN_intro (ltN_intro (ltN_intro k2 k1) (ltN_comm_intro k1 k2))
  (ltN_intro (ltN_comm_intro (ltN_comm_intro k1 k2) (ltN_intro k2 k1)) x) = ltN_intro k2 (ltN_intro k1 x).
Proof. curry1. Qed.

(* [NO USE] *)
Lemma fold_left_ltN_comp_correct {n} (P:ltN n -> Prop)
  (H:forall (k Sk:ltN n), proj1_sig Sk = S(proj1_sig k) -> P k -> P Sk)
  (H0:forall (k0:ltN n), proj1_sig k0 = 0 -> P k0)
  : (forall (kL:ltN n), proj1_sig kL = pred n -> P kL).
Proof with curry1.
induction n...
specialize(IHn (fun k => P(ltN_S k)))...
assert_hypothesis IHn...
{ refine (H _ _ _ H3)... }
assert_hypothesis IHn...
{ pose (ltN_of_nat 0 (S n) eq_refl) as k1.
  specialize(H0 k1)...
  specialize(H k1 (ltN_S k0))...
}
destruct n...
assert(kL'p : (n <? S n) = true). { curry1. }
pose (ltN_of_nat n (S n) kL'p) as kL'.
specialize(IHn kL')...
assert(ltN_S kL' = kL)...
destruct kL as [kL kLP]...
Qed.

(* [NO USE] *)
Lemma forall_ltN_iff_forall_ltN_S (P:forall {n}, ltN n -> Prop) n :
  (forall n1 (k1:ltN n1) n2 (k2:ltN n2), proj1_sig k1 = proj1_sig k2 -> (P k1 <-> P k2)) ->
  ((forall k : ltN n, P k) <-> (forall k : ltN (S n), P k)) <->
    ((forall k : ltN n, P k) -> P(ltN_of_nat n (S n) (Nat.leb_refl (S n)))).
Proof with curry1.
split...
split...
- destruct(proj1_sig k <? n)eqn:E.
  + rewrite (H _ _ n (ltN_of_nat(proj1_sig k) _ E))...
  + assert(k = ltN_of_nat n (S n) (Nat.leb_refl (S n)))...
    destruct k...
- assert(kP:proj1_sig k <? S n = true). { destruct k... }
  specialize(H1(ltN_of_nat _ _ kP))...
  rewrite (H _ _ n k) in H1...
Qed.

Lemma fold_exist_0_ltb_S n p : exist (fun k : nat => (k <? S n) = true) 0 p = ltN_of_nat 0 (S n) eq_refl.
Proof. curry1. Qed.
#[export] Hint Rewrite fold_exist_0_ltb_S : curry1_rewrite.

Lemma ltN_S_eq_ltN_S n x y : @ltN_S n x = ltN_S y <-> x = y.
Proof. rewrite ltN_PI; simplnat; rewrite proj1_sig_injective; reflexivity. Qed.
#[export] Hint Rewrite ltN_S_eq_ltN_S : curry1_rewrite.

Lemma ltN_of_nat_proj1_sig {n} (k:ltN n) p : ltN_of_nat (proj1_sig k) n p = k.
Proof. curry1. Qed.
#[export] Hint Rewrite @ltN_of_nat_proj1_sig : curry1_rewrite.

Lemma ltN_intro_same_l_injective {n} z x y : @ltN_intro n z x = ltN_intro z y <-> x = y.
Proof. rewrite <- ltN_pop_iff_ltN_intro; curry1. Qed.
#[export] Hint Rewrite @ltN_intro_same_l_injective : curry1_rewrite.

Lemma ltN_pop_ltN_intro_both_same_l {n} k (x:ltN(S n)) y :
  ltN_pop (ltN_intro k x) (ltN_intro k y) =
    match ltN_pop x y with
    | AA tt => AA tt
    | BB y' => BB (ltN_intro (ltN_comm_intro x k) y')
    end.
Proof with curry1.
dest_match_step...
rewrite ltN_intro_ltN_intro_comm...
Qed.

Lemma ltN_pop_ltN_intro {n} (k1 k2:ltN(S(S n))) x0 :
  ltN_pop k1 (ltN_intro k2 x0) =
    match ltN_pop k2 k1 with
    | AA _ => BB x0
    | BB k1' =>
      let k2' := ltN_comm_intro k1' k2 in
      match ltN_pop k1' x0 with
      | AA tt => AA tt
      | BB x2 => BB (ltN_intro k2' x2)
      end
    end.
Proof with curry1.
dest_match_step...
dest_match_step...
rewrite <- ltN_intro_ltN_intro_comm...
Qed.

(* ltN_pred *)

Lemma ltN_pred_lemma0 {n} (k0:ltN(S n)) k (E : proj1_sig k0 = S k) : (k <? n) = true.
Proof. destruct k0; curry1. Qed.

Definition ltN_pred {n} (k:ltN(S n)) : option(ltN n) :=
  match proj1_sig k as k0 return proj1_sig k = k0 -> option(ltN n) with
  | 0 => fun _ => None
  | S k => fun E => Some(ltN_of_nat k n (ltN_pred_lemma0 _ _ E))
  end eq_refl.

Lemma ltN_pred_eq_Some {n} k (k':ltN n) : ltN_pred k = Some k' <-> k = ltN_S k'.
Proof with curry1.
unfold ltN_pred.
rewrite_dtt_nat...
split...
unfold ltN_S...
Qed.
#[export] Hint Rewrite @ltN_pred_eq_Some : curry1_rewrite.

Lemma ltN_pred_eq_None {n} (k:ltN(S n)) : ltN_pred k = None <-> k = ltN_of_nat 0 (S n) eq_refl.
Proof with curry1.
unfold ltN_pred...
rewrite_dtt_nat...
split...
Qed.
#[export] Hint Rewrite @ltN_pred_eq_None : curry1_rewrite.

(* beq_ltN *)

Definition beq_ltN {n} (k1 k2:ltN n) : bool := proj1_sig k1 =? proj1_sig k2.

Lemma beq_iff_true_beq_ltN n : beq_iff_true (@beq_ltN n).
Proof. intro; unfold beq_ltN; curry1; rewrite proj1_sig_injective; reflexivity.  Qed.

Lemma beq_ltN_iff_true {n} (k1 k2:ltN n) : beq_ltN k1 k2 = true <-> k1 = k2.
Proof. apply beq_iff_true_beq_ltN. Qed.
#[export] Hint Rewrite @beq_ltN_iff_true : curry1_rewrite.

Lemma beq_refl_beq_ltN n k : @beq_ltN n k k = true.
Proof. rewrite (beq_iff_true_beq_ltN _ _); reflexivity. Qed.
#[export] Hint Rewrite beq_refl_beq_ltN : curry1_rewrite.

(* cmp_ltN *)


Definition cmp_ltN {n} (x y : ltN n) : ord := cmp_nat (proj1_sig x) (proj1_sig y).

Lemma cmp_ltN_Eq_iff_eq n x y : @cmp_ltN n x y = Eq <-> x = y.
Proof. unfold cmp_ltN; rewrite ltN_PI; curry1. Qed.
#[export] Hint Rewrite cmp_ltN_Eq_iff_eq : curry1_rewrite.

Lemma cmp_Eq_iff_eq_cmp_ltN n : cmp_Eq_iff_eq (@cmp_ltN n).
Proof. intros x y; curry1. Qed.

Lemma cmp_Eq_iff_eq_cmp_ltN_iff_true n : cmp_Eq_iff_eq (@cmp_ltN n) <-> True.
Proof. curry1; apply cmp_Eq_iff_eq_cmp_ltN. Qed.
#[export] Hint Rewrite cmp_Eq_iff_eq_cmp_ltN_iff_true : curry1_rewrite.

Lemma cmp_Lt_trans_cmp_ltN n : cmp_Lt_trans (@cmp_ltN n).
Proof with curry1.
unfold cmp_Lt_trans...
destruct x, y, z; unfold cmp_ltN in *...
for_lia1.
Qed.

Lemma cmp_Lt_trans_cmp_ltN_iff_true n : cmp_Lt_trans (@cmp_ltN n) <-> True.
Proof. curry1; apply cmp_Lt_trans_cmp_ltN. Qed.
#[export] Hint Rewrite cmp_Lt_trans_cmp_ltN_iff_true : curry1_rewrite.

Lemma asym_asym o : asym(asym o) = o.
Proof. rewrite asym_shift; reflexivity. Qed.
#[export] Hint Rewrite asym_asym : curry1_rewrite.

Lemma cmp_asym_cmp_ltN n : cmp_asym (@cmp_ltN n).
Proof with curry1.
unfold cmp_asym, cmp_ltN...
destruct x as [x Hx], y as [y Hy]...
rewrite <- (cmp_asym_cmp_nat x y)...
Qed.

Lemma cmp_asym_cmp_ltN_iff_true n : cmp_asym (@cmp_ltN n) <-> True.
Proof. curry1; apply cmp_asym_cmp_ltN. Qed.
#[export] Hint Rewrite cmp_asym_cmp_ltN_iff_true : curry1_rewrite.

Lemma cmp_P_cmp_ltN n : cmp_P (@cmp_ltN n).
Proof. constructor; curry1. Qed.

Lemma cmp_P_cmp_ltN_iff_true n : cmp_P (@cmp_ltN n) <-> True.
Proof. curry1; apply cmp_P_cmp_ltN. Qed.

Lemma opmap_ltN_S_eq_Some_0 n o : opmap ltN_S o = Some(ltN_of_nat 0 (S n) eq_refl) <-> False.
Proof. destruct o; curry1. Qed.
#[export] Hint Rewrite opmap_ltN_S_eq_Some_0 : curry1_rewrite.

Lemma opmap_ltN_S_eq_Some_ltN_S n o (k:ltN n) : opmap ltN_S o = Some (ltN_S k) <-> o = Some k.
Proof. destruct o; curry1. Qed.
#[export] Hint Rewrite opmap_ltN_S_eq_Some_ltN_S : curry1_rewrite.

Lemma cmp_ltN_ltN_S_ltN_S n x y : cmp_ltN (@ltN_S n x) (ltN_S y) = cmp_ltN x y.
Proof. unfold cmp_ltN; curry1. Qed.
#[export] Hint Rewrite cmp_ltN_ltN_S_ltN_S : curry1_rewrite.

Lemma cmp_ltN_0_left_eq_Lt n p k : cmp_ltN (ltN_of_nat 0 n p) k = Lt <-> (k <> ltN_of_nat 0 n p).
Proof. destruct k as [[|k] Hk]; curry1. Qed.
#[export] Hint Rewrite cmp_ltN_0_left_eq_Lt : curry1_rewrite.

Lemma ltN_intro_ltN_S_0 {n} k p : @ltN_intro n (ltN_S k) (ltN_of_nat 0 n p) = ltN_of_nat 0 (S n) eq_refl.
Proof. destruct k; curry1. Qed.
#[export] Hint Rewrite @ltN_intro_ltN_S_0 : curry1_rewrite.

Lemma ltN_intro_ltN_S_both {n} x (y:ltN n) : ltN_intro (ltN_S x) (ltN_S y) = ltN_S (ltN_intro x y).
Proof with curry1.
rewrite ltN_PI...
dest_match_step...
Qed.
#[export] Hint Rewrite @ltN_intro_ltN_S_both : curry1_rewrite.

Lemma ltN_intro_0 n p k : (ltN_intro (ltN_of_nat 0 (S n) p) k) = ltN_S k.
Proof. rewrite ltN_PI; curry1. Qed.
#[export] Hint Rewrite @ltN_intro_0 : curry1_rewrite.

(* Up/Down Casts *)

Definition ltN_down_cast {n} (k:ltN n) m (H:proj1_sig k <? m = true) : ltN m := ltN_of_nat (proj1_sig k) m H.

Lemma ltN_up_cast_lemma0 {n} (k:ltN n) m (H:n <= m) : proj1_sig k <? m = true.
Proof. destruct k; for_lia1. Qed.

Definition ltN_up_cast {n} (k:ltN n) m (H:n <= m) : ltN m :=
  ltN_of_nat (proj1_sig k) m (ltN_up_cast_lemma0 k m H).

Lemma ltN_up_cast_ltN_down_cast {n m} (H1:m <= n) k (H2:proj1_sig k <? m = true) :
  ltN_up_cast (ltN_down_cast k m H2) n H1 = k.
Proof. compute; curry1. Qed.
#[export] Hint Rewrite @ltN_up_cast_ltN_down_cast : curry1_rewrite.

Lemma ltN_down_cast_ltN_up_cast {n m} (H1:m <= n) k (H2:proj1_sig k <? m = true) :
  (ltN_down_cast (ltN_up_cast k n H1) m H2) = k.
Proof. compute; curry1. Qed.
#[export] Hint Rewrite @ltN_down_cast_ltN_up_cast : curry1_rewrite.