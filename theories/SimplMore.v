Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.


Ltac simplmore0 := simplablist1.

Ltac simplmore := simplmore0.
