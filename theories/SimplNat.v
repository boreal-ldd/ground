Require Import Arith.
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplEq.
Require Import SimplOption.
Require Import SimplCmp.
Require Import SimplSimpl.

Ltac simplnat0 := simplsimpl; repeat (autorewrite with simplnat0_rewrite in *; simplsimpl).
#[export] Hint Unfold lt : simplnat0_rewrite.
#[export] Hint Unfold gt : simplnat0_rewrite.

#[export] Hint Rewrite Nat.add_assoc : simplnat0_rewrite.
#[export] Hint Rewrite Nat.add_0_r : simplnat0_rewrite.
#[export] Hint Rewrite Nat.add_succ_r  : simplnat0_rewrite.

#[export] Hint Rewrite Nat.max_id : simplnat0_rewrite.
#[export] Hint Rewrite Nat.eqb_refl : simplnat0_rewrite.
#[export] Hint Rewrite Nat.sub_0_r : simplnat0_rewrite.

#[export] Hint Rewrite Nat.sub_0_le : simplnat0_rewrite.
#[export] Hint Rewrite Nat.eq_add_0 : simplnat0_rewrite.
#[export] Hint Rewrite Nat.succ_inj_wd : simplnat0_rewrite.

#[export] Hint Rewrite <- Nat.succ_le_mono : simplnat0_rewrite. (* no hyp *)
#[export] Hint Rewrite <- Nat.succ_lt_mono : simplnat0_rewrite. (* no hyp *)
#[export] Hint Rewrite Nat.le_succ_l : simplnat0_rewrite.
#[export] Hint Rewrite Nat.lt_succ_r: simplnat0_rewrite.

#[export] Hint Rewrite Nat.nle_gt : simplnat0_rewrite. (* forall n m : nat, ~ n <= m <-> m < n *)
#[export] Hint Rewrite Nat.nlt_ge : simplnat0_rewrite. (* forall n m : nat, ~ n < m <-> m <= n *)

#[export] Hint Rewrite Nat.max_r_iff: simplnat0_rewrite. (* forall n m : nat, Nat.max n m = m <-> n <= m *)
#[export] Hint Rewrite Nat.max_l_iff: simplnat0_rewrite. (* forall n m : nat, Nat.max n m = n <-> m <= n *)
#[export] Hint Rewrite Nat.min_r_iff: simplnat0_rewrite.
#[export] Hint Rewrite Nat.min_l_iff: simplnat0_rewrite.

#[export] Hint Rewrite Nat.eqb_eq: simplnat0_rewrite. (* forall n m : nat, (n =? m) = true <-> n = m *)
#[export] Hint Rewrite Nat.le_0_r: simplnat0_rewrite. (* forall n : nat, n <= 0 <-> n = 0 *)
#[export] Hint Rewrite Nat.sub_diag: simplnat0_rewrite. (* forall n : nat, n - n = 0 *)
#[export] Hint Rewrite Nat.leb_refl : simplnat0_rewrite.
#[export] Hint Rewrite Nat.ltb_irrefl: simplnat0_rewrite. (* forall n : nat, (n <? n) = false *)

#[export] Hint Rewrite Nat.max_lub_iff: simplnat0_rewrite. (* forall n m p : nat, Nat.max n m <= p <-> n <= p /\ m <= p *)
#[export] Hint Rewrite Nat.max_lub_lt_iff: simplnat0_rewrite. (* forall n m p : nat, Nat.max n m < p <-> n < p /\ m < p *)

(* MOVE TO for_lia1 *)
#[export] Hint Rewrite Nat.max_le_iff: for_lia1. (* forall n m p : nat, p <= Nat.max n m <-> p <= n \/ p <= m *)
#[export] Hint Rewrite Nat.max_lt_iff: for_lia1. (* forall n m p : nat, p < Nat.max n m <-> p < n \/ p < m *)

#[export] Hint Rewrite Nat.min_glb_lt_iff: simplnat0_rewrite. (*  forall n m p : nat, p < Nat.min n m <-> p < n /\ p < m *)
#[export] Hint Rewrite Nat.min_glb_iff: simplnat0_rewrite. (*  forall n m p : nat, p <= Nat.min n m <-> p <= n /\ p <= m *)

#[export] Hint Rewrite Nat.min_0_r: simplnat0_rewrite. (* forall n : nat, Nat.min n 0 = 0 *)
#[export] Hint Rewrite Nat.min_0_l: simplnat0_rewrite. (* forall n : nat, Nat.min 0 n = 0 *)
#[export] Hint Rewrite Nat.max_0_r: simplnat0_rewrite. (* forall n : nat, Nat.max n 0 = n *)
#[export] Hint Rewrite Nat.max_0_l: simplnat0_rewrite. (* forall n : nat, Nat.max 0 n = n *)

#[export] Hint Rewrite Nat.min_id: simplnat0_rewrite. (* forall n : nat, Nat.min n n = n *)
#[export] Hint Rewrite Nat.max_id: simplnat0_rewrite. (* forall n : nat, Nat.max n n = n *)
(* (* [WIP] *)
Nat.min_max_absorption: forall n m : nat, Nat.max n (Nat.min n m) = n
Nat.max_min_absorption: forall n m : nat, Nat.min n (Nat.max n m) = n
Nat.min_assoc: forall m n p : nat, Nat.min m (Nat.min n p) = Nat.min (Nat.min m n) p
Nat.sub_min_distr_r: forall n m p : nat, Nat.min (n - p) (m - p) = Nat.min n m - p
Nat.min_max_distr:  forall n m p : nat, Nat.min n (Nat.max m p) = Nat.max (Nat.min n m) (Nat.min n p)
Nat.add_min_distr_l: forall n m p : nat, Nat.min (p + n) (p + m) = p + Nat.min n m
Nat.mul_min_distr_l: forall n m p : nat, Nat.min (p * n) (p * m) = p * Nat.min n m
Nat.add_min_distr_r: forall n m p : nat, Nat.min (n + p) (m + p) = Nat.min n m + p
Nat.mul_min_distr_r: forall n m p : nat, Nat.min (n * p) (m * p) = Nat.min n m * p
Nat.max_min_distr:  forall n m p : nat, Nat.max n (Nat.min m p) = Nat.min (Nat.max n m) (Nat.max n p)
Nat.min_max_modular:  forall n m p : nat, Nat.min n (Nat.max m (Nat.min n p)) = Nat.max (Nat.min n m) (Nat.min n p)
Nat.max_min_modular:  forall n m p : nat, Nat.max n (Nat.min m (Nat.max n p)) = Nat.min (Nat.max n m) (Nat.max n p)
Nat.sub_min_distr_l: forall n m p : nat, Nat.min (p - n) (p - m) = p - Nat.max n m
Nat.sub_max_distr_l: forall n m p : nat, Nat.max (p - n) (p - m) = p - Nat.min n m

Nat.min_max_absorption: forall n m : nat, Nat.max n (Nat.min n m) = n
Nat.max_min_absorption: forall n m : nat, Nat.min n (Nat.max n m) = n
Nat.max_assoc: forall m n p : nat, Nat.max m (Nat.max n p) = Nat.max (Nat.max m n) p
Max.max_assoc: forall m n p : nat, Nat.max m (Nat.max n p) = Nat.max (Nat.max m n) p
Nat.max_l_iff: forall n m : nat, Nat.max n m = n <-> m <= n
Nat.max_r_iff: forall n m : nat, Nat.max n m = m <-> n <= m
Nat.log2_lor: forall a b : nat, Nat.log2 (Nat.lor a b) = Nat.max (Nat.log2 a) (Nat.log2 b)
Nat.mul_max_distr_l: forall n m p : nat, Nat.max (p * n) (p * m) = p * Nat.max n m
Nat.add_max_distr_r: forall n m p : nat, Nat.max (n + p) (m + p) = Nat.max n m + p
Nat.add_max_distr_l: forall n m p : nat, Nat.max (p + n) (p + m) = p + Nat.max n m
Nat.min_max_distr:   forall n m p : nat, Nat.min n (Nat.max m p) = Nat.max (Nat.min n m) (Nat.min n p)
Nat.max_min_distr:   forall n m p : nat, Nat.max n (Nat.min m p) = Nat.min (Nat.max n m) (Nat.max n p)
Max.plus_max_distr_l: forall n m p : nat, Nat.max (p + n) (p + m) = p + Nat.max n m
Max.plus_max_distr_r: forall n m p : nat, Nat.max (n + p) (m + p) = Nat.max n m + p
Nat.sub_max_distr_r: forall n m p : nat, Nat.max (n - p) (m - p) = Nat.max n m - p
Nat.mul_max_distr_r: forall n m p : nat, Nat.max (n * p) (m * p) = Nat.max n m * p
Nat.sub_min_distr_l: forall n m p : nat, Nat.min (p - n) (p - m) = p - Nat.max n m
Nat.sub_max_distr_l: forall n m p : nat, Nat.max (p - n) (p - m) = p - Nat.min n m
Nat.max_min_modular:  forall n m p : nat, Nat.max n (Nat.min m (Nat.max n p)) = Nat.min (Nat.max n m) (Nat.max n p)
Nat.min_max_modular:  forall n m p : nat, Nat.min n (Nat.max m (Nat.min n p)) = Nat.max (Nat.min n m) (Nat.min n p)
*)

Ltac simplnat1_ltac :=
  match goal with
  | HH : ?n <= ?m |- _ =>
    match goal with
    | H : context[Nat.max n m] |- _ => rewrite (Nat.max_r n m) in H
    | |-  context[Nat.max n m] => rewrite (Nat.max_r n m)
    | H : context[Nat.max m n] |- _ => rewrite (Nat.max_l m n) in H
    | |-  context[Nat.max m n] => rewrite (Nat.max_l m n)
    end
  end.

(* Section 1. = / <> / <= / >= / < / > *)

Ltac simplnat1 :=
  simplnat0;
  repeat (
    (simplnat1_ltac ||
      (progress (autorewrite with simplnat1_rewrite in *))
    );
    simplnat0
  ).

Lemma n_neq_S_n n : n = S n <-> False.
Proof. lia. Qed.

Lemma S_n_eq_O n : S n = 0 <-> False.
Proof. lia. Qed.
#[export] Hint Rewrite S_n_eq_O : simplnat1_rewrite.

Lemma O_eq_S_n n : 0 = S n <-> False.
Proof. lia. Qed.
#[export] Hint Rewrite O_eq_S_n : simplnat1_rewrite.

Lemma S_gt_O n : S n > 0 <-> True.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite S_gt_O : simplnat1_rewrite.

Lemma O_lt_S n : 0 < S n <-> True.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite O_lt_S : simplnat1_rewrite.

Lemma lt_O n : n < 0 <-> False.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite lt_O : simplnat1_rewrite.

Lemma n_lt_n n : n < n <-> False.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite n_lt_n : simplnat1_rewrite.

Lemma n_gt_n n : n > n <-> False.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite n_gt_n : simplnat1_rewrite.

Lemma S_pred_eq_same n : S (pred n) = n <-> n <> 0.
Proof. destruct n; simplnat1. Qed.
#[export] Hint Rewrite S_pred_eq_same : simplnat1_rewrite.

(* SubSection 1. From Nat.leb to Nat.le (and Nat.ltb/Nat.lt) *)

Ltac for_lia1_ltac :=
  match goal with
  | |- ?n <> ?m => intro
  end.

Ltac for_lia1 :=
  simplnat1;
  repeat(
    (for_lia1_ltac ||
      (autorewrite with for_lia1 in *)
    );
    simplnat1
  ); try lia.

#[export] Hint Rewrite andb_true_iff : for_lia1.
#[export] Hint Rewrite orb_true_iff : for_lia1.
#[export] Hint Rewrite andb_false_iff : for_lia1.
#[export] Hint Rewrite orb_false_iff : for_lia1.
#[export] Hint Rewrite Nat.leb_gt  : for_lia1. (* forall x y : nat, (x <=? y) = false <->   y <  x *)
#[export] Hint Rewrite Nat.leb_le  : for_lia1. (* forall n m : nat, (n <=? m) = true  <->   n <= m *)
#[export] Hint Rewrite leb_iff_conv: for_lia1. (* forall m n : nat, (n <=? m) = false <->   m <  n *)
#[export] Hint Rewrite Nat.leb_nle : for_lia1. (* forall x y : nat, (x <=? y) = false <-> ~ x <= y *)
#[export] Hint Rewrite Nat.eqb_eq  : for_lia1. (* forall n m : nat, (n =? m)  = true  <->   n =  m *)
#[export] Hint Rewrite Nat.eqb_neq : for_lia1. (* forall x y : nat, (x =? y)  = false <->   x <> y *)
#[export] Hint Rewrite Nat.ltb_lt  : for_lia1. (* forall n m : nat, (n <? m)  = true  <->   n <  m *)
#[export] Hint Rewrite Nat.ltb_ge  : for_lia1. (* forall x y : nat, (x <? y)  = false <->   y <= x *)

(* MOVE TO for_lia1 *)
#[export] Hint Rewrite Nat.min_le_iff: for_lia1. (*  forall n m p : nat, Nat.min n m <= p <-> n <= p \/ m <= p *)
#[export] Hint Rewrite Nat.min_lt_iff: for_lia1. (*  forall n m p : nat, Nat.min n m < p <-> n < p \/ m < p *)
#[export] Hint Rewrite Nat.max_le_iff: for_lia1. (* forall n m p : nat, p <= Nat.max n m <-> p <= n \/ p <= m *)
#[export] Hint Rewrite Nat.max_lt_iff: for_lia1. (* forall n m p : nat, p < Nat.max n m <-> p < n \/ p < m *)

(* TEST *)
#[export] Hint Rewrite Nat.leb_gt  : simplnat1_rewrite. (* forall x y : nat, (x <=? y) = false <->   y <  x *)
#[export] Hint Rewrite Nat.leb_le  : simplnat1_rewrite. (* forall n m : nat, (n <=? m) = true  <->   n <= m *)
#[export] Hint Rewrite leb_iff_conv: simplnat1_rewrite. (* forall m n : nat, (n <=? m) = false <->   m <  n *)
#[export] Hint Rewrite Nat.leb_nle : simplnat1_rewrite. (* forall x y : nat, (x <=? y) = false <-> ~ x <= y *)
#[export] Hint Rewrite Nat.eqb_eq  : simplnat1_rewrite. (* forall n m : nat, (n =? m)  = true  <->   n =  m *)
#[export] Hint Rewrite Nat.eqb_neq : simplnat1_rewrite. (* forall x y : nat, (x =? y)  = false <->   x <> y *)
#[export] Hint Rewrite Nat.ltb_lt  : simplnat1_rewrite. (* forall n m : nat, (n <? m)  = true  <->   n <  m *)
#[export] Hint Rewrite Nat.ltb_ge  : simplnat1_rewrite. (* forall x y : nat, (x <? y)  = false <->   y <= x *)

(* SubSection 2. tactic based rewritting *)

Lemma rewrite_lt_false_with_le {m n} : m <= n -> (n < m <-> False).
Proof. lia. Qed.
Lemma rewrite_le_false_with_lt {m n} : n < m -> (m <= n <-> False).
Proof. lia. Qed.
Lemma rewrite_le_true_with_lt {m n} : n < m -> (n <= m <-> True).
Proof. simplprop. lia. Qed.
Lemma rewrite_le_eq_with_ge {x y} : x <= y -> (y <= x <-> x = y).
Proof. lia. Qed.
Lemma rewrite_lt_eq_with_ge_S {x y} : y < x ->  (x <= S y <-> x = S y).
Proof. lia. Qed.
Lemma rewrite_lt_false_with_lt {n m} : n < m -> (m < n <-> False).
Proof. lia. Qed.
(* NO HINT REWRITE *)
Lemma rewrite_lt_with_le {n m} : n <= m -> (n < m <-> n <> m).
Proof. lia. Qed.

Lemma rewrite_le_true_with_leb_true {m n} : (m <=? n) = true -> (m <= n <-> True).
Proof. for_lia1. Qed.
Lemma rewrite_lt_false_with_leb_true {m n} : (m <=? n) = true -> (n < m <-> False).
Proof. for_lia1. Qed.
Lemma rewrite_le_false_with_leb_false {m n} : (m <=? n) = false -> (m <= n <-> False).
Proof. for_lia1. Qed.
Lemma rewrite_lt_true_with_leb_false {m n} : (m <=? n) = false -> (n < m <-> True).
Proof. for_lia1. Qed.

Lemma rewrite_leb_with_le {n m} : n <= m -> (n <=? m) = true.
Proof. for_lia1. Qed.
Lemma rewrite_ltb_false_with_le {m n} : n <= m -> (m <? n = false).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.
Lemma rewrite_leb_false_with_lt {m n} : n < m -> (m <=? n = false).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.
Lemma rewrite_leb_true_with_lt {m n} : n < m -> (n <=? m = true).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.
Lemma rewrite_leb_eqb_with_ge {x y} : x <= y -> (y <=? x) = (x =? y).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.
Lemma rewrite_ltb_false_with_lt {n m} : n < m -> (m <? n = false).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.
Lemma rewrite_ltb_true_with_lt {n m} : n < m -> (n <? m = true).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.

(* NO HINT REWRITE *)
Lemma rewrite_ltb_with_le {n m} : n <= m -> (n <? m) = negb(n =? m).
Proof. rewrite bool_eq_iff_iff; for_lia1; lia. Qed.

(* LTAC REWRITE *)
Lemma max_with_lt_r n1 n2 : n1 < n2 -> max n1 n2 = n2.
Proof. simplnat1. lia. Qed.

(* LTAC REWRITE *)
Lemma max_with_lt_l n1 n2 (H:n2 < n1) : max n1 n2 = n1.
Proof. simplnat1. lia. Qed.

(* LTAC REWRITE *)
Lemma min_with_lt_l n1 n2 : n1 < n2 -> min n1 n2 = n1.
Proof. simplnat1. lia. Qed.

(* LTAC REWRITE *)
Lemma min_with_lt_r n1 n2 : n2 < n1 -> min n1 n2 = n2.
Proof. simplnat1. lia. Qed.

(* Check Nat.max_r. forall n m : nat, n <= m -> Nat.max n m = m *) (* LTAC REWRITE *)
(* Check Nat.max_l. forall n m : nat, m <= n -> Nat.max n m = n *) (* LTAC REWRITE *)
(* Check Nat.min_r. forall n m : nat, m <= n -> Nat.min n m = m *) (* LTAC REWRITE *)
(* Check Nat.min_l. forall n m : nat, n <= m -> Nat.min n m = n *) (* LTAC REWRITE *)

Lemma minus_with_le n m : n <= m -> n - m = 0.
Proof. lia. Qed.
Lemma minus_with_lt n m : n <  m -> n - m = 0.
Proof. lia. Qed.

Lemma rewrite_beq_with_neq n0 n1 : n0 <> n1 -> n0 =? n1 = false.
Proof. for_lia1. Qed.
Lemma rewrite_beq_with_neq_asym n0 n1 : n0 <> n1 -> n1 =? n0 = false.
Proof. for_lia1. Qed.

(* [TODO] symmetrize and idem for [max] *)
Lemma min_lt_with_lt_l a b c : a < c -> (min a b < c <-> True).
Proof. for_lia1. Qed.
Lemma min_ltb_with_lt_l a b c : a < c -> (min a b <? c = true).
Proof. for_lia1. Qed.
Lemma min_lt_with_lt_r a b c : b < c -> (min a b < c <-> True).
Proof. for_lia1. Qed.
Lemma min_ltb_with_lt_r a b c : b < c -> (min a b <? c = true).
Proof. for_lia1. Qed.

Ltac simplnat2_ltac := match goal with
  | H : 0 = 0 |- _ => clear H
  | H : S _ = S _ |- _ => inversion H
  | H : S _ = 0 |- _ => inversion H
  | H : 0 = S _ |- _ => inversion H
  | H : 0 = ?E |- _ => symmetry in H
  | H : S _ = ?E |- _ => symmetry in H
  | R : ?E = S _ , H : context[?E] |- _ => rewrite R in H
  | R : ?E = S _ |-    context[?E]      => rewrite R
  | R : ?E = 0   , H : context[?E] |- _ => rewrite R in H
  | R : ?E = 0   |-    context[?E]      => rewrite R
  | HH : ?m <= ?n |- _ =>
    match goal with
    | H : context[m <= n] |- _ => rewrite (rewrite_le_false_with_lt HH) in H
    | |- context[m <= n] => rewrite (rewrite_le_false_with_lt HH)
    | H : context[n <= m] |- _ => rewrite (rewrite_le_eq_with_ge HH) in H
    | |- context[n <= m] => rewrite (rewrite_le_eq_with_ge HH)
    | H : context[max n m] |- _ => rewrite (Nat.max_l _ _ HH) in H
    | |-  context[max n m]      => rewrite (Nat.max_l _ _ HH)
    | H : context[max m n] |- _ => rewrite (Nat.max_r _ _ HH) in H
    | |-  context[max m n]      => rewrite (Nat.max_r _ _ HH)
    | H : context[min n m] |- _ => rewrite (Nat.min_r _ _ HH) in H
    | |-  context[min n m]      => rewrite (Nat.min_r _ _ HH)
    | H : context[min m n] |- _ => rewrite (Nat.min_l _ _ HH) in H
    | |-  context[min m n]      => rewrite (Nat.min_l _ _ HH)
    | H : context[m - n] |- _ => rewrite(minus_with_le _ _ HH) in H
    | |-  context[m - n]      => rewrite(minus_with_le _ _ HH)
    | H : context[m <=? n] |- _ => rewrite(rewrite_leb_with_le HH) in H
    | |-  context[m <=? n]      => rewrite(rewrite_leb_with_le HH)
    | H : context[n <?  m] |- _ => rewrite(rewrite_ltb_false_with_le HH) in H
    | |-  context[n <?  m]      => rewrite(rewrite_ltb_false_with_le HH)
    | H : context[n <=? m] |- _ => rewrite(rewrite_leb_eqb_with_ge HH) in H
    | |-  context[n <=? m]      => rewrite(rewrite_leb_eqb_with_ge HH)
(*  | H : context[m <?  n] |- _ => rewrite(rewrite_ltb_with_le HH) in H
    | |-  context[m <?  n]      => rewrite(rewrite_ltb_with_le HH) *)
    end
  | HH : ?n < ?m |- _ =>
    match goal with
    | H : context[m <= n] |- _ => rewrite (rewrite_le_false_with_lt HH) in H
    | |-  context[m <= n] => rewrite (rewrite_le_false_with_lt HH)
    | H : context[n <= m] |- _ => rewrite (rewrite_le_true_with_lt HH) in H
    | |-  context[n <= m] => rewrite (rewrite_le_true_with_lt HH)
    | H : context[m < n] |- _ => rewrite (rewrite_lt_false_with_lt HH) in H
    | |-  context[m < n]      => rewrite (rewrite_lt_false_with_lt HH)
    | H : context[max n m] |- _ => rewrite (max_with_lt_r _ _ HH) in H
    | |-  context[max n m]      => rewrite (max_with_lt_r _ _ HH)
    | H : context[max m n] |- _ => rewrite (max_with_lt_l _ _ HH) in H
    | |-  context[max m n]      => rewrite (max_with_lt_l _ _ HH)
    | H : context[min n m] |- _ => rewrite (min_with_lt_l _ _ HH) in H
    | |-  context[min n m]      => rewrite (min_with_lt_l _ _ HH)
    | H : context[min m n] |- _ => rewrite (min_with_lt_r _ _ HH) in H
    | |-  context[min m n]      => rewrite (min_with_lt_r _ _ HH)
    | H : context[m <=? n] |- _ => rewrite(rewrite_leb_false_with_lt HH) in H
    | |-  context[m <=? n]      => rewrite(rewrite_leb_false_with_lt HH)
    | H : context[n <=? m] |- _ => rewrite(rewrite_leb_true_with_lt HH) in H
    | |-  context[n <=? m]      => rewrite(rewrite_leb_true_with_lt HH)
    | H : context[m <? n] |- _ => rewrite (rewrite_ltb_false_with_lt HH) in H
    | |-  context[m <? n]      => rewrite (rewrite_ltb_false_with_lt HH)
    | H : context[n <? m] |- _ => rewrite (rewrite_ltb_true_with_lt HH) in H
    | |-  context[n <? m]      => rewrite (rewrite_ltb_true_with_lt HH)
    | H : context[n - m] |- _ => rewrite(minus_with_lt _ _ HH) in H
    | |-  context[n - m]      => rewrite(minus_with_lt _ _ HH)
    | H : context[m <= S n] |- _ => rewrite(rewrite_lt_eq_with_ge_S HH) in H
    | |-  context[m <= S n]      => rewrite(rewrite_lt_eq_with_ge_S HH)
    | H : context[min n _ < m]  |- _ => rewrite(min_lt_with_lt_l n _ m HH) in H
    | |- context[min n _ < m]        => rewrite(min_lt_with_lt_l n _ m HH)
    | H : context[min _ n < m]  |- _ => rewrite(min_lt_with_lt_r n _ m HH) in H
    | |- context[min _ n < m]        => rewrite(min_lt_with_lt_r n _ m HH)
    | H : context[min n _ <? m] |- _ => rewrite(min_ltb_with_lt_l n _ m HH) in H
    | |- context[min n _ <? m]       => rewrite(min_ltb_with_lt_l n _ m HH)
    | H : context[min _ n <? m] |- _ => rewrite(min_ltb_with_lt_r n _ m HH) in H
    | |- context[min _ n <? m]       => rewrite(min_ltb_with_lt_r n _ m HH)
    end
  | HH : ?n0 <> ?n1 |- _ => match goal with
    | H : context[n0 =? n1] |- _ => rewrite(rewrite_beq_with_neq n0 n1 HH) in H
    | |- context[n0 =? n1] => rewrite(rewrite_beq_with_neq n0 n1 HH)
    | H : context[n1 =? n0] |- _ => rewrite(rewrite_beq_with_neq_asym n0 n1 HH) in H
    | |- context[n1 =? n0] => rewrite(rewrite_beq_with_neq_asym n0 n1 HH)
    end
(* [TEST]
  | HH : (?m <=? ?n) = true |- _ =>
    match goal with
    | H : context[m <= n] |- _ => rewrite (rewrite_le_true_with_leb_true HH) in H
    | |- context[m <= n] => rewrite (rewrite_le_true_with_leb_true HH)
    | H : context[n < m] |- _ => rewrite (rewrite_lt_false_with_leb_true HH) in H
    | |- context[n < m] => rewrite (rewrite_lt_false_with_leb_true HH)
    end
  | HH : (?m <=? ?n) = false |- _ =>
    match goal with
    | H : context[?m <= ?n] |- _ => rewrite (rewrite_lt_false_with_leb_true HH) in H
    | |- context[?m <= ?n] => rewrite (rewrite_lt_false_with_leb_true HH)
    | H : context[n < m] |- _ => rewrite (rewrite_lt_true_with_leb_false HH) in H
    | |- context[n < m] => rewrite (rewrite_lt_true_with_leb_false HH)
    end
 *)
  end.

Ltac simplnat2 :=
  simplnat1;
  repeat (
    (simplnat2_ltac ||
      (progress (autorewrite with simplnat2_rewrite in *))
    );
    simplnat1
  ).

Ltac for_lia2 :=
  simplnat2;
  repeat(
    (for_lia1_ltac ||
      (autorewrite with for_lia1 in *)
    );
    simplnat2
  ).

Lemma ltb_S_eq_leb n m : (n <? S m) = (n <=? m).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.
#[export] Hint Rewrite ltb_S_eq_leb : simplnat1_rewrite.

(* Section 2. Max/Min *)

Lemma min_to_if_leb n m : min n m = if Nat.leb n m then n else m.
Proof with simplnat2.
rewrite Nat.min_comm.
symmetry.
apply Nat.min_unicity...
destruct(n <=? m) eqn:E...
Qed.

Lemma max_to_if_leb n m : max n m = if Nat.leb n m then m else n.
Proof with simplnat2.
rewrite Nat.max_comm.
symmetry.
apply Nat.max_unicity...
destruct(n <=? m) eqn:E...
Qed.

Lemma max_0 n m : max n m = 0 <-> n = 0 /\ m = 0.
Proof with simplnat2.
split...
destruct n, m; simpl in *...
Qed.
#[export] Hint Rewrite max_0 : simplnat2_rewrite.

Lemma le_plus_minus_r n m :
  n + (m - n) = max n m.
Proof with simplnat2.
rewrite max_to_if_leb.
dest_if; for_lia1; lia.
Qed.
#[export] Hint Rewrite le_plus_minus_r : simplnat2_rewrite.

Lemma beq_min_r n m : Nat.eqb n (min n m) = Nat.leb n m.
Proof with simplnat2.
rewrite min_to_if_leb.
dest_if; for_lia1.
Qed.
#[export] Hint Rewrite beq_min_r : simplnat2_rewrite.

Lemma leb_min_r_dis a b c : Nat.leb a (min b c) = andb (Nat.leb a b) (Nat.leb a c).
Proof with simplnat2.
apply bool_eq_iff_iff.
rewrite min_to_if_leb; dest_if; for_lia1; lia.
Qed.
#[export] Hint Rewrite leb_min_r_dis : simplnat2_rewrite.

Lemma leb_S_min_r_dis a b c : Nat.leb a (S(min b c)) = andb (Nat.leb a (S b)) (Nat.leb a (S c)).
Proof with simplnat2.
apply bool_eq_iff_iff.
rewrite min_to_if_leb; dest_if; for_lia1; lia.
Qed.
#[export] Hint Rewrite leb_S_min_r_dis : simplnat2_rewrite.

Lemma leb_min_l_dis a b c : Nat.leb (min a b) c = orb (Nat.leb a c) (Nat.leb b c).
Proof with simplnat2.
apply bool_eq_iff_iff.
rewrite min_to_if_leb; dest_if; for_lia1; lia.
Qed.
#[export] Hint Rewrite leb_min_l_dis : simplnat2_rewrite.

Lemma le_0 x : (0 <= x) <-> True.
Proof. simplprop. apply Nat.le_0_l. Qed.
#[export] Hint Rewrite le_0 : simplnat2_rewrite.

Lemma S_x_le_x x : S x <= x <-> False.
Proof. lia. Qed.
#[export] Hint Rewrite S_x_le_x  : simplnat2_rewrite.

Lemma x_le_S_x x : x <= S x <-> True.
Proof. simplprop. Qed.
#[export] Hint Rewrite x_le_S_x : simplnat2_rewrite.

Lemma fold_S_n_leb_m n m : match m with 0 => false | S m' => n <=? m' end = (n <? m).
Proof. reflexivity. Qed.
#[export] Hint Rewrite fold_S_n_leb_m : simplnat2_rewrite.

Lemma match_nat_id i : match i with 0 => 0 | S n => S n end = i.
Proof. dest_match. Qed.
#[export] Hint Rewrite match_nat_id : simplnat2_rewrite.

Lemma match_nat_id_r i : match i with 0 => 0 | _ => i end = i.
Proof. dest_match. Qed.
#[export] Hint Rewrite match_nat_id_r : simplnat2_rewrite.

Lemma match_nat_converge i {B} (x:B) : match i with 0 => x | _ => x end = x.
Proof. dest_match. Qed.
#[export] Hint Rewrite match_nat_converge : simplnat2_rewrite.

Lemma asym_S_le n m : S n <=? m = negb(m <=? n).
Proof. rewrite bool_eq_iff_iff; for_lia2. Qed.

Lemma beq_nat_iff_true : beq_iff_true beq_nat.
Proof with simplnat2.
intros n m...
Qed.

Lemma max_leb n1 n2 n3 :
  (max n1 n2 <=? n3) = (n1 <=? n3) && (n2 <=? n3).
Proof with simplnat2.
rewrite bool_eq_iff_iff; for_lia2.
Qed.
#[export] Hint Rewrite max_leb : simplnat2_rewrite.

Lemma lt_max_l n m : n < max n m <-> n < m.
Proof. for_lia1. Qed.
Lemma lt_max_r n m : m < max n m <-> m < n.
Proof. for_lia1. Qed.
#[export] Hint Rewrite lt_max_l : simplnat2_rewrite.
#[export] Hint Rewrite lt_max_r : simplnat2_rewrite.

Lemma leb_0 a : Nat.leb a 0 = Nat.eqb a 0.
Proof. destruct a; auto. Qed.
#[export] Hint Rewrite leb_0 : simplnat2_rewrite.

Lemma min_pred_pred n m : min (pred n) (pred m) = pred(min n m).
Proof with simplnat2.
destruct n, m...
Qed.
#[export] Hint Rewrite min_pred_pred : simplnat2_rewrite.

(* NO HINT REWRITE *)
Lemma rewrite_le_pred n m : n <= pred m <-> n < m \/ (n = 0 /\ m = 0).
Proof with simplnat2.
destruct m...
Qed.

Lemma pred_minus_pred n m : pred n - pred m = if m =? 0 then pred n else n - m .
Proof. destruct n, m; simplnat2. Qed.

Inductive le0 : nat -> nat -> Prop :=
| Le0 n : le0 0 n
| LeS n m : le0 n m -> le0 (S n) (S m).

(* NO HINT REWRITE *)
Lemma le_refl x : le0 x x.
Proof with simplnat1.
induction x; constructor...
Qed.

(* NO HINT REWRITE *)
Lemma le0_n_S_m {n m} (H:le0 n m) : le0 n (S m).
Proof with simplnat1.
induction H; constructor...
Qed.

(* NO HINT REWRITE *)
Lemma le_iff_le0 x y : le x y <-> le0 x y.
Proof with simplnat1.
split...
- induction H.
  + apply le_refl.
  + apply (le0_n_S_m IHle).
- induction H.
  + apply Nat.le_0_l.
  + apply (le_n_S _ _ IHle0).
Qed.

(* NO HINT REWRITE *)
Lemma le_of_forall_lt_neq n0 n :
  n0 <= n <-> (forall k , k < n0 -> k <> n) .
Proof with simplnat2.
generalize dependent n0.
induction n; split...
- specialize(H 0)...
  destruct n0...
- destruct n0...
  rewrite IHn...
  specialize(H(S n))...
Qed.

(* NO HINT REWRITE *)
Lemma rewrite_forall_nat (P:nat->Prop) :
  (forall n, P n) <-> P 0 /\ (forall n, P(S n)).
Proof with simplnat2.
split...
destruct n...
Qed.

(* NO HINT REWRITE *)
Lemma ge_of_forall_lt_neq n0 n :
  n <= n0 <-> (forall k , n0 < k -> k <> n) .
Proof with simplnat2.
generalize dependent n.
induction n0; destruct n; split; intros; try intro...
- rewrite rewrite_forall_nat in H...
  specialize(H0 n)...
- rewrite rewrite_forall_nat in H...
  specialize(H0 n)...
  lia.
Qed.

Ltac simplnat := simplnat2.

(* Section. To Be Sorted *)

Definition SS (n:nat) := n + 1.

Lemma S_eq_SS n : S n = SS n.
Proof. unfold SS. simplnat. Qed.

Lemma SS_leb n m : (SS n <=? m) = (n <? m).
Proof. unfold SS; simplnat. Qed.

Lemma max_SS n m : max (SS n) (SS m) = SS(max n m).
Proof. repeat rewrite <- S_eq_SS. reflexivity. Qed.
#[export] Hint Rewrite max_SS : simplnat1_rewrite.

Lemma match_nat_same {A} n (c:A) : match n with 0 | S _ => c end = c.
Proof. destruct n; reflexivity. Qed.
#[export] Hint Rewrite @match_nat_same : simplnat1_rewrite.

Lemma rewrite_max_ltb n m k : (max n m <? k) = (n <? k) && (m <? k).
Proof with simplnat.
rewrite bool_eq_iff_iff...
split...
Qed.

Lemma rewrite_min_ltb a b c : min a b <? c = (a <? c) || (b <? c).
Proof with simplnat.
rewrite bool_eq_iff_iff...
split; for_lia1.
Qed.

Lemma rewrite_eq_S n m :
     match m with
     | 0 => false
     | S m' => n =? m'
     end = (m =? S n).
Proof. destruct m; simplnat. apply Nat.eqb_sym. Qed.
#[export] Hint Rewrite rewrite_eq_S : simplnat1_rewrite.

Fixpoint fold_nat_k {A:Type} (f:nat -> A -> A) (k:nat) (a:A) : A :=
match k with
| 0 => a
| S k => f k (fold_nat_k f k a)
end.

Lemma not_neq_nat (n m:nat) : ~ n <> m <-> n = m.
Proof. destruct(n =? m)eqn:E; simplnat. Qed.
#[export] Hint Rewrite not_neq_nat : simplnat1_rewrite.

Lemma pred_eq_same_r n : n = pred n <-> n = 0.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite pred_eq_same_r : simplnat1_rewrite.

Lemma pred_eq_same_l n : n = pred n <-> n = 0.
Proof. simplprop. lia. Qed.
#[export] Hint Rewrite pred_eq_same_l : simplnat1_rewrite.

Lemma pred_lt_same n : pred n < n <-> 0 < n.
Proof. lia. Qed.
#[export] Hint Rewrite pred_lt_same : simplnat1_rewrite.

Lemma pred_le_same n : pred n <= n <-> True.
Proof. lia. Qed.
#[export] Hint Rewrite pred_le_same : simplnat1_rewrite.


Lemma min_aba a b : min (min a b) a = min a b.
Proof. for_lia1. Qed.
#[export] Hint Rewrite @min_aba : simplnat1_rewrite.

Lemma min_abb a b : min (min a b) b = min a b.
Proof. for_lia1. Qed.
#[export] Hint Rewrite @min_abb : simplnat1_rewrite.

Lemma plus_a_b_lt_a a b : a + b < a <-> False.
Proof. lia. Qed.
#[export] Hint Rewrite @plus_a_b_lt_a : simplnat1_rewrite.

Lemma rewrite_ltb_min a b c : a <? min b c = (a <? b) && (a <? c).
Proof. rewrite bool_eq_iff_iff; for_lia1. Qed.


Lemma match_SS i {T} (c0:T) cS : match SS i with 0 => c0 | S i' => cS i' end = cS i.
Proof. rewrite <- S_eq_SS; reflexivity. Qed.
#[export] Hint Rewrite @match_SS : simplnat1_rewrite.

Lemma S_eq_SS_iff i j : S i = SS j <-> i = j.
Proof. rewrite <- S_eq_SS; simplnat. Qed.
#[export] Hint Rewrite @S_eq_SS_iff : simplnat1_rewrite.

Lemma SS_eq_S_iff i j : SS i = S j <-> i = j.
Proof. rewrite <- S_eq_SS; simplnat. Qed.
#[export] Hint Rewrite @SS_eq_S_iff : simplnat1_rewrite.

Lemma leb_pred_same n : (n <=? (pred n)) = (n =? 0).
Proof with simplnat1.
destruct n.
- reflexivity.
- rewrite asym_S_le...
Qed.

(* [WIP] *)
Lemma minus_lt_minus_case00 (n1 n2 m:nat) : n1 - m < n2 - m <-> n1 < n2 /\ m < n2.
Proof. lia. Qed.
#[export] Hint Rewrite minus_lt_minus_case00 : simplnat1_rewrite.

Lemma minus_ltb_minus_case00 (n1 n2 m:nat) : (n1 - m <? n2 - m) = (n1 <? n2) && (m <? n2).
Proof with simplnat.
rewrite eq_iff_eq_true...
rewrite andb_true_iff...
Qed.

Lemma minus_le_minus_case00 (n1 n2 m:nat) : n1 - m <= n2 - m <-> n1 <= n2 \/ n1 <= m.
Proof. lia. Qed.
#[export] Hint Rewrite minus_le_minus_case00 : simplnat1_rewrite.

(* Cmp Nat *)

Fixpoint cmp_nat (x y:nat) : ord :=
  match x, y with
  | 0, 0 => Eq
  | _, 0 => Gt
  | 0, _ => Lt
  | S x, S y => cmp_nat x y
  end.

Lemma cmp_nat_eq_iff x y o : cmp_nat x y = o <-> (match o with Eq => x = y | Lt => x < y | Gt => y < x end).
Proof with simplnat.
generalize dependent o.
generalize dependent y.
induction x; destruct y, o; simplnat; rewrite IHx; simplnat.
Qed.

Lemma cmp_nat_Eq_iff_eq x y : cmp_nat x y = Eq <-> x = y.
Proof. rewrite cmp_nat_eq_iff; reflexivity. Qed.
#[export] Hint Rewrite cmp_nat_Eq_iff_eq : simplnat1_rewrite.

Lemma cmp_Eq_iff_eq_cmp_nat : cmp_Eq_iff_eq cmp_nat <-> True.
Proof. simplnat; intros x y; apply cmp_nat_Eq_iff_eq. Qed.
#[export] Hint Rewrite cmp_Eq_iff_eq_cmp_nat : simplnat1_rewrite.

Lemma cmp_nat_eq_Lt_iff_lt x y : cmp_nat x y = Lt <-> x < y.
Proof. rewrite cmp_nat_eq_iff; reflexivity. Qed.
#[export] Hint Rewrite cmp_nat_eq_Lt_iff_lt : simplnat1_rewrite.

Lemma cmp_nat_eq_Gt_iff_lt x y : cmp_nat x y = Gt <-> x > y.
Proof. rewrite cmp_nat_eq_iff; reflexivity. Qed.
#[export] Hint Rewrite cmp_nat_eq_Gt_iff_lt : simplnat1_rewrite.

Lemma cmp_Lt_trans_cmp_nat : cmp_Lt_trans cmp_nat.
Proof with simplnat.
unfold cmp_Lt_trans...
for_lia1.
Qed.

Lemma cmp_Lt_trans_cmp_nat_iff_true : cmp_Lt_trans cmp_nat <-> True.
Proof. simplnat; apply cmp_Lt_trans_cmp_nat. Qed.
#[export] Hint Rewrite cmp_Lt_trans_cmp_nat_iff_true : simplnat1_rewrite.

Lemma cmp_asym_cmp_nat : cmp_asym cmp_nat.
Proof with simplnat.
unfold cmp_asym, asym...
generalize dependent y.
induction x; destruct y...
Qed.

Lemma cmp_asym_cmp_nat_iff_true : cmp_asym cmp_nat <-> True.
Proof. simplnat; apply cmp_asym_cmp_nat. Qed.
#[export] Hint Rewrite cmp_asym_cmp_nat_iff_true : simplnat1_rewrite.

Lemma cmp_P_cmp_nat : cmp_P cmp_nat.
Proof. constructor; simplnat. Qed.

Lemma cmp_P_cmp_nat_iff_true : cmp_P cmp_nat <-> True.
Proof. simplnat; apply cmp_P_cmp_nat. Qed.
#[export] Hint Rewrite cmp_P_cmp_nat_iff_true : simplnat1_rewrite.

(*
Ltac simplnat_step := match goal with
  | H : 0 = S _ |- _ => inversion H
  | H : S _ = 0 |- _ => inversion H
  | H : 0 = S _ |- _ => inversion H
  | H : S _ = S _ |- _ => inv H
  | H : 0 = _ |- _ => symmetry in H
  | H : S _ = _ |- _ => symmetry in H
  | S : ?E = 0, H : context[?E] |- _ =>
    rewrite S in H
  | S : ?E = 0 |- context[?E] =>
    rewrite S
  | S : ?E = S _, H : context[?E] |- _ =>
    rewrite S in H
  | S : ?E = S _ |- context[?E] =>
    rewrite S
  | H : ?x <= ?y, HH : context[?y <= ?x] |- _ => rewrite (le_le_iff_eq x y H) in HH
  | H : ?x <= ?y |- context[?y <= ?x] => rewrite (le_le_iff_eq x y H)
  (* s_x_le_x *)
  | H : context[S ?x <= ?x ] |- _ =>
    rewrite(s_x_le_x x) in H
  | |- context[S ?x <= ?x ] =>
    rewrite(s_x_le_x x)

  (* x_le_s_x *)
  | H : context[ ?x <= S ?x ] |- _ =>
    rewrite(x_le_s_x x) in H
  | |- context[ ?x <= S ?x ] =>
    rewrite(x_le_s_x x)
  | H : context [0 <= ?x] |- _ => rewrite (O_le x) in H
  | |- context [0 <= ?x] => rewrite (O_le x)
  | H : context [S ?n = 0] |- _ => rewrite (S_n_eq_O n) in H
  | |- context [S ?n = 0] => rewrite (S_n_eq_O n)
  | H : context [0 = S ?n] |- _ => rewrite (O_eq_S_n n) in H
  | |- context [0 = S ?n] => rewrite (O_eq_S_n n)
  | H : context [S ?n = S ?m] |- _ => rewrite (S_eq_rewrite n m) in H
  | |- context [S ?n = S ?m] => rewrite (S_eq_rewrite n m)
  | H : context [S ?n <= S ?m] |- _ => rewrite (S_le_S n m) in H
  | |- context [S ?n <= S ?m] => rewrite (S_le_S n m)
  | H : context [?n <= ?n] |- _ => rewrite (rewrite_P (le_n n)) in H
  | |- context [?n <= ?n] => rewrite (rewrite_P (le_n n))
  | H : context [S ?n >= S ?m] |- _ => rewrite (S_ge_S n m) in H
  | |- context [S ?n >= S ?m] => rewrite (S_ge_S n m)
  | H : context [?n >= ?n] |- _ => rewrite (rewrite_P (le_n n)) in H
  | |- context [?n >= ?n] => rewrite (rewrite_P (le_n n))
  | H : context [?n = S ?n] |- _ => rewrite (n_neq_S_n n) in H
  | |- context [?n = S ?n] => rewrite (n_neq_S_n n)
  | H : context [ _ + ( _ + _ ) ] |- _ => rewrite Nat.add_assoc in H
  | |- context [ _ + ( _ + _ ) ] => rewrite Nat.add_assoc
  | H : context [ _ + 0 ] |- _ => rewrite Nat.add_0_r in H
  | |- context [ _ + 0 ] => rewrite Nat.add_0_r
  | H : context [ 0 + _ ] |- _ => simpl in H
  | |- context [ 0 + _ ] => simpl
  | H : context [plus _ (S _)] |- _ => rewrite Nat.add_succ_r in H
  | |- context [plus _ (S _)] => rewrite Nat.add_succ_r
  | H : context [plus _   0  ] |- _ => rewrite <- plus_n_O in H
  | |- context [plus _   0  ] => rewrite <- plus_n_O
  | H : context [plus (S _) _] |- _ => simpl in H
  | |- context [plus (S _) _] => simpl
  | H : _ <= 0 |- _ => inv H
  | H : 0 <= _ |- _ => clear H
  | H : context [S _ > 0] |- _ => rewrite S_gt_O in H
  | |- context [S _ > 0] => rewrite S_gt_O
  | H : context [0 < S _ ] |- _ => rewrite O_lt_S in H
  | |- context [0 < S _ ] => rewrite O_lt_S
  | H : context [ S _ > S _ ] |- _ => rewrite S_gt_S in H
  | |- context [ S _ > S _ ] => rewrite S_gt_S
  | H : context [ S _ < S _ ] |- _ => rewrite S_lt_S in H
  | |- context [ S _ < S _ ] => rewrite S_lt_S
  | H : context [ _ < 0 ] |- _ => rewrite lt_O in H
  | |- context [ _ < 0 ] => rewrite lt_O
  | H : context [ 0 > _ ] |- _ => rewrite O_gt in H
  | |- context [ 0 > _ ]  => rewrite O_gt
  | H : context [ ?n < ?n ] |- _ => rewrite n_lt_n in H
  | |- context [ ?n < ?n ] => rewrite n_lt_n
  | H : context [ ?n > ?n ] |- _ => rewrite n_gt_n in H
  | |- context [ ?n > ?n ] => rewrite n_gt_n
  | H : context [max ?n ?m = 0] |- _ => rewrite max_0 in H
  | |- context [max ?n ?m = 0] => rewrite max_0
  | H : context [max ?n ?n] |- _ => rewrite Nat.max_id in H
  | |- context [max ?n ?n] => rewrite Nat.max_id
  | H : context [Nat.eqb ?x ?x] |- _ => rewrite Nat.eqb_refl in H
  | |- context [Nat.eqb ?x ?x] => rewrite Nat.eqb_refl
  | H : context[?n - 0] |- _ => rewrite Nat.sub_0_r in H
  | |- context[?n - 0] => rewrite Nat.sub_0_r
  | H : (?m <=? ?n) = true |- _ =>
    rewrite Nat.leb_le in H
  | H : context[?n + (?m - ?n)] |- _ =>
    rewrite le_plus_minus_r in H
  | |- context[?n + (?m - ?n)] =>
    rewrite le_plus_minus_r
  | HH : ?n <= ?m |- _ => match goal with
    | H : context[max ?n ?m] |- _ =>
      rewrite (Nat.max_r _ _ HH) in H
    | |- context[max ?n ?m] =>
      rewrite (Nat.max_r _ _ HH)
    | H : context[max ?m ?n] |- _ =>
      rewrite (Nat.max_l _ _ HH) in H
    | |- context[max ?m ?n] =>
      rewrite (Nat.max_l _ _ HH)
    end
  | H : context[(0 <=? 0)] |- _ => simpl in H
  | H : context[(S _ <=? 0)] |- _ => simpl in H
  | H : context[(0 <=? S _)] |- _ => simpl in H
  | H : context[(S _ <=? S _)] |- _ => simpl in H
  | H : context[Nat.leb (S _) ?n = true]
    |- context[match ?n with _ => _ end] =>
      let D := fresh "N" in destruct n eqn:E; simpl in H

  (* Nat.eq_add_0 *)
  | H : context[?n + ?m = 0] |- _ =>
    rewrite (Nat.eq_add_0 n m) in H
  | |- context[?n + ?m = 0] =>
    rewrite (Nat.eq_add_0 n m)
end.

Ltac simplnat := simplprop; repeat(simplnat_step; simplprop).
*)

