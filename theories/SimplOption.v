Require Import Arith.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplEq.
Require Import SimplFun.
Require Import SimplBoolFun.

(* Section 1. Option *)

Ltac simploption1_step :=
  match goal with
  | H : None = Some _ |- _ => inversion H
  | H : Some _ = None |- _ => inversion H
  | H : None   = ?E |- _ => symmetry in H
  | H : Some _ = ?E |- _ => symmetry in H
  | R : ?E = Some _ , H : context[?E] |- _ => rewrite R in H
  | R : ?E = Some _ |-    context[?E]      => rewrite R
  | R : ?E = None   , H : context[?E] |- _ => rewrite R in H
  | R : ?E = None   |-    context[?E]      => rewrite R
  end.

Ltac simploption1 := simplbool; repeat (
  progress (autorewrite with simploption1_rewrite in *) || (try simploption1_step);
  simplbool).

Lemma some_injective {A} (x0 x1:A) :
  Some x0 = Some x1 <-> x0 = x1.
Proof. split; solve_by_invert. Qed.
#[export] Hint Rewrite @some_injective : simploption1_rewrite.

Lemma some_eq_none {A} (x0:A) :
  Some x0 = None <-> False.
Proof. split; solve_by_invert. Qed.
#[export] Hint Rewrite @some_eq_none : simploption1_rewrite.

Lemma none_eq_some {A} (x0:A) :
  None = Some x0 <-> False.
Proof. split; solve_by_invert. Qed.
#[export] Hint Rewrite @none_eq_some : simploption1_rewrite.

Lemma match_option_id A (x:option A) :
  match x with Some y => Some y | None => None end = x.
Proof. dest_match. Qed.
#[export] Hint Rewrite @match_option_id : simploption1_rewrite.

Lemma else_None_eq_None A (c:bool) (th0:option A) :
  (if c then th0 else None) = None <-> (c = true -> th0 = None).
Proof. dest_match_step; simploption1. Qed.
#[export] Hint Rewrite else_None_eq_None : simploption1_rewrite.

Lemma then_Some_eq_None A (c:bool) (x:A) (el0:option A) :
  (if c then (Some x) else el0) = None <->
    (c = false /\ el0 = None).
Proof. dest_match_step; simploption1. Qed.
#[export] Hint Rewrite then_Some_eq_None : simploption1_rewrite.

Definition beq_option {A} (beq:A->A->bool) (o1 o2:option A) : bool :=
  match o1, o2 with
  | Some x1, Some x2 => beq x1 x2
  | None, None => true
  | _, _ => false
  end.

Lemma beq_option_iff_true {A} {beqA:@beqt A} (HA:beq_iff_true beqA) :
  beq_iff_true (beq_option beqA).
Proof with simploption1.
intros x y...
unfold beq_option; dest_match; simploption1.
Qed.
#[export] Hint Resolve beq_option_iff_true : simpleq_iff_true.

Lemma beq_option_iff_true_R {A} {beqA:@beqt A} (HA:beq_iff_true beqA) : beq_iff_true (beq_option beqA) <-> True.
Proof. simplprop. auto with simpleq_iff_true. Qed.
#[export] Hint Rewrite @beq_option_iff_true_R : simpleq_iff_true.

Lemma eqb_iff_true : beq_iff_true eqb.
Proof. intros [|] [|]; simplbool. Qed.
#[export] Hint Resolve eqb_iff_true : simpleq_iff_true.

Lemma eqb_iff_true_R : beq_iff_true eqb <-> True.
Proof. simplprop. auto with simpleq_iff_true. Qed.
#[export] Hint Rewrite @eqb_iff_true_R : simpleq_iff_true.

Lemma beq_option_eqb_iff_true : beq_iff_true (beq_option eqb).
Proof with simploption1.
auto with simpleq_iff_true.
Qed.

Ltac dest_option_step := match goal with
  | o : option _ |- _ => destruct o
end.

Ltac dest_option := simploption1; repeat(dest_option_step; simploption1).

Definition opmap {A B} (f:A->B) (x:option A) : option B :=
match x with
| Some a => Some(f a)
| None   => None
end.

Lemma fold_opmap {A B} f x : match x with Some a => Some (f a) | None => None end = @opmap A B f x.
Proof. reflexivity. Qed.
#[export] Hint Rewrite @fold_opmap : simploption1_rewrite.

Lemma opmap_compose {A B C} (f:A->B) (g:B->C) x :
  opmap g (opmap f x) = opmap (comp f g) x.
Proof. unfold opmap, comp. destruct x; trivial. Qed.
#[export] Hint Rewrite @opmap_compose : simploption1_rewrite.

Definition isSome {A} (x:option A) :=
  match x with Some _ => true | None => false end.

Definition isNone {A} (o:option A) := negb(isSome o).

Lemma isNone_eq_true {A} a : @isNone A a = true <-> a = None.
Proof. destruct a; unfold isNone; simploption1. Qed.
#[export] Hint Rewrite @isNone_eq_true : simploption1_rewrite.
Lemma isSome_eq_false {A} a : @isSome A a = false <-> a = None.
Proof. destruct a; simploption1. Qed.
#[export] Hint Rewrite @isSome_eq_false : simploption1_rewrite.

Lemma match_option_converge {A} (x:option A) {B} (y:B) :
  match x with Some _ | None => y end = y.
Proof. dest_match. Qed.
#[export] Hint Rewrite @match_option_converge : simploption1_rewrite.

Lemma forall_Some_elim A F (f1 f2:A->F) (P:A->Prop) :
  (forall x : A, Some(f1 x) = Some(f2 x) -> P x) <->
  (forall x : A, f1 x = f2 x -> P x).
Proof with simploption1.
split...
autospec...
Qed.
#[export] Hint Rewrite @forall_Some_elim : simploption1_rewrite.

Lemma opmap_id {A} : @opmap A _ id = id.
Proof. apply functional_extensionality; intro x; destruct x; simploption1. Qed.
#[export] Hint Rewrite @opmap_id : simploption1_rewrite.

Lemma else_None_eq_Some A (c:bool) t (r:A) :
  (if c then t else None) = Some r <-> c = true /\ t = Some r.
Proof. dest_match_step; simploption1. Qed.
#[export] Hint Rewrite else_None_eq_Some : simploption1_rewrite.

Lemma then_None_eq_Some A (c:bool) t (r:A) :
  (if c then None else t) = Some r <-> c = false /\ t = Some r.
Proof. dest_match_step; simploption1. Qed.
#[export] Hint Rewrite then_None_eq_Some : simploption1_rewrite.

Lemma isSome_opmap {A B} f o : isSome(@opmap A B f o) = isSome o.
Proof. destruct o; reflexivity. Qed.
#[export] Hint Rewrite @isSome_opmap : simploption1_rewrite.

Lemma if_then_Some_eq_None (b:bool) A (x:A) (opy:option A) :
  (if b then Some x else opy) = None <-> b = false /\ opy = None.
Proof. destruct b; simploption1. Qed.

Lemma if_else_Some_eq_None (b:bool) A (x:A) (opy:option A) :
  (if b then opy else Some x) = None <-> b = true /\ opy = None.
Proof. destruct b; simploption1. Qed.
#[export] Hint Rewrite if_then_Some_eq_None : simploption1_rewrite.
#[export] Hint Rewrite if_else_Some_eq_None : simploption1_rewrite.

Lemma match_option_Some_eq_Some_eq_None {A B} (o:option A) (fSome:A -> B) (fNone:option B) :
  match o with
  | Some a => Some(fSome a)
  | None => fNone
  end = None <-> o = None /\ fNone = None.
Proof. dest_match_step; simploption1. Qed.
#[export] Hint Rewrite @match_option_Some_eq_Some_eq_None : simploption1_rewrite.

Definition unop {A} (opa:option A) (H:opa <> None) : A :=
  match opa as o return (o <> None -> A) with
  | Some a => fun _ => a
  | None => fun H => match proj1 (neq_False None) H with end
  end H.

Lemma unop_rewrite {A} x H y : @unop A x H = y <-> x = Some y.
Proof. destruct x; simploption1. Qed.
