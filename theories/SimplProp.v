Require Import Arith.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.ProofIrrelevance.

Infix "=J" := JMeq (at level 70, no associativity).

Definition JMneq {A B} (x:A) (y:B) := x =J y -> False.

Infix "=J!" := JMneq (at level 70, no associativity).

Lemma apply_f {A B} (f: A -> B) {x y} : x = y -> f x = f y.
Proof. intro. subst. reflexivity. Qed.

Lemma apply_intro {A B} (f1 f2:A->B) (x1 x2:A) :
  f1 = f2 -> x1 = x2 -> f1 x1 = f2 x2.
Proof. intros. subst. reflexivity. Qed.

Ltac inv H := inversion H; subst; clear H.

Lemma rewrite_P (P:Prop) : P -> (P <-> True).
Proof. tauto. Qed.

Lemma rewrite_not_P (P:Prop) : (P -> False) -> (P <-> False).
Proof. tauto. Qed.

Lemma rewrite_neq {A} (x y:A) : x <> y -> (x = y <-> False).
Proof. tauto. Qed.

Lemma and_diag P : (P /\ P) <-> P.
Proof. tauto. Qed.

Lemma or_diag P : (P \/ P) <-> P.
Proof. tauto. Qed.

Lemma iff_refl P : (P <-> P) <-> True.
Proof. tauto. Qed.

Lemma and_False_l P : (False /\ P) <-> False.
Proof. tauto. Qed.

Lemma and_False_r P : (P /\ False) <-> False.
Proof. tauto. Qed.

Lemma and_True_l P : (True /\ P) <-> P.
Proof. tauto. Qed.

Lemma and_True_r P : (P /\ True) <-> P.
Proof. tauto. Qed.

Lemma or_False_l P : (False \/ P) <-> P.
Proof. tauto. Qed.

Lemma or_False_r P : (P \/ False) <-> P.
Proof. tauto. Qed.

Lemma or_True_l P : (True \/ P) <-> True.
Proof. tauto. Qed.

Lemma or_True_r P : (P \/ True) <-> True.
Proof. tauto. Qed.

Lemma eq_True {A} (x:A) : x = x <-> True.
Proof. tauto. Qed.

Lemma neq_False {A} (x:A) : x <> x <-> False.
Proof. tauto. Qed.

Lemma iff_True_l P : (True <-> P) <-> P.
Proof. tauto. Qed.

Lemma iff_True_r P : (P <-> True) <-> P.
Proof. tauto. Qed.

Lemma False_imp (P:Prop) : (False -> P) <-> True.
Proof. firstorder. Qed.

Lemma imp_True (P:Prop) : (P -> True) <-> True.
Proof. firstorder. Qed.

Lemma True_imp (P:Prop) : (True -> P) <-> P.
Proof. firstorder. Qed.

Lemma JMeq_intro {A} {x y:A} : x = y -> x =J y.
Proof.
intros. subst. apply JMeq_refl.
Qed.

Lemma forall_swap A B (P : A -> B -> Prop) :
  (forall a b, P a b) <-> (forall b a, P a b).
Proof. split; auto. Qed.

Lemma JMeq_refl_rewrite {A} (a:A) : a =J a <-> True.
Proof. tauto. Qed.

Definition id {A} := (fun x:A => x).

Lemma id_unfold {A} (x:A) : id x = x.
Proof. reflexivity. Qed.

Lemma not_True : ~ True <-> False.
Proof. split; auto. Qed.

Lemma not_False : ~ False <-> True.
Proof. split; auto. Qed.

Lemma forall_comm A (P1:Prop) (P2:A->Prop) :
  (forall x : A, P1 -> P2 x) <-> (P1 -> forall x, P2 x).
Proof. firstorder. Qed.

Lemma forall_elim_eq_refl_l A (x0:A) (P:A->Prop) :
  (forall x : A, x = x0 -> P x) <-> P x0.
Proof. firstorder. subst. trivial. Qed.

Lemma forall_elim_eq_refl_r A (x0:A) (P:A->Prop) :
  (forall x : A, x0 = x -> P x) <-> P x0.
Proof. firstorder. subst. trivial. Qed.

Ltac ltac_forall_comm_H H E := match E with
  | ?A -> ?B => ltac_forall_comm_H H A || ltac_forall_comm_H B || fail 1
  | forall x : ?A, ?P1 -> @?P2 x =>
      rewrite (forall_comm A P1 P2) in H
end.

Ltac ltac_forall_comm_goal E := match E with
  | ?A -> ?B => ltac_forall_comm_goal A || ltac_forall_comm_goal B || fail 1
  | forall x : ?A, ?P1 -> @?P2 x =>
      rewrite (forall_comm A P1 P2)
end.

Ltac simplprop_step := match goal with
  | H : context [existT] |- _ => simpl_existTs
  | H : eq_refl =J ?e |- _ =>
    repeat (match goal with
      | X : e =J _|- _ => clear X
      | X : _ =J e|- _ => clear X
      | X : e = _ |- _ => clear X
      | X : _ = e |- _ => clear X
    end); destruct e; idtac "destruct " e " in " H
  | H : ?t |- _ =>
    match t with
      | context [ @eq_rect _ _ _ _ _ ?p ] =>
        match goal with
        | H : eq_refl =J p |- _ => fail 1
        | _ => destruct p
        | _ =>
          let X := fresh "e" in remember p as X in H;
          dependent destruction X; simpl in H; auto
      end
      | context [ @eq_rect_r _ _ _ _ _ ?p ] =>
        unfold eq_rect_r in H; simpl
      | context [ @eq_rect _ _ _ _ _ ?p _ ] =>
        let X := fresh "e" in remember p as X in H;
          dependent destruction X; simpl in H; auto
      | context [@eq_rec_r _ _ _ _ _ _ ] => unfold eq_rec_r in H
      | context [@eq_rec _ _ _ ] => unfold eq_rec in H
      | context [solution_left _ _ _ _ ] => unfold solution_left in H
      | context [solution_right _ _ _ _ ] => unfold solution_right in H
      | context [simplification_heq] => unfold simplification_heq in H
      | context [simplification_K _ _ _ _] => unfold simplification_K in H
    end
  | [ |- ?t ] =>
    match t with
      | context [ @eq_rect _ _ _ _ _ ?p ] =>
        match goal with
        | H : eq_refl =J p |- _ => fail 1
        | _ => destruct p
        | _ =>
          let X := fresh "e" in remember p as X;
          dependent destruction X; simpl; auto
      end
      | context [ @eq_rect_r _ _ _ _ _ ?p ] =>
        unfold eq_rect_r; simpl
      | context [ @eq_rect _ _ _ _ _ ?p _ ] =>
        let X := fresh "e" in remember p as X;
          dependent destruction X; simpl; auto
      | context [@eq_rec_r _ _ _ _ _ _ ] => unfold eq_rec_r
      | context [@eq_rec _ _ _ ] => unfold eq_rec
      | context [solution_left _ _ _ _ ] => unfold solution_left
      | context [solution_right _ _ _ _ ] => unfold solution_right
      | context [simplification_heq] => unfold simplification_heq
      | context [simplification_K _ _ _ _] => unfold simplification_K
    end
  | H : False |- _ => inversion H
  | H : context [~ True] |- _ => rewrite not_True in H
  | |-  context [~ True]      => rewrite not_True
  | H : context [~ False] |- _ => rewrite not_False in H
  | |-  context [~ False]      => rewrite not_False
  | H : False -> _ |- _ =>
    let T := type of H in idtac "clear " H " : " T; clear H
  | H : True |- _ => clear H
  | H : ~ False |- _ => clear H
  | H : context [ @eq_rec_r ] |- _ => unfold eq_rec_r in H
  | H : context [ @eq_rec ] |- _ => inv H
  | H : context [ @eq_rect_r ] |- _ => unfold eq_rect_r in H
  | H : context [ @eq_rect ] |- _ => inv H
  | H : ?P /\ ?Q |- _ => destruct H
  | H : exists x, ?P |- _ => destruct H as [x H]
  | H : ?X =J ?Y |- _ => apply JMeq_eq in H
  | H : ?X =J ?X |- _ => clear H
  | H1 : ?A -> ?B, H2 : ?A |- _ => specialize (H1 H2)
  | |- ?X =J ?X => apply JMeq_refl
  | |- ?X =J ?Y => apply JMeq_intro
  | H : ?A = ?A |- _ => clear H
  | H : context [?A = ?A] |- _ => rewrite eq_True
  | H1 : ?x <> ?y, H2 : ?x = ?y |- _ =>
    apply H2 in H1; inversion H1
  | H : ?x <> ?y |- ?x = ?y => exfalso
  | H : ?x = ?y |- ?x <> ?y => exfalso
  | H : ?x <> ?x |- _ => exfalso; apply H; reflexivity
  | H : context [False /\ ?P] |- _ => rewrite and_False_l in H
  | |- context [False /\ ?P] => rewrite and_False_l
  | H : context [True /\ ?P] |- _ => rewrite and_True_l in H
  | |- context [True /\ ?P] => rewrite and_True_l
  | H : context [False \/ ?P] |- _ => rewrite or_False_l in H
  | |- context [False \/ ?P] => rewrite or_False_l
  | H : context [True \/ ?P] |- _ => rewrite or_True_l in H
  | |- context [True \/ ?P] => rewrite or_True_l
  | H : context [?P /\ False] |- _ => rewrite and_False_r in H
  | |- context [?P /\ False] => rewrite and_False_r
  | H : context [?P /\ True] |- _ => rewrite and_True_r in H
  | |- context [?P /\ True] => rewrite and_True_r
  | H : context [?P \/ False] |- _ => rewrite or_False_r in H
  | |- context [?P \/ False] => rewrite or_False_r
  | H : context [?P \/ True] |- _ => rewrite or_True_r in H
  | |- context [?P \/ True] => rewrite or_True_r
  | H : context [?x = ?x] |- _ => rewrite (eq_True x) in H
  | |- context [?x = ?x] => rewrite (eq_True x)
  | H : context [?x <> ?x] |- _ => rewrite (neq_False x) in H
  | |- context [?x <> ?x] => rewrite (neq_False x)
  | H1 : ?P , H2: ?P |- _=> clear H2
  | H : context [True <-> ?P] |- _ => rewrite iff_True_l in H
  | |- context [True <-> ?P] => rewrite iff_True_l
  | H : context [?P <-> True] |- _ => rewrite iff_True_r in H
  | |- context [?P <-> True] => rewrite iff_True_r
  | H : context[False -> ?P] |- _ => rewrite (False_imp P) in H
  | |- context[False -> ?P] => rewrite (False_imp P)
  | H : context[?P -> True] |- _ => rewrite (imp_True P) in H
  | |- context[?P -> True] => rewrite (imp_True P)
  | H : context [True -> ?P] |- _ => rewrite True_imp in H
  | |- context [True -> ?P] => rewrite True_imp
  | H : ?P -> True |- _ => clear H
  | |- ?P -> True => intro H; trivial
  | H : context [?P /\ ?P] |- _ => rewrite and_diag in H
  | |- context [?P /\ ?P] => rewrite and_diag
  | H : context [?P \/ ?P] |- _ => rewrite or_diag in H
  | |- context [?P \/ ?P] => rewrite or_diag
  | H : context [?P <-> ?P] |- _ => rewrite iff_refl in H
  | |- context [?P <-> ?P] => rewrite iff_refl
  | H : context [?A =J ?A] |- _ => rewrite (JMeq_refl_rewrite A) in H
  | |- context [?A =J ?A] => rewrite (JMeq_refl_rewrite A)
  | |- (?A <> ?B) => intro
(* experiment on rewrite_P *)
| HH : ?P |- _ => match type of P with
  | Prop => match goal with
    | H : context[?P /\ _] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[_ /\ ?P] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[?P \/ _] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[_ \/ ?P] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[?P <-> _] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[_ <-> ?P] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[?P -> _] |- _ => rewrite (rewrite_P P HH) in H
    | H : context[_ -> ?P] |- _ => rewrite (rewrite_P P HH) in H
    | |- context[?P /\ _] => rewrite (rewrite_P P HH)
    | |- context[_ /\ ?P] => rewrite (rewrite_P P HH)
    | |- context[?P \/ _] => rewrite (rewrite_P P HH)
    | |- context[_ \/ ?P] => rewrite (rewrite_P P HH)
    | |- context[?P <-> _] => rewrite (rewrite_P P HH)
    | |- context[_ <-> ?P] => rewrite (rewrite_P P HH)
    | |- context[?P -> _] => rewrite (rewrite_P P HH)
    | |- context[_ -> ?P] => rewrite (rewrite_P P HH)

    end
  end
(* experiment on rewrite_P *)
  | H1 : ?x <> ?y, H2 : context [?x = ?y] |- _ =>
    rewrite (rewrite_neq x y H1) in H2
  | H1 : ?x <> ?y |- context [?x = ?y] =>
    rewrite (rewrite_neq x y H1)
  | t : unit |- _ => destruct t
  | H : ?A =J ?B |- ?A = ?B => apply JMeq_eq, H
  | |- ?A <-> True => apply rewrite_P
  | |- ?A <-> False => apply rewrite_not_P
  | |- True <-> ?A => symmetry; apply rewrite_P
  | |- False <-> ?A => symmetry; apply rewrite_not_P
  | H : context [id ?x] |- _ => rewrite (id_unfold x) in H
  | |- context [id ?x] => rewrite (id_unfold x)
(* experiment on auto specialize eq_refl *)
| H : context [forall x : ?A, ?P1 -> @?P2 x] |- _ =>
  match type of H with
  | ?E => ltac_forall_comm_H H E
  end
| |- context [forall x : ?A, ?P1 -> @?P2 x] =>
  match goal with
  | |- ?E => ltac_forall_comm_goal E
  end
| H : context[forall x : ?A, x = ?x0 -> @?P x] |- _ =>
  idtac "specialized"H"using (eq_refl"x0")";
  rewrite (forall_elim_eq_refl_l A x0 P) in H
| H : context[forall x : ?A, ?x0 = x -> @?P x] |- _ =>
  idtac "specialized"H"using (eq_refl"x0")";
  rewrite (forall_elim_eq_refl_r A x0 P) in H
| |- context[forall x : ?A, x = ?x0 -> @?P x] =>
  idtac "specialized goal using (eq_refl"x0")";
  rewrite (forall_elim_eq_refl_l A x0 P)
| |- context[forall x : ?A, ?x0 = x -> @?P x] =>
  idtac "specialized goal using (eq_refl"x0")";
  rewrite (forall_elim_eq_refl_r A x0 P)
(* pair-related lemmas *)
| H : context[pair ?a1 ?b1 = pair ?a2 ?b2] |- _ => rewrite (pair_equal_spec a1 a2 b1 b2) in H
| |- context[pair ?a1 ?b1 = pair ?a2 ?b2] => rewrite (pair_equal_spec a1 a2 b1 b2)
| H : context[match ?x with pair _ _ => _ end] |- _ => (let DP:=fresh "DP" in destruct x eqn:DP)
| |- context[match ?x with pair _ _ => _ end] => (let DP:=fresh "DP" in destruct x eqn:DP)
end.

Ltac simplprop :=
  simpl in *; try intros; subst; simpl; auto; try reflexivity; simpl_existTs; subst;
  repeat (
    (simplprop_step || (progress (autorewrite with simplprop1_rewrite in *)));
    subst; simpl; simpl_existTs; auto;
    try reflexivity;
    subst;
    try intros);
  simpl; auto.

Ltac autospec_gen mytac := repeat
match goal with
  | X : ?A, H : forall (Y:?A), _ |- _ => specialize (H X); mytac
end.

Ltac autospec := simplprop; autospec_gen simplprop.

Ltac gen_solve_by_inverts tac n := tac;
  match goal with | H : ?T |- _ =>
  match type of T with Prop =>
    solve [
      inversion H; tac;
      match n with S (S (?n')) => gen_solve_by_inverts tac (S n') end ]
  end end.

Ltac solve_by_inverts n := simplprop;
  match goal with | H : ?T |- _ =>
  match type of T with Prop =>
    solve [
      inversion H; simplprop;
      match n with S (S (?n')) => solve_by_inverts (S n') end ]
  end end.

Ltac solve_by_invert :=
  solve_by_inverts 1.

(* In nested matches, get the argument of the inner most match *)
(* [from coq-club@inria.fr by michael.soegtrop@intel.com *)

Ltac get_inner_match term :=
  match term with
  | context [match ?inner with _ => _ end] => get_inner_match inner
  | _ => term
  end.

(**  Destruct the argument of a match in the goal or hypothesis *)
(* [from coq-club@inria.fr by michael.soegtrop@intel.com *)

Ltac dest_match_step :=
  match goal with
  | |- context [match ?term with _ => _ end] =>
    let EQ:=fresh "D" in
    let inner:=get_inner_match term in
    destruct inner eqn:EQ
  | H: context [match ?term with _ => _ end] |- _ =>
    let EQ:=fresh "D" in
    let inner:=get_inner_match term in
    destruct inner eqn:EQ
  | H : _ \/ _ |- _ => destruct H
  end.

(* also work for if *)

(* (* [V1] *)
Ltac gen_dest_match tac :=
  tac; repeat(dest_match_step; tac; try gen_solve_by_inverts tac 1); subst.
 *)

   (* [V2] *)
Ltac gen_dest_match tac :=
  tac; repeat(dest_match_step; tac); subst.

Ltac dest_match :=
  simplprop; repeat(dest_match_step; simplprop; try solve_by_invert); subst.

Ltac dest_if_step := match goal with
  | [ |- ?t ] => match t with
    | context [ if ?E then _ else _ ] =>
      let D:= fresh "D" in destruct E eqn:D; simpl in D
    end
  | H : context [ if ?E then _ else _ ] |- _ =>
      let D:= fresh "D" in destruct E eqn:D; simpl in D
  end.

Ltac gen_dest_if tac :=
  tac;
  repeat (
    dest_if_step;
    tac;
    try gen_solve_by_inverts tac 1);
  subst.

Ltac dest_if :=
  simplprop;
  repeat (
    dest_if_step;
    simplprop;
    try solve_by_inverts 1);
  subst.

(* Section 4. Extra *)

Ltac rewrite_subst_step := match goal with
  | S : ?A = ?A |- _ => clear S
  | S : ?A = ?B, H : ?C = ?A |- _ => rewrite S in H
  | S : ?A = ?B, H : context [?A] |- _ => rewrite S in H
  | S : ?A = ?B |- context [?A] => rewrite S
end.

Ltac rewrite_subst := simplprop; repeat(rewrite_subst_step; simplprop).

(* $2019-04-18 *)
Lemma forall_unit {P:unit -> Prop} : (forall x, P x) <-> P tt.
Proof with simplprop.
split...
Qed.

Lemma forall_iff_eq_same_l (A:Type) (y1 y2:A) :
  (forall x, x = y1 <-> x = y2) <-> (y1 = y2).
Proof with simplprop.
split...
specialize(H y1)...
Qed.
#[export] Hint Rewrite forall_iff_eq_same_l : simplprop1_rewrite.

Require Import Coq.Logic.FunctionalExtensionality.

Lemma fold_id_unit : (fun _ => tt) = id.
Proof with simplprop.
apply functional_extensionality...
Qed.
#[export] Hint Rewrite fold_id_unit : simplprop1_rewrite.

Lemma rewrite_fun_unit A (f:unit -> A) : f = (fun _ => f tt).
Proof with simplprop.
apply functional_extensionality...
Qed.

Ltac rewrite_fun_unit_step := match goal with
  | H : context[fun (x:unit) => @?f x] |- _ =>
    match type of f with
    | _ -> ?A => match A with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of A with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_fun_unit _ f) in H; simpl in H);
    idtac "normalized :" f
  | |- context[fun (x:unit) => @?f x] =>
    match type of f with
    | _ -> ?A => match A with
      | Type => fail 2
      | Set  => fail 2
      | Prop => fail 2
      | _ => match type of A with
        | Type => idtac
        | Set  => idtac
        | ?TA  => fail 3
        end
      end
    | _ => fail 1
    end;
    progress (rewrite (rewrite_fun_unit _ f); simpl);
    idtac "normalized :" f
  end.

Ltac rewrite_fun_unit := repeat rewrite_fun_unit_step.

Ltac dtt_dest_match_step :=
match goal with
| |- context [match ?term with _ => _ end] =>
  let inner:=get_inner_match term in
  let RE:=fresh "RE" in
  let RX:=fresh "RX" in
  remember inner as RX eqn:RE;
  dependent destruction RX
| H: context [match ?term with _ => _ end] |- _ =>
  let inner:=get_inner_match term in
  let DD:=fresh "DD" in
  let DX:=fresh "DX" in
  remember inner as RX eqn:RE;
  dependent destruction RX
end.

(* Extra Rewritting Rules *)
Lemma P_and_iff_and_P (P Q1 Q2:Prop) : (P /\ Q1 <-> Q2 /\ P) <-> (P -> (Q1 <-> Q2)).
Proof. firstorder. Qed.
#[export] Hint Rewrite P_and_iff_and_P : simplprop1_rewrite.

Lemma and_P_iff_and_P (P Q1 Q2:Prop) : (Q1 /\ P <-> Q2 /\ P) <-> (P -> (Q1 <-> Q2)).
Proof. firstorder. Qed.
#[export] Hint Rewrite and_P_iff_and_P : simplprop1_rewrite.

Lemma P_and_iff_P_and (P Q1 Q2:Prop) : (P /\ Q1 <-> P /\ Q2) <-> (P -> (Q1 <-> Q2)).
Proof. firstorder. Qed.
#[export] Hint Rewrite P_and_iff_P_and : simplprop1_rewrite.

Lemma and_P_iff_P_and (P Q1 Q2:Prop) : (Q1 /\ P <-> P /\ Q2) <-> (P -> (Q1 <-> Q2)).
Proof. firstorder. Qed.
#[export] Hint Rewrite and_P_iff_P_and : simplprop1_rewrite.

Lemma eq_sym_iff {A} (x y:A) : (x = y) <-> (y = x).
Proof. firstorder. Qed.

Lemma eq_sym_iff_True A (x y:A) : (x = y <-> y = x) <-> True.
Proof. firstorder. Qed.
#[export] Hint Rewrite eq_sym_iff_True : simplprop1_rewrite.

Lemma P_iff_P_and_Q (P Q:Prop) : (P <-> P /\ Q) <-> (P -> Q). Proof. firstorder. Qed.
Lemma P_iff_Q_and_P (P Q:Prop) : (P <-> Q /\ P) <-> (P -> Q). Proof. firstorder. Qed.
#[export] Hint Rewrite P_iff_P_and_Q : simplprop1_rewrite.
#[export] Hint Rewrite P_iff_Q_and_P : simplprop1_rewrite.

Lemma P_and_Q_iff_P (P Q:Prop) : (P /\ Q <-> P) <-> (P -> Q). Proof. firstorder. Qed.
Lemma Q_and_P_iff_P (P Q:Prop) : (Q /\ P <-> P) <-> (P -> Q). Proof. firstorder. Qed.
#[export] Hint Rewrite P_and_Q_iff_P : simplprop1_rewrite.
#[export] Hint Rewrite Q_and_P_iff_P : simplprop1_rewrite.

Lemma neq_sym {A:Type} (x y:A) : x <> y <-> y <> x.
Proof. firstorder. Qed.

(* Dependent Type Theory - Stuff *)

Lemma exist_eq_l A P (x:A) y v : exist P x y = v <-> x = proj1_sig v.
Proof with simplprop.
split...
destruct v...
specialize (proof_irrelevance _ p y)...
Qed.
#[export] Hint Rewrite exist_eq_l : simplprop1_rewrite.

Lemma exist_eq_r A P (x:A) y v : v = exist P x y <-> proj1_sig v = x.
Proof with simplprop.
split...
symmetry...
Qed.
#[export] Hint Rewrite exist_eq_r : simplprop1_rewrite.

Example exist_neq_l A P (x:A) y v : exist P x y <> v <-> x <> proj1_sig v.
Proof. simplprop. Qed.

Example exist_neq_r A P (x:A) y v : v <> exist P x y <-> proj1_sig v <> x.
Proof. simplprop. Qed.

Lemma exist_injective (A:Type) (P:A -> Prop) (k1 k2:A) p1 p2 :
  exist P k1 p1 = exist P k2 p2 <-> k1 = k2.
Proof. simplprop. Qed.

Lemma sig_PI {A} (P:A -> Prop) (x y:sig P) : x = y <-> proj1_sig x = proj1_sig y.
Proof. destruct x, y; simplprop. Qed.

Lemma proj1_sig_injective {A} (P:A -> Prop) (x y:sig P) : proj1_sig x = proj1_sig y <-> x= y.
Proof. destruct x, y; simplprop. Qed.

Lemma match_False_iff_False (x:False) (CTX:False -> Prop) : CTX(match x with end) <-> False.
Proof. simplprop. Qed.
#[export] Hint Rewrite match_False_iff_False : simplprop1_rewrite.

Ltac simpldtt1_step :=
match goal with
| H : context[exist ?P ?k1 ?p1 = exist ?P ?k2 ?p2] |- _ =>
  rewrite (exist_injective _ P k1 k2 p1 p2) in H
| |- context[exist ?P ?k1 ?p1 = exist ?P ?k2 ?p2] =>
  rewrite (exist_injective _ P k1 k2 p1 p2)
end.

Ltac assert_hypothesis H :=
  match type of H with
  | ?HH -> ?HG => (
    let E := fresh "HH" in
    assert(E : HH); [ | specialize(H E) ]
  )
  | (forall x : ?HH, @?HG x) => (
    match type of HH with
    | Prop => (
      let E := fresh "HH" in
      assert(E : HH); [ | specialize(H E) ]
    )
    | _ => idtac "[assert_hypothesis] implicant is not Prop"; fail 2
    end
  )
  | _ => idtac "[assert_hypothesis] not an implication"; fail 1
  end.
