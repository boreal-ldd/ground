Require Import Arith.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.

Ltac simplsimpl1 := simploption1; repeat (progress (autorewrite with simplsimpl1_rewrite in *); simploption1).

(* Section 2. Pair *)

Definition beq_pair {A B} (eqA:@beqt A) (eqB:@beqt B) (z1 z2:A*B) : bool :=
match z1, z2 with
| (x1, y1), (x2, y2) => eqA x1 x2 && eqB y1 y2
end.

Lemma beq_pair_iff_true {A B}
  {eqA:@beqt A} (HA:beq_iff_true eqA)
  {eqB:@beqt B} (HB:beq_iff_true eqB) : beq_iff_true (beq_pair eqA eqB).
Proof with simplsimpl1.
intros x1 x2.
unfold beq_pair; dest_match...
rewrite andb_true_iff...
rewrite (HA _ _), (HB _ _)...
Qed.
#[export] Hint Resolve beq_pair_iff_true : simpleq_iff_true.

Lemma beq_pair_iff_true_R {A B}
  {eqA:@beqt A} (HA:beq_iff_true eqA)
  {eqB:@beqt B} (HB:beq_iff_true eqB) : beq_iff_true (beq_pair eqA eqB) <-> True.
Proof. simplprop; auto with simpleq_iff_true. Qed.
#[export] Hint Rewrite @beq_pair_iff_true_R : simpleq_iff_true.

Lemma forall_pair_elim2 A B F G (f1 f2:A->B->F) (g1 g2:A->B->G) (P:A->B->Prop) :
  (forall (x:A) (y:B), (f1 x y, g1 x y) = (f2 x y, g2 x y) -> P x y) <->
  (forall (x:A) (y:B), f1 x y = f2 x y -> g1 x y = g2 x y -> P x y).
Proof with simplsimpl1.
split; intros; specialize(H x y)...
Qed.
#[export] Hint Rewrite forall_pair_elim2 : simplsimpl1_rewrite.

(* [MOVEME] *)
Lemma isSome_Some_true (B:Type) (a:option bool) (b:option B) :
  isSome
     match a with
     | Some true => b
     | _ => None
     end = true <-> a = Some true /\ isSome b = true.
Proof with simplsimpl1.
split...
dest_match_step...
dest_match_step...
Qed.
#[export] Hint Rewrite isSome_Some_true : simplsimpl1_rewrite.

Ltac simplsimpl2_ltac :=
  match goal with
  | H : None = Some _ |- _ => inversion H
  | H : Some _ = None |- _ => inversion H
  | H : None   = ?E |- _ => symmetry in H
  | H : Some _ = ?E |- _ => symmetry in H
  | R : ?E = Some _ , H : context[?E] |- _ => rewrite R in H
  | R : ?E = Some _ |-    context[?E]      => rewrite R
  | R : ?E = None   , H : context[?E] |- _ => rewrite R in H
  | R : ?E = None   |-    context[?E]      => rewrite R
  end.

Ltac simplsimpl2 := simplsimpl1; repeat(simplsimpl2_ltac; simplsimpl1).

Ltac simplsimpl := simplsimpl2.

(* Section 3. Quantifier *)

Definition qinv (A:Type) : Prop := forall (p:A -> bool), (forall a, p a = true) \/ (exists a, p a = false).

Lemma qinv'_of_qinv {A:Type} (Q:qinv A) (p:A -> bool) (b:bool) : (forall a, p a = b) \/ (exists a, p a = negb b).
Proof with simplsimpl1.
destruct b...
specialize(Q (fun a => negb(p a)))...
rewrite forall_neg_eq, exist_neg_eq in Q...
Qed.

Lemma exists_as_neg_forall_with_qinv {A} (Q:qinv A) (p:A -> bool) b :
  (exists a, p a = b) <-> ~(forall a, p a = negb b).
Proof with simplsimpl1.
split...
- intro H...
  specialize(H a)...
- destruct(qinv'_of_qinv Q p (negb b))...
  + apply H in H0...
  + exists a...
Qed.

Lemma neg_exists_as_forall {A} (p:A -> bool) b :
  ~(exists a, p a = b) <-> (forall a, p a = negb b).
Proof with simplsimpl1.
split...
- destruct(eqb(p a)(negb b))eqn:E...
  apply H...
  exists a...
- intros [a Ha]...
  specialize(H a)...
Qed.

Lemma neg_forall_as_exists_with_qinv {A} (Q:qinv A) (p:A -> bool) b :
  ~(forall a, p a = b) <-> (exists a, p a = negb b).
Proof with simplsimpl1.
rewrite exists_as_neg_forall_with_qinv...
Qed.

Lemma neg_neg_forall_with_qinv {A} (Q:qinv A) (p:A -> bool) b :
  ~ ~ (forall a, p a = b) <-> (forall a, p a = b).
Proof with simplsimpl1.
rewrite neg_forall_as_exists_with_qinv...
rewrite neg_exists_as_forall...
Qed.

(*

Ltac forall_pair_elim2 := match goal with
(* forall_pair_elim2 *)
| H : context[forall (x: ?A) (y: ?B), (@?f1 x y, @?g1 x y) = (@?f2 x y, @?g2 x y) -> @?P x y] |- _ =>
  idtac H f1 f2 g1 g2 P;
  rewrite (forall_pair_elim2 A B _ _ f1 f2 g1 g2 P) in H
| |- context[forall (x: ?A) (y: ?B), (@?f1 x y, @?g1 x y) = (@?f2 x y, @?g2 x y) -> @?P x y] =>
  rewrite (forall_pair_elim2 A B _ _ f1 f2 g1 g2 P)
end.

Ltac flop H E := match E with
| forall (x:?A), ?E => flop H E
| context[(?x1, ?y1) = (?x2, ?y2)] => idtac H
end.

(* Section Extra *)

Ltac simplsimpl_step1 := match goal with
(* @length_ntimes *)
| H : context[@length ?A (ntimes ?n ?a ?l )] |- _ =>
  rewrite(@length_ntimes n A a l) in H
| |- context[@length ?A (ntimes ?n ?a ?l )] =>
  rewrite(@length_ntimes n A a l)

(* @length_make *)
| H : context[@length ?A (make ?n ?a )] |- _ =>
  rewrite(@length_make n A a) in H
| |- context[@length ?A (make ?n ?a )] =>
  rewrite(@length_make n A a)

(* @nth_error_ntimes_same *)
| H : context[@List.nth_error ?A (ntimes ?n ?a ?l ) ?n ] |- _ =>
  rewrite(@nth_error_ntimes_same A l n a) in H
| |- context[@List.nth_error ?A (ntimes ?n ?a ?l ) ?n ] =>
  rewrite(@nth_error_ntimes_same A l n a)

(* @nth_ntimes *)
| H : context[@List.nth ?A ?n (ntimes ?n ?a ?l ) ?a0 ] |- _ =>
  rewrite(@nth_ntimes A l n a a0) in H
| |- context[@List.nth ?A ?n (ntimes ?n ?a ?l ) ?a0 ] =>
  rewrite(@nth_ntimes A l n a a0)

(* @opmap_compose *)
| H : context[@opmap ?B ?C ?g (@opmap ?A ?B ?f ?x )] |- _ =>
  rewrite(@opmap_compose A B C f g x) in H
| |- context[@opmap ?B ?C ?g (@opmap ?A ?B ?f ?x )] =>
  rewrite(@opmap_compose A B C f g x)

(* @cons_injective *)
| H : context[@cons ?A ?x0 ?l0 = cons ?x1 ?l1 ] |- _ =>
  rewrite(@cons_injective A x0 x1 l0 l1) in H
| |- context[@cons ?A ?x0 ?l0 = cons ?x1 ?l1 ] =>
  rewrite(@cons_injective A x0 x1 l0 l1)

(* @cons_eq_nil *)
| H : context[@cons ?A ?x ?l = nil] |- _ =>
  rewrite(@cons_eq_nil A x l) in H
| |- context[@cons ?A ?x ?l = nil] =>
  rewrite(@cons_eq_nil A x l)

(* @nil_eq_cons *)
| H : context[nil = @cons ?A ?x ?l ] |- _ =>
  rewrite(@nil_eq_cons A x l) in H
| |- context[nil = @cons ?A ?x ?l ] =>
  rewrite(@nil_eq_cons A x l)

(* @some_injective *)
| H : context[@Some ?A ?x0 = Some ?x1 ] |- _ =>
  rewrite(@some_injective A x0 x1) in H
| |- context[@Some ?A ?x0 = Some ?x1 ] =>
  rewrite(@some_injective A x0 x1)

(* @some_eq_none *)
| H : context[@Some ?A ?x0 = None] |- _ =>
  rewrite(@some_eq_none A x0) in H
| |- context[@Some ?A ?x0 = None] =>
  rewrite(@some_eq_none A x0)

(* @none_eq_some *)
| H : context[None = @Some ?A ?x0 ] |- _ =>
  rewrite(@none_eq_some A x0) in H
| |- context[None = @Some ?A ?x0 ] =>
  rewrite(@none_eq_some A x0)

(* @rewrite_pair *)
| H : context[@pair ?A ?B ?x1 ?x2 = pair ?y1 ?y2 ] |- _ =>
  rewrite(@rewrite_pair A B x1 y1 x2 y2) in H
| |- context[@pair ?A ?B ?x1 ?x2 = pair ?y1 ?y2 ] =>
  rewrite(@rewrite_pair A B x1 y1 x2 y2)

(* @mapAB_id_id *)
| H : context[@mapAB ?A ?A ?B ?B id id] |- _ =>
  rewrite(@mapAB_id_id A B) in H
| |- context[@mapAB ?A ?A ?B ?B id id] =>
  rewrite(@mapAB_id_id A B)

(* @AA_inj *)
| H : context[@AA ?A ?B ?x = AA ?y ] |- _ =>
  rewrite(@AA_inj A B x y) in H
| |- context[@AA ?A ?B ?x = AA ?y ] =>
  rewrite(@AA_inj A B x y)

(* @BB_inj *)
| H : context[@BB ?A ?B ?x = BB ?y ] |- _ =>
  rewrite(@BB_inj A B x y) in H
| |- context[@BB ?A ?B ?x = BB ?y ] =>
  rewrite(@BB_inj A B x y)

(* @AA_BB *)
| H : context[@AA ?A ?B ?x = BB ?y ] |- _ =>
  rewrite(@AA_BB A B x y) in H
| |- context[@AA ?A ?B ?x = BB ?y ] =>
  rewrite(@AA_BB A B x y)

(* @BB_AA *)
| H : context[@BB ?A ?B ?x = AA ?y ] |- _ =>
  rewrite(@BB_AA A B x y) in H
| |- context[@BB ?A ?B ?x = AA ?y ] =>
  rewrite(@BB_AA A B x y)

(* @make_S *)
| H : context[@make (S ?n ) ?A ?x ] |- _ =>
  rewrite(@make_S A n x) in H
| |- context[@make (S ?n ) ?A ?x ] =>
  rewrite(@make_S A n x)

(* @make_0 *)
| H : context[@make 0 ?A ?x ] |- _ =>
  rewrite(@make_0 A x) in H
| |- context[@make 0 ?A ?x ] =>
  rewrite(@make_0 A x)

(* nat_match_id_v2 *)
| H : context[match ?i with 0 => 0 | S _ => ?i end] |- _ =>
  rewrite(nat_match_id_v2 i) in H
| |- context[match ?i with 0 => 0 | S _ => ?i end] =>
  rewrite(nat_match_id_v2 i)

(* nat_match_converge *)
| H : context[match ?i with 0 => ?x | S _ => ?x end] |- _ =>
  rewrite(nat_match_converge i x) in H
| |- context[match ?i with 0 => ?x | S _ => ?x end] =>
  rewrite(nat_match_converge i x)

(* nat_match_converge (Hand Crafted) *)
| H : context[match ?i with 0 => ?x | S _ => ?x end] |- _ =>
  rewrite(nat_match_converge i) in H
| |- context[match ?i with 0 => ?x | S _ => ?x end] =>
  rewrite(nat_match_converge i)

(* Nat.min_0_r *)
| H : context[Nat.min ?n 0] |- _ =>
  rewrite(Nat.min_0_r n) in H
| |- context[Nat.min ?n 0] =>
  rewrite(Nat.min_0_r n)

(* Nat.min_0_l *)
| H : context[Nat.min 0 ?n ] |- _ =>
  rewrite(Nat.min_0_l n) in H
| |- context[Nat.min 0 ?n ] =>
  rewrite(Nat.min_0_l n)

(* Nat.max_0_r *)
| H : context[Nat.max ?n 0] |- _ =>
  rewrite(Nat.max_0_r n) in H
| |- context[Nat.max ?n 0] =>
  rewrite(Nat.max_0_r n)

(* Nat.min_0_l *)
| H : context[Nat.max 0 ?n ] |- _ =>
  rewrite(Nat.min_0_l n) in H
| |- context[Nat.max 0 ?n ] =>
  rewrite(Nat.min_0_l n)

(* apply_id *)
| H : context[@id ?A ?x ] |- _ =>
  rewrite(apply_id A x) in H
| |- context[@id ?A ?x ] =>
  rewrite(apply_id A x)

(* apply_comp *)
| H : context[@comp ?A ?B ?C ?f ?g ?x ] |- _ =>
  rewrite(apply_comp A B C f g x) in H
| |- context[@comp ?A ?B ?C ?f ?g ?x ] =>
  rewrite(apply_comp A B C f g x)

(* match_option_id *)
| H : context[match ?x with Some y => Some y | None => None end] |- _ =>
  rewrite(match_option_id _ x) in H
| |- context[match ?x with Some y => Some y | None => None end] =>
  rewrite(match_option_id _ x)

(* END of auto generated tactic *)

| x : ?A * ?B |- _ => destruct x

| H : context[fst(pair _ _)] |- _ => simpl in H
| H : context[snd(pair _ _)] |- _ => simpl in H
| H : pair _ _ = _ |- _ => symmetry in H

| HH : length ?l <= ?i,
  H : context[List.nth_error ?l ?i] |- _ =>
    rewrite (proj2(List.nth_error_None l i) HH) in H
| HH : length ?l <= ?i
  |- context[List.nth_error ?l ?i] =>
    rewrite (proj2(List.nth_error_None l i) HH)

| S : ?x = pair _ _, H : context[?x] |- _ => rewrite S in H
| S : ?x = pair _ _ |- context[?x] => rewrite S

| H : context[match ?x with pair _ _ => _ end] |- _ =>
  let X := fresh "P" in destruct x eqn:X
| |- context[match ?x with pair _ _ => _ end] =>
  let X := fresh "P" in destruct x eqn:X

| H : context [isSome (Some _)] |- _ => simpl in H
| H : context [isSome  None   ] |- _ => simpl in H

| H : context[@length ?A ?l = 0] |- _ =>
  rewrite (@List.length_zero_iff_nil A l) in H
| |- context[@length ?A ?l = 0] =>
  rewrite (@List.length_zero_iff_nil A l)

| o : option _ |- _ => match goal with
  | H : isSome o = true   |- _ => destruct o; simpl in H; inv H
  | H : isSome o = false  |- _ => destruct o; simpl in H; inv H
  end
| H : Some _ = _ |- _ => symmetry in H
| H : None = _ |- _ => symmetry in H
| S : ?E = Some _, H : context [?E] |- _ => rewrite S in H
| S : ?E = Some _ |- context [?E]  => rewrite S
| S : ?E = None, H : context [?E] |- _ => rewrite S in H
| S : ?E = None |- context [?E] => rewrite S
| H : context [length(List.map _ _)] |- _ => rewrite List.map_length in H
| |- context [length(List.map _ _)] => rewrite List.map_length
| H : forall l : list _, length l = S _ -> _ |- _ =>
  rewrite rewrite_listSn in H
| H : forall l : list _, length l = 0 -> _ |- _ =>
  specialize (H nil)
| H : context [length nil] |- _ => simpl in H
| H : forall l : list bool, length l = ?n -> ?E |- _ =>
  match E with
  | context [l] => fail 1
  | _ => specialize (H(make n false)); rewrite length_make in H
  end
| H : context [(_ =? _) = true] |- _ =>
  rewrite Nat.eqb_eq in H
| |- context [(_ =? _) = true] =>
  rewrite Nat.eqb_eq
| l : list ?A |- _ =>
  match goal with
  | H : l = make (length l) ?x |- _ =>
    let TH := type of H in
    (repeat match goal with
    | HH : context[l] |- _ =>
      match type of HH with
      | TH => fail 1
      | _  => generalize dependent HH
      end
    end);
    generalize dependent H;
    generalize dependent l;
    rewrite (elim_eq_make_length A x); intros
  end
| H : context[@List.map ?A ?A id ?l] |- _ =>
  rewrite (@list_map_id A l) in H
| |- context[@List.map ?A ?A id ?l] =>
  rewrite (@list_map_id A l)
end.

Ltac simplsimpl1 := simplbool; repeat(simplsimpl_step1; simplbool).

Ltac simplsimpl_step2 := match goal with
(* @forallb_true *)
| H : context[@List.forallb ?A (fun _ => true) ?l ] |- _ =>
  rewrite(@forallb_true A l) in H
| |- context[@List.forallb ?A (fun _ => true) ?l ] =>
  rewrite(@forallb_true A l)

| H : context[List.nth_error _ _ = None] |- _ =>
  rewrite List.nth_error_None in H
| |- context[List.nth_error _ _ = None] =>
  rewrite List.nth_error_None
| HH : length ?l <= ?i, H : context[pop_nth ?l ?i] |- _ =>
  rewrite (pop_nth_with_le HH) in H
| HH : length ?l <= ?i |- context[pop_nth ?l ?i] =>
  rewrite (pop_nth_with_le HH)
(* length_ablist_compose_unitA_idB_with_length_eq_nA *)
| HH : length ?lc = list_count (isAB true) ?lC,
  H : length(ablist_compose_unitA_idB ?lC ?c) = length ?lC |- _ =>
  rewrite (length_ablist_compose_unitA_idB_with_length_eq_nA HH) in H
| HH : length ?lc = list_count (isAB true) ?lC
  |- length(ablist_compose_unitA_idB ?lC ?c) = length ?lC =>
  rewrite (length_ablist_compose_unitA_idB_with_length_eq_nA HH)
(* list_count_A_ablist_compose_unitA_idB *)
| HH : length ?lc = list_count(isAB true) ?lC,
  H : context[list_count (isAB true) (ablist_compose_unitA_idB ?lC ?lc)] |- _ =>
  rewrite (list_count_A_ablist_compose_unitA_idB HH) in H
| HH : length ?lc = list_count(isAB true) ?lC
  |- context[list_count (isAB true) (ablist_compose_unitA_idB ?lC ?lc)] =>
  rewrite (list_count_A_ablist_compose_unitA_idB HH)
(* List.app_length *)
| H : context[List.length(List.app ?l1 ?l2)] |- _ =>
  rewrite (List.app_length l1 l2)
| |- context[List.length(List.app ?l1 ?l2)] =>
  rewrite (List.app_length l1 l2)

(* @ntimes_eq_nil *)
| H : context[@ntimes ?n ?A ?d ?l = nil] |- _ =>
  rewrite(@ntimes_eq_nil n A d l) in H
| |- context[@ntimes ?n ?A ?d ?l = nil] =>
  rewrite(@ntimes_eq_nil n A d l)

(* @list_count_ntimes *)
| H : context[@list_count ?A ?p (ntimes ?n ?d ?l )] |- _ =>
  rewrite(@list_count_ntimes A p n d l) in H
| |- context[@list_count ?A ?p (ntimes ?n ?d ?l )] =>
  rewrite(@list_count_ntimes A p n d l)

(* @list_count_make *)
| H : context[@list_count ?A ?p (make ?n ?d )] |- _ =>
  rewrite(@list_count_make A p n d) in H
| |- context[@list_count ?A ?p (make ?n ?d )] =>
  rewrite(@list_count_make A p n d)

(* list_nth_error_nil *)
| H : context[@List.nth_error ?A nil ?i ] |- _ =>
  rewrite(list_nth_error_nil A i) in H
| |- context[@List.nth_error ?A nil ?i ] =>
  rewrite(list_nth_error_nil A i)

(* list_nth_error_nil *)
| H : context[@List.nth_error ?A nil ?i ] |- _ =>
  rewrite(list_nth_error_nil A i) in H
| |- context[@List.nth_error ?A nil ?i ] =>
  rewrite(list_nth_error_nil A i)

(* @nth_error_map *)
| H : context[List.nth_error (@List.map ?A ?B ?f ?l ) ?i ] |- _ =>
  rewrite(@nth_error_map A B f l i) in H
| |- context[List.nth_error (@List.map ?A ?B ?f ?l ) ?i ] =>
  rewrite(@nth_error_map A B f l i)

(* Nat.sub_diag *)
| H : context[ ?n - ?n ] |- _ =>
  rewrite(Nat.sub_diag n) in H
| |- context[ ?n - ?n ] =>
  rewrite(Nat.sub_diag n)

| H : context[isAB _ (AA _)] |- _ => simpl in H
| H : context[isAB _ (BB _)] |- _ => simpl in H
end.

Ltac simplsimpl2 := simplsimpl1; repeat(simplsimpl_step2; simplsimpl1).

Ltac plop T X := rewrite T in X.

Ltac simplsimpl_step3 := match goal with
| H : context[length(ablist_compose_list ?mapAC ?mapA ?mapB ?mapC ?lAB ?lC)] |- _ =>
  rewrite (length_ablist_compose_list mapAC mapA mapB mapC lAB lC) in H
| |- context[length(ablist_compose_list ?mapAC ?mapA ?mapB ?mapC ?lAB ?lC)] =>
  rewrite (length_ablist_compose_list mapAC mapA mapB mapC lAB lC)

| H : context[list_last_error nil] |- _ => simpl in H
| H : context[list_last_error (cons _ _)] |- _ => simpl in H

| H : context[uclist_normalized ?eq (uclist_normalize ?eq ?uc)] |- _ =>
  rewrite (uclist_normalized_normalize eq uc) in H
| |- context[uclist_normalized ?eq (uclist_normalize ?eq ?uc)] =>
  rewrite (uclist_normalized_normalize eq uc)

| H : context[list_last_error (List.map ?f ?l)] |- _ =>
  rewrite (list_last_error_map f l) in H
| |- context[list_last_error (List.map ?f ?l)] =>
  rewrite (list_last_error_map f l)

| H : context[list_last_error ?l = None] |- _ =>
  rewrite (list_last_error_eq_None l) in H
| |- context[list_last_error ?l = None] =>
  rewrite (list_last_error_eq_None l)

| H : context[list_last_error (uclist_map2_rec ?f ?lA ?lB ?tA ?tB)] |- _ =>
  rewrite (list_last_error_uclist_map2_rec f lA lB tA tB) in H
| |- context[list_last_error (uclist_map2_rec ?f ?lA ?lB ?tA ?tB)] =>
  rewrite (list_last_error_uclist_map2_rec f lA lB tA tB)

| H : context[rm_trail ?p ?l = nil] |- _ =>
  rewrite (rm_trail_eq_nil p l) in H
| |- context[rm_trail ?p ?l = nil] =>
  rewrite (rm_trail_eq_nil p l)

| H : context[match ?l with nil => nil | cons _ _ => ?l end] |- _ =>
  rewrite (list_match_id l) in H
| |- context[match ?l with nil => nil | cons _ _ => ?l end] =>
  rewrite (list_match_id l)

| H1 : uclist_normalized ?eq ?uc = true,
  H : context[uclist_normalize ?eq ?uc] |- _ =>
  rewrite (uclist_normalize_normalized H1)
| H1 : uclist_normalized ?eq ?uc = true
  |- context[uclist_normalize ?eq ?uc] =>
  rewrite (uclist_normalize_normalized H1)

| H : context[match ?x with AA _ => ?c | BB _ => ?c end] |- _ =>
  rewrite (AB_converge x c) in H
| |- context[match ?x with AA _ => ?c | BB _ => ?c end] =>
  rewrite (AB_converge x c)

| H : context[fun X : AB ?A ?B => match X with AA _ => ?c | BB _ => ?c end] |- _ =>
  rewrite (fun_AB_converge A B c) in H
| |- context[fun X : AB ?A ?B => match X with AA _ => ?c | BB _ => ?c end] =>
  rewrite (fun_AB_converge A B c)

| H : context[List.map (fun _ => ?b) ?l] |- _ =>
  rewrite(map_cst_to_make b l) in H
| |- context[List.map (fun _ => ?b) ?l] =>
  rewrite(map_cst_to_make b l)

| H : context[match ?l with nil => nil | cons h t => cons h t end] |- _ =>
  rewrite (list_match_id_v2 l) in H
| |- context[match ?l with nil => nil | cons h t => cons h t end] =>
  rewrite (list_match_id_v2 l)

| H : context[rm_trail ?p (ntimes ?n ?x ?l)] |- _ =>
  rewrite (rm_trail_ntimes p n x l) in H
| |- context[rm_trail ?p (ntimes ?n ?x ?l)] =>
  rewrite (rm_trail_ntimes p n x l)

| H : context[rm_trail ?p (make ?n ?x)] |- _ =>
  rewrite (rm_trail_make p n x) in H
| |- context[rm_trail ?p (make ?n ?x)] =>
  rewrite (rm_trail_make p n x)

| H : context[match ?l with nil => nil | cons _ _ => @List.map ?A ?B ?f ?l end] |- _ =>
  rewrite(@list_match_map A B f l) in H
| |- context[match ?l with nil => nil | cons _ _ => @List.map ?A ?B ?f ?l end] =>
  rewrite(@list_match_map A B f l)

| H : context[List.forallb ?p (@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB ?lC)] |- _ =>
  match p with
  | id => fail 1
  | _ => rewrite (@forallb_ablist_compose_list A B C D p mapAC mapA mapB mapC lAB lC) in H
  end
| |- context[List.forallb ?p (@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB ?lC)] =>
  match p with
  | id => fail 1
  | _ => rewrite (@forallb_ablist_compose_list A B C D p mapAC mapA mapB mapC lAB lC)
  end

| H : context[@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB (@List.map ?C0 ?C ?f ?lC0)] |- _ =>
  rewrite(@ablist_compose_list_map_r A B C C0 D mapAC mapA mapB f mapC lAB lC0) in H
| |- context[@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB (@List.map ?C0 ?C ?f ?lC0)] =>
  rewrite(@ablist_compose_list_map_r A B C C0 D mapAC mapA mapB f mapC lAB lC0)

(* @ablist_compose_list_map_r *)
| H : context[@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB (@List.map ?C0 ?C ?f ?lC0 )] |- _ =>
  rewrite(@ablist_compose_list_map_r A B C C0 D mapAC mapA mapB f mapC lAB lC0) in H
| |- context[@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB (@List.map ?C0 ?C ?f ?lC0 )] =>
  rewrite(@ablist_compose_list_map_r A B C C0 D mapAC mapA mapB f mapC lAB lC0)

(* @list_count_eq_0 *)
| H : context[@list_count ?A ?p ?l = 0] |- _ =>
  rewrite(@list_count_eq_0 A p l) in H
| |- context[@list_count ?A ?p ?l = 0] =>
  rewrite(@list_count_eq_0 A p l)

(* @list_last_error_ablist_compose_list *)
| H : context[list_last_error (@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB ?lC )] |- _ =>
  rewrite(@list_last_error_ablist_compose_list A B C D mapAC mapA mapB mapC lAB lC) in H
| |- context[list_last_error (@ablist_compose_list ?A ?B ?C ?D ?mapAC ?mapA ?mapB ?mapC ?lAB ?lC )] =>
  rewrite(@list_last_error_ablist_compose_list A B C D mapAC mapA mapB mapC lAB lC)

(* @list_count_until_0 *)
| H : context[@list_count_until ?A ?pU ?p ?l 0] |- _ =>
  rewrite(@list_count_until_0 A pU p l) in H
| |- context[@list_count_until ?A ?pU ?p ?l 0] =>
  rewrite(@list_count_until_0 A pU p l)

(* @map_ntimes *)
| H : context[@List.map ?A ?B ?f (ntimes ?n ?x ?l )] |- _ =>
  rewrite(@map_ntimes A B f n x l) in H
| |- context[@List.map ?A ?B ?f (ntimes ?n ?x ?l )] =>
  rewrite(@map_ntimes A B f n x l)

(* @list_last_error_ntimes *)
| H : context[@list_last_error ?A (ntimes ?n ?x ?l )] |- _ =>
  rewrite(@list_last_error_ntimes A n x l) in H
| |- context[@list_last_error ?A (ntimes ?n ?x ?l )] =>
  rewrite(@list_last_error_ntimes A n x l)
end.

Ltac simplsimpl3 := simplsimpl2; repeat(simplsimpl_step3; simplsimpl2).

Ltac simplsimpl_step4 := match goal with
(* @list_forallb_ntimes *)
| H : context[@List.forallb ?A ?p (ntimes ?n ?x ?l )] |- _ =>
  rewrite(@list_forallb_ntimes A p n x l) in H
| |- context[@List.forallb ?A ?p (ntimes ?n ?x ?l )] =>
  rewrite(@list_forallb_ntimes A p n x l)

(* @list_forallb_make *)
| H : context[@List.forallb ?A ?p (make ?n ?x )] |- _ =>
  rewrite(@list_forallb_make A p n x) in H
| |- context[@List.forallb ?A ?p (make ?n ?x )] =>
  rewrite(@list_forallb_make A p n x)

(* @list_count_beq_0 *)
| H : context[(@list_count ?A ?p ?l =? 0)] |- _ =>
  rewrite(@list_count_beq_0 A p l) in H
| |- context[(@list_count ?A ?p ?l =? 0)] =>
  rewrite(@list_count_beq_0 A p l)

(* @list_forallb_A_vs_make_A *)
| H : context[List.forallb (@isAB unit ?B true) ?l = true] |- _ =>
  rewrite(@list_forallb_A_vs_make_A B l) in H
| |- context[List.forallb (@isAB unit ?B true) ?l = true] =>
  rewrite(@list_forallb_A_vs_make_A B l)

(* @list_forallb_B_vs_make_B *)
| H : context[List.forallb (@isAB ?A unit false) ?l = true] |- _ =>
  rewrite(@list_forallb_B_vs_make_B A l) in H
| |- context[List.forallb (@isAB ?A unit false) ?l = true] =>
  rewrite(@list_forallb_B_vs_make_B A l)

(* @nth_error_make *)
| H : context[@List.nth_error ?A (make ?n ?d ) ?i ] |- _ =>
  rewrite(@nth_error_make A n d i) in H
| |- context[@List.nth_error ?A (make ?n ?d ) ?i ] =>
  rewrite(@nth_error_make A n d i)

(* @nat_match_id *)
| H : context[match ?i with 0 => 0 | S n => S n end] |- _ =>
  rewrite(@nat_match_id i) in H
| |- context[match ?i with 0 => 0 | S n => S n end] =>
  rewrite(@nat_match_id i)

(* @fun_negb_isAB *)
| H : context[(fun x : AB => negb (@isAB ?A ?B ?b x))] |- _ =>
  rewrite(@fun_negb_isAB A B b) in H
| |- context[(fun x : AB => negb (@isAB ?A ?B ?b x))] =>
  rewrite(@fun_negb_isAB A B b)

(* @List.app_nil_r *)
| H : context[@List.app ?A ?l nil] |- _ =>
  rewrite(@List.app_nil_r A l) in H
| |- context[@List.app ?A ?l nil] =>
  rewrite(@List.app_nil_r A l)

(* @ntimes_cons_d *)
| H : context[@ntimes ?n ?A ?d (cons ?d ?l )] |- _ =>
  rewrite(@ntimes_cons_d A n d l) in H
| |- context[@ntimes ?n ?A ?d (cons ?d ?l )] =>
  rewrite(@ntimes_cons_d A n d l)

(* @fun_negb *)
| H : context[(fun x : ?A => negb _)] |- _ =>
  rewrite(@fun_negb A) in H
| |- context[(fun x : ?A => negb _)] =>
  rewrite(@fun_negb A)

(* @fun_fold_negb [HC] *)
| H : context[(fun x : ?A => if @?f x then false else true)] |- _ =>
  match type of f with
  | A -> bool => rewrite(@fun_fold_negb A f) in H
  end
| |- context[(fun x : ?A => if @?f x then false else true)] =>
  match type of f with
  | A -> bool => rewrite(@fun_fold_negb A f)
  end

(* @fun_fold_ITF_id [HC] *)
| H : context[(fun x : ?A => if @?f x then true else false)] |- _ =>
  match type of f with
  | A -> bool =>   rewrite(@fun_fold_ITF_id A f) in H
  end
| |- context[(fun x : ?A => if @?f x then true else false)] =>
  match type of f with
  | A -> bool =>   rewrite(@fun_fold_ITF_id A f)
  end

(* @comp_assoc *)
| H : context[@comp ?A ?B ?C ?f1 (@comp ?B ?C ?D ?f2 ?f3 )] |- _ =>
  rewrite(@comp_assoc A B C D f1 f2 f3) in H
| |- context[@comp ?A ?B ?C ?f1 (@comp ?B ?C ?D ?f2 ?f3 )] =>
  rewrite(@comp_assoc A B C D f1 f2 f3)

(* @comp_negb_negb *)
| H : context[comp negb negb] |- _ =>
  rewrite(@comp_negb_negb) in H
| |- context[comp negb negb] =>
  rewrite(@comp_negb_negb)

(* @comp_id_l *)
| H : context[@comp ?A ?A ?B id ?f ] |- _ =>
  rewrite(@comp_id_l A B f) in H
| |- context[@comp ?A ?A ?B id ?f ] =>
  rewrite(@comp_id_l A B f)

(* @comp_id_r *)
| H : context[@comp ?A ?B ?B ?f id ] |- _ =>
  rewrite(@comp_id_r A B f) in H
| |- context[@comp ?A ?B ?B ?f id ] =>
  rewrite(@comp_id_r A B f)

(* @comp_f_negb_negb *)
| H : context[@comp ?A bool bool (comp ?f negb) negb] |- _ =>
  rewrite(@comp_f_negb_negb A f) in H
| |- context[@comp ?A bool bool (comp ?f negb) negb] =>
  rewrite(@comp_f_negb_negb A f)

(* fun_fold_isAB_true *)
| H : context[(fun x : AB ?A ?B => match x with AA _ => true | BB _ => false end)] |- _ =>
  rewrite(fun_fold_isAB_true A B) in H
| |- context[(fun x : AB ?A ?B => match x with AA _ => true | BB _ => false end)] =>
  rewrite(fun_fold_isAB_true A B)

(* fun_fold_isAB_false *)
| H : context[(fun x : AB ?A ?B => match x with AA _ => false | BB _ => true end)] |- _ =>
  rewrite(fun_fold_isAB_false A B) in H
| |- context[(fun x : AB ?A ?B => match x with AA _ => false | BB _ => true end)] =>
  rewrite(fun_fold_isAB_false A B)

(* @map_iff_nil *)
| H : context[@List.map ?A ?B ?f ?l = nil] |- _ =>
  rewrite(@map_iff_nil A B f l) in H
| |- context[@List.map ?A ?B ?f ?l = nil] =>
  rewrite(@map_iff_nil A B f l)

(* comp_isAB_negb *)
| H : context[comp (@isAB ?A ?B ?b ) negb] |- _ =>
  rewrite(comp_isAB_negb A B b) in H
| |- context[comp (@isAB ?A ?B ?b ) negb] =>
  rewrite(comp_isAB_negb A B b)

(* comp_isAB_xorb *)
| H : context[comp (@isAB ?A ?B ?b1 ) (xorb ?b2 )] |- _ =>
  rewrite(comp_isAB_xorb A B b1 b2) in H
| |- context[comp (@isAB ?A ?B ?b1 ) (xorb ?b2 )] =>
  rewrite(comp_isAB_xorb A B b1 b2)

(* @ntll_0 *)
| H : context[@ntll ?A 0 ?l ] |- _ =>
  rewrite(@ntll_0 A l) in H
| |- context[@ntll ?A 0 ?l ] =>
  rewrite(@ntll_0 A l)

(* @nhdl_0 *)
| H : context[@nhdl ?A 0 ?l ] |- _ =>
  rewrite(@nhdl_0 A l) in H
| |- context[@nhdl ?A 0 ?l ] =>
  rewrite(@nhdl_0 A l)

end.

Ltac simplsimpl4 := simplsimpl3; repeat(simplsimpl_step4; simplsimpl3).

Ltac simplsimpl_step5 := match goal with
(* rewrite_ntimes_eq_same_n *)
| H : context[@ntimes ?n ?A ?x1 ?l1 = ntimes ?n ?x2 ?l2 ] |- _ =>
  rewrite(rewrite_ntimes_eq_same_n A n x1 l1 x2 l2) in H
| |- context[@ntimes ?n ?A ?x1 ?l1 = ntimes ?n ?x2 ?l2 ] =>
  rewrite(rewrite_ntimes_eq_same_n A n x1 l1 x2 l2)
end.

Ltac simplsimpl5 := simplsimpl4; repeat(simplsimpl_step5; simplsimpl4).
Ltac simplsimpl := simplsimpl5.

*)