Require Import Arith.
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.
Require Import FunInd.
Require Import Recdef.

Require Import SimplProp.
Require Import SimplBool.
Require Import SimplOption.
Require Import SimplEq.
Require Import SimplCmp.
Require Import SimplSimpl.

Require Import SimplNat.
Require Import SimplAB.

Require Import SimplList_Chap0_Standard.
Require Import SimplList_Chap1_Construct.
Require Import SimplList_Chap2_Predicate.
Require Import SimplList_Chap3_PredicateOnConstruct.
Require Import SimplList.
Require Import SimplABList.
Require Import SimplMore.

Require Import MiniBool_VAX.
Require Import MiniBool_Logic.

Require Import SimplDTT.
Require Import SimplLtN.
Require Import SimplArray.

Definition slist (A:Type) : nat -> Type :=
  (fun n => {l:list A | List.length l =? n = true}).

Lemma slist_eq_inj {A:Type} {n} (l1 l2:slist A n) :
  l1 = l2 <-> proj1_sig l1 = proj1_sig l2.
Proof with simpllist1.
destruct l1, l2...
setoid_rewrite exist_eq_l...
Qed.

Fixpoint list_of_array {A n} (a:array A n) : list A :=
match n as n0 return array A n0 -> list A with
| 0 => fun _ => nil
| S n => fun a => cons (a(ltN_of_nat 0 (S n) eq_refl)) (list_of_array (stail a))
end a.

Lemma length_list_of_array {A n} a : length(@list_of_array A n a) = n.
Proof. induction n; curry1. Qed.
#[export] Hint Rewrite @length_list_of_array : curry1_rewrite.

Lemma slist_of_array_lemma1 {A n} (a:array A n) : length(@list_of_array A n a) =? n = true.
Proof. curry1. Qed.

Definition slist_of_array {A n} (a:array A n) : slist A n :=
 (exist _ (list_of_array a) (slist_of_array_lemma1 a)).

Lemma slist_nth_lemma1 {A n} (l:slist A n) (k:ltN n) :
  List.nth_error (proj1_sig l) (proj1_sig k) = None -> False.
Proof. destruct l as [l Hl], k as [k Hk]; curry1. Qed.

Definition slist_nth {A n} (l:slist A n) (k:ltN n) : A :=
match List.nth_error (proj1_sig l) (proj1_sig k) as o0
  return (List.nth_error (proj1_sig l) (proj1_sig k) = o0 -> A) with
| Some a => fun _ : List.nth_error (proj1_sig l) (proj1_sig k) = Some a => a
| None => fun E => match slist_nth_lemma1 _ _ E with end
end eq_refl.

Definition array_of_slist {A n} (l:slist A n) : array A n := (fun k => slist_nth l k).

Lemma nth_error_list_of_array {A n} (a:array A n) (k:ltN n) :
  List.nth_error (list_of_array a) (proj1_sig k) = Some(a k).
Proof with curry1.
induction n...
generalize k; rewrite forall_ltN_S...
unfold shead, stail...
apply IHn.
Qed.
#[export] Hint Rewrite @nth_error_list_of_array : curry1_rewrite.


Lemma array_of_slist_of_array {A n} (a:array A n) : array_of_slist(slist_of_array a) = a.
Proof with curry1.
unfold array_of_slist, slist_nth; apply functional_extensionality...
rewrite_dtt_option...
split...
dest_match_step.
Qed.
#[export] Hint Rewrite @array_of_slist_of_array : curry1_rewrite.

Lemma slist_tail_lemma1 {A n} (l:slist A (S n)) : proj1_sig l = nil -> False.
Proof. destruct l; curry1. Qed.

Lemma slist_tail_lemma2 {A n} (l:slist A (S n)) a l0 :
  proj1_sig l = (a :: l0)%list -> length l0 =? n = true.
Proof. destruct l; curry1. Qed.

Definition slist_tail {A n} (l:slist A (S n)) : slist A n :=
match proj1_sig l as l0 return proj1_sig l = l0 -> slist A n with
| nil => fun E => match slist_tail_lemma1 _ E with end
| cons _ l => fun E => exist _ l (slist_tail_lemma2 _ _ _ E)
end eq_refl.

Lemma stail_array_of_slist {A n} (l:slist A(S n)) :
  stail (array_of_slist l) = array_of_slist(slist_tail l).
Proof with curry1.
apply functional_extensionality...
unfold stail, slist_tail.
destruct l as [[|l0 l] Hl]...
dependent destruction Hl...
unfold array_of_slist, slist_nth...
repeat rewrite_dtt_option.
repeat split...
dest_match_step.
Qed.

Lemma list_of_array_of_slist {A n} (l:slist A n) : list_of_array(array_of_slist l) = proj1_sig l.
Proof with curry1.
induction n...
- destruct l as [l Hl]...
- rewrite stail_array_of_slist.
  rewrite IHn. clear IHn.
  unfold array_of_slist.
  unfold slist_nth...
  destruct l as [l Hl]...
  destruct l as [|l0 l]...
Qed.
#[export] Hint Rewrite @list_of_array_of_slist : curry1_rewrite.

Lemma slist_of_array_of_slist {A n} (l:slist A n) : slist_of_array(array_of_slist l) = l.
Proof with curry1.
unfold slist_of_array.
destruct l...
Qed.
#[export] Hint Rewrite @slist_of_array_of_slist : curry1_rewrite.

Lemma slist_of_array_shift {A n} a l : @slist_of_array A n a = l <-> a = array_of_slist l.
Proof. split; curry1. Qed.
#[export] Hint Rewrite @slist_of_array_shift : curry1_rewrite.

Lemma array_of_slist_shift {A n} a l : @array_of_slist A n l = a <-> l = slist_of_array a.
Proof. split; curry1. Qed.
#[export] Hint Rewrite @array_of_slist_shift : curry1_rewrite.

Definition cmp_slist {A} (cA:cmp A) {n} (s1 s2:slist A n) : ord := cmp_sig _ (cmp_list cA) s1 s2.

Lemma cmp_P_cmp_slist  {A} (cA:cmp A) (PA:@cmp_P A cA) n : cmp_P (@cmp_slist _ cA n).
Proof. apply cmp_P_sig, cmp_P_cmp_list, PA. Qed.

Definition cmp_slist_bool {n} (s1 s2:slist bool n) : ord := cmp_slist cmp_bool s1 s2.

Lemma cmp_P_cmp_slist_bool n : cmp_P (@cmp_slist_bool n).
Proof. apply cmp_P_cmp_slist, cmp_P_bool. Qed.

(* Annexe on MultiType List *)

Definition incr_array (A:nat -> Type) (n:nat) : Type := (forall (k:ltN n), A(proj1_sig k)).

Inductive incr_list (A:nat -> Type) : Type :=
| incr_nil : incr_list A
| incr_cons (l:incr_list (fun n => A(S n))) (x:A 0) : incr_list A.

Fixpoint incr_list_length {A} (l:incr_list A) : nat :=
match l with
| incr_nil _ => 0
| incr_cons _ l _ => S(incr_list_length l)
end.

Definition test_ignore_dtt : nat -> Type := (fun _ => bool).

Fixpoint decr_list (A:nat -> Type) (n:nat) : Type :=
match n with
| 0 => unit
| S n => (A n * (decr_list A n))
end.

Definition decr_cons {A n} (x:A n) (l:decr_list A n) : decr_list A (S n) := (x, l).
Definition decr_nil (A:nat -> Type) : decr_list A 0 := tt.
Definition decr_head {A n} (l:decr_list A(S n)) : A n := fst l.
Definition decr_tail {A n} (l:decr_list A(S n)) : decr_list A n := snd l.

Lemma decr_list_nth_lemma1 {n} (k:ltN(S n)) n0 : proj1_sig k = S n0 -> (n0 <? n) = true.
Proof. destruct k as [k Hk]; curry1. Qed.

Fixpoint decr_list_nth {A n} (l:decr_list A n) (k:ltN n) {struct n} : A(n - (S(proj1_sig k))) :=
match n as n0 return (decr_list A n0 -> forall k0 : ltN n0, A (n0 - S (proj1_sig k0))) with
  | 0 => fun l0 k0 => match ltN_0_empty k0 return (A (0 - S (proj1_sig k0))) with end
  | S n0 => fun l0 k0 =>
      match proj1_sig k0 as n2 return (proj1_sig k0 = n2 -> A (S n0 - S n2)) with
      | 0 => fun _ => eq_rect_r (fun n2 : nat => A n2) (fst l0) (Nat.sub_0_r n0)
      | S n2 => fun E =>
          decr_list_nth (snd l0) (ltN_of_nat n2 n0 (decr_list_nth_lemma1 k0 n2 E))
      end eq_refl
  end l k.

Definition test_decr_list_nth_1 : decr_list (fun n => ltN(S n)) 6 :=
    let f0 := fun n => ltN_of_nat 0 (S n) eq_refl in
      ((f0 5, (f0 4, (f0 3, (f0 2, (f0 1, (f0 0, tt))))))).

Compute test_decr_list_nth_1.